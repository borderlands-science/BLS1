## Alignments Folder

In this folder, you will find all the alignments of cluster representatives. These alignments were built with different methods on the same 9,667 sequences. These alignments were used as input for FastTree to infer phylogenies. Figure 1 in the paper shows results for the distance between these phylogenies and the Greengenes+SEPP phylogeny. All the phylogenies in questions are in the `trees` subfolder. The `stats.py` script computes the distance statistics features in Table 1.

The paper also features some other metrics about the alignments, in Figure 3, Figure 4 and Table 2. These metrics were computed with the two jupyter notebooks in the `alignment-analysis` folder.

`clean_pasta_alignment.ipynb` took as input the initial PASTA alignment of 10,900 cluster representatives (`pasta_10900_before_removal_of_unalignable_sequences.pasta`) for the total dataset of 950k fragments (obtained from the `../clustering/cd-hit` folder), and returned an alignment of 9667 acceptable sequences (the remainder, found in `unalignable_2k.fasta`, being mostly artifacts and sequencing errors accounting for a fraction of a percent of the overall dataset). All other alignments feature the same 9667 sequences.

The alignments featured in this folder were then used to generate alignments of the full dataset (950k sequences) with scripts found in `../clustering/cluster_alignment`, which were then used for the computation of effect sizes (see `./effect-sizes`).
