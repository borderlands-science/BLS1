import os
import sys

sys.path.append(os.path.abspath(os.pardir))

import ast
import subprocess
from skbio.tree import TreeNode
from eval.lca_arch import verify_rerooting


def assess(input_file):
    """
    Create and reroot a phylogenetic tree generated from a BLS alignment.
    
    Parameters:
    input_file (str): The input file containing the BLS alignment in FASTA format.
    
    If the rerooted tree does not already exist, this function:
    - Generates a tree using FastTree with double precision.
    - Reroots the generated tree using t2t.
    - Verifies the rerooted tree by checking the reasonable split of archaeal tips.
    
    Prints:
    - Output and error messages from the FastTree and t2t processes.
    """
    bls_name = input_file.split('.')[0]

    if not os.path.exists(bls_name + '_rerooted.nwk'):
        # generate tree from bls alignment - make sure to use FastTree Double Precision
        subprocess.call(
            'FastTreeDbl -spr 4 -gamma -fastest -no2nd -nt < ' + bls_name + '.fasta > ' + bls_name + '.nwk',
            shell=True)

        # run t2t to reroot bls tree
        # NOTE: need to make sure that correct version of t2t is available and run
        tt_process = subprocess.Popen(['t2t', 'reroot', '-t', bls_name + '.nwk', '-n',
                                       'arch_tips.txt', '-o', bls_name +
                                       '_rerooted.nwk'],
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE)
        stdout, stderr = tt_process.communicate()
        print(stdout, stderr)

        # verify reasonable archaea split 
        verify_rerooting(bls_name + '_rerooted.nwk', archaea_file='arch_tips.txt')
        return


if __name__ == "__main__":
    i_f = sys.argv[1]
    assess(i_f)
