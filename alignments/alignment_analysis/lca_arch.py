from skbio import TreeNode
import subprocess
import numpy as np


def display_root_children_info(tree, at):
    """
    Display information about the root's children in a phylogenetic tree.
    
    Parameters:
    tree (TreeNode): The tree whose root children information is to be displayed.
    at (list of str): A list of archaeal tips to check against.
    
    Prints:
    - Children of the root.
    - Number of archaeal tips in each child's subtree.
    """
    r = tree.root()
    print(r.children)
    root_children_tips = {}
    for ch_i in range(len(r.children)):
        root_children_tips[ch_i] = list([n.name for n in r.children[ch_i].tips()])
    root_children_arch_count = {}
    for ch_i in range(len(r.children)):
        root_children_arch_count[ch_i] = len([_ for _ in root_children_tips[ch_i] if _ in at])
    print('no. of arch tips in each root\'s child\'s subtree:', root_children_arch_count)


def display_arch_lca_info(tree, at):
    """
    Display information about the lowest common ancestor (LCA) of archaeal tips.
    
    Parameters:
    tree (TreeNode): The tree containing the archaeal tips.
    at (list of str): A list of archaeal tips.
    
    Prints:
    - Distance from the root to the LCA of archaeal tips.
    - Depth of the LCA.
    - Whether the LCA is the root.
    - Number of all tips, archaeal tips, and tips in the archaeal subtree.
    """
    r = tree.root()
    _lca = tree.lca(at)
    d = r.distance(_lca)
    print('arch lca distance to root:', d)
    print('depth:', len(_lca.ancestors()))
    print('is arch lca root?', _lca.is_root())
    print('no. all tips:', len(list(tree.tips())))
    print('no. of arch tips:', len(at))
    print('no. tips in arch subtree:', len(list(_lca.tips())))
    print()


def verify_rerooting(filename, archaea_file='arch_tips.txt'):
    """
    Verify and potentially reroot a phylogenetic tree based on archaeal tips.
    
    Parameters:
    filename (str): The file path of the target tree.
    archaea_file (str): The file path of the file containing archaeal tips. Default is 'arch_tips.txt'.
    
    Prints:
    - Information about root's children and archaeal LCA.
    - Information about removed tips and rerooting process.
    
    Writes:
    - A file with the archaeal tips that is used for rerooting, if it is performed.
    """
    target_tree = TreeNode.read(filename)
    arch_tips = []
    with open(archaea_file, 'r') as f:
        arch_tips.extend(f.read().split())
    display_root_children_info(target_tree, arch_tips)
    display_arch_lca_info(target_tree, arch_tips)

    arch_tip_tip_dists = target_tree.tip_tip_distances(arch_tips)
    ids = arch_tip_tip_dists.ids
    sum_for_each = np.mean(arch_tip_tip_dists.data, axis=0)
    sum_for_each, ids = zip(*sorted(zip(sum_for_each, ids), reverse=True))
    ids = list(ids)
    root = target_tree.root()
    lca = target_tree.lca(arch_tips)
    removed_arch_tips = []
    while len(list(lca.tips())) > 200:
        removed_arch_tips.append(ids.pop(0))
        sub_arch_tips = [_ for _ in arch_tips if _ not in removed_arch_tips]
        lca = target_tree.lca(sub_arch_tips)
        dist = root.distance(lca)
        print('removed tips:', removed_arch_tips)
        print('arch lca distance to root:', dist)
        print('depth:', len(lca.ancestors()))
        print('is arch lca root?', lca.is_root())
        print('no. tips in arch subtree:', len(list(lca.tips())))
        print()

    if len(removed_arch_tips) > 0:
        print('REROOTING!')
        good_archaea = [_ for _ in arch_tips if _ not in removed_arch_tips]
        with open('good_arch_' + filename.split('.')[0] + '.txt', 'w') as outfile:
            for a in good_archaea:
                outfile.write(a)
                outfile.write('\n')

        # NOTE: need to make sure that correct version of t2t is available and run
        tt_process = subprocess.Popen(['t2t', 'reroot', '-t', filename, '-n',
                                       'good_arch_' + filename.split('.')[0] + '.txt', '-o',
                                        filename.split('.')[0] + '_twice.nwk'], stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE)
        stdout, stderr = tt_process.communicate()
        print(stdout)
        print(stderr)

        new_tree = TreeNode.read(filename.split('.')[0] + '_twice.nwk')
        print('num of tips in rerooted tree:', len(list(new_tree.tips())))
        display_root_children_info(new_tree, arch_tips)
        display_root_children_info(new_tree, good_archaea)
        display_arch_lca_info(new_tree, arch_tips)
        display_arch_lca_info(new_tree, good_archaea)
