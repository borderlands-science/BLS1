This folder contains scripts and data for analyzing alignments and their corresponding phylogenetic trees. The key functionalities include generating alignment quality info such as sum of pairs score, generating trees from multiple sequence alignments, rerooting trees, and calculating distances between trees using different metrics.

## Contents

- **bls-results-gap-placement.ipynb**: Analyzing gap placements in BLS results.
- **16s_conservation.csv**: CSV file containing conservation data for 16S rRNA sequences.
- **bls-results-structure-analysis.ipynb**: Analyzing the structure of BLS results.
- **stats.py**: Getting different statistics on alignments.
- **assess_bls_rlg.py**: Assessing and rerooting phylogenetic trees generated from BLS alignments.
- **lca_arch.py**: Verifying rerooting of phylogenetic trees based on archaeal tips.
- **kendall.py**: Calculating Kendall-Colijn distances between phylogenetic trees.
- **tqDist-1.0.2.tar.gz**: the [tqDist library](https://www.birc.au.dk/~cstorm/software/tqdist/#files), used for calculating triplet distances.
