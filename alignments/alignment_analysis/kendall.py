from scipy.spatial.distance import correlation
from skbio import TreeNode
import numpy as np
import itertools
import time
import sys


def timing_decorator(func):
    """
    A decorator that prints the time a function takes to execute.
    
    Parameters:
    func (function): The function to be wrapped by the decorator.
    
    Returns:
    function: The wrapped function with timing functionality.
    """
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        elapsed_time = end_time - start_time
        print(f"Function '{func.__name__}' took {elapsed_time:.6f} seconds to execute.")
        return result

    return wrapper


def get_max_dbl(tree):
    """
    Calculate the maximum distance between the root and any tip of the tree.
    
    Parameters:
    tree (TreeNode): The tree whose maximum distance to any tip is calculated.
    
    Returns:
    float: The maximum distance from the root to any tip in the tree.
    """
    root = tree.root()
    mdbl = -1
    for t in tree.tips():
        dbl = root.distance(t)
        if dbl > mdbl:
            mdbl = dbl
    return mdbl


@timing_decorator
def kc_distance(tree1, tree2, lamda=0):
    """
    Calculate the Kendall-Colijn distance between two phylogenetic trees.
    
    Parameters:
    tree1 (TreeNode): The first tree for comparison.
    tree2 (TreeNode): The second tree for comparison.
    lamda (float): The lambda parameter for weighting branch lengths. Default is 0.
    
    Returns:
    float: the norm of the difference vector.
    
    Raises:
    ValueError: If the trees do not have the same set of samples.
    """
    tips1 = [int(_.name) for _ in tree1.tips()]
    tips2 = [int(_.name) for _ in tree2.tips()]

    tips1 = np.sort(tips1)
    tips2 = np.sort(tips2)

    if not np.array_equal(tips1, tips2):
        raise ValueError("Trees must have the same samples")

    max_dbl1 = get_max_dbl(tree1)
    max_dbl2 = get_max_dbl(tree2)

    k = tips1.shape[0]
    n = (k * (k - 1)) // 2
    # 'm' is for the topological distance
    m = [np.ones(n + k), np.ones(n + k)]
    # 'M' is for the branch-length based distance
    tips1_edge_lengths = [tip.length for tip in tree1.tips()]
    tips2_edge_lengths = [tip.length for tip in tree2.tips()]
    M = [np.concatenate((np.zeros(n), np.array(tips1_edge_lengths))),
         np.concatenate((np.zeros(n), np.array(tips2_edge_lengths)))]
    assert M[0].shape == m[0].shape
    assert M[1].shape == m[1].shape
    for tree_index, tree in enumerate([tree1, tree2]):
        stack = [(tree.root(), 0, 0)]
        while len(stack) > 0:
            u, depth_top, depth_bl = stack.pop()
            for v in u.children:
                stack.append((v, depth_top + 1, depth_bl + v.length))
            for c1, c2 in itertools.combinations(u.children, 2):
                for u in c1.tips():
                    for v in c2.tips():
                        if not isinstance(u, int):
                            u = int(u.name)
                        if not isinstance(v, int):
                            v = int(v.name)
                        x = np.where(tips1 == u)[0][0]
                        y = np.where(tips2 == v)[0][0]

                        if x < y:
                            a, b = x, y
                        else:
                            a, b = y, x

                        pair_index = a * (a - 2 * k + 1) // -2 + b - a - 1
                        assert m[tree_index][pair_index] == 1
                        m[tree_index][pair_index] = depth_top
                        assert M[tree_index][pair_index] == 0
                        M[tree_index][pair_index] = depth_bl / max_dbl1 if tree_index == 0 else depth_bl / max_dbl2
    v = [(1 - lamda) * m[0] + lamda * M[0], (1 - lamda) * m[1] + lamda * M[1]]
    return np.linalg.norm(v[0] - v[1])


def eval_kc(_name_, benchmark, lam):
    """
    Evaluate the Kendall-Colijn distance between a target tree and a benchmark tree.
    
    Parameters:
    _name_ (str): The file path of the target tree.
    benchmark (str): The file path of the benchmark tree.
    lam (float): The lambda parameter for weighting branch lengths.
    
    Returns:
    float: the Kendall-Colijn distance.
    
    Raises:
    AssertionError: If lam is not a float or is not in the range [0, 1].
    """
    assert type(lam) == float
    assert 0 <= lam <= 1

    GG = TreeNode.read(benchmark)
    target_tree = TreeNode.read(_name_)

    print('Calculating KC. dist...')
    kcd, corr_dist = kc_distance(GG, target_tree, lamda=float(lam))
    return kcd, corr_dist


if __name__ == "__main__":
    print(eval_kc(sys.argv[1], sys.argv[2], float(sys.argv[3])))
