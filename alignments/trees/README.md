### Phylogenetic trees 

This directory contains the phylogenies of cluster representatives. Most of the trees found in this directory were generated with `FastTree`.

`bls.nwk`, `greedy.nwk`, `pasta.nwk`, `mafft.nwk`, `muscle.nwk`, `ppp.nwk` are the phylogenies inferred with `FastTree` from the alignments of the same name in the parent directory. 

These are the phylogenies on which metrics such as the Kendall-Colijn and Triplet distance, reported in Table 1 of the main text, have been computed.

We have also included our reference phylogeny, against which the distance was computed.

`gg_13_5_otus_99_annotated.tree` from the [Greengenes website](https://greengenes.secondgenome.com/?prefix=downloads/greengenes_database/gg_13_5/) is the Greengenes phylogeny which we use as a global reference.

`Greengenes_tree_with_bls_tips_inserted.nwk` is the same tree as gg_13_5, but with BLS tips inserted with SEPP, and after shearing to only keep 9667 BLS tips.

`Greengenes13_5_with_bls_inserted_sheared_to_bls_tips_rerooted` is the same tree after rerooting using archaeal tips.

This is how the reference tree against which we computed the distances presented in Table 1 was obtained.
