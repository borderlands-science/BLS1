from Bio import AlignIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC, Gapped
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment

#This script takes as input the initial PASTA alignment of the 10.9k cluster representatives and identifies sequences that cannot be aligned to the other, which are then output to unalignable.fasta
#A lot of these sequences correspond to sequencing errors and chimeric sequences, and only account for a fraction of a percent of the full sequence dataset.
#After this preprocessing, we obtained an alignment of 9667 sequences in pasta.fasta, which was used as the basis for the building of BLS puzzles.

def assess_column_conservation(aln):
    column_cons = []
    n = len(aln[0])
    for col in range(n):
        cnt = aln[:, col].count("-")
        score = (len(aln[:, col]) - cnt) / len(aln[:, col])
        column_cons.append(score)
    return column_cons


def score_alignment_entry(aln, rID, col_scores):
    score = 0
    seq = aln[rID]
    for i in range(len(seq)):
        if seq[i] != "-":
            score += col_scores[i]
    return score / len(seq)


sub_align = MultipleSeqAlignment([], Gapped(IUPAC.unambiguous_rna, "-"))
align = AlignIO.read("pasta_10900_before_removal_of_unalignable_sequences.fasta", "fasta")
unfit_sequences = []
column_cons = assess_column_conservation(align)
rID = 0
sc = score_alignment_entry(align, rID, column_cons)

for i in range(len(align)):
    sc = score_alignment_entry(align, i, column_cons)
    print(sc)
    if sc > 0.1684:
        sub_align.append(align[i])
    else:
        unfit_sequences.append(align[i])

# find first non-gapped entry
for entry in sub_align:
    for ind, nuc in enumerate(entry):
        if nuc != "-":
            print(ind)
            break

# find last non-gapped entry
for entry in sub_align:
    for ind, nuc in enumerate(reversed(entry)):
        if nuc != "-":
            print(ind)
            break

unoccupied_columns = []

n = len(sub_align[0])
for col in range(n):
    cnt = align[:, col].count("-")
    score = (len(align[:, col]) - cnt) / len(align[:, col])
    print(score)
    if score < 0.05:
        unoccupied_columns.append(col)

print(unoccupied_columns)

clean_align = MultipleSeqAlignment([], Gapped(IUPAC.unambiguous_rna, "-"))
for entry in sub_align:
    this_seq = []
    safe_to_add = True
    for ind, nuc in enumerate(entry.seq):
        if ind not in unoccupied_columns:
            this_seq.append(nuc)
        elif ind in unoccupied_columns:
            if nuc != "-":
                safe_to_add = False
    if safe_to_add:
        rec = SeqRecord(Seq("".join(this_seq)), id=entry.id)
        clean_align.append(rec)
    else:
        unfit_sequences.append(entry)
