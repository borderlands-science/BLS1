import sys
import bp

# This script, written by Daniel McDonald, leverages the iow library to preprocess the trees we work with in order to ensure they have tip names that correspond to the rest of our data.





t = bp.to_skbio_treenode(bp.parse_newick(open(sys.argv[1]).read()))

try:
    ids = {str(int(i.strip().replace("B","").replace("C","").replace("A","").replace("D","").replace("E","").replace("b","").replace("a",""))) for i in open('../correct_arch.ids')}
except:
    ids = {str(int(i.strip().replace("B","").replace("C","").replace("A","").replace("D","").replace("E","").replace("b","").replace("a",""))) for i in open('../../correct_arch.ids')}
    
print(list(ids)[:10])
print(len([n.name for n in t.tips()]))
overlap = {n.name for n in t.tips()} & ids
print(len(overlap))
assert len(overlap) > 0
with open('archaea.ids', 'w') as fp:
    fp.write('\n'.join(overlap))
    fp.write('\n')

