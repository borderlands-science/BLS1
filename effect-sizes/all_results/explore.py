import sys
import pandas as pd
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import seaborn as sns

# Author: Jérôme Waldispuhl

# This script takes as input data from effect sizes (in particular the csv file with statistics for all variables and all alignments
# it returns visual comparisons of performance of different alignments over the set of variables, using a classification of variables provided in classification.csv
# see the arguments required on line 33 to 37. Figures produced can be find in the effect sizes section of the supplementary material.


################################################################################

# variables

normalized_names = {'alloff':'BLS with offsets','greednov':'Greedy','maffas':'MAFFT','mainres':'BLS','muscle':'MUSCLE','newsepp':'SEPP','pasta':'PASTA','ppnov':'post-processing PASTA'}

# utils

def delta_with_baseline(x,df2compare):
    myvar=x['variable']
    baseline_value = df2compare.loc[df_baseline['variable']==myvar]['mean']
    return float(x['mean'] - baseline_value)

################################################################################

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Process effect sizes')
    parser.add_argument('-f', '--file', dest='datafile', nargs=1, type=str, required=True, help='csv file storing effect sizes')
    parser.add_argument('-c', '--compare', dest='methodnames', nargs=2, type=str, help='rank variables in decreasing order of the difference of effect sizes value between two methods (i.e., it requires two arguments)')
    parser.add_argument('-r', '--rank', dest='rank', action='store_true', help='rank variable in decreasing order of effect sizes')
    parser.add_argument('-s', '--split', dest='classfile', nargs=1, type=str, help='csv file classifying variables')
    
    args = parser.parse_args()

    # parse data
    df = pd.read_csv(args.datafile[0])
    
    list_methods = df['label'].drop_duplicates().sort_values().reset_index(drop=True)
    full_list_variables = df['variable'].drop_duplicates().sort_values().reset_index(drop=True)
        
    ## stats

    print(f'>>>> All ({len(full_list_variables)})')
    for my_method in list_methods:
        subdf = df.loc[df['label']==my_method]
        avg_mean = subdf['mean'].mean()
        avg_std = subdf['std'].mean()
        print(f'{my_method:12} ==> {avg_mean:.3f} ({avg_std:.3f})')

    # compute averages for categories of variables if classification is provided
    if (args.classfile):
        df_class = pd.read_csv(args.classfile[0])
        # sanity check
        if not full_list_variables.equals(df_class['variable'].sort_values().reset_index(drop=True)):
            print('ERROR: classification of variables corrupted')
            for var1 in full_list_variables:
                if not df_class['variable'].isin([str(var1)]).any():
                    print('variable not found:',var1)
            for var1 in df_class['variable']:
                if not full_list_variables.isin([str(var1)]).any():
                    print('variable not used:',var1)
            sys.exit(1)
        list_categories = df_class['category'].drop_duplicates().sort_values().reset_index(drop=True)

        for my_category in list_categories:
            variable_list = df_class.loc[df_class['category']==my_category]['variable']
            print(f'>>>> {my_category} ({len(variable_list)})')
            for my_method in list_methods:
                subdf = df.loc[(df['label']==my_method) & (df['variable'].isin(variable_list))]
                avg_mean = subdf['mean'].mean()
                avg_std = subdf['std'].mean()
                print(f'{my_method:12} ==> {avg_mean:.3f} ({avg_std:.3f})')

    # compare with effect size from baseline method
    if (args.methodnames):
        method0 = args.methodnames[0]
        method1 = args.methodnames[1]
        method0_name = normalized_names[method0]
        method1_name = normalized_names[method1]
        if list_methods.isin([method0]).any() and list_methods.isin([method1]).any():
            print(f'>> Rank variables in decreasing order of the difference of effect sizes between {method0} and {method1}')
            df_baseline = df.loc[(df['label']==method1)]
            df['delta'] = df.apply(delta_with_baseline,df2compare=df_baseline,axis=1)
            df.sort_values('delta',ascending=False,inplace=True)
            
            base_filename = f'{method0_name}_vs_{method1_name}'

            # store data in csv
            df.loc[df['label']==method0].to_csv(f'{base_filename}.csv',columns=['variable','delta'])
            print(f'>> Comparison of effect sizes stored in {base_filename}.csv')

            # make color map
            colorpanel = list(sns.color_palette('dark'))
            bar_colors = []  # list of colors for each bar in plot
            colormap = {}   # assignment of colors to categories
            for varname in df.loc[df['label']==method0]['variable']:
                catname = df_class.loc[df_class['variable']==varname]['category'].values[0]
                if not catname in colormap:
                    if len(colormap)<len(colorpanel):
                        colormap[catname]=colorpanel[len(colormap)]
                bar_colors.append(colormap[catname])

            # make figure delta
            xvals = np.arange(len(full_list_variables))
            yvals = df.loc[df['label']==method0]['delta']
            barlist = plt.bar(xvals,yvals,color=bar_colors)
            plt.title(f'{method0_name} vs. {method1_name}')
            plt.xlabel('variables')
            plt.ylabel('delta effect size')
            
            bar_labels = list(colormap.keys())
            handles = [plt.Rectangle((0,0),1,1, color=colormap[label]) for label in bar_labels]
            plt.legend(handles, bar_labels)
            
            # save figure and reset plt
            print(f'>> figure comparison of effect sizes saved in {base_filename}.png')
            plt.savefig(f'{base_filename}.png')
            plt.clf()
            
            # make scatterplot comparison figure
            mean0 = []
            mean1 = []
            colors2 = []
            COLOR = 'white'
            plt.rcParams['text.color'] = COLOR
            plt.rcParams['axes.labelcolor'] = COLOR
            plt.rcParams['xtick.color'] = COLOR
            plt.rcParams['ytick.color'] = COLOR
            for varname in full_list_variables:
                mean1.append(float(df.loc[(df['label']==method1) & (df['variable']==varname)]['mean']))
                mean0.append(float(df.loc[(df['label']==method0) & (df['variable']==varname)]['mean']) * -1.)
                catname = df_class.loc[df_class['variable']==varname]['category'].values[0]
                colors2.append(colormap[catname])
            df2 = pd.DataFrame(data={'var':full_list_variables,'mean0':mean0,'mean1':mean1,'color':colors2})
            #df2.to_csv('test.csv')
            df2.sort_values(by=['mean0'],ascending=True,inplace=True)
            #df2.sort_values(by=['mean1'],ascending=False,inplace=True)
            plt.figure(figsize=(10,15))
            sns.barplot(x='mean0',y='var',data=df2,color='blue')
            sns.barplot(x='mean1',y='var',data=df2,color='red')
            #plt.title(f'{method0_name} vs. {method1_name}')
            plt.xlabel('Effect size', fontsize=16, fontweight='bold')
            plt.text(-0.4, 50, "BLS", fontsize=25, fontweight="bold");
            plt.text(0.25, 50, "SEPP", fontsize=25, fontweight="bold");
            plt.xticks([0.4,0.2,0,0.2,0.4])
            idx=0
            for myvar in df2['var']:
                v0 = float(df2.loc[(df2['var']==myvar)]['mean0'])
                v1 = float(df2.loc[(df2['var']==myvar)]['mean1'])
                delta = -(v0+v1)
                if delta>=0.1:
                    deltacolor='darkgreen'
                elif delta>=0.05:
                    deltacolor='limegreen'
                elif delta>=0.01:
                    deltacolor='palegreen'
                elif delta>=-0.01:
                    deltacolor='whitesmoke'
                elif delta>=-0.05:
                    deltacolor='mistyrose'
                elif delta>=-0.1:
                    deltacolor='lightsalmon'
                elif delta<-0.1:
                    deltacolor='red'

                plt.text(x=v0-0.01, y=idx, s=f'{delta:.2}',ha='right', va='center',fontsize=10,fontweight='bold',color=deltacolor)
                idx += 1

            #bar_labels = list(colormap.keys())
            #handles = [plt.Rectangle((0,0),1,1, color=colormap[label]) for label in bar_labels]
            #plt.legend(handles, bar_labels)
            # save figure and reset plt
            print(f'>> figure showing correlation of effect sizes saved in {base_filename}_histogram.png')
            plt.savefig(f'{base_filename}_histogram.png',bbox_inches='tight',transparent=True)
            plt.clf()

        else:
            print('WARNING: methods ',method0,' or ',method1,'not found. comparison aborted.')
            print(method0,list_methods.isin([method0]).any())
            print(method1,list_methods.isin([method1]).any())

    # compare with effect size from baseline method
    if (args.rank):
        df.sort_values('mean',ascending=False,inplace=True)
        for my_method in list_methods:
            base_filename = f'{my_method}'
            # store data in csv
            df.loc[df['label']==my_method].to_csv(f'{base_filename}.csv',columns=['variable','mean'])
            print(f'>> ranked effect sizes of {my_method} stored in {base_filename}.csv')