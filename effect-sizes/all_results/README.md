### Effect size results

This folder contains the long version of the results presented in the paper and in the supplementary material effect size section (section 14).

The zip files contain the qzv files output by Qiime2 at the end of the effect size computation. They show how well different phylogenies associated with different alignments differentiate between categories for each variable.
To view the content of a qzv file, you can use https://view.qiime2.org/

We also provide two CSV files produced by the jupyter notebooks in the effect-sizes folder, one for general effect size results, and one for significance computations related to BLS and SEPP.

`all_ef_results` is a xlsx file that contains a visual summary of all the results: all alignments on all variables.

`explore.py` and the accompanying `classification.csv` is a script used to generate most of the figures shown in section 14 of the supplementary material.

The results presented in the CSV files were obtained exactly from the alignment provided in the folders labeled with alignment methods in the parent directory (e.g. `../bls`). For MUSCLE and MAFFT in particular, the results included in the CSV files are the highest results obtained from effect size runs on MUSCLE and MAFFT, in order to ensure we were not underestimating the performance of the benchmarks (see Supp. Mat. section 14.5). For BLS, PASTA, Post-Processed PASTA and Greedy, the effect sizes reported come from the alignment with the distance statistics listed in table 1 in the main text.

In order to reproduce these results, see the README in the parent directory. We understand installing and running all the required scripts may be challenging, so we will be happy to help; please do not hesitate to reach out to us. 

