#!/bin/bash
set -x 
set -e

# This script takes as input two folders, each with an alignment (full_agp_alignment_FOLDERNAME.fasta)
#it then does four things for each folder.
# 1. it computes a benchmark de novo alignment and tree with MAFFT. This step can be skipped if enough benchmarks have already been computed
# 2. it applies a mask to the alignment, removing highly gapped columns to make computing a phylogeny easier.
# 3. it executes fasttree on both the masked and unmasked version of the alignment.
# 4. it roots all the generated trees.

python filter_fasta.py bls/full_agp_alignment_bls.fasta &
python filter_fasta.py muscle/full_agp_alignment_muscle.fasta &
wait

qiime phylogeny align-to-tree-mafft-fasttree \
    --i-sequences bls/full_agp_alignment_bls.fasta.filtered.unaligned.qza \
    --output-dir bls/full_agp_alignment_bls.fasta.filtered.unaligned-mafft-fasttree \
    --p-n-threads 16

qiime phylogeny align-to-tree-mafft-fasttree \
    --i-sequences muscle/full_agp_alignment_muscle.fasta.filtered.unaligned.qza \
    --output-dir muscle/full_agp_alignment_muscle.fasta.filtered.unaligned-mafft-fasttree \
    --p-n-threads 16

qiime alignment mask \
    --i-alignment bls/full_agp_alignment_bls.fasta.filtered.qza \
    --o-masked-alignment bls/full_agp_alignment_bls.fasta.filtered.masked.qza &#

qiime alignment mask \
    --i-alignment muscle/full_agp_alignment_muscle.fasta.filtered.qza \
    --o-masked-alignment muscle/full_agp_alignment_muscle.fasta.filtered.masked.qza &
wait

qiime phylogeny fasttree \
    --i-alignment bls/full_agp_alignment_bls.fasta.filtered.masked.qza \
    --o-tree bls/full_agp_alignment_bls.fasta.filtered.masked.tree.qza \
    --p-n-threads 4 &

qiime phylogeny fasttree \
    --i-alignment muscle/full_agp_alignment_muscle.fasta.filtered.masked.qza \
    --o-tree muscle/full_agp_alignment_muscle.fasta.filtered.masked.tree.qza \
    --p-n-threads 4 &
    
qiime phylogeny fasttree \
    --i-alignment bls/full_agp_alignment_bls.fasta.filtered.qza \
    --o-tree bls/full_agp_alignment_bls.fasta.filtered.tree.qza \
    --p-n-threads 4 &

qiime phylogeny fasttree \
    --i-alignment muscle/full_agp_alignment_muscle.fasta.filtered.qza \
    --o-tree muscle/full_agp_alignment_muscle.fasta.filtered.tree.qza \
    --p-n-threads 4 &
wait

qiime phylogeny midpoint-root \
    --i-tree bls/full_agp_alignment_bls.fasta.filtered.masked.tree.qza \
    --o-rooted-tree  bls/full_agp_alignment_bls.fasta.filtered.masked.tree.rooted.qza &

qiime phylogeny midpoint-root \
    --i-tree muscle/full_agp_alignment_muscle.fasta.filtered.masked.tree.qza \
    --o-rooted-tree muscle/full_agp_alignment_muscle.fasta.filtered.masked.tree.rooted.qza &
wait
