#!/bin/bash

# This scripts is designed to run three steps of preprocessing on two alignments.
# It should always be run after q2-denovo.sh
# After running q2-denovo, there will be three subfolders for each alignment being tested.
# The three subfolders represent the de novo tree estimated with MAFFT, the unmasked tree built from the alignment of interest, and the masked tree build from the same alignment.
# For each subfolder, the tree is rerooted and unifrac is run to build distance matrices that are then used to compute effect sizes in run_effect_sizes.py

set -x 
set -e

echo "$PWD"

export UNIFRAC_GPU_INFO=Y
export UNIFRAC_USE_GPU=Y

pushd muscle/
qiime tools export --input-path full_agp_alignment_muscle.fasta.filtered.tree.qza --output-path rt-unmasked-tree 
python ../make_overlap.py rt-unmasked-tree/tree.nwk
t2t reroot --tips archaea.ids -t rt-unmasked-tree/tree.nwk -o rt-unmasked-tree/archaea-rooted.nwk
ssu -t rt-unmasked-tree/archaea-rooted.nwk -i ../raw.nobloom.minfeat.mindepth.sepp.biom.bls-overlap.even1k.remapped -m unweighted_fp32 -o rt-unmasked-tree/archaea-rooted.nwk.uw.dm
popd

pushd bls/
qiime tools export --input-path full_agp_alignment_bls.fasta.filtered.tree.qza --output-path rt-unmasked-tree 
python ../make_overlap.py rt-unmasked-tree/tree.nwk
t2t reroot --tips archaea.ids -t rt-unmasked-tree/tree.nwk -o rt-unmasked-tree/archaea-rooted.nwk
ssu -t rt-unmasked-tree/archaea-rooted.nwk -i ../raw.nobloom.minfeat.mindepth.sepp.biom.bls-overlap.even1k.remapped -m unweighted_fp32 -o rt-unmasked-tree/archaea-rooted.nwk.uw.dm
popd

pushd muscle/full_agp_alignment_muscle.fasta.filtered.unaligned-mafft-fasttree
qiime tools export --input-path rooted_tree.qza --output-path rt-tree 
python ../../make_overlap.py rt-tree/tree.nwk
t2t reroot --tips archaea.ids -t rt-tree/tree.nwk -o rt-tree/archaea-rooted.nwk
ssu -t rt-tree/archaea-rooted.nwk -i ../../raw.nobloom.minfeat.mindepth.sepp.biom.bls-overlap.even1k.remapped -m unweighted_fp32 -o rt-tree/archaea-rooted.nwk.uw.dm
popd

pushd bls/full_agp_alignment_bls.fasta.filtered.unaligned-mafft-fasttree
qiime tools export --input-path rooted_tree.qza --output-path rt-tree 
python ../../make_overlap.py rt-tree/tree.nwk
t2t reroot --tips archaea.ids -t rt-tree/tree.nwk -o rt-tree/archaea-rooted.nwk
ssu -t rt-tree/archaea-rooted.nwk -i ../../raw.nobloom.minfeat.mindepth.sepp.biom.bls-overlap.even1k.remapped -m unweighted_fp32 -o rt-tree/archaea-rooted.nwk.uw.dm
popd

pushd muscle
qiime tools export --input-path full_agp_alignment_muscle.fasta.filtered.masked.tree.rooted.qza --output-path rt-tree 
python ../make_overlap.py rt-tree/tree.nwk
t2t reroot --tips archaea.ids -t rt-tree/tree.nwk -o rt-tree/archaea-rooted.nwk
ssu -t rt-tree/archaea-rooted.nwk -i ../raw.nobloom.minfeat.mindepth.sepp.biom.bls-overlap.even1k.remapped -m unweighted_fp32 -o rt-tree/archaea-rooted.nwk.uw.dm
popd

pushd bls
qiime tools export --input-path full_agp_alignment_bls.fasta.filtered.masked.tree.rooted.qza --output-path rt-tree 
python ../make_overlap.py rt-tree/tree.nwk
t2t reroot --tips archaea.ids -t rt-tree/tree.nwk -o rt-tree/archaea-rooted.nwk
ssu -t rt-tree/archaea-rooted.nwk -i ../raw.nobloom.minfeat.mindepth.sepp.biom.bls-overlap.even1k.remapped -m unweighted_fp32 -o rt-tree/archaea-rooted.nwk.uw.dm
popd
