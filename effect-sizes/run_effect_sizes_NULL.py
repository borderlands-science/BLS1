import csv
from subprocess import Popen, PIPE, DEVNULL
import os
import random

#This script is a slightly altered version of run_effect_sizes.py where the metadata is shuffled, in order to mimic a situation where the annotations of the data would not have any particular signal.
#This setting is used to obtain a null model which is then used to compute p-values in order to identify distributions of pairwise effect sizes that are significantly distinct from the null model.


#all variables
VARS = ['thyroid', 'acne_medication', 'cosmetics_frequency', 'homecooked_meals_frequency', 'tonsils_removed', 'smoking_frequency', 'antibiotic_history', 'age_cat', 'lung_disease', 'exercise_location', 'meat_eggs_frequency', 'latitude', 'olive_oil', 'flossing_frequency', 'diet_type', 'diabetes', 'ibd', 'vivid_dreams', 'red_meat_frequency', 'acid_reflux', 'qiita_empo_2', 'poultry_frequency', 'chickenpox', 'bowel_movement_quality', 'fungal_overgrowth', 'race', 'salted_snacks_frequency', 'kidney_disease', 'softener', 'liver_disease', 'qiita_empo_3', 'alcohol_consumption', 'sugary_sweets_frequency', 'multivitamin', 'longitude', 'alzheimers', 'skin_condition', 'bmi_cat', 'probiotic_frequency', 'weight_change', 'artificial_sweeteners', 'high_fat_red_meat_frequency', 'alcohol_frequency', 'fruit_frequency', 'autoimmune', 'ready_to_eat_meals_frequency', 'cancer', 'cdiff', 'prepared_meals_frequency', 'frozen_dessert_frequency', 'whole_eggs', 'cardiovascular_disease', 'bowel_movement_frequency', 'milk_substitute_frequency', 'qiita_empo_1', 'whole_grain_frequency', 'teethbrushing_frequency', 'gluten', 'collection_timestamp', 'sibo', 'one_liter_of_water_a_day_frequency', 'seafood_frequency', 'lactose', 'asd', 'drinks_per_session', 'milk_cheese_frequency', 'migraine', 'pool_frequency', 'sleep_duration', 'ibs', 'types_of_plants', 'sugar_sweetened_drink_frequency', 'country_residence', 'drinking_water_source', 'mental_illness', 'exercise_frequency', 'vegetable_frequency', 'nail_biter', 'seasonal_allergies','sex','country']
#All alignments
ALIGNMENTS_TO_TEST =  ["bls","greedy","mafft","muscle","pasta","sepp","ppp"]

bad_answers = ['Not provided', 'nan', 'not provided', 'not applicable', 'Not applicable', 'not collected', 'Not collected',
              'Not sure', 'not sure', 'unspecified', 'Unspecified', 'I don\'t know', 'None of the above',
               'none of the above','LabControl test', '']

metadata_filename_per_var = {}


def shuffle_metadata(lst):
    final_rows = [lst[0]]
    samples = [x[0] for x in lst[1:]]
    categories = [x[1] for x in lst[1:]]
    random.shuffle(categories)
    for i in range(len(samples)):
        new_row = [samples[i],categories[i]]
        final_rows.append(new_row)
    return final_rows

def get_metadata_for_var(varname, raw_info_fname, bad_answers):
    rows = []
    outname = varname + "shuffled_metadata.tsv"
    with open(raw_info_fname) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\t')
        line_count = 0
        for row in csv_reader:
            rows.append(row)
    varind = rows[0].index(varname)
    newrows = []
    for row in rows:
        if row[varind] not in bad_answers:
            newrows.append([row[0],row[varind]])
    newrows = shuffle_metadata(newrows)
    with open(outname,'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter='\t')
        for r in newrows:
            csv_writer.writerow(r)
    return outname

for v in VARS:
    metvar = get_metadata_for_var(v,'raw.columns_of_interest.txt', bad_answers)
    metadata_filename_per_var[v] = metvar

print(os.getcwd())

for var in VARS:
    cmd_list = []
    #builds a list of commands to all alignments for one variable
    #this avoids simultaneously opening the same alignment distance matrix which breaks qiime.
    for i, aln in enumerate(ALIGNMENTS_TO_TEST):
        dst = var+"."+aln+".effect_size_result_SHUFFLED.qza"
        command = "bash evident_flexible_v3.sh -m " + metadata_filename_per_var[var] + " -v "+var+" -d "+dst + " -t " + aln + " > alex_test_{}.txt".format(i)
        cmd_list.append(command)
    print(cmd_list)
    procs_list = [Popen(cmd, stdout=DEVNULL, stderr=DEVNULL, shell=True, bufsize=0) for cmd in cmd_list]
    
    #runs the commands, waiting for everyone to be done before going to the next variable to avoid working simultaneously on the same alignments
    for proc in procs_list:
        print(proc)
        output = proc.communicate()
        print(output)

print("FINISHED!")
