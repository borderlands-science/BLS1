#!/bin/bash
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem 64g
#SBATCH --time 24:00:00
#SBATCH --partition gpu
#SBATCH --gres=gpu:1

# this script runs evident on all subfolders for all variables called in its input, and generates visualization.
# This script is called through run_effect_sizes.py and run_effect_sizes_NULL>py and is not designed to be executed independently.

# -m is a metadata filename
# -v is a value fro variable
# -d is value for dst
# -t is stringified version of TODO separated by space
#       (no quotes in the middle). 
#       E.g. `... -t "sepp_daniel offres")`

while getopts m:v:d:t: flag
do
    case "${flag}" in
        m) metadata_filename=${OPTARG};;
        v) variable=${OPTARG};;
        d) dst=${OPTARG};;
        t) todo_str=${OPTARG};;
    esac
done

# log related stuff
declare time_subfolder="$(date +%Y-%m-%d_%H:%M:%S.%6N)"
declare logs_tools_subfolder="ef_logs/${time_subfolder}/tools"
declare logs_diversity_subfolder="ef_logs/${time_subfolder}/diversity"
declare logs_multivariate_subfolder="ef_logs/${time_subfolder}/multivariate"
declare logs_visualize_subfolder="ef_logs/${time_subfolder}/visualize"
mkdir -p ${logs_tools_subfolder}
mkdir -p ${logs_diversity_subfolder}
mkdir -p ${logs_multivariate_subfolder}
mkdir -p ${logs_visualize_subfolder}

declare -a processes_et_stuff=()

echo ${TMPDIR} 
# set -x
# set -e # << uncomment if exit on any non standard exit code
# NOTE: ${md} converted to ${metadata_filename}

echo ${todo_str}
declare -a TODO=${todo_str}
echo $TODO


wait_and_process() {
    _args=($@)
    toexit=false
    for j in $(seq 0 4 $((${#_args[@]}-1)) )
    do
        loc_proc=${_args[j]}
        loc_com=${_args[j+1]//'<!>'/ }
        loc_path=${_args[j+2]//'<!>'/ }
        loc_logs_failed_cmd=${_args[j+3]//'<!>'/ }
        wait ${loc_proc}
        if [ $? -ne 0 ];then # failed
            echo "ERRORR!!!"
            echo ${loc_com} >> ${logs_tools_subfolder}/failed_cmd.log
            toexit=true
        else
            # Remove raw file if successful execution
            rm -f ${loc_path}
        fi
    done
    # uncomment if exit after a round with a error
    # if [ toexit ];then
    #     exit 100
    # fi
}

processes_et_stuff=()
for f in $(find . -name "*.dm" -regex "./${todo_str}/rt-tree/.+" -print)
do
    if [[ -f "${f}.qza" ]];then
        continue
    fi
    Todo=false
    for val in ${TODO[@]}; do
        if [[ "$f" == *"$val"* ]];then
            Todo=true
        fi
    done
    if [[ "$f" == "results" ]];then
        Todo=false
    fi
    if [[ "$f" == "multivar_results" ]];then
        Todo=false
    fi
    if $Todo ; then
        logname=${f//\//_}
        raw_folder_path="${logs_tools_subfolder}/raw"
        mkdir -p ${raw_folder_path}
        raw_log_path="${raw_folder_path}/l${logname}.log"
        qcmd="qiime tools import --input-path $f --output-path ${f}.qza --type DistanceMatrix &>> ${raw_log_path} &"
        qiime tools import --input-path $f --output-path ${f}.qza --type DistanceMatrix &>> ${raw_log_path} &
        processes_et_stuff+=($! ${qcmd// /'<!>'} ${raw_log_path// /'<!>'} ${logs_tools_subfolder// /'<!>'})
    fi
done

echo "tools: WAITING FOR ${#processes_et_stuff[@]} TASKS TO BE COMPLETE"
wait_and_process ${processes_et_stuff[@]}

processes_et_stuff=()
for f in $(find . -name "*.dm.qza" -regex "./${todo_str}/rt-tree/.+" -print)
do
    Todo=false
    for val in ${TODO[@]}; do
        if [[ "$f" == *"$val"* ]];then
            Todo=true
        fi
    done
    if [[ "$f" == "results" ]];then
        Todo=false
    fi
    if [[ "$f" == "multivar_results" ]];then
        Todo=false
    fi
    if $Todo ; then
        d=$(dirname $f)
        name=$(basename $f .qza)
        logname=${f//\//_}
        raw_folder_path="${logs_diversity_subfolder}/raw"
        mkdir -p ${raw_folder_path}
        raw_log_path="${raw_folder_path}/l${logname}.log"
        # attention - quotes escaped!
        qcmd="qiime diversity filter-distance-matrix --i-distance-matrix ${f} --o-filtered-distance-matrix ${d}/${name}.clean${variable}.qza --m-metadata-file ${metadata_filename} --p-where \"[${variable}] not in ('Not provided', 'nan', 'Not provided', 'not applicable', 'not provided', 'not collected', 'Not collected')\" &>> ${raw_log_path}"
        qiime diversity filter-distance-matrix --i-distance-matrix ${f} --o-filtered-distance-matrix ${d}/${name}.clean${variable}.qza --m-metadata-file ${metadata_filename} --p-where "[${variable}] not in ('Not provided', 'nan', 'Not provided', 'not applicable', 'not provided', 'not collected', 'Not collected')" &>> ${raw_log_path} &
        processes_et_stuff+=($! ${qcmd// /'<!>'} ${raw_log_path// /'<!>'} ${logs_diversity_subfolder// /'<!>'})
    fi
done

echo "diversity: WAITING FOR ${#processes_et_stuff[@]} TASKS TO BE COMPLETE"
wait_and_process ${processes_et_stuff[@]}

processes_et_stuff=()
for f in $(find . -name "*.dm.clean${variable}.qza" -regex "./[^/]+/rt-tree/.+" -print)
do
    Todo=false
    for val in ${TODO[@]}; do
        if [[ $f == *"$val"* ]] ; then
            Todo=true
        fi
    done

    if $Todo ; then
	    logname=${f//\//_}
        raw_folder_path="${logs_multivariate_subfolder}/raw"
        mkdir -p ${raw_folder_path}
        raw_log_path="${raw_folder_path}/l${logname}.log"
        qcmd="qiime evident multivariate-effect-size-by-category \
            --i-data ${f} \
            --m-sample-metadata-file ${metadata_filename} \
            --p-group-columns ${variable} \
            --p-max-levels-per-category 10000000 \
            --p-min-count-per-level 50 \
            --p-pairwise \
            --p-n-jobs 8 \
            --o-effect-size-results ${f}.${variable}.qza &>> ${raw_log_path}"
        qiime evident multivariate-effect-size-by-category \
            --i-data ${f} \
            --m-sample-metadata-file ${metadata_filename} \
            --p-group-columns ${variable} \
            --p-max-levels-per-category 10000000 \
            --p-min-count-per-level 50 \
            --p-pairwise \
            --p-n-jobs 8 \
            --o-effect-size-results ${f}.${variable}.qza &>> ${raw_log_path} &
        processes_et_stuff+=($! ${qcmd// /'<!>'} ${raw_log_path// /'<!>'} ${logs_multivariate_subfolder// /'<!>'})
    fi
done

echo "multivariate: WAITING FOR ${#processes_et_stuff[@]} TASKS TO BE COMPLETE"
wait_and_process ${processes_et_stuff[@]}

processes_et_stuff=()
for f in $(find . -name "*.${variable}.qza" -regex "./${todo_str}/rt-tree/.+" -print)
do
Todo=false
    for val in ${TODO[@]}; do
        if [[ "$f" == *"$val"* ]];then
            Todo=true
        fi
    done
    if [[ "$f" == "results" ]];then
        Todo=false
    fi
    if [[ "$f" == "multivar_results" ]];then
        Todo=false
    fi
    if $Todo ; then
	    logname=${f//\//_}
	raw_folder_path="${logs_visualize_subfolder}/raw"
	mkdir -p ${raw_folder_path}
        raw_log_path="${raw_folder_path}/l${logname}.log"
        qcmd="qiime evident visualize-results \
            --i-results ${f} \
            --o-visualization ${f}.qzv &>> ${raw_log_path}"
        qiime evident visualize-results \
            --i-results ${f} \
            --o-visualization ${f}.qzv &>> ${raw_log_path} &
        processes_et_stuff+=($! ${qcmd// /'<!>'} ${raw_log_path// /'<!>'} ${logs_visualize_subfolder// /'<!>'})
    fi
done

echo "visualize: WAITING FOR ${#processes_et_stuff[@]} TASKS TO BE COMPLETE"
wait_and_process ${processes_et_stuff[@]}

#mkdir -p results
for f in $(find . -name "*.${variable}.qza" -regex "./${todo_str}/rt-tree/.+" -print
)
do
Todo=false
    for val in ${TODO[@]}; do
        if [[ "$f" == *"$val"* ]];then
            Todo=true
        fi
    done
    if [[ "$f" == "results" ]];then
        Todo=false
    fi
    if [[ "$f" == "multivar_results" ]];then
        Todo=false
    fi
    if $Todo ; then
        # NOTE: ${dst} is defined by the flag arguments above
        # dst=$(echo ${f#"./"} | tr "/" "-")
        cp ${f} multivar_results/${dst}
        cp ${f}.qzv multivar_results/${dst}.qzv
    fi
done
wait

# custom exit code just for fun
exit 10

# delete all the .dm files in the subfolder after output is generated
# + keeping *.nwk.uw.dm and *.nwk.uw.dm.qza 
#   as it needs to be generated once
find . -name "*.dm*" \
  -not -path "./sepp_daniel/*" \
  -not -path "./results/*" \
  -not -path "./results_technical/*" \
  -not -path "*.nwk.uw.dm" \
  -not -path "*.nwk.uw.dm.qza" \
  -not -path "*.nwk" \
  -regex "^./[^/]+/[^/]+/.+" \
  -print
