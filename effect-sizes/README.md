### Installation

Effect sizes for Borderlands Science are computed with Qiime2 and Evident. Qiime recommends a python environment for installation.

We have included a requirements.txt files that contains all the libraries we use and their versions, but we insist you should install Qiime2 first, and then install missing libraries. Note that we recommend **numpy 1.22.0** specifically; earlier versions cause issues with some packages, and later versions with others.

We recommend python 3.8.

Install Qiime2 with conda [here](https://anaconda.org/qiime2/qiime2).

Notable packages you will need to install after Qiime:
- evident
- iow
- tax2tree
- fasttree

We recommend installing the python packages with pip and other software with conda.

Note: for tax2tree, we have made changes to the util file which is not yet on pip. For now, the code in the package, usually located at a path that looks like miniconda/envs/YOUR_ENV/lib/python3.8/site-packages/t2t/util.py

In the folder, we have provided some scripts which are explained in the step-by-step process at the bottom of this README, and some essential data such as the biom table used for this dataset (`raw.nobloom.mindfeat.mindepth.sepp.bion.bls-overlap.even1k`), and the metadata collection (`raw.columns_of_interest.txt`), as well as archaeal ids and predicted phylogenies which are found in each folder.

### How to Run
Follow these steps to run effect size computations on Borderlands Science results with Qiime2.

The following steps take as input an alignment of all input sequence fragments from the American Gut Project (about 950k), generates a tree with FastTree, reroots it using archaeal tips, and then uses American Gut data to compute effect sizes associated with the tree. Since SEPP proceeds by insertion into an existing phylogeny, it follows slightly different steps.

NOTE: We have provided folders with complete results for Borderlands Science (BLS), MUSCLE, MAFFT, SEPP, PASTA, Post-processed PASTA (ppp), and Greedy (all results featured in the paper). These results include the mean pairwise effect sizes presented as CSV files and a visual summary as a xlsx file as well as qzv files containing the exact distributions of distances, but do NOT include distance matrices used in the computation of the effect sizes as these are 50 to 100 GB per alignment and could not fit on the repository. You can follow the following instructions to generate the matrices. The space-intensive steps are steps 5 and 9.


1. Make sure you have enabled your conda environment with Qiime2 and evident installed. 

2. Create a new folder with the name of the alignment you want to run. 

3. Insert the full alignment (950k sequences) into the folder with the name full_agp_alignment_FOLDERNAME.fasta, so here, for instance, full_agp_alignment_MUSCLE.fasta. This has already been done for you for BLS and MUSCLE. Also insert the archaea.ids file. Please note that SEPP has a distinct archaea.ids file in its folder. That is intentional because the tree is different.

4. Open the first script of the pipeline, q2-denovo.sh. This bash script builds a de novo phylogeny for the same data so it can be used as benchmark, with MAFFT. Set the filenames to the folders you want to investigate. Here, we have done it for BLS and MUSCLE. Run it with bash q2-denovo.sh (we recommend using nohup). This step will not work on the SEPP folder, but subsequent steps will. Because building the phylogeny is computationally expensive, we have already provided the rooted tree in the `rt-tree` subfolder for every alignment. 

5. Open the second script, root-then-unifrac.sh . Once again, change the two filenames to the two alignments you want to test. This will run tax2tree to root the trees correctly and unifrac to generate the distance matrices we need for effect size computations. You can run this step on the BLS, MUSCLE, and SEPP folders, or any other folder you will have assembled following the previous rules.

6. Everything else is done through python. To run regular effect sizes, the script is run_effect_sizes.py

7. Set the variables to test as the variable VARS. the default is everything and is listed as a comment at the top. You can just uncomment that.

8. Set the folders containing the alignments/trees to test. Enter folder names in the variable ALIGNMENTS_TO_TEST (here those would be revmafft and revmuscle)

9. Execute the python file, and the effect size results will be generated in the form of .qza files.

10. The Effect size results compilation.ipynb notebook parses these qza files and generates a csv with the results for each alignment tested, for each variable.

11. The all_results/explore.py file generates visual summaries of the differences in observed effect sizes for two variables, taking as input the csv file generated at step 10, and the name of two variables of interest.

12. A null model (shuffled effect sizez) can be computed by repeating steps 6 to 9 with the file run_effect_sizes_NULL.py instead of run_effect_sizes.py

13. Then, for step 10, the notebook Effect sizes null model and p-values.ipynb notebook parses these qza files for both shuffled and real results, computes p-values, and generates a csv with the results for each alignment tested, for each variable.
