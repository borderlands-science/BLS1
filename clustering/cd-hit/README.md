This folder contains the initial data behind Borderlands Science, distributed across the following files:

- ag.150nt.fna : contains the 950k sequence fragments in Borderlands Science
- ag_small.clstr: CD-hit output for clustering these 950k sequences into 11k clusters.
- extract_representative_sequences.ipynb: python notebook to extract sequence representatives from each clusters and put them in a fasta file.
- ag_11k_rep.fasta : resulting fasta file of representative sequences.

CD-hit was first run with similarity threshold of 0.9, and produced 100k clusters. This threshold was then lowered to 0.87 to obtain 11k clusters. The other parameters were default.

To see how we aligned the cluster representative sequences, see the borderlands-science/alignments folder.
