import os
import csv
from scipy.stats import entropy
from sklearn import preprocessing
from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np
import itertools

#This is a utility script that computes several useful statistics about a large multiple sequence alignment.

def produce_count_dicts(in_db, target_seq, l, start_i=0, end_i=None):
    count_dicts = [{'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 0} for _ in range(l)]
    if target_seq is not None:
        for k in in_db:
            if end_i is None:
                end_i = l
            assert end_i == start_i + l
            t_s = in_db[k][target_seq][start_i:end_i]
            for i in range(len(t_s)):
                count_dicts[i][t_s[i]] += 1
    return count_dicts


def score_count_dicts(count_dicts):
    score = 0
    for c_d in count_dicts:
        for e in itertools.combinations(c_d.keys(), 2):
            if (e[0] == 'A' and e[1] == 'G') or (e[0] == 'C' and e[1] == 'T'):
                score -= (c_d[e[0]] * c_d[e[1]]) / 2
            else:
                score -= c_d[e[0]] * c_d[e[1]]
        for k in c_d:
            if k != '-':
                score += c_d[k] * (c_d[k] - 1) / 2
            else:
                score -= c_d[k] * (c_d[k] - 1) / 2
    return score

if __name__ == '__main__':
    cluster_files = []
    cluster_stats = []
    for cluster_file in os.scandir('clusters'):
        if cluster_file.name == '.gitkeep':
            continue
        seqs = {}
        for record in SeqIO.parse(cluster_file, 'fasta'):
            seqs[record.id] = {'seq': str(record.seq)}

        count_dicts = produce_count_dicts(seqs, 'seq', len(seqs[list(seqs.keys())[0]]['seq']))
        seq_cnt = len(seqs)
        if seq_cnt == 1:
            pk = [list(cd.values()) for cd in count_dicts]
        else:
            pk = [[cd[v] / seq_cnt for v in cd] for cd in [_ for _ in count_dicts if _['-'] < int(0.5 * seq_cnt)]]
        entropies = []

        for c in pk:
            entropies.append(entropy(c, base=2))

        e_a = np.array(entropies)
        n_hgc = sum([1 for _ in count_dicts if _['-'] > int(0.75 * seq_cnt)])
        if seq_cnt == 1:
            sop = score_count_dicts([_ for _ in count_dicts])
        else:
            sop = score_count_dicts([_ for _ in count_dicts if _['-'] < int(0.5 * seq_cnt)])

        stats_file = cluster_file.name.split('.')[0] + '.txt'
        output_file = open('cluster_stats/' + stats_file, 'w')
        output_file.write("** Alignment Stats **\n")
        output_file.write("column count:" + str(len(count_dicts)) + '\n')
        output_file.write("number of highly gapped columns (more than 75%):" + str(n_hgc) + '\n')
        output_file.write("score (SoP):" + str(sop) + '\n')
        output_file.write('\n')
        output_file.write("** Entropy Stats **\n")
        output_file.write("column's with gaps less than half:" + str(len(pk)) + '\n')
        output_file.write("Min E:" + str(min(entropies)) + '\n')
        output_file.write("Max E:" + str(max(entropies)) + '\n')
        output_file.write("Max E possible:" + str(entropy([0.2, 0.2, 0.2, 0.2, 0.2], base=2)) + '\n')
        output_file.write("Avg E:" + str(np.mean(e_a)) + '\n')
        output_file.write("St. D:" + str(np.std(e_a)) + '\n')
        output_file.write("Var. :" + str(np.var(e_a)) + '\n')
        output_file.close()
        cluster_files.append([stats_file, sop, seq_cnt])
        cluster_stats.append(sop/(seq_cnt**2))
    clusters_normalized = preprocessing.normalize(np.array([cluster_stats]))
    for i in range(len(cluster_files)):
        cluster_files[i].append(clusters_normalized[0][i])
    cluster_files.sort(key=lambda x: x[3], reverse=True)
    with open('cluster_stats/sorted_clusters.csv', 'w', newline='') as sorted_file:
        writer = csv.writer(sorted_file)
        writer.writerow(['cluster file', 'SoP', 'num seqs', 'SoP/num seqs normalized'])
        writer.writerows(cluster_files)
    plt.hist(clusters_normalized[0])
    plt.title('Sum of Pairs normalized by number of sequences')
    plt.ylabel('Density')
    plt.xlabel('SoP/num seqs normalized')
    plt.savefig('cluster_stats/sopHistogram.png')
    plt.clf()

