import os, sys
import glob
import json
import numpy as np
from multiprocessing import Process
from Bio import SeqIO
from Bio.Seq import Seq

#This script takes as input a small BLS alignment (9667 sequences) and propagates this alignment of cluster representatives to the entire 950k sequences dataset. 
#For each cluster representative, it aligns the rest of the cluster sequences to the representative, then merges the aligned cluster together

def build_cluster_dict(parms):
    record_dict = SeqIO.to_dict(SeqIO.parse("../cd-hit/" + parms["record_file"], "fasta"))
    cluster_dict = {}
    curr_rep = 0

    with open("../cd-hit/" + parms["cluster_file"]) as f:
        lines = f.readlines()
        for line in lines:
            if ">Cluster" in line:
                continue
            elif "*" in list(line):
                id = line.split(">")[1].split(".")[0]
                cluster_dict[id] = []
                curr_rep = id
            else:
                id = line.split(">")[1].split(".")[0]
                cluster_dict[curr_rep].append(record_dict[id])
    return cluster_dict

def align_cluster(parms, rep_seq, cluster):
    for cluster_seq in cluster:
        cluster_seq.seq = Seq(get_profile_alignment(parms, rep_seq.seq, cluster_seq.seq))

    cluster.insert(0, rep_seq)
    SeqIO.write(cluster, "clusters/rep_" + str(rep_seq.id) + "_cluster.fasta", "fasta")

def get_profile_alignment(parms, rep_seq, cluster_seq):
    matrix = np.zeros((len(rep_seq) + 1, len(cluster_seq) + 1), np.float)
    trace_matrix = matrix.copy()
    for pos in range(1, len(rep_seq) - (len(cluster_seq) - 1)):
        matrix[pos][0] = parms["gap_open"] + ((pos - 1) * parms["gap_extension"])
        trace_matrix[pos][0] = 1

    for i in range(1, matrix.shape[0]):
        for j in range(max(1, i - (len(rep_seq) - len(cluster_seq))), min(i + 1, matrix.shape[1])):
            match = round(matrix[i - 1, j - 1] + (parms["match"] if cluster_seq[j - 1] == rep_seq[i - 1]
                                                  else parms["mismatch"]), 2)
            if j == matrix.shape[1] - 1 and i > len(cluster_seq):
                skip = matrix[i - 1, j]
            elif len(cluster_seq[i:]) < len(rep_seq[j:]) and i > j:
                if trace_matrix[i - 1, j] == 0:
                    skip = matrix[i - 1, j] + parms["gap_open"]
                else:
                    skip = matrix[i - 1, j] + parms["gap_extension"]
            else:
                skip = -2000000
            skip = round(skip, 2)
            matrix[i, j] = max(match, skip)
            if skip >= match:
                trace_matrix[i, j] = 1
    new_cluster_seq = traceback(trace_matrix, cluster_seq)
    return new_cluster_seq

def traceback(matrix, cluster_seq):
    new_cluster_seq = ''
    i, j = matrix.shape[0] - 1, matrix.shape[1] - 1
    while i != 0 or j != 0:
        if matrix[i][j] == 0:
            new_cluster_seq += cluster_seq[-1]
            cluster_seq = cluster_seq[:-1]
            i -= 1
            j -= 1
        else:
            new_cluster_seq += '-'
            i -= 1
    return new_cluster_seq[::-1]

def build_clusters(parms, reps_dict, cluster_dict):
    invalid_reps = []
    reps = list(cluster_dict.keys())
    clusters = list(cluster_dict.values())
    for i in range(0, len(reps), parms["num_subprocess"]):
        reps_subset = reps[i:i + parms["num_subprocess"]]
        clusters_subset = clusters[i:i + parms["num_subprocess"]]
        jobs = []
        for j in range(len(reps_subset)):
            if reps_subset[j] in reps_dict.keys():
                rep_seq = reps_dict[reps_subset[j]]
                this_proc = Process(target=align_cluster, args=(parms, rep_seq, clusters_subset[j]))
                jobs.append(this_proc)
                this_proc.start()
            else:
                invalid_reps.append(reps_subset[j])
        for k in jobs:
            k.join()
    print("Representative sequences not found:")
    print(invalid_reps)

def verify_lengths(expected_length):
    same_length = True
    print("Verifying lengths")
    for cluster_file in os.scandir('clusters'):
        cluster = SeqIO.to_dict(SeqIO.parse(cluster_file, "fasta"))
        for seq in cluster.values():
            if len(seq.seq) != expected_length:
                print(cluster_file + ": " + str(seq.id))
                same_length = False
    if same_length:
        print("All sequences same length")
    return same_length

def merge_clusters():
    all_sequences = []
    for cluster_file in os.scandir('clusters'):
        all_sequences.extend(list(SeqIO.parse(cluster_file, "fasta")))
    SeqIO.write(all_sequences, "mega_alignment.fasta", "fasta")

if __name__ == '__main__':
    reps_file = sys.argv[1]
    reps_dict = SeqIO.to_dict(SeqIO.parse(reps_file, "fasta"))
    puzzle_parms = glob.glob('parameters/profile_aln_parms.json')[0]
    with open(puzzle_parms) as parms_file:
        parms = json.load(parms_file)
    cluster_dict = build_cluster_dict(parms)

    build_clusters(parms, reps_dict, cluster_dict)
    verify_lengths(len(list(reps_dict.values())[0].seq))
    merge_clusters()

