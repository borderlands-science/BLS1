## Clustering 

Clustering was performed via cd-hit, and then the script extract_representative_sequences was used to get the representatives.

To see details about initial data and clustering, see the README in the cd-hit folder.

## Merging clusters based on representative alignment to obtain large alignments

This is done in the cluster-alignments folder.

Here is how to convert an alignment of cluster representatives (the 9.667k sequences shown in the cd-hit subfolder) to an alignment of 950k sequence fragments:


1. Go to borderlands-science/clustering/cluster_alignment
2. If running locally, change the # subprocesses in parameters/profile_aln_parms.json to a number suitable for your personal computer
3. Run nohup python align_clusters.py [path to fasta file] &
4. When done it'll output mega_alignment.fasta in the same directory. We suggest you immediately rename this file to represent the input data.

These large 950k alignments are then used to compute effect sizes. These computations can be found in the borderlands-science/effect-sizes folder.
