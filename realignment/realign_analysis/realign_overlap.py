import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import pickle
import math
import pandas as pd

if __name__ == '__main__':
    dfs_file = sys.argv[1]
    offsets = sys.argv[2:]
    file = open("dfs_results/" + dfs_file, 'rb')
    pickle_data = pickle.load(file)

    for key in list(pickle_data.keys()):
        for offset in pickle_data[key].keys():
            df = pickle_data[key][offset]
            df = df.notnull().astype('int')
            pickle_data[key][offset] = df

    take_sum = lambda s1, s2: s1 + s2
    covered_by_all = {}
    key = list(pickle_data.keys())[0]
    df = pickle_data[key][int(offsets[0])]
    for offset in offsets[1:]:
        df = df.combine(pickle_data[key][int(offset)], take_sum)
    for i in range(len(offsets)):
        df.replace(i, math.nan, inplace=True)
    df = df.dropna(how='all')
    df = df.dropna(how='all', axis='columns')

    found_pos_dict = df.to_dict(orient='index')
    for seq, cols in found_pos_dict.items():
        columns = []
        for col, value in cols.items():
            if not pd.isna(value):
                columns.append(col)
        print(seq, columns)
