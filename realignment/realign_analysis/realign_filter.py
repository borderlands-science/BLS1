import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import pickle


def filter_df(df, columns, sequences):
    df = df[df.columns.intersection(columns)]
    df = df[df.index.isin(sequences)]
    return df


if __name__ == '__main__':
    dfs_file = sys.argv[1]
    start_col = int(sys.argv[2])
    end_col = int(sys.argv[3])
    sequences = sys.argv[4:]
    file = open("dfs_results/" + dfs_file, 'rb')
    pickle_data = pickle.load(file)

    filtered_cols = list(range(start_col, end_col+1))

    for key in list(pickle_data.keys()):
        for offset in pickle_data[key].keys():
            pickle_data[key][offset] = filter_df(pickle_data[key][offset], filtered_cols, sequences)
            pickle_data[key][offset].to_csv('figures/csv/' + key + '_' + str(offset) + '.csv', encoding='utf-8',
                                            index=False)

    date = dfs_file.split('.')[0].split('_')[-1]
    pickle.dump(pickle_data, open("dfs_results/analyze_results_dfs_cols_" + str(start_col) + "_" + str(end_col) + "_" +
                                  date + ".pickle", "wb"))
