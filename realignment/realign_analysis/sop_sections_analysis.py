import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import csv
import copy
import argparse
import itertools
import pandas as pd
from datetime import date
import realignment.realign_analysis.realign_filter as rf
from Bio import SeqIO


def count_columns(section_df):
    counted_cols = []
    nucs = ['A', 'C', 'G', 'T', '-']
    for column in section_df:
        column_list = list(map(section_df[column].tolist().count, nucs))
        column_dict = {nucs[i]: column_list[i] for i in range(len(nucs))}
        counted_cols.append(column_dict)
    return counted_cols


def score_count_dicts(count_dicts):
    score = 0
    for col_dict in count_dicts:
        total = sum(col_dict.values())
        for combo in itertools.combinations(col_dict.keys(), 2):
            if combo == ('A', 'G') or combo == ('C', 'T'):
                score -= (col_dict[combo[0]] * col_dict[combo[1]]) / 2
            else:
                score -= col_dict[combo[0]] * col_dict[combo[1]]
        for nuc in col_dict:
            match_score = col_dict[nuc] * (col_dict[nuc] - 1) / 2
            if nuc == '-':
                if col_dict[nuc] == total:
                    match_score = 0
                else:
                    match_score = -match_score
            score += match_score
    return score


def map_alignments(old_align, new_align):
    align_map = {}
    for i in range(len(old_align)):
        align_map[old_align[i].id] = {}
        old_seq = copy.deepcopy(old_align[i])
        old_row_finder = old_seq.seq
        new_seq = copy.deepcopy(new_align[i])
        new_row_finder = new_seq.seq
        old_seq.seq = str(old_seq.seq).replace('-', '')
        for nuc in old_seq:
            old_pos = old_row_finder.index(nuc)
            new_pos = new_row_finder.index(nuc)
            old_row_finder = ('-' * (old_pos + 1)) + old_row_finder[old_pos + 1:]
            new_row_finder = ('-' * (new_pos + 1)) + new_row_finder[new_pos + 1:]
            align_map[old_align[i].id][old_pos] = new_pos

    return align_map


def calculate_section_sop(df, columns, sequences):
    section_df = copy.deepcopy(df)
    section_df = rf.filter_df(section_df, columns, sequences)
    col_count_dicts = count_columns(section_df)
    if len(section_df.index) == 1:
        sop = score_count_dicts(col_count_dicts)
    else:
        sop = score_count_dicts([x for x in col_count_dicts if x['-'] < int(0.9 * len(section_df.index))])
    return sop


def get_new_columns_range(align_map, sequences, start_col, num_cols):
    first_columns = []
    last_columns = []
    for seq in sequences:
        try:
            first_columns.append(align_map[seq][start_col])
        except KeyError:
            pass
        try:
            last_columns.append(align_map[seq][start_col + (num_cols - 1)])
        except KeyError:
            pass
    if len(first_columns) < 5 or len(last_columns) < 5:
        return -1, -1
    most_common_first = max(set(first_columns), key=first_columns.count)
    most_common_last = max(set(last_columns), key=last_columns.count)
    return most_common_first, most_common_last


def calculate_all_section_sops(old_df, new_df, align_map, num_cols, col_overlap, num_seqs, seq_overlap):
    scores_dict = {}
    cur_seq = 0
    all_sequences = old_df.index.tolist()
    all_old_cols = old_df.columns.tolist()
    all_new_cols = new_df.columns.tolist()

    while cur_seq < len(all_sequences):
        cur_col = 0
        sequences = all_sequences[cur_seq:cur_seq + num_seqs]
        while cur_col < len(all_old_cols):
            columns = all_old_cols[cur_col:cur_col + num_cols]
            new_first_col, new_last_col = get_new_columns_range(align_map, sequences, cur_col, num_cols)
            if (new_first_col == -1 and new_last_col == -1) or (new_first_col >= new_last_col):
                cur_col += 1
                continue
            new_columns = all_new_cols[new_first_col:new_last_col + 1]
            old_sop = calculate_section_sop(old_df, columns, sequences)
            new_sop = calculate_section_sop(new_df, new_columns, sequences)
            scores_dict[((sequences[0], sequences[-1]), (columns[0], columns[-1]), (new_columns[0], new_columns[-1]))] \
                = [old_sop, new_sop, new_sop - old_sop]
            cur_col += num_cols - col_overlap
        cur_seq += num_seqs - seq_overlap
    return scores_dict


def output_files(output_file, scores_dict, avg_score_diff, worse_results, better_results, unchanged_results):
    today = date.today().strftime("%Y-%m-%d")
    output_path = os.path.join('sop_section_results', today)
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    with open(output_path + "/" + output_file + "_summary", 'w') as results_file:
        results_file.write("Total windows: " + str(len(scores_dict)) + "\n")
        results_file.write("Windows unchanged: " + str(len(unchanged_results)) + "\n")
        results_file.write("Windows improved: " + str(len(better_results)) + " Average improvement: " +
                           str(round(sum(better_results) / len(better_results), 2)) + "\n")
        results_file.write("Windows diminished: " + str(len(worse_results)) + " Average diminished: " +
                           str(round(sum(worse_results) / len(worse_results), 2)) + "\n")
        results_file.write("Average difference: " + str(avg_score_diff))
        results_file.close()

    with open(output_path + "/" + output_file + "_details.csv", 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["start sequence", "end sequence", "start column", "end column", "old SoP", "new SoP",
                         "difference"])
        for section, scores in scores_dict.items():
            writer.writerow([section[0][0], section[0][1], section[1][0], section[1][1], section[2][0], section[2][1],
                             scores[0], scores[1], scores[2]])
    return


def main(output_file, old_align_df, new_align_df, align_map, num_cols, col_overlap, num_seqs, seq_overlap):
    scores_dict = calculate_all_section_sops(old_align_df, new_align_df, align_map, num_cols, col_overlap, num_seqs,
                                             seq_overlap)

    scores_dict = {k: v for k, v in sorted(scores_dict.items(), key=lambda item: item[1][2])}
    avg_score_diff = round(sum([v[2] for v in scores_dict.values()]) / len(scores_dict), 2)
    worse_results = [i[2] for i in filter(lambda x: (x[2] < 0), scores_dict.values())]
    better_results = [i[2] for i in filter(lambda x: (x[2] > 0), scores_dict.values())]
    unchanged_results = [i[2] for i in filter(lambda x: (x[2] == 0), scores_dict.values())]
    output_files(output_file, scores_dict, avg_score_diff, worse_results, better_results, unchanged_results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", type=str)
    parser.add_argument("output_file", type=str)
    parser.add_argument("-b", "--base_file", required=False, type=str,
                        help="relative path to the original base fasta file.",
                        default="../../alignments/cleaned_aligned_PASTA_9667.fasta")
    parser.add_argument("-c", "--cols", required=False, type=int,
                        help="number of columns per window",
                        default=20)
    parser.add_argument("-co", "--col_overlap", required=False, type=int,
                        help="number of overlap columns between windows",
                        default=10)
    parser.add_argument("-s", "--seqs", required=False, type=int,
                        help="number of sequences per window",
                        default=100)
    parser.add_argument("-so", "--seq_overlap", required=False, type=int,
                        help="number of overlap sequences between windows",
                        default=50)

    args = vars(parser.parse_args())
    new_align_file = args['input_file']
    old_align_file = args['base_file']
    output_file = args['output_file']
    num_cols = args['cols']
    col_overlap = args['col_overlap']
    num_seqs = args['seqs']
    seq_overlap = args['seq_overlap']

    try:
        old_align_list = list(SeqIO.parse(old_align_file, "fasta"))
        new_align_list = list(SeqIO.parse(new_align_file, "fasta"))
    except FileNotFoundError:
        raise

    old_align_list = [seq for seq in sorted(old_align_list, key=lambda item: item.id)]
    new_align_list = [seq for seq in sorted(new_align_list, key=lambda item: item.id)]
    alignment_map = map_alignments(old_align_list, new_align_list)
    old_align_columns = list(range(len(old_align_list[0].seq)))
    old_align_ids = [x.id for x in old_align_list]
    old_align_seqs = [list(x.seq) for x in old_align_list]
    old_align_df = pd.DataFrame(old_align_seqs, columns=old_align_columns, index=old_align_ids)
    new_align_columns = list(range(len(new_align_list[0].seq)))
    new_align_ids = [x.id for x in new_align_list]
    new_align_seqs = [list(x.seq) for x in new_align_list]
    new_align_df = pd.DataFrame(new_align_seqs, columns=new_align_columns, index=new_align_ids)

    main(output_file, old_align_df, new_align_df, alignment_map, num_cols, col_overlap, num_seqs, seq_overlap)
