import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import copy
import pickle
import pandas as pd
import plotly.graph_objects as go

if __name__ == '__main__':
    dfs_file = sys.argv[1]
    frame_count = int(sys.argv[2])
    file = open("dfs_results/" + dfs_file, 'rb')
    pickle_data = pickle.load(file)

    for key in list(pickle_data.keys())[:3]:
        for offset in pickle_data[key].keys():
            seq_guide_df = pickle_data[key][offset]
            if seq_guide_df.isnull().all().all():
                continue
            seq_guide = pickle_data[key][offset].values.tolist()
            columns = seq_guide_df.columns.values
            rows = seq_guide_df.index.values

            max_count = 0
            nuc_movement = []
            sol_count = []
            for row in seq_guide:
                nuc_movement.append([(x[0] / float(x[1])) if type(x) is list and float(x[1]) > 0 else 0 for x in row])
                row_count = [x[1] if type(x) is list else 0 for x in row]
                if max(row_count) > max_count:
                    max_count = max(row_count)
                sol_count.append(row_count)

            pos = 0
            frame_data = []
            while pos < max_count:
                frame_nucs = copy.deepcopy(nuc_movement)
                for i in range(len(nuc_movement)):
                    frame_nucs[i] = [frame_nucs[i][x] if sol_count[i][x] >= pos else 0 for x in range(len(frame_nucs[i]))]
                frame_data.append(frame_nucs)
                pos = pos + (max_count / frame_count)

            dfs = [pd.DataFrame(index=rows, columns=columns, data=frame_data[i]) for i in range(len(frame_data))]

            frames = [go.Frame(data=go.Heatmap(z=df.values, x=df.columns, y=df.index), name=i*(max_count/frame_count))
                      for i, df in enumerate(dfs)]

            fig = go.Figure(data=frames[0].data, frames=frames).update_layout(
                updatemenus=[
                    {
                        "buttons": [{"args": [None, {"frame": {"duration": 500, "redraw": True}}],
                                     "label": "Play", "method": "animate", },
                                    {"args": [[None], {"frame": {"duration": 0, "redraw": False},
                                                       "mode": "immediate", "transition": {"duration": 0}, }, ],
                                     "label": "Pause", "method": "animate", }, ],
                        "type": "buttons",
                    }
                ],
                # iterate over frames to generate steps... NB frame name...
                sliders=[{"steps": [{"args": [[f.name], {"frame": {"duration": 0, "redraw": True},
                                                         "mode": "immediate", }, ],
                                     "label": f.name, "method": "animate", }
                                    for f in frames], }],
                height=800,
                yaxis={"title": 'Sequences'},
                xaxis={"title": key + " " + str(offset), "tickangle": 45, 'side': 'top'},
                title_x=0.5,

            )

            date = dfs_file.split('.')[0].split('_')[-1]
            directory = os.path.dirname("figures/" + date + "/")
            if not os.path.exists(directory):
                os.mkdir(directory)
            fig_file = "figures/" + date + "/" + key + "_" + str(offset) + "_" + date + ".html"
            fig.write_html(fig_file)