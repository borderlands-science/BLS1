from Bio import SeqIO
import math

SKIP_PROFILE_PENALTY = -25
NUM_OF_SEQS = 9667


def produce_profiles(in_dict, length, start_i=0, end_i=None):
    count_dicts = [{'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 0} for _ in range(length)]
    if in_dict is not None:
        for k in in_dict:
            if end_i is None:
                end_i = length
            assert end_i == start_i + length
            sliced_seq = in_dict[k][start_i:end_i]
            for i in range(len(sliced_seq)):
                if sliced_seq[i] in count_dicts[i]:
                    count_dicts[i][sliced_seq[i]] += 1
    return count_dicts


def normalize_profiles(profiles):
    for p in profiles:
        for n in ['A', 'C', 'G', 'T', '-']:
            p[n] = (p[n] / NUM_OF_SEQS) * 100


def score_wrt_profile(cds, profile, position):
    score = 0
    if profile is None:
        return SKIP_PROFILE_PENALTY
    target_profile = cds[position]
    for n1 in ['A', 'C', 'G', 'T', '-']:
        for n2 in ['A', 'C', 'G', 'T', '-']:
            if n1 == n2:
                score += profile[n1] * target_profile[n1]
            else:
                score -= profile[n1] * target_profile[n2]
    return score


seqs_p = {}
seqs_m = {}
other_name = 'maffas_2.fasta'
for record in SeqIO.parse('pasta_structure_only.fasta', 'fasta'):
    seqs_p[record.id] = str(record.seq)
for record in SeqIO.parse(other_name, 'fasta'):
    seqs_m[record.id] = str(record.seq)

profiles_p = produce_profiles(seqs_p, len(seqs_p[list(seqs_p.keys())[0]]))
profiles_m = produce_profiles(seqs_m, len(seqs_m[list(seqs_m.keys())[0]]))
normalize_profiles(profiles_p)
normalize_profiles(profiles_m)

len_p = len(profiles_p)
len_m = len(profiles_m)
# i -> m -> seq
# j -> p -> cds
dynamic_table = [['X' for _ in range(len_p + 1)] for _ in range(len_m + 1)]
dynamic_table[0][0] = 0
for i in range(1, len_m + 1):
    dynamic_table[i][0] = dynamic_table[i - 1][0] + score_wrt_profile(profiles_m, None, i - 1)
for j in range(1, len_p + 1):
    dynamic_table[0][j] = dynamic_table[0][j - 1] + score_wrt_profile(profiles_p, None, j - 1)

chosen_paths = [[-1 for _ in range(len_p + 1)] for _ in range(len_m + 1)]
for i in range(1, len_m + 1):
    chosen_paths[i][0] = 2
for j in range(1, len_p + 1):
    chosen_paths[0][j] = 0

for i in range(1, len_m + 1):
    for j in range(1, len_p + 1):
        # moving horizontal = inserting semi-gap in mafft profiles
        horizontal = dynamic_table[i][j - 1] + score_wrt_profile(profiles_p, None, j - 1)
        # horizontal = -math.inf
        # moving diagonal = matching profiles in respective positions
        diagonal = dynamic_table[i - 1][j - 1] + score_wrt_profile(profiles_p, profiles_m[i - 1], j - 1)
        # moving vertical = inserting semi-gap in pasta profiles
        vertical = dynamic_table[i - 1][j] + score_wrt_profile(profiles_m, None, i - 1)
        dynamic_table[i][j] = max(horizontal, diagonal, vertical)
        if dynamic_table[i][j] == horizontal:
            chosen_paths[i][j] = 0
        if dynamic_table[i][j] == diagonal:
            chosen_paths[i][j] = 1
        if dynamic_table[i][j] == vertical:
            chosen_paths[i][j] = 2

reconstructed_m = []
reconstructed_p = []
x = len_m
y = len_p
direction = chosen_paths[x][y]  # starting from last (bottom right) cell, moving towards 'direction'
while x > 0 or y > 0:

    if direction == 2:
        x -= 1
        reconstructed_m.append(str(x) + '\t')
        reconstructed_p.append('#\t')
    elif direction == 1:
        x -= 1
        y -= 1
        reconstructed_m.append(str(x) + '\t')
        reconstructed_p.append(str(y) + '\t')
    elif direction == 0:
        y -= 1
        reconstructed_m.append('#\t')
        reconstructed_p.append(str(y) + '\t')
    else:
        print("wtf")
        exit(-1)
    direction = chosen_paths[x][y]

reconstructed_m.reverse()
reconstructed_p.reverse()
print(''.join(reconstructed_m), ''.join(reconstructed_p), sep='\n')
with open('column_mapping_' + other_name + '_.txt', 'w') as out:
    out.write('pasta_structure_only.fasta\n')
    out.write(''.join(reconstructed_p))
    out.write('\n')
    out.write(other_name + '\n')
    out.write(''.join(reconstructed_m))
    out.write('\n')
