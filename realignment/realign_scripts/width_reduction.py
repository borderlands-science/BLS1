import os,sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

from Bio import SeqIO
from realignment.realign_scripts.align import produce_count_dicts


def width_reduction(filename):
    """
    Reduces the width of sequence alignments by removing columns that are entirely gaps. This is done by
    iterating over the alignment and checking for columns where every sequence has a gap. These columns are then
    removed, effectively reducing the width of the alignment.

    Parameters:
    filename : str
        The filename of the fasta file containing the sequence alignments to be processed.

    Returns:
    None
        The function writes the reduced-width alignment to a new file with '_WR.fasta' appended to the original filename.
        It doesn't return any value.
    """
    seqs = {}
    for record in SeqIO.parse('results/' + filename + '.fasta', 'fasta'):
        seqs[record.id] = str(record.seq)

    count_dicts = produce_count_dicts(seqs, len(seqs[list(seqs.keys())[0]]))
    seq_cnt = len(seqs)
    cnt = 0
    for i in range(len(count_dicts)):
        if count_dicts[i]['-'] == seq_cnt:
            for seq_num in seqs:
                s = seqs[seq_num]
                seqs[seq_num] = s[:i - cnt] + s[i - cnt + 1:]
            cnt += 1

    with open('results/' + filename + '_WR.fasta', 'w') as out:
        for seq_num in seqs:
            out.write('>' + seq_num + '\n')
            out.write(seqs[seq_num])
            out.write('\n')

    print('Width reduction done.')
