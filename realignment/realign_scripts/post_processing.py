import os,sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

from Bio import SeqIO
from realignment.realign_scripts.align import produce_count_dicts, get_best_seq_dp


def post_processing(filename, pp_n):
    """
    Post-processes a Multiple Sequence Alignment.

    This function reads a fasta file containing sequence alignments, removes gaps from each sequence and reinserts them to improve
    the alignment, and then writes the updated sequences back to a new fasta file. This is part of a post-processing
    step that follows the initial alignment computation.

    Parameters:
    filename : str
        Name of the fasta file containing the initial sequence alignments.
    pp_n : int
        The number of post-processing iterations to perform on the sequence alignments.

    Returns:
    None
    """
    seqs = {}
    for i in range(pp_n):
        print('pp no.', i + 1, sep='')

        for record in SeqIO.parse('results/' + filename + str(i) + '.fasta', 'fasta'):
            seqs[record.id] = str(record.seq)

        count_dicts = produce_count_dicts(seqs, len(seqs[list(seqs.keys())[0]]))

        for seq_num in seqs:
            seqs[seq_num] = get_best_seq_dp(seqs[seq_num].replace('-', ''), count_dicts)

        with open('results/' + filename + str(i + 1) + '.fasta', 'w') as out:
            for seq_num in seqs:
                out.write('>' + seq_num + '\n')
                out.write(seqs[seq_num])
                out.write('\n')
