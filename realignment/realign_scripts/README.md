

### How to run:
    python solution_realignment.py -i [Input File] -o [Output File Name] [Additional Arguments]
Input file is a pickle file created from `analyze_results_forRealign.py`\
Results are placed in the directory specified in `[Output File Name]`

### Additional Argument Help:
- -a [string][consensus|simple|sample] Alignment type, used to determine whether we use our standard alignment
methods, simple alignment, or sampling
- -s [string] If using sampling, determines the number of samples to use. Default 100
- -p [string] If using simple alignment, determines which path has priority during Needleman-Wunsch backtracking if
multiple paths are valid. Default 'vdh' indicates 'vertical|diagonal|horizontal'
- -ssw [float] Float value between 0 and 1 to indicate the weight that Rfam sequences impact the realignment
- -off [string] Indicates which puzzle offsets and directions to consider. Offsets 0,1,2 are for left to right
puzzles, while offsets 100,101,102 indicate offset puzzles going from right to left. Default is '0-100'
- -w [string] Weight of each offset towards the realignment. Default is '1-1'
- -ppn [int] Number of post processing iterations that will be performed. Default is 4
- -flex [int] When choosing valid representatives for a consensus alignment, flexibility indicates how close a
realignment can be to the best option to also be considered a valid representative
- -d [string] Specify the distance metric used to normalize length of sequences. Hamming or Levenshtein.
Default is Levenshtein
- -baf [string] Base alignment file that was used to produce puzzles in this dataset.
Uses cleaned_aligned_PASTA_9667.fasta by default
