import copy
import time
import numpy as np
import itertools
import Levenshtein
from scipy.spatial.distance import hamming

#This script builds a progressive alignment from an input consisting of a set of sequences.
#It starts from comparing two sequences and progressively updates the alignment as more sequences are added through the function update_list
#It keep tracks of distances between sequences in order to be able to assess the quality of the alignment in downstream analysis.

def initial_dist_list(sequences, distance_metric):
    """
    Creates an initial list of distances between sequences.

    Parameters:
    sequences : list
        A list of sequence dictionaries with sequence identifiers as keys and sequences as values.
    distance_metric : str
        The metric used to calculate distance between sequences ('hamming' or 'levenshtein').

    Returns:
    numpy.ndarray
        A matrix of distances between each pair of sequences.
    """
    distance_list = np.full((len(sequences), len(sequences)), np.inf)
    for i in range(len(sequences)):
        first_seq = list(sequences[i].values())[0]
        for j in range(len(sequences)):
            if i != j:
                sec_seq = list(sequences[j].values())[0]
                if distance_metric == 'hamming':
                    distance = calc_hamming_distance(first_seq, sec_seq)
                else:
                    distance = Levenshtein.distance(first_seq, sec_seq)
                distance_list[i][j] = distance
    return distance_list


def update_distance_list(distance_list, seq1_pos, seq2_pos, n_rows, length_adjusted, profile_col_count_list,
                         distance_metric):
    """
    Updates the distance matrix after merging two sequences into a new profile.

    Parameters:
    distance_list : numpy.ndarray
        The current distance matrix.
    seq1_pos : int
        The position index of the first sequence in the alignment.
    seq2_pos : int
        The position index of the second sequence in the alignment.
    n_rows : int
        The current number of rows in the distance matrix.
    length_adjusted : bool
        Indicates if the length of the profile was adjusted.
    profile_col_count_list : list
        A list of profile column counts.
    distance_metric : str
        The metric used to calculate distance between sequences.

    Returns:
    numpy.ndarray
        The updated distance matrix.
    """
    if seq1_pos > seq2_pos:
        seq1_pos, seq2_pos = seq2_pos, seq1_pos
    distance_list = np.delete(distance_list, seq1_pos, 0)
    distance_list = np.delete(distance_list, seq2_pos - 1, 0)

    old_distances_1 = copy.deepcopy(distance_list[:, seq1_pos])
    old_distances_2 = copy.deepcopy(distance_list[:, seq2_pos])

    distance_list = np.delete(distance_list, seq1_pos, 1)
    distance_list = np.delete(distance_list, seq2_pos - 1, 1)
    new_distance_list = np.full((n_rows - 1, n_rows - 1), np.inf)
    new_distance_list[:n_rows - 2, :n_rows - 2] = distance_list
    distance_list = new_distance_list
    if length_adjusted:
        add_new_profile_distance(distance_list, profile_col_count_list, distance_metric)
    else:
        new_distances = (old_distances_1 + old_distances_2) / 2
        add_average_profile_distance(distance_list, new_distances)
    return distance_list


def update_list(updated_list, seq1_pos, seq2_pos, new_profile):
    """
    Updates the list of profiles after merging two sequences into a new profile.

    Parameters:
    updated_list : list
        The current list of sequence profiles.
    seq1_pos : int
        The position index of the first sequence in the list.
    seq2_pos : int
        The position index of the second sequence in the list.
    new_profile : dict
        The new profile created from merging two sequences.

    Returns:
    list
        The updated list of sequence profiles.
    """
    if seq1_pos > seq2_pos:
        seq1_pos, seq2_pos = seq2_pos, seq1_pos
    updated_list.pop(seq1_pos)
    updated_list.pop(seq2_pos - 1)
    updated_list.append(new_profile)
    return updated_list


def calc_hamming_distance(seq_1, seq_2):
    """
    Calculates the Hamming distance between two sequences.

    Parameters:
    seq_1 : str
        The first sequence.
    seq_2 : str
        The second sequence.

    Returns:
    float
        The Hamming distance between the two sequences.
    """
    max_length = max(len(seq_1), len(seq_2))
    seq_1_adj = adjust_seq_length(seq_1, max_length)
    seq_2_adj = adjust_seq_length(seq_2, max_length)
    return hamming(list(seq_1_adj), list(seq_2_adj)) * max_length


def add_average_profile_distance(distance_list, new_distances):
    """
    Adds the average profile distance to the distance matrix.

    Parameters:
    distance_list : numpy.ndarray
        The current distance matrix.
    new_distances : list
        A list of new distances calculated for the merged profile.

    Returns:
    None
    """
    for i in range(len(new_distances)):
        distance_list[i][-1] = new_distances[i]
        distance_list[-1][i] = new_distances[i]
    return


def add_new_profile_distance(distance_list, profile_col_count_list, distance_metric):
    """
    Adds a new profile distance to the distance matrix.

    Parameters:
    distance_list : numpy.ndarray
        The current distance matrix.
    profile_col_count_list : list
        A list of profile column counts.
    distance_metric : str
        The metric used to calculate distance between profiles.

    Returns:
    None
    """
    new_profile = profile_col_count_list[-1]
    for i in range(len(profile_col_count_list) - 1):
        profile = profile_col_count_list[i]
        if distance_metric == 'hamming':
            distance = calc_profile_hamming_distance(profile, new_profile)
        else:
            distance = calc_profile_levenshtein_distance(profile, new_profile)
        distance_list[i][-1] = distance
        distance_list[-1][i] = distance
    return


def calc_profile_hamming_distance(profile_col_count_1, profile_col_count_2):
    """
    Calculates the Hamming distance between two profile column counts.

    Parameters:
    profile_col_count_1 : list
        The first profile's column counts.
    profile_col_count_2 : list
        The second profile's column counts.

    Returns:
    float
        The Hamming distance between the two profile column counts.
    """
    distance = 0
    max_length = max(len(profile_col_count_1), len(profile_col_count_2))
    adj_profile_1 = adjust_profile_col_count_length(profile_col_count_1, max_length)
    adj_profile_2 = adjust_profile_col_count_length(profile_col_count_2, max_length)
    for i in range(len(adj_profile_1)):
        distance += (1 - sum(adj_profile_1[i] * adj_profile_2[i]))
    return distance


def calc_profile_levenshtein_distance(profile_1_col_count, profile_2_col_count):
    """
    Calculates the Levenshtein distance between two profile column counts.

    Parameters:
    profile_1_col_count : list
        The first profile's column counts.
    profile_2_col_count : list
        The second profile's column counts.

    Returns:
    float
        The Levenshtein distance between the two profile column counts.
    """
    if len(profile_1_col_count) < len(profile_2_col_count):
        profile_1_col_count, profile_2_col_count = profile_2_col_count, profile_1_col_count

    matrix = np.full((len(profile_1_col_count) + 1, len(profile_2_col_count) + 1), np.inf)
    matrix[0, 0] = 0

    for pos in range(1, len(profile_1_col_count) - (len(profile_2_col_count) - 1)):
        matrix[pos][0] = matrix[pos - 1][0] + 1

    for i in range(1, matrix.shape[0]):
        for j in range(max(1, i - (len(profile_1_col_count) - len(profile_2_col_count))), min(i + 1, matrix.shape[1])):
            match_score = matrix[i - 1, j - 1] + (1 - sum(profile_1_col_count[i-1] * profile_2_col_count[j-1]))
            gap_score = matrix[i - 1, j] + 1
            matrix[i, j] = min(match_score, gap_score)
    return matrix[-1, -1]


def adjust_seq_length(seq, max_length):
    """
    Adjusts the length of a sequence by padding it with gaps.

    Parameters:
    seq : str
        The sequence to be adjusted.
    max_length : int
        The desired length of the sequence.

    Returns:
    str
        The adjusted sequence.
    """
    seq = seq.ljust(max_length, '~')
    return seq


def adjust_profile_col_count_length(profile, max_length):
    """
    Adjusts the length of a profile column count list by padding it with zeros.

    Parameters:
    profile : list
        The profile column count list to be adjusted.
    max_length : int
        The desired length of the profile column count list.

    Returns:
    list
        The adjusted profile column count list.
    """
    if len(profile) >= max_length:
        return profile
    else:
        extra = [np.zeros(5)] * (max_length - len(profile))
        adj_profile = profile + extra
    return adj_profile


def count_col_nucleotides(profile):
    """
    Counts the nucleotides in each column of a profile.

    Parameters:
    profile : list
        The profile to count nucleotides in.

    Returns:
    list
        A list of dictionaries with nucleotide counts for each column in the profile.
    """
    nucs = ['-', 'A', 'C', 'G', 'T']
    col_count = []
    totals = [list(map(list(filter(None, i)).count, nucs))
              for i in itertools.zip_longest(*profile)]
    for column in totals:
        normalized_totals = [float(i) / sum(column) for i in column]
        col_count.append(np.array(normalized_totals))
    return col_count


def map_sequence_columns(sequence):
    """
    Maps each nucleotide in a sequence to its corresponding column count.

    Parameters:
    sequence : str
        The sequence to map.

    Returns:
    list
        A list of dictionaries representing the nucleotide counts for each column in the sequence.
    """
    nuc_dict = {'-': 0, 'A': 1, 'C': 2, 'G': 3, 'T': 4}
    col_count = [np.zeros(5) for i in range(len(sequence))]
    for i, nuc in enumerate(sequence):
        col_count[i][nuc_dict[nuc]] = 1
    return col_count


def get_min_dist_position(distance_list):
    """
    Finds the position of the minimum distance in a distance matrix.

    Parameters:
    distance_list : numpy.ndarray
        The distance matrix.

    Returns:
    tuple
        The row and column indices of the minimum distance in the distance matrix.
    """
    pos = np.where(distance_list == distance_list.min())
    return pos[0][0], pos[1][0]


def profile_alignment(profile_1, profile_2):
    """
    Aligns two profiles by merging them into a new profile.

    Parameters:
    profile_1 : dict
        The first profile to align.
    profile_2 : dict
        The second profile to align.

    Returns:
    tuple
        A new profile resulting from the alignment and a boolean indicating if the profile length was adjusted.
    """
    profile_1_list = list(profile_1.values())
    profile_2_list = list(profile_2.values())
    if len(profile_1_list[0]) == len(profile_2_list[0]):
        new_profile = profile_1
        new_profile.update(profile_2)
        return new_profile, False
    if len(profile_1_list[0]) < len(profile_2_list[0]):
        profile_1, profile_2 = profile_2, profile_1
        profile_1_list = list(profile_1.values())
        profile_2_list = list(profile_2.values())

    trace_matrix = calc_alignment_matrix(profile_1_list, profile_2_list)

    profile_1_cols = [list(filter(None, i)) for i in itertools.zip_longest(*profile_1_list)]
    profile_2_cols = [list(filter(None, i)) for i in itertools.zip_longest(*profile_2_list)]
    new_profile_list = traceback(trace_matrix, profile_1_cols, profile_2_cols)
    new_profile_keys = list(profile_1.keys()) + list(profile_2.keys())
    new_profile = dict(zip(new_profile_keys, new_profile_list))
    return new_profile, True


def calc_alignment_matrix(profile_1_list, profile_2_list):
    """
    Calculates the alignment matrix for two profiles.

    Parameters:
    profile_1_list : list
        The first profile's list of sequences.
    profile_2_list : list
        The second profile's list of sequences.

    Returns:
    numpy.ndarray
        The alignment matrix.
    """
    nucs = ['-', 'A', 'C', 'G', 'T']
    profile_1_count = len(profile_1_list)
    profile_2_count = len(profile_2_list)
    profile_1_col_count = [list(map(list(filter(None, i)).count, nucs)) for i in itertools.zip_longest(*profile_1_list)]
    profile_2_col_count = [list(map(list(filter(None, i)).count, nucs)) for i in itertools.zip_longest(*profile_2_list)]
    matrix = np.zeros((len(profile_1_col_count) + 1, len(profile_2_col_count) + 1), np.float)
    trace_matrix = matrix.copy()

    for pos in range(1, len(profile_1_col_count) - (len(profile_2_col_count) - 1)):
        matrix[pos][0] = matrix[pos - 1][0] + calc_sum_of_pairs(profile_1_col_count[0], [profile_2_count, 0, 0, 0, 0],
                                                                profile_1_count, profile_2_count)
        trace_matrix[pos][0] = 1
    for pos in range(1, len(profile_2_col_count)+1):
        matrix[pos-1][pos] = -np.inf

    for i in range(1, matrix.shape[0]):
        for j in range(max(1, i - (len(profile_1_col_count) - len(profile_2_col_count))), min(i+1, matrix.shape[1])):
            match_score = matrix[i - 1, j - 1] + calc_sum_of_pairs(profile_1_col_count[i - 1],
                                                                   profile_2_col_count[j - 1], profile_1_count,
                                                                   profile_2_count)
            gap_score = matrix[i - 1, j] + calc_sum_of_pairs(profile_1_col_count[i-1], [profile_2_count, 0, 0, 0, 0],
                                                             profile_1_count, profile_2_count)
            matrix[i, j] = max(match_score, gap_score)
            if gap_score >= match_score:
                trace_matrix[i, j] = 1
    return trace_matrix


def calc_sum_of_pairs(profile_1_nucs, profile_2_nucs, profile_1_count, profile_2_count):
    """
    Calculates the sum of pairs score for aligning two profiles.

    Parameters:
    profile_1_nucs : list
        The nucleotide counts for the first profile.
    profile_2_nucs : list
        The nucleotide counts for the second profile.
    profile_1_count : int
        The number of sequences in the first profile.
    profile_2_count : int
        The number of sequences in the second profile.

    Returns:
    float
        The sum of pairs score.
    """
    match_sop = get_matching_combos(profile_1_nucs, profile_2_nucs)
    mismatch_sop = get_mismatching_combos(profile_1_nucs, profile_2_nucs, profile_2_count)
    match_gap_sop = get_match_gap_combos(profile_1_nucs, profile_2_nucs, profile_1_count, profile_2_count)
    sop = match_sop + mismatch_sop + match_gap_sop
    return sop


def get_matching_combos(profile_1_nucs, profile_2_nucs):
    """
    Calculates the matching combinations score for two profiles.

    Parameters:
    profile_1_nucs : list
        The nucleotide counts for the first profile.
    profile_2_nucs : list
        The nucleotide counts for the second profile.

    Returns:
    float
        The matching combinations score.
    """
    total = 0
    for i in range(1, 5):
        total += profile_1_nucs[i] * profile_2_nucs[i]
    return total


def get_mismatching_combos(profile_1_nucs, profile_2_nucs, profile_2_count):
    """
    Calculates the mismatching combinations score for two profiles.

    Parameters:
    profile_1_nucs : list
        The nucleotide counts for the first profile.
    profile_2_nucs : list
        The nucleotide counts for the second profile.
    profile_2_count : int
        The number of sequences in the second profile.

    Returns:
    float
        The mismatching combinations score.
    """
    total = 0
    for i in range(1, 5):
        total += profile_1_nucs[i] * (profile_2_count - profile_2_nucs[0] - profile_2_nucs[i])
    return -total


def get_match_gap_combos(profile_1_nucs, profile_2_nucs, profile_1_count, profile_2_count):
    """
    Calculates the match-gap combinations score for two profiles, considering the matches between nucleotides and gaps.

    Parameters:
    profile_1_nucs : list
        The nucleotide counts for the first profile.
    profile_2_nucs : list
        The nucleotide counts for the second profile.
    profile_1_count : int
        The number of sequences in the first profile.
    profile_2_count : int
        The number of sequences in the second profile.

    Returns:
    float
        The match-gap combinations score.
    """
    total = profile_1_nucs[0] * (profile_2_count - profile_2_nucs[0])
    total += profile_2_nucs[0] * (profile_1_count - profile_1_nucs[0])
    return -(total * 2)


def traceback(matrix, profile_1, profile_2):
    """
    Performs traceback on the alignment matrix to construct the aligned profiles.

    Parameters:
    matrix : numpy.ndarray
        The alignment matrix.
    profile_1 : list
        The first list of profile columns.
    profile_2 : list
        The second list of profile columns.

    Returns:
    list
        The list of aligned profile columns.
    """
    new_profile_1 = []
    new_profile_2 = []
    profile_2_col_count = len(profile_2[0])
    i, j = matrix.shape[0] - 1, matrix.shape[1] - 1
    while i != 0 or j != 0:
        if matrix[i][j] == 0:
            new_profile_1.append(profile_1[-1])
            new_profile_2.append(profile_2[-1])
            profile_1 = profile_1[:-1]
            profile_2 = profile_2[:-1]
            i -= 1
            j -= 1
        else:
            new_profile_1.append(profile_1[-1])
            new_profile_2.append(['-'] * profile_2_col_count)
            profile_1 = profile_1[:-1]
            i -= 1
    new_profile = [''.join(list(filter(None, i))[::-1]) for i in itertools.zip_longest(*new_profile_1)]
    new_profile += [''.join(list(filter(None, i))[::-1]) for i in itertools.zip_longest(*new_profile_2)]
    return new_profile


def build_progressive_alignment(sequences, output_name, distance_metric):
    """
    Constructs a progressive alignment of a set of sequences.

    Parameters:
    sequences : dict
        A dictionary of sequences to be aligned.
    output_name : str
        The name of the output file.
    distance_metric : str
        The metric used to calculate distance between sequences.

    Returns:
    None
    """
    start_time = time.time()
    profile_list = [{k: v} for k, v in sequences.items()]
    profile_col_count_list = [map_sequence_columns(v) for v in sequences.values()]
    distance_list = initial_dist_list(profile_list, distance_metric)
    initial_time = time.time()
    print("INITIAL DISTANCE TIME", str(round(initial_time - start_time, 1)))

    n_rows = distance_list.shape[0]
    while n_rows != 1:
        min_row, min_col = get_min_dist_position(distance_list)
        profile_1 = profile_list[min_row]
        profile_2 = profile_list[min_col]
        new_profile, length_adjusted = profile_alignment(profile_1, profile_2)

        profile_list = update_list(profile_list, min_row, min_col, new_profile)
        profile_col_count_list = update_list(profile_col_count_list, min_row, min_col,
                                             count_col_nucleotides(list(new_profile.values())))
        distance_list = update_distance_list(distance_list, min_row, min_col, n_rows, length_adjusted,
                                             profile_col_count_list, distance_metric)
        n_rows -= 1

    print("NORMALIZATION RUN TIME", str(round(time.time() - initial_time, 1)))
    with open('results/' + output_name + '_PP0.fasta', 'w') as out:
        for seq_key, seq in profile_list[0].items():
            out.write('>' + seq_key + '\n')
            out.write(seq)
            out.write('\n')

