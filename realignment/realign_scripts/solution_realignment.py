import os,sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import time
import argparse
from realignment.realign_scripts.align import align
from realignment.realign_scripts.post_processing import post_processing
from realignment.realign_scripts.width_reduction import width_reduction

#This is the script users will interact with when computing sequence alignments of Borderlands Science puzzle solutions.
#It has extensive command line arguments for the user to interact with and then calls align.py with the relevant parameters.
#It takes as input the output of the solutions processing pipeline, analyze_results_forRealign.py




start_time = time.time()
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
                help="relative path to the input file.")
ap.add_argument("-o", "--output", required=True,
                help="output file name")
ap.add_argument("-a", "--alignment_type", required=False,
                help="String to determine whether to use simple realignment, or sampling",
                default="consensus")
ap.add_argument("-s", "--sampling", required=False,
                help="integer value, number of samples used in reconstruction. suggested value is between 100-500. "
                     "higher values will extend running time",
                default="100")
ap.add_argument("-p", "--path_order", required=False,
                help="String to determine which directions take priority in the case of equal score in pairwise "
                     "alignment",
                default="vdh")
ap.add_argument("-ssw", "--secondarystructure-weight", required=False,
                help="If indicated, must be a float value between 0-1. RFAM sequences' profile will be used to "
                     "provide more information with the weight specified.",
                default=None)
ap.add_argument("-off", "--offset", required=False,
                help="offsets to be considered e.g. 0, 0-1-101, all (=0, 1, 2, 100, 101, 102 in order)",
                default="0-100")
ap.add_argument("-w", "--weights", required=False,
                help="weight of each offset. should be provided in the same order as offsets provided",
                default='1-1')
ap.add_argument("-ppn", "--ppnum", required=False,
                help="number of post processing_scripts iterations to perform",
                default=4)
ap.add_argument("-flex", "--representative_flexible", required=False,
                help="Allows realignment to be more flexible on minimum levenshtein distance to accept representatives",
                default=0)
ap.add_argument("-d", "--dist_metric", required=False,
                help="Specify distance metric for normalizing distance. hamming or levenshtein",
                default='levenshtein')
ap.add_argument("-baf", "--base_alignment_file", required=False,
                help="Base alignment file used to produce puzzles in this dataset",
                default='../../alignments/cleaned_aligned_PASTA_9667.fasta')

args = vars(ap.parse_args())

if args['offset'] == 'all':
    offsets = [0, 1, 2, 100, 101, 102]
else:
    offsets = [int(_) for _ in args['offset'].split('-')]
    for o in offsets:
        if o not in [0, 1, 2, 100, 101, 102]:
            print('invalid offset provided')
            exit(2)
weights = [float(_) for _ in args['weights'].split('-')]

if args['secondarystructure_weight'] is None:
    ssw = None
else:
    ssw = float(args['secondarystructure_weight'])

align(args['input'], args['output'], offsets, weights, args['alignment_type'], int(args['sampling']),
      args['path_order'], ssw, args['dist_metric'], args['base_alignment_file'], int(args['representative_flexible']))
post_processing(args['output'] + '_PP', int(args['ppnum']))
width_reduction(args['output'] + '_PP' + str(args['ppnum']))

print('\nRealignment done. Running time =', time.time() - start_time)
