import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

from Levenshtein import distance
from Bio import SeqIO
import numpy as np
import pickle
import copy
import math
import random
import itertools
from realignment.realign_scripts import progressive_alignment as pa

#This is the main script for aligning puzzle solutions into a global alignment.
#It takes as input a pickle file containing puzzle solutions and parameters for the alignment, namely the weight of solutions including a guide offset
#It then assembles the puzzle solutions associated to each sequence ID into a sequence, fills in the eventual gaps with data from the initial dataset
#and then computes a progressive alignment of the sequences minimizing a distance metric such as Levenshtein distance


def count_columns(seqs):
    """
    Count the occurrences of each nucleotide in each column of a set of sequences.

    Parameters:
    seqs : list of str
        A list of sequences.

    Returns:
    list of dict
        A list of dictionaries, each representing the count of nucleotides in a column.
    """
    longest_seq = max(len(seq) for seq in seqs)
    seqs_list = list(seqs)
    for i, seq in enumerate(seqs_list):
        seqs_list[i] = seq.ljust(longest_seq, '-')

    counted_cols = []
    nucs = ['A', 'C', 'G', 'T', '-']
    columns = [list(filter(None, i)) for i in itertools.zip_longest(*seqs_list)]
    for column in columns:
        column_list = list(map(column.count, nucs))
        column_dict = {nucs[i]: column_list[i] for i in range(len(nucs))}
        counted_cols.append(column_dict)
    return counted_cols


def score_count_dicts(count_dicts):
    """
    Calculate a score based on the nucleotide counts in each column.

    Parameters:
    count_dicts : list of dict
        A list of dictionaries containing nucleotide counts for each column.

    Returns:
    int
        The calculated score.
    """
    score = 0
    for col_dict in count_dicts:
        total = sum(col_dict.values())
        for combo in itertools.combinations(col_dict.keys(), 2):
            if combo == ('A', 'G') or combo == ('C', 'T'):
                score -= (col_dict[combo[0]] * col_dict[combo[1]]) / 2
            else:
                score -= col_dict[combo[0]] * col_dict[combo[1]]
        for nuc in col_dict:
            match_score = col_dict[nuc] * (col_dict[nuc] - 1) / 2
            if nuc == '-':
                if col_dict[nuc] == total:
                    match_score = 0
                else:
                    match_score = -match_score
            score += match_score
    return score


def get_column_consensus(aln):
    """
    Calculate the consensus for each column in a sequence alignment.

    Parameters:
    aln : list of str
        A list of aligned sequences.

    Returns:
    list of dict
        A list of dictionaries, each representing the consensus of a column in the alignment.
    """
    if aln is None or len(aln) == 0:
        raise ValueError("alignment is empty. Invalid")

    nucs = ['A', 'C', 'G', 'T', '-']
    consensuses = []

    for i in range(0, len(aln[0])):
        col = [nuc[i].upper() for nuc in aln]
        col[:] = [x if x != 'U' else 'T' for x in col]
        totals = list(map(col.count, nucs))
        totals = [float(i) / sum(totals) * 100 for i in totals]
        consensuses.append(dict(zip(nucs, totals)))
    return consensuses


def produce_count_dicts(in_dict, length, start_i=0, end_i=None):
    """
    Produce count dictionaries for a sequence over a specified range.

    Parameters:
    in_dict : dict
        Dictionary containing sequences.
    length : int
        Length of the sequence.
    start_i : int, optional
        Starting index for counting (default is 0).
    end_i : int, optional
        Ending index for counting (default is None).

    Returns:
    list of dict
        A list of dictionaries with counts of nucleotides.
    """
    count_dicts = [{'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 0} for _ in range(length)]
    if in_dict is not None:
        for k in in_dict:
            if end_i is None:
                end_i = length
            assert end_i == start_i + length
            sliced_seq = in_dict[k][start_i:end_i]
            for i in range(len(sliced_seq)):
                if sliced_seq[i] in count_dicts[i]:
                    count_dicts[i][sliced_seq[i]] += 1
    return count_dicts


def add_to_count_dicts(count_dicts, seq):
    """
    Add nucleotide counts of a sequence to existing count dictionaries.

    Parameters:
    count_dicts : list of dict
        Existing count dictionaries.
    seq : str
        The sequence to be added to the count.

    Returns:
    None
    """
    assert len(count_dicts) == len(seq)
    for i in range(len(seq)):
        count_dicts[i][seq[i]] += 1


def score_wrt_count_dicts(count_dicts, nuc, ind):
    """
    Score a nucleotide in a specific position relative to count dictionaries.

    Parameters:
    count_dicts : list
        List of count dictionaries.
    nuc : str
        Nucleotide to score.
    ind : int
        Index of the column in the alignment.

    Returns:
    int
        The score of the nucleotide at the specified index.
    """
    score = count_dicts[ind][nuc]
    for n_ in count_dicts[ind]:
        if n_ != nuc:
            score -= count_dicts[ind][n_]
    return score


def count_alignments(path, index_seq, index_cds, memo):
    """
    Count the number of alignments possible for a given path matrix.

    Parameters:
    path : list
        2D list representing possible paths in the alignment matrix.
    index_seq : int
        Current index in the sequence.
    index_cds : int
        Current index in the profiles.
    memo : list
        2D list for memoization.

    Returns:
    int
        The total number of alignments possible.
    """
    if index_seq == 0 and index_cds == 0:
        return 1
    if memo[index_seq][index_cds] is not None:
        return sum(memo[index_seq][index_cds].values())
    result = {}
    for direction in path[index_seq][index_cds]:
        # direction == 0 -> index_cds -= 1
        # direction == 1 -> index_seq -= 1, index_cds -= 1
        # direction == 2 -> index_seq -= 1
        y = 1 if direction > 0 else 0
        x = 1 if direction < 2 else 0
        result[direction] = count_alignments(path, index_seq - y, index_cds - x, memo)
    memo[index_seq][index_cds] = result
    return sum(result.values())


def assign_probabilities_to_paths(path, len_seq, len_cds):
    """
    Assign probabilities to each path in an alignment matrix.

    Parameters:
    path : list
        2D list representing possible paths in the alignment matrix.
    len_seq : int
        Length of the sequence.
    len_cds : int
        Length of the profiles.

    Returns:
    list
        2D list with probabilities assigned to each path.
    """
    paths_with_probabilities = [[None for _ in range(len_cds + 1)] for __ in range(len_seq + 1)]
    count_alignments(path, len_seq, len_cds, paths_with_probabilities)
    for row in paths_with_probabilities:
        for cell in row:
            if cell is None:
                continue
            s = sum(cell.values())
            for direction in cell:
                cell[direction] /= s
    return paths_with_probabilities


def get_best_seq_sop_combination(new_aln, seq_samples):
    """
    Determine the best sequence alignment by scoring each possible combination.

    Parameters:
    new_aln : dict
        Dictionary containing current alignments.
    seq_samples : dict
        Dictionary containing possible sequence samples.

    Returns:
    dict
        Updated alignments with the best sequence combination.
    """
    col_count_dicts = count_columns(list(new_aln.values()))
    best_sop = score_count_dicts(col_count_dicts)

    updated = True
    i = 0
    while updated and i < 5:
        updated = False
        keys = list(new_aln.keys())
        random.shuffle(keys)
        for key in keys:
            old_seq = new_aln[key]
            if len(seq_samples[key]) > 1:
                for seq in seq_samples[key]:
                    new_aln[key] = seq
                    col_count_dicts = count_columns(list(new_aln.values()))
                    sop = score_count_dicts(col_count_dicts)
                    if sop > best_sop:
                        best_sop = sop
                        old_seq = seq
                        updated = True
                    else:
                        new_aln[key] = old_seq
        i += 1
    return new_aln


def sample_alignment(paths_with_probability, len_seq, len_cds, count_dicts, input_seq, number_of_samples):
    """
    Sample alignments based on probabilities in the path matrix.

    Parameters:
    paths_with_probability : list
        2D list with probabilities assigned to each path.
    len_seq : int
        Length of the sequence.
    len_cds : int
        Length of the profiles.
    count_dicts : list
        List of count dictionaries.
    input_seq : str
        Input sequence for alignment.
    number_of_samples : int
        Number of samples to generate.

    Returns:
    set
        Set of sampled alignments.
    """
    samples_set = set()
    for _ in range(number_of_samples):
        """
        Reconstructed sequence alignment contains 2 possible types of characters: ~ corresponding to a gap, and a nuc
        Reconstructed profiles alignment contains 3 possible types of characters: #, x, and +
                                                   # = a gap insertion in the profiles
                                                   x = using an empty profile (not having puzzle information)
                                                   + = using a profile that has information from puzzles
        an alignment would look like the following:              xx++x####x+++x++xx+##
                                                                 aca-~cgccc--a~~-aacaa
        """
        alignment_pattern_seq = []
        alignment_pattern_cds = []
        y = len_seq
        x = len_cds
        while y > 0 or x > 0:
            direction = np.random.choice(list(paths_with_probability[y][x].keys()),
                                         p=list(paths_with_probability[y][x].values()))
            if direction == 2:
                y -= 1
                alignment_pattern_seq.append(input_seq[y])
                alignment_pattern_cds.append('#')
            elif direction == 1:
                y -= 1
                x -= 1
                alignment_pattern_seq.append(input_seq[y])
                if sum([count_dicts[x][_] for _ in count_dicts[x]]) == 0:
                    alignment_pattern_cds.append('x')
                else:
                    alignment_pattern_cds.append('+')
            elif direction == 0:
                x -= 1
                alignment_pattern_seq.append('~')
                if sum([count_dicts[x][_] for _ in count_dicts[x]]) == 0:
                    alignment_pattern_cds.append('x')
                else:
                    alignment_pattern_cds.append('+')
            else:
                print("wtf")
                exit(-1)
        alignment_pattern_seq.reverse()
        alignment_pattern_cds.reverse()
        alignment_seq = ''.join(alignment_pattern_seq)
        alignment_cds = ''.join(alignment_pattern_cds)
        final_alignment_seq = ''
        for i in range(len(alignment_seq)):
            if alignment_seq[i] == '~' and alignment_cds[i] == 'x':
                continue
            if alignment_seq[i] == '~':
                final_alignment_seq += '-'
            else:
                final_alignment_seq += alignment_seq[i]
        samples_set.add(final_alignment_seq)
    return samples_set


def consensus_alignment(possible_paths, len_seq, len_cds, count_dicts, input_seq,
                        alignment_pattern_seq, alignment_pattern_cds):
    """
    Construct a set of all optimal alignments from all possible paths.

    Parameters:
    possible_paths : list
        2D list of possible paths in the alignment matrix.
    len_seq : int
        Length of the sequence.
    len_cds : int
        Length of the coding sequences.
    count_dicts : list
        List of count dictionaries for each column.
    input_seq : str
        The input sequence for alignment.
    alignment_pattern_seq : list
        List to hold the alignment pattern for the sequence.
    alignment_pattern_cds : list
        List to hold the alignment pattern for the coding sequences.

    Returns:
    set
        A set of strings representing all optimal alignments.
    """
    samples_set = set()

    if len_seq == 0 and len_cds == 0:
        alignment_pattern_seq.reverse()
        alignment_pattern_cds.reverse()
        alignment_seq = ''.join(alignment_pattern_seq)
        alignment_cds = ''.join(alignment_pattern_cds)
        final_alignment_seq = ''
        for i in range(len(alignment_seq)):
            if alignment_seq[i] == '~' and alignment_cds[i] == 'x':
                continue
            if alignment_seq[i] == '~':
                final_alignment_seq += '-'
            else:
                final_alignment_seq += alignment_seq[i]
        samples_set.add(final_alignment_seq)
        return samples_set

    for direction in possible_paths[len_seq][len_cds]:
        y = len_seq
        x = len_cds
        divergent_seq = copy.deepcopy(alignment_pattern_seq)
        divergent_cds = copy.deepcopy(alignment_pattern_cds)
        if direction == 2:
            y -= 1
            divergent_seq.append(input_seq[y])
            divergent_cds.append('#')
            samples_set.update(
                consensus_alignment(possible_paths, y, x, count_dicts, input_seq, divergent_seq, divergent_cds))
        elif direction == 1:
            y -= 1
            x -= 1
            divergent_seq.append(input_seq[y])
            if sum([count_dicts[x][_] for _ in count_dicts[x]]) == 0:
                divergent_cds.append('x')
            else:
                divergent_cds.append('+')
            samples_set.update(
                consensus_alignment(possible_paths, y, x, count_dicts, input_seq, divergent_seq, divergent_cds))
        elif direction == 0:
            x -= 1
            divergent_seq.append('~')
            if sum([count_dicts[x][_] for _ in count_dicts[x]]) == 0:
                divergent_cds.append('x')
            else:
                divergent_cds.append('+')
            samples_set.update(
                consensus_alignment(possible_paths, y, x, count_dicts, input_seq, divergent_seq, divergent_cds))
        else:
            print("wtf")
            exit(-1)

    return samples_set


def initialize_dp(len_seq, len_cds, count_dicts, mode=0):
    """
    Initialize the dynamic programming matrix and path matrix for alignment.

    Parameters:
    len_seq : int
        Length of the sequence.
    len_cds : int
        Length of the coding sequences.
    count_dicts : list
        List of count dictionaries for each column.
    mode : int, optional
        Mode of the operation (0 or 1), default is 0.

    Returns:
    tuple
        Tuple containing the dynamic programming matrix and the path matrix.
    """
    # mode = 0: dynamic table & path array for sampling in nw_sample_seq_cd
    # mode = 1: dynamic table & path array for direct alignment reconstruction in get_best_seq_dp
    dynamic_table = [['X' for _ in range(len_cds + 1)] for _ in range(len_seq + 1)]
    dynamic_table[0][0] = 0
    if mode == 0:
        for i in range(1, len_seq + 1):
            dynamic_table[i][0] = 0
        for j in range(1, len_cds + 1):
            dynamic_table[0][j] = dynamic_table[0][j - 1] + score_wrt_count_dicts(count_dicts, '-', j - 1)

        chosen_paths = [[[] for _ in range(len_cds + 1)] for _ in range(len_seq + 1)]
        for i in range(1, len_seq + 1):
            chosen_paths[i][0].append(2)
        for j in range(1, len_cds + 1):
            chosen_paths[0][j].append(0)

    elif mode == 1:
        for j in range(1, len_cds + 1):
            dynamic_table[0][j] = dynamic_table[0][j - 1] + score_wrt_count_dicts(count_dicts, '-', j - 1)

        chosen_paths = [[-1 for _ in range(len_cds + 1)] for _ in range(len_seq + 1)]
        for j in range(1, len_cds + 1):
            chosen_paths[0][j] = 0
    else:
        print('wrong mode in initialize_dp. must be 0 or 1')
        exit(-2)

    return dynamic_table, chosen_paths


def nw_sample_seq_cd(input_seq, count_dicts, num_of_samples, seq_identifier, output_name, alignment_type, rep_flexible):
    """
    Perform Needleman-Wunsch alignment using sampling for a given sequence against coding sequence profiles.

    Parameters:
    input_seq : str
        The input sequence for alignment.
    count_dicts : list
        List of count dictionaries for each column in the coding sequences.
    num_of_samples : int
        Number of alignment samples to generate.
    seq_identifier : str
        Identifier for the sequence.
    output_name : str
        Name for the output file.
    alignment_type : str
        Type of alignment process (e.g., 'sample', 'consensus').
    rep_flexible : int
        Flexibility parameter for representative alignment.

    Returns:
    tuple
        Tuple containing the representative alignment and a list of alternative sampled optimal alignments.
    """
    input_seq = input_seq.replace(' ', '')
    input_seq = input_seq.replace('-', '')
    len_seq = len(input_seq)
    len_cds = len(count_dicts)
    dynamic_table, chosen_paths = initialize_dp(len_seq, len_cds, count_dicts, mode=0)

    profile_gap_penalty = -25  # TODO: maybe change this to get from input params?

    for i in range(1, len_seq + 1):
        for j in range(1, len_cds + 1):
            # moving horizontal = inserting gap in sequence (matching a gap with a profile)
            horizontal = dynamic_table[i][j - 1] + score_wrt_count_dicts(count_dicts, '-', j - 1)
            # moving diagonal = matching nuc and a profile
            diagonal = dynamic_table[i - 1][j - 1] + score_wrt_count_dicts(count_dicts, input_seq[i - 1], j - 1)
            # moving vertical = skipping a profile
            vertical = dynamic_table[i - 1][j] + profile_gap_penalty

            dynamic_table[i][j] = max(horizontal, diagonal, vertical)
            if dynamic_table[i][j] == horizontal:
                chosen_paths[i][j].append(0)
            if dynamic_table[i][j] == diagonal:
                chosen_paths[i][j].append(1)
            if dynamic_table[i][j] == vertical:
                chosen_paths[i][j].append(2)

    if alignment_type == "consensus":
        samples = list(consensus_alignment(chosen_paths, len_seq, len_cds, count_dicts, input_seq, [], []))
    else:
        path_with_prob = assign_probabilities_to_paths(chosen_paths, len_seq, len_cds)
        samples = list(sample_alignment(path_with_prob, len_seq, len_cds, count_dicts, input_seq, num_of_samples))
    min_distance = math.inf
    rep = ""
    sample_distances = {}
    for sample in samples:
        sample_distance = sum([distance(sample, x) for x in samples])
        sample_distances[sample] = sample_distance
        if sample_distance < min_distance:
            min_distance = sample_distance
            rep = sample

    best_samples = [k for k, v in sample_distances.items() if v <= min_distance + rep_flexible]
    log_path = os.path.join('samples', output_name)
    if not os.path.exists(log_path):
        os.mkdir(log_path)
    with open('./samples/' + output_name + '/' + seq_identifier + '_samples.fasta', 'w') as out:
        for i in range(len(samples)):
            out.write('>' + str(i + 1) + '\n')
            out.write(samples[i])
            out.write('\n')
        out.write('>rep\n')
        out.write(rep)
        out.write('\n')

    return rep, best_samples


def nw_simple_seq_cd(input_seq, count_dicts, path_order):
    """
    Perform a simple Needleman-Wunsch alignment of a sequence against coding sequence profiles.

    Parameters:
    input_seq : str
        The input sequence for alignment.
    count_dicts : list
        List of count dictionaries for each column in the coding sequences.
    path_order : str
        Order of preference for alignment paths.

    Returns:
    tuple
        Tuple containing the aligned sequence and the coding sequence pattern.
    """
    input_seq = input_seq.replace(' ', '')
    input_seq = input_seq.replace('-', '')
    len_seq = len(input_seq)
    len_cds = len(count_dicts)
    dynamic_table = [['X' for _ in range(len_cds + 1)] for _ in range(len_seq + 1)]
    chosen_paths = [[-1 for _ in range(len_cds + 1)] for _ in range(len_seq + 1)]

    profile_gap_penalty = -25

    for i in range(1, len_seq + 1):
        chosen_paths[i][0] = 2
    for j in range(1, len_cds + 1):
        chosen_paths[0][j] = 0

    dynamic_table[0][0] = 0
    for i in range(1, len_seq + 1):
        dynamic_table[i][0] = 0
    for j in range(1, len_cds + 1):
        dynamic_table[0][j] = dynamic_table[0][j - 1] + score_wrt_count_dicts(count_dicts, '-', j - 1)

    for i in range(1, len_seq + 1):
        for j in range(1, len_cds + 1):
            # moving horizontal = inserting gap in sequence (matching a gap with a profile)
            h = dynamic_table[i][j - 1] + score_wrt_count_dicts(count_dicts, '-', j - 1)
            # moving diagonal = matching nuc and a profile
            d = dynamic_table[i - 1][j - 1] + score_wrt_count_dicts(count_dicts, input_seq[i - 1], j - 1)
            # moving vertical = skipping a profile
            v = dynamic_table[i - 1][j] + profile_gap_penalty

            dynamic_table[i][j] = max(h, d, v)

            p_o = [_ for _ in path_order[::-1]]
            for priority in p_o:
                if priority == 'h':
                    if dynamic_table[i][j] == h:
                        chosen_paths[i][j] = 0
                if priority == 'd':
                    if dynamic_table[i][j] == d:
                        chosen_paths[i][j] = 1
                if priority == 'v':
                    if dynamic_table[i][j] == v:
                        chosen_paths[i][j] = 2

    # Reconstructed sequence alignment contains 2 possible types of characters: ~ corresponding to a gap, and a nuc
    # Reconstructed profiles alignment contains 3 possible types of characters: #, x, and +
    #                                            # = a gap insertion in the profiles
    #                                            x = using an empty profile (not having puzzle information)
    #                                            + = using a profile that has information from puzzles
    # an alignment would look like the following:              xx++x####x+++x++xx+##
    #                                                          aca-~cgccc--a~~-aacaa

    reconstructed_seq = []
    reconstructed_cds = []
    x = len_seq
    y = len_cds
    direction = chosen_paths[x][y]  # starting from last (bottom right) cell, moving towards 'direction'
    while x > 0 or y > 0:

        if direction == 2:
            x -= 1
            reconstructed_seq.append(input_seq[x])
            reconstructed_cds.append('#')
        elif direction == 1:
            x -= 1
            y -= 1
            reconstructed_seq.append(input_seq[x])
            if sum([count_dicts[y][_] for _ in count_dicts[y]]) == 0:
                reconstructed_cds.append('x')
            else:
                reconstructed_cds.append('+')
        elif direction == 0:
            y -= 1
            reconstructed_seq.append('~')
            if sum([count_dicts[y][_] for _ in count_dicts[y]]) == 0:
                reconstructed_cds.append('x')
            else:
                reconstructed_cds.append('+')
        else:
            print("wtf")
            exit(-1)
        direction = chosen_paths[x][y]

    reconstructed_seq.reverse()
    reconstructed_cds.reverse()
    return ''.join(reconstructed_seq), ''.join(reconstructed_cds)


def get_best_seq_dp(seq, count_dicts):
    """
    Get the best sequence alignment using dynamic programming.

    Parameters:
    seq : str
        The input sequence for alignment.
    count_dicts : list
        List of count dictionaries for each column in the coding sequences.

    Returns:
    str
        The best-aligned sequence.
    """
    len_seq = len(seq)
    len_cds = len(count_dicts)
    dynamic_table, chosen_paths = initialize_dp(len_seq, len_cds, count_dicts, mode=1)

    for i in range(1, len_seq + 1):
        for j in range(i, len_cds + 1):
            # We're only inserting a gap in the sequence or matching a nuc with a profile (=matching gap/nuc with
            # profile), so we only move Horizontal 'h' or Diagonal 'd', and not Vertical 'v'
            horizontal = -math.inf
            if dynamic_table[i][j - 1] != 'X':
                horizontal = dynamic_table[i][j - 1] + score_wrt_count_dicts(count_dicts, '-', j - 1)
            diagonal = dynamic_table[i - 1][j - 1] + score_wrt_count_dicts(count_dicts, seq[i - 1], j - 1)

            dynamic_table[i][j] = max(horizontal, diagonal)
            if dynamic_table[i][j] == horizontal:
                chosen_paths[i][j] = 0
            if dynamic_table[i][j] == diagonal:
                chosen_paths[i][j] = 1

    reconstructed_seq = []
    x = len_seq
    y = len_cds
    direction = chosen_paths[x][y]  # starting from last (bottom right) cell, moving towards 'direction'
    while x > 0 or y > 0:
        if direction == 2:
            x -= 1
            reconstructed_seq.append(seq[x])
        elif direction == 1:
            x -= 1
            y -= 1
            reconstructed_seq.append(seq[x])
        elif direction == 0:
            y -= 1
            reconstructed_seq.append('-')
        else:
            print('wtf')
            exit(-1)
        direction = chosen_paths[x][y]

    reconstructed_seq.reverse()

    return ''.join(reconstructed_seq)


def init_nucdict(input_data, offset):
    """
    Initialize a nucleotide dictionary based on input data and an offset.

    Parameters:
    input_data : dict
        Dictionary containing alignment data.
    offset : int
        Offset value for alignment.

    Returns:
    dict
        A dictionary with nucleotide counts.
    """
    input_data_o = input_data['mapped_guide_df'][offset].to_dict(orient='index')
    target = {}
    for seq in input_data_o:
        target[seq] = {}
        for col in input_data_o[seq]:
            if type(input_data_o[seq][col]) == dict:
                target[seq][col] = input_data_o[seq][col]
        if not target[seq]:
            target.pop(seq)
    return target


def add_nucdict(base, augm, offset, weight=1.0):
    """
    Add nucleotide information to a base dictionary from an augment dictionary.

    Parameters:
    base : dict
        Base dictionary to be augmented.
    augm : dict
        augment dictionary containing additional nucleotide data.
    offset : int
        Offset value for alignment.
    weight : float, optional
        Weight of the augment information, default is 1.0.

    Returns:
    None
    """
    for seq in augm:
        if seq not in base:
            base[seq] = {}
        for col in augm[seq]:
            if col + offset not in base[seq]:
                base[seq][col + offset] = {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 0}
            for nuc in augm[seq][col]:
                base[seq][col + offset][nuc] += weight * augm[seq][col][nuc]


def nrm_nucdict(base):
    """
    Normalize the nucleotide dictionary.

    Parameters:
    base : dict
        The nucleotide dictionary to normalize.

    Returns:
    None
    """
    for seq in base:
        for col in base[seq]:
            s = sum(base[seq][col].values())
            for nuc in base[seq][col]:
                base[seq][col][nuc] = base[seq][col][nuc] * 100 / s


def get_nucdict(input_file, offsets, weights):
    """
    Generate a nucleotide dictionary from an input file, offsets, and weights.

    Parameters:
    input_file : str
        File path of the input data.
    offsets : list
        List of offset values for alignment.
    weights : list
        List of weights for each offset.

    Returns:
    dict
        A dictionary with nucleotide information.
    """
    big_df = pickle.load(open(input_file, 'rb'))

    nuc_dict = {}
    for o_i in range(len(offsets)):
        o = offsets[o_i]
        nuc_dict_o = init_nucdict(big_df, o)
        nrm_nucdict(nuc_dict_o)
        w = 1
        if weights is not None:
            w = weights[o_i]
        if o >= 100:
            o = - (o - 100)
        add_nucdict(nuc_dict, nuc_dict_o, o, w)
    nrm_nucdict(nuc_dict)

    return nuc_dict


def map_alignments(old_align, new_align):
    """
    Map the alignments from old alignment to new alignment.

    Parameters:
    old_align : iterable
        Iterable of old alignment sequences.
    new_align : iterable
        Iterable of new alignment sequences.

    Returns:
    dict
        A dictionary mapping the old alignments to the new alignments.
    """
    align_map = {}
    for i in range(len(old_align)):
        align_map[old_align[i].id] = {}
        old_seq = copy.deepcopy(old_align[i])
        old_row_finder = old_seq.seq
        new_seq = copy.deepcopy(new_align[i])
        new_row_finder = new_seq.seq
        old_seq.seq = str(old_seq.seq).replace('-', '')
        for nuc in old_seq:
            old_pos = old_row_finder.index(nuc)
            new_pos = new_row_finder.index(nuc)
            old_row_finder = ('-' * (old_pos + 1)) + old_row_finder[old_pos + 1:]
            new_row_finder = ('-' * (new_pos + 1)) + new_row_finder[new_pos + 1:]
            align_map[old_align[i].id][old_pos] = new_pos

    return align_map


def fill_empty_nucdict_cols(nuc_dict, base_file):
    """
    Fill empty columns in the nucleotide dictionary with data from a base file.

    Parameters:
    nuc_dict : dict
        The nucleotide dictionary to be filled.
    base_file : str
        File path of the base file.

    Returns:
    None
    """
    seqs = [str(x.seq) for x in list(SeqIO.parse(base_file, "fasta"))]

    column_guide = get_column_consensus(seqs)
    for seq in nuc_dict:
        nf = nuc_dict[seq]
        cols = sorted(nf.keys())
        all_cols = list(range(cols[0], cols[-1] + 1))
        for col in all_cols:
            if col not in nuc_dict[seq].keys():
                nuc_dict[seq][col] = column_guide[col]


def add_ss_info_to_nucdict(nuc_dict, ss_weight, base_file):
    """
    Add secondary structure information to the nucleotide dictionary.

    Parameters:
    nuc_dict : dict
        The nucleotide dictionary to be augmented.
    ss_weight : float
        Weight for the secondary structure information.
    base_file : str
        File path of the base file with secondary structure information.

    Returns:
    None
    """
    if ss_weight is None:
        return

    filename = '../../alignments/rfam_aligned_bls_fixednames_UtoT_PP4.fasta'
    aln_list = list(SeqIO.parse(base_file, "fasta"))
    ss_seqs_list = list(SeqIO.parse(filename, "fasta"))

    aln_list = [seq for seq in sorted(aln_list, key=lambda item: item.id)]
    ss_seqs_list = [seq for seq in sorted(ss_seqs_list, key=lambda item: item.id)]

    for i in range(len(aln_list)):
        assert aln_list[i].id == ss_seqs_list[i].id
    column_map = map_alignments(aln_list, ss_seqs_list)

    aln_dict = {}
    for aln_seq in aln_list:
        aln_dict[aln_seq.id] = str(aln_seq.seq)

    # count dicts should only have RFAM sequences
    ss_seqs_dict = {}
    for ss_seq in ss_seqs_list:
        if ss_seq.id not in aln_dict:
            ss_seqs_dict[ss_seq.id] = str(ss_seq.seq)

    ss_countdicts = produce_count_dicts(ss_seqs_dict, len(ss_seqs_list[0].seq))
    for cd in ss_countdicts:
        s = sum(cd.values())
        for nuc in cd:
            cd[nuc] = cd[nuc] * 100 / s

    for s in aln_dict.keys():
        s_cmap = column_map[s]
        for col in s_cmap:
            cd = ss_countdicts[s_cmap[col]]
            if s in list(nuc_dict.keys()) and col in list(nuc_dict[s].keys()):
                for nuc in cd:
                    nuc_dict[s][col][nuc] += ss_weight * cd[nuc]
                    nuc_dict[s][col][nuc] /= (1 + ss_weight)  # normalization


def align_seqs_with_profile(nuc_dict, alignment_type, num_of_samples, path_order, output_name, base_file, rep_flexible):
    """
    Align sequences with a profile.

    Parameters:
    nuc_dict : dict
        The nucleotide dictionary containing profile information.
    alignment_type : str
        Type of alignment (e.g., 'simple', 'sample').
    num_of_samples : int
        Number of samples for alignment.
    path_order : str
        Order of paths for alignment.
    output_name : str
        Name for the output file.
    base_file : str
        File path of the base alignment file.
    rep_flexible : int
        Flexibility parameter for representative alignment.

    Returns:
    dict
        Dictionary of aligned sequences.
    """
    aln = {}
    for record in SeqIO.parse(base_file, 'fasta'):
        aln[record.id] = str(record.seq)

    print('Aligning Sequences with profiles...')
    new_aln = {}
    alt_aln_options = {}
    for seq in nuc_dict:
        nf = nuc_dict[seq]
        cols = sorted(nf.keys())
        s = aln[seq][cols[0]:cols[-1] + 1]
        count_dicts = [v for k, v in sorted(nf.items(), key=lambda item: item[0])]

        if alignment_type == "simple":
            a1, a2 = nw_simple_seq_cd(s, count_dicts, path_order)
            a1n = ''
            a2n = ''
            for i in range(len(a1)):
                if a1[i] == '~' and a2[i] == 'x':
                    continue
                if a1[i] == '~':
                    a1n += '-'
                else:
                    a1n += a1[i]
                a2n += a2[i]

            rep_alignment = a1n
        else:
            alt_aln_options[seq] = []
            rep_alignment, alt_alignments = nw_sample_seq_cd(s, count_dicts, num_of_samples, seq, output_name,
                                                             alignment_type, rep_flexible)
            for new_align in alt_alignments:
                alt_aln_options[seq].append(aln[seq][:cols[0]] + new_align + aln[seq][cols[-1] + 1:])

        new_aln[seq] = aln[seq][:cols[0]] + rep_alignment + aln[seq][cols[-1] + 1:]

    for seq in aln:
        if seq not in new_aln:
            new_aln[seq] = aln[seq]
            if alignment_type != "simple":
                alt_aln_options[seq] = [aln[seq]]

    if alignment_type != "simple":
        new_aln = get_best_seq_sop_combination(new_aln, alt_aln_options)

    return new_aln


def align_seqs_to_msa(new_aln, output_name):
    """
    Align sequences to a multiple sequence alignment (MSA) and write the result to a file.

    Parameters:
    new_aln : dict
        Dictionary containing the new alignments of sequences.
    output_name : str
        Name of the file to write the MSA results.

    Returns:
    None
    """
    sorted_keys = sorted(new_aln, key=lambda x: len(new_aln[x]), reverse=True)
    max_length = len(new_aln[sorted_keys[0]])
    realigned_count_dicts = produce_count_dicts(None, max_length)
    realigned_seqs = {}

    print('Aligning Sequences with each other (normalizing lengths)...')
    for key in sorted_keys:
        target_seq = new_aln[key]
        if len(target_seq) == max_length:
            realigned_seqs[key] = target_seq
            add_to_count_dicts(realigned_count_dicts, target_seq)
            continue

        chosen_seq_var = get_best_seq_dp(target_seq, realigned_count_dicts)
        add_to_count_dicts(realigned_count_dicts, chosen_seq_var)
        realigned_seqs[key] = chosen_seq_var

    print('Done.')

    with open(output_name + '_PP0.fasta', 'w') as out:
        for seq_num in realigned_seqs:
            out.write('>' + seq_num + '\n')
            out.write(realigned_seqs[seq_num])
            out.write('\n')


def align(input_file, output_name, offsets, weights, alignment_type, num_of_samples, path_order, ss_weight,
          distance_metric, base_file, rep_flexible):
    """
    Perform alignment of sequences using various parameters and write the results to a file.

    Parameters:
    input_file : str
        File path of the input data for alignment.
    output_name : str
        Name of the file to write the alignment results.
    offsets : list
        List of offset values to be used in the alignment.
    weights : list
        List of weights corresponding to each offset value.
    alignment_type : str
        The type of alignment method to use (e.g., 'simple', 'sample', 'consensus').
    num_of_samples : int
        Number of samples to generate if the alignment type is 'sample'.
    path_order : str
        The order of path preference in the alignment process if alignment type is simple.
    ss_weight : float
        Weight given to secondary structure information in the alignment.
    distance_metric : str
        The distance metric to use in progressive alignment.
    base_file : str
        File path of the base alignment file.
    rep_flexible : int
        Flexibility parameter for representative alignment in 'consensus' mode.

    Returns:
    None
    """
    print('input', input_file)
    print('output', output_name)
    print('offsets', offsets)
    print('offset weights', weights)
    print('progressive alignment distance metric', distance_metric)
    print('alignment method', alignment_type)
    if alignment_type == 'sample':
        print('number of samples', num_of_samples)
    if alignment_type == 'consensus':
        print('representative flexibility', rep_flexible)
    if ss_weight is not None:
        print('using secondary structure info with weight', ss_weight)

    nuc_dict = get_nucdict(input_file, offsets, weights)

    fill_empty_nucdict_cols(nuc_dict, base_file)

    add_ss_info_to_nucdict(nuc_dict, ss_weight, base_file)

    profile_aln = align_seqs_with_profile(nuc_dict, alignment_type, num_of_samples, path_order, output_name, base_file,
                                          rep_flexible)

    if distance_metric == "profile":
        align_seqs_to_msa(profile_aln, output_name)
    else:
        pa.build_progressive_alignment(profile_aln, output_name, distance_metric)
