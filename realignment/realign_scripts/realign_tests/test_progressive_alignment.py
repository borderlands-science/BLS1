import unittest
import itertools
import numpy as np
from realignment.realign_scripts import progressive_alignment as pa


#This script is just quality assessment for the realign scripts and shows output from our alignment pipeline for toy inputs
class TestProgressiveAlignment(unittest.TestCase):

    def test_map_sequence_cols(self):
        test_sequence = 'ACGT-'
        col_count = pa.map_sequence_columns(test_sequence)
        expected_col_count = [np.array([0.0, 1.0, 0.0, 0.0, 0.0]), np.array([0.0, 0.0, 1.0, 0.0, 0.0]),
                              np.array([0.0, 0.0, 0.0, 1.0, 0.0]), np.array([0.0, 0.0, 0.0, 0.0, 1.0]),
                              np.array([1.0, 0.0, 0.0, 0.0, 0.0])]
        for i in range(5):
            np.testing.assert_array_equal(col_count[i], expected_col_count[i])

    def test_initial_hamming_dist(self):
        test_sequences = [{0: 'ACTGGTCA'}, {1: 'AACCTTGG'}, {2: '-ACTGGTCA'}, {3: 'AACCTT'}]
        distance_list = pa.initial_dist_list(test_sequences, 'hamming')

        # Test hamming distance excludes comparing same sequences
        for i in range(4):
            self.assertEqual(distance_list[i][i], np.inf)

        # Test hamming distance between 2 sequences of equal length
        self.assertEqual(distance_list[0][1], 6)

        # Test hamming distance between 2 sequences with offset 1
        self.assertEqual(distance_list[0][2], 8)

        # Test 2 sequences where the only difference is length
        self.assertEqual(distance_list[1][3], 2)

    def test_calc_hamming_distance(self):
        test_sequences = ['ACTGGTCA', 'AACCTTGG', '-ACTGGTCA', 'AACCTT']

        # Test same sequence offset by one
        distance = pa.calc_hamming_distance(test_sequences[0], test_sequences[2])
        self.assertEqual(distance, 8)

        # Test substring of another sequence
        distance = pa.calc_hamming_distance(test_sequences[1], test_sequences[3])
        self.assertEqual(distance, 2)

        # Test regular case with same lengths
        distance = pa.calc_hamming_distance(test_sequences[0], test_sequences[1])
        self.assertEqual(distance, 6)

    def test_sequence_adjustment(self):
        test_sequences = ['ACTGGTCA', 'AACCTTGG', '-ACTGGTCA', 'AACCTT']

        # Test regular adjustment
        adjusted_seq = pa.adjust_seq_length(test_sequences[0], 10)
        self.assertEqual(adjusted_seq, 'ACTGGTCA~~')

        # Test no adjustment
        adjusted_seq = pa.adjust_seq_length(test_sequences[1], 8)
        self.assertEqual(adjusted_seq, 'AACCTTGG')

        # Test shorter max length
        adjusted_seq = pa.adjust_seq_length(test_sequences[2], 4)
        self.assertEqual(adjusted_seq, '-ACTGGTCA')

    def test_profile_adjustment(self):
        test_col_count = [np.array([0, 1, 0, 0, 0]), np.array([0, 0, 1, 0, 0]), np.array([0, 0, 0, 1, 0])]

        # Test unchanged length
        adjusted_profile = pa.adjust_profile_col_count_length(test_col_count, 3)
        self.assertEqual(len(adjusted_profile), 3)

        # Test adjusting length
        adjusted_profile = pa.adjust_profile_col_count_length(test_col_count, 5)
        self.assertEqual(len(adjusted_profile), 5)
        np.testing.assert_array_equal(adjusted_profile[0], np.array([0, 1, 0, 0, 0]))
        np.testing.assert_array_equal(adjusted_profile[3], np.array([0, 0, 0, 0, 0]))
        np.testing.assert_array_equal(adjusted_profile[4], np.array([0, 0, 0, 0, 0]))

        # Test shorter length
        adjusted_profile = pa.adjust_profile_col_count_length(test_col_count, 2)
        self.assertEqual(len(adjusted_profile), 3)

    def test_get_min_dist_position(self):
        test_sequences = [{0: 'ACTGGTCA'}, {1: 'AACCTTGG'}, {2: '-ACTGGTCA'}, {3: 'AACCTT'}]
        distance_list = pa.initial_dist_list(test_sequences, 'hamming')

        min_row, min_col = pa.get_min_dist_position(distance_list)
        self.assertEqual(min_row, 1)
        self.assertEqual(min_col, 3)

    def test_profile_alignment(self):
        test_sequences = [{0: 'ACTGGTCA'}, {1: 'AACCTTGG'}, {2: '-ACTGGTCA'}, {3: 'AACCTT'}]

        # Test that 2 sequences of same length will simply be added together without calcs
        new_profile, length_adjusted = pa.profile_alignment(test_sequences[0], test_sequences[1])
        self.assertFalse(length_adjusted)
        self.assertEqual(new_profile[0], 'ACTGGTCA')
        self.assertEqual(new_profile[1], 'AACCTTGG')

        # Test that flipping profiles won't mix up sequence ids
        new_profile_2, length_adjusted = pa.profile_alignment(test_sequences[3], new_profile)
        self.assertTrue(length_adjusted)
        self.assertEqual(new_profile_2[0], 'ACTGGTCA')
        self.assertEqual(new_profile_2[1], 'AACCTTGG')
        self.assertEqual(new_profile_2[3], 'AACCTT--')

        # Test simple case where gaps are only added at the end
        new_profile, length_adjusted = pa.profile_alignment(test_sequences[3], test_sequences[1])
        self.assertTrue(length_adjusted)
        self.assertEqual(new_profile[1], 'AACCTTGG')
        self.assertEqual(new_profile[3], 'AACCTT--')

        # Test case where gaps are added at the front
        new_profile, length_adjusted = pa.profile_alignment(test_sequences[0], test_sequences[2])
        self.assertTrue(length_adjusted)
        self.assertEqual(new_profile[0], '-ACTGGTCA')
        self.assertEqual(new_profile[2], '-ACTGGTCA')

    def test_calc_alignment_matrix(self):
        test_sequences = ['ACTGGTCA', 'AACCTTGG', '-ACTGGTCA', 'AACCTT']

        # Test that both gaps are added at the end
        trace_matrix = pa.calc_alignment_matrix([test_sequences[1]], [test_sequences[3]])
        self.assertEqual(trace_matrix[8][6], 1.0)
        self.assertEqual(trace_matrix[7][6], 1.0)

        # Test that gaps can be in multiple areas
        trace_matrix = pa.calc_alignment_matrix([test_sequences[0]], [test_sequences[3]])
        self.assertEqual(trace_matrix[7][5], 1.0)
        self.assertEqual(trace_matrix[2][1], 1.0)

    def test_traceback(self):
        test_sequences = ['ACTGGTCA', 'AACCTTGG', '-ACTGGTCA', 'AACCTT']

        # Test that both gaps are added at the end
        trace_matrix = pa.calc_alignment_matrix([test_sequences[1]], [test_sequences[3]])
        profile_1_cols = [list(filter(None, i)) for i in itertools.zip_longest(*[test_sequences[1]])]
        profile_2_cols = [list(filter(None, i)) for i in itertools.zip_longest(*[test_sequences[3]])]
        new_profile_list = pa.traceback(trace_matrix, profile_1_cols, profile_2_cols)
        self.assertEqual(new_profile_list[0], 'AACCTTGG')
        self.assertEqual(new_profile_list[1], 'AACCTT--')

        trace_matrix = pa.calc_alignment_matrix([test_sequences[2]], [test_sequences[1]])
        profile_1_cols = [list(filter(None, i)) for i in itertools.zip_longest(*[test_sequences[2]])]
        profile_2_cols = [list(filter(None, i)) for i in itertools.zip_longest(*[test_sequences[1]])]
        new_profile_list = pa.traceback(trace_matrix, profile_1_cols, profile_2_cols)
        self.assertEqual(new_profile_list[0], '-ACTGGTCA')
        self.assertEqual(new_profile_list[1], '-AACCTTGG')

    def test_get_matching_combos(self):

        # Test that pairs of gaps count as zero
        col_1_nucs = [3, 0, 0, 0, 0]
        col_2_nucs = [2, 0, 0, 0, 0]
        score = pa.get_matching_combos(col_1_nucs, col_2_nucs)
        self.assertEqual(score, 0)

        # Test that no pairs of gaps result in 0
        col_1_nucs = [0, 1, 0, 1, 0]
        col_2_nucs = [1, 0, 1, 0, 1]
        score = pa.get_matching_combos(col_1_nucs, col_2_nucs)
        self.assertEqual(score, 0)

        # Test that a single pair counts as 1
        col_1_nucs = [0, 1, 0, 0, 0]
        col_2_nucs = [0, 1, 0, 0, 0]
        score = pa.get_matching_combos(col_1_nucs, col_2_nucs)
        self.assertEqual(score, 1)

        # Test that scores add up
        col_1_nucs = [0, 1, 1, 1, 0]
        col_2_nucs = [0, 1, 1, 1, 0]
        score = pa.get_matching_combos(col_1_nucs, col_2_nucs)
        self.assertEqual(score, 3)

        # Test that score is equal to A*B
        col_1_nucs = [0, 3, 0, 0, 0]
        col_2_nucs = [0, 2, 0, 0, 0]
        score = pa.get_matching_combos(col_1_nucs, col_2_nucs)
        self.assertEqual(score, 6)

    def test_get_mismatching_combos(self):

        # Test that no mismatch results in 0
        col_1_nucs = [0, 1, 0, 0, 0]
        col_2_nucs = [0, 1, 0, 0, 0]
        score = pa.get_mismatching_combos(col_1_nucs, col_2_nucs, sum(col_2_nucs))
        self.assertEqual(score, 0)

        # Test that one mismatch results in -1
        col_1_nucs = [0, 1, 0, 0, 0]
        col_2_nucs = [0, 0, 1, 0, 0]
        score = pa.get_mismatching_combos(col_1_nucs, col_2_nucs, sum(col_2_nucs))
        self.assertEqual(score, -1)

        # Test that mismatch doesn't include gaps
        col_1_nucs = [0, 1, 0, 0, 0]
        col_2_nucs = [1, 0, 0, 0, 0]
        score = pa.get_mismatching_combos(col_1_nucs, col_2_nucs, sum(col_2_nucs))
        self.assertEqual(score, 0)

        # Test multiple mismatches stack
        col_1_nucs = [0, 1, 1, 0, 0]
        col_2_nucs = [0, 1, 1, 0, 0]
        score = pa.get_mismatching_combos(col_1_nucs, col_2_nucs, sum(col_2_nucs))
        self.assertEqual(score, -2)

        # Test multiples still give correct score
        col_1_nucs = [0, 3, 2, 0, 0]
        col_2_nucs = [0, 1, 1, 1, 0]
        score = pa.get_mismatching_combos(col_1_nucs, col_2_nucs, sum(col_2_nucs))
        self.assertEqual(score, -10)

        # Test reverse cols
        score = pa.get_mismatching_combos(col_2_nucs, col_1_nucs, sum(col_1_nucs))
        self.assertEqual(score, -10)

    def test_get_match_gap_combos(self):

        # Test that no gaps results in 0
        col_1_nucs = [0, 1, 0, 0, 0]
        col_2_nucs = [0, 1, 0, 0, 0]
        score = pa.get_match_gap_combos(col_1_nucs, col_2_nucs, sum(col_1_nucs), sum(col_2_nucs))
        self.assertEqual(score, 0)

        # Test that one gaps results in -2
        col_1_nucs = [1, 0, 0, 0, 0]
        col_2_nucs = [0, 1, 0, 0, 0]
        score = pa.get_match_gap_combos(col_1_nucs, col_2_nucs, sum(col_1_nucs), sum(col_2_nucs))
        self.assertEqual(score, -2)

        # Test that all gap combos are included
        col_1_nucs = [1, 1, 0, 0, 0]
        col_2_nucs = [1, 1, 0, 0, 0]
        score = pa.get_match_gap_combos(col_1_nucs, col_2_nucs, sum(col_1_nucs), sum(col_2_nucs))
        self.assertEqual(score, -4)

        # Test that multiples scale correctly
        col_1_nucs = [1, 1, 1, 1, 0]
        col_2_nucs = [2, 3, 0, 0, 0]
        score = pa.get_match_gap_combos(col_1_nucs, col_2_nucs, sum(col_1_nucs), sum(col_2_nucs))
        self.assertEqual(score, -18)

    def test_calc_profile_distance(self):

        # Test simple case with only 1 sequence per profile
        profile_1_col_count = [np.array([0, 1, 0, 0, 0]), np.array([0, 1, 0, 0, 0]), np.array([0, 1, 0, 0, 0])]
        profile_2_col_count = [np.array([0, 0, 1, 0, 0]), np.array([0, 1, 0, 0, 0]), np.array([0, 1, 0, 0, 0])]
        distance = pa.calc_profile_hamming_distance(profile_1_col_count, profile_2_col_count)
        self.assertEqual(distance, 1.0)

        # Test case where 2 profiles don't line up at all
        profile_1_col_count = [np.array([0, 1, 0, 0, 0]), np.array([0, 1, 0, 0, 0]), np.array([0, 1, 0, 0, 0])]
        profile_2_col_count = [np.array([0, 0, 1, 0, 0]), np.array([0, 0, 1, 0, 0]), np.array([0, 0, 1, 0, 0])]
        distance = pa.calc_profile_hamming_distance(profile_1_col_count, profile_2_col_count)
        self.assertEqual(distance, 3.0)

        # Test no matches where each profile has 2 sequences
        profile_1_col_count = [np.array([0.5, 0.5, 0, 0, 0]), np.array([0.5, 0.5, 0, 0, 0]),
                               np.array([0.5, 0.5, 0, 0, 0])]
        profile_2_col_count = [np.array([0, 0, 0.5, 0.5, 0]), np.array([0, 0, 0.5, 0.5, 0]),
                               np.array([0, 0, 0.5, 0.5, 0])]
        distance = pa.calc_profile_hamming_distance(profile_1_col_count, profile_2_col_count)
        self.assertEqual(distance, 3.0)

        # Test case where sets are the same
        profile_1_col_count = [np.array([0, 0, 0.5, 0.5, 0]), np.array([0, 0, 0.5, 0.5, 0]),
                               np.array([0, 0, 0.5, 0.5, 0])]
        profile_2_col_count = [np.array([0, 0, 0.5, 0.5, 0]), np.array([0, 0, 0.5, 0.5, 0]),
                               np.array([0, 0, 0.5, 0.5, 0])]
        distance = pa.calc_profile_hamming_distance(profile_1_col_count, profile_2_col_count)
        self.assertEqual(distance, 1.5)

    def test_calc_profile_levenshtein_distance(self):

        # Test case where alignments are off by one
        profile_1_col_count = [np.array([0, 1, 0, 0, 0]), np.array([0, 0, 1, 0, 0]),
                               np.array([0, 0, 0, 1, 0])]
        profile_2_col_count = [np.array([0, 0, 0, 0, 1]), np.array([0, 1, 0, 0, 0]),
                               np.array([0, 0, 1, 0, 0]), np.array([0, 0, 0, 1, 0])]
        distance = pa.calc_profile_levenshtein_distance(profile_1_col_count, profile_2_col_count)
        self.assertEqual(distance, 1)

    def test_count_col_nucleotides(self):
        test_profile = ['ACTGGTCA', 'AACCTTGG']

        col_count = pa.count_col_nucleotides(test_profile)
        self.assertEqual(len(col_count), 8)
        np.testing.assert_array_equal(col_count[0], np.array([0, 1, 0, 0, 0]))
        np.testing.assert_array_equal(col_count[1], np.array([0, 0.5, 0.5, 0, 0]))
        np.testing.assert_array_equal(col_count[4], np.array([0, 0, 0, 0.5, 0.5]))
