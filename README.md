## Borderlands Science Code Repository

This code repository is part of the code and data release for the Borderlands Science project. It focuses exclusively on the *code* and the way the results presented in the article were obtained.

For the article main text, supplementary material, data and data-related documentation, see our data release website: https://games.cs.mcgill.ca/bls/

You can also refer to alternate hosting locations for this code release, [on our local gitlab](	
https://jwgitlab.cs.mcgill.ca/sarrazin/borderlands-science-) or [on FigShare](	
https://www.doi.org/10.6084/m9.figshare.24962349)

### Outline:

- **alignments**: alignment files used in and produced by the pipeline, code to analyze and evaluate alignments, phylogenies inferred from alignments. All alignment and tree-related results in the paper will be found here.
- **clustering**: Clustering scripts used to identify representative sequences (out of an initial dataset of ~1Mil), and to realign all sequences together from an alignment of cluster representatives.
- **effect-sizes**: Scripts for evaluating effect sizes for the multiple sequence alignments. All effect size related results featured in the paper will be found here.
- **processing_scripts**: Various scripts used in the pipeline, ranging from building and analysis of puzzles to gathering and manipulation of player submitted solutions.
- **realignment**: Contains the code for combining player submitted solutions into multiple sequence alignment and related analysis.

More info regarding running the code is provided in each folder. All codes are in python and bash. The alignment-related code is relatively simple to run, using mostly standard python libraries, but the effect size code requires QIIME2 and several associated libraries:

- [qiime2](https://anaconda.org/qiime2/qiime2)
- [tax2tree](https://github.com/biocore/tax2tree)
- [fasttree](http://www.microbesonline.org/fasttree/)
- [evident](https://pypi.org/project/evident/)
- [iow](https://pypi.org/project/iow/)


Before running scripts, make sure you have the required libraries installed. A list of global requirements is available in the root folder of the repository. These requirements do not include effect sizes, which have their own set of requirements listed in the effect-sizes folder (due to being very different and significantly heavier than the rest of the project's).

Copyright 2023 McGill Games for Good (https://games.cs.mcgill.ca/) under a BSD 3-clause license (see LICENSE.MD)
