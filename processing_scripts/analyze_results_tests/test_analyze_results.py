import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import copy
import unittest
import processing_scripts.analyze_results_forRealign as ar
from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle
from processing_scripts.dnapuzzle import Puzzle as DnaPuzzle
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import pickle
import pandas as pd
import math


class TestBuilder(unittest.TestCase):
    test_file = "'../../alignments/test_alignment.fasta"
    puzzle = ['-ACCT--G', 'GCCUG---', 'GGCTG---',
              'AAACG---', 'GAGTA---', 'G-C-g-GG']
    consensus = [('G', 'T'), ('A', 'T'), ('T', 'G'), ('G', 'T'),
                 ('-', 'C'), ('T', 'G'), ('G', 'T'), ('A', 'C')]
    columns = list(range(8))
    ids = list(range(6))
    test_puzzle = Puzzle(puzzle=puzzle, ids=ids, consensus=consensus, columns=columns)

    def assert_frame_equal(self, a, b):
        try:
            pd.testing.assert_frame_equal(a, b)
        except AssertionError as e:
            raise AssertionError from e

    def assert_frame_not_equal(self, a, b):
        try:
            pd.testing.assert_frame_equal(a, b)
        except AssertionError:
            # frames are not equal
            pass
        else:
            # frames are equal
            raise AssertionError

    def setUp(self):
        aln_file = '../../alignments/cleaned_aligned_PASTA_9667.fasta'
        self.align_list = list(SeqIO.parse(aln_file, "fasta"))
        self.align_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))

    def test_consensus(self):
        nucs = ['-', 'A', 'C', 'G', 'T']

        # Test that in the event there's only 1 nucleotide that is what will be returned
        column = [0, 3, 0, 0, 0]
        consensus = ar.get_consensus(column, nucs)
        self.assertEqual(consensus, ('A', '-'))

        # Test that it returns the top two nucleotides
        column = [0, 3, 0, 0, 2]
        consensus = ar.get_consensus(column, nucs)
        self.assertEqual(consensus, ('A', 'T'))

        # Test it does not just get the first two nucleotides found
        column = [0, 3, 0, 1, 2]
        consensus = ar.get_consensus(column, nucs)
        self.assertEqual(consensus, ('A', 'T'))

        # Test that it can handle an empty column
        column = [0, 0, 0, 0, 0]
        consensus = ar.get_consensus(column, nucs)
        self.assertEqual(consensus, ('-', '-'))

    def test_column_conservation(self):
        sequences = None
        with self.assertRaises(ValueError):
            ar.assess_column_conservation(sequences)
        sequences = []
        with self.assertRaises(ValueError):
            ar.assess_column_conservation(sequences)

        sequences = [SeqRecord(Seq("AAAAA")), SeqRecord(Seq("ACCCA")), SeqRecord(Seq("ACGGA")), SeqRecord(Seq("TTTGA"))]

        # Test that consensus returned is all correct, and scores are accurate
        consensus = ar.assess_column_conservation(sequences)
        self.assertEqual(consensus[0], ('A', 'T'))
        self.assertEqual(consensus[1][0], 'C')
        self.assertEqual(consensus[-1], ('A', '-'))

        # Test that invalid nucleotides do not impact results
        sequences = [SeqRecord(Seq("AAAyx")), SeqRecord(Seq("ACCyx")), SeqRecord(Seq("ACGyx")), SeqRecord(Seq("TTTyA"))]
        consensus = ar.assess_column_conservation(sequences)
        self.assertEqual(consensus[3], ('-', '-'))
        self.assertEqual(consensus[4], ('A', '-'))

    def test_find_offset(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test base case, comparing puzzle guide to itself
        valid, offset = ar.find_offset(puzzle, puzzle.consensus)
        self.assertTrue(valid)
        self.assertEqual(offset, 0)

        # Test base case with offset 1
        puzzle.columns = list(range(1, 9))
        valid, offset = ar.find_offset(puzzle, puzzle.consensus)
        self.assertTrue(valid)
        self.assertEqual(offset, 1)

        # Test base case with offset 2
        puzzle.columns = list(range(2, 10))
        valid, offset = ar.find_offset(puzzle, puzzle.consensus)
        self.assertTrue(valid)
        self.assertEqual(offset, 2)

        # Test fail case
        puzzle.columns = list(range(4, 12))
        valid, offset = ar.find_offset(puzzle, puzzle.consensus)
        self.assertFalse(valid)
        self.assertEqual(offset, 3)

        # Test flipped puzzle offset 0
        puzzle.consensus = puzzle.consensus[::-1]
        puzzle.columns = list(range(7, -1, -1))
        valid, offset = ar.find_offset(puzzle, puzzle.consensus[::-1])
        self.assertTrue(valid)
        self.assertEqual(offset, 0)

        # Test flipped puzzle offset 1
        puzzle.consensus = puzzle.consensus[::-1]
        puzzle.columns = list(range(7, -1, -1))
        consensus = puzzle.consensus[::-1]
        consensus.insert(0, ('-', '-'))
        valid, offset = ar.find_offset(puzzle, consensus)
        self.assertTrue(valid)
        self.assertEqual(offset, 1)

        # Test flipped puzzle offset 2
        puzzle.consensus = puzzle.consensus[::-1]
        puzzle.columns = list(range(7, -1, -1))
        consensus = puzzle.consensus[::-1]
        consensus.insert(0, ('-', '-'))
        consensus.insert(0, ('-', '-'))
        valid, offset = ar.find_offset(puzzle, consensus)
        self.assertTrue(valid)
        self.assertEqual(offset, 2)

        # Test flipped puzzle fail
        puzzle.consensus = puzzle.consensus[::-1]
        puzzle.columns = list(range(4, -4, -1))
        valid, offset = ar.find_offset(puzzle, puzzle.consensus[::-1])
        self.assertFalse(valid)
        self.assertEqual(offset, 3)

    def test_validate_puzzle(self):
        sequences = {0: SeqRecord(Seq("-ACCT--G")), 1: SeqRecord(Seq("GCCUG---")), 2: SeqRecord(Seq("GGCTG---")),
                     3: SeqRecord(Seq("AAACG---")), 4: SeqRecord(Seq("GAGTA---")), 5: SeqRecord(Seq('G-C-g-GG'))}
        puzzle = DnaPuzzle(puzzle=self.test_puzzle.puzzle, IDs=self.test_puzzle.ids, columns=self.test_puzzle.columns,
                           flanks=self.test_puzzle.columns[-2:])
        puzzle.uncollapsed_puzzle = self.test_puzzle.uncollapsed_puzzle

        # Test base case puzzle
        valid = ar.validate_puzzle(puzzle, sequences)
        self.assertTrue(valid)

        # Test non-matching sequence
        sequences = {0: SeqRecord(Seq("AACCT--G")), 1: SeqRecord(Seq("GCCUG---")), 2: SeqRecord(Seq("GGCTG---")),
                     3: SeqRecord(Seq("AAACG---")), 4: SeqRecord(Seq("GAGTA---")), 5: SeqRecord(Seq('G-C-g-GG'))}
        valid = ar.validate_puzzle(puzzle, sequences)
        self.assertFalse(valid)

        # Test invalid sequence size
        sequences = {0: SeqRecord(Seq("AACCT--G-")), 1: SeqRecord(Seq("GCCUG----")), 2: SeqRecord(Seq("GGCTG----")),
                     3: SeqRecord(Seq("AAACG----")), 4: SeqRecord(Seq("GAGTA----")), 5: SeqRecord(Seq('G-C-g-GG-'))}
        valid = ar.validate_puzzle(puzzle, sequences)
        self.assertFalse(valid)

    def test_validate_flank(self):
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.flanks = puzzle.columns[-2:]

        # Test base case
        valid = ar.validate_flank(puzzle)
        self.assertTrue(valid)

        # Test fail case
        puzzle.flanks = [100, 106]
        puzzle.puzzle[0] = '-ACCT--G--'
        valid = ar.validate_flank(puzzle)
        self.assertFalse(valid)

    def test_mapping(self):
        puzzle = DnaPuzzle(puzzle=self.test_puzzle.puzzle, IDs=self.test_puzzle.ids, columns=self.test_puzzle.columns)
        puzzle.uncollapsed_puzzle = self.test_puzzle.uncollapsed_puzzle
        sol = ['-A-CCTG-', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']

        # Test mapping to basic solution
        sol_map = ar.map_orig_sol(puzzle, sol)
        self.assertEqual(sol_map[0], {1: 0, 2: 0, 3: 1, 4: 0, 5: 0})
        self.assertEqual(sol_map[4], {0: 5, 1: 5, 2: 6, 3: 5, 4: 4, 5: 2})

        # Test that all columns with nucleotides in uncollapsed puzzle are there. Should be no key for column 5
        # since it has no nucleotides
        self.assertEqual(len(sol_map.keys()), 7)
        check_column_5 = True if 5 not in sol_map.keys() else False
        self.assertTrue(check_column_5)

        # Test that first sequence is not in column 0 since there is no nucleotide there
        self.assertFalse(0 in sol_map[0].keys())
        self.assertTrue(1 in sol_map[0].keys())

        # Test shifting all nucleotides up by 1
        puzzle.uncollapsed_puzzle = ['ACCTG---', 'GCCTG---', 'GGCTG---', 'AAACG---', 'GAGTA---', 'GCGGG---']
        sol = ['-ACCTG--', '-GCCTG--', '-GGCTG--', '-AAACG--', '-GAGTA--', '-GCGGG--']
        sol_map = ar.map_orig_sol(puzzle, sol)
        self.assertEqual(sol_map[0], {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1})
        self.assertEqual(sol_map[4], {0: 5, 1: 5, 2: 5, 3: 5, 4: 5, 5: 5})

        # Test shifting all nucleotides down by 1
        puzzle.uncollapsed_puzzle = ['-ACCTG--', '-GCCTG--', '-GGCTG--', '-AAACG--', '-GAGTA--', '-GCGGG--']
        sol = ['ACCTG---', 'GCCTG---', 'GGCTG---', 'AAACG---', 'GAGTA---', 'GCGGG---']
        sol_map = ar.map_orig_sol(puzzle, sol)
        self.assertEqual(sol_map[1], {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0})
        self.assertEqual(sol_map[4], {0: 3, 1: 3, 2: 3, 3: 3, 4: 3, 5: 3})

        # Test that a nucleotide removed in the solution won't show up in the final mapping
        puzzle.uncollapsed_puzzle = ['-ACCTG--', '-GCCTG--', '-GGCTG--', '-AAACG--', '-GAGTA--', '-GCGGG--']
        sol = ['ACC-----', 'GCCTG---', 'GGCTG---', 'AAACG---', 'GAGTA---', 'GCGGG---']
        sol_map = ar.map_orig_sol(puzzle, sol)
        self.assertEqual(sol_map[4], {1: 3, 2: 3, 3: 3, 4: 3, 5: 3})

        # Test that a nucleotide in the solution doesn't exist in the uncollapsed puzzle it will be excluded
        puzzle.uncollapsed_puzzle = ['-ACCTG--', '-GCCTG--', '-GGCTG--', '-AAACG--', '-GAGTA--', '-GCGGG--']
        sol = ['ACCXX---', 'GCCTG---', 'GGCTG---', 'AAACG---', 'GAGTA---', 'GCGGX---']
        sol_map = ar.map_orig_sol(puzzle, sol)
        self.assertEqual(sol_map[4], {1: 3, 2: 3, 3: 3, 4: 3, 5: 3})
        self.assertEqual(sol_map[5], {1: 4, 2: 4, 3: 4, 4: 4})

    def test_seq_seq(self):
        puzzle = DnaPuzzle(puzzle=self.test_puzzle.puzzle, IDs=self.test_puzzle.ids, columns=self.test_puzzle.columns)
        puzzle.uncollapsed_puzzle = self.test_puzzle.uncollapsed_puzzle
        sol = ['-A-CCTG-', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        sol_map = ar.map_orig_sol(puzzle, sol)

        # Test base case where nuc in question hasn't moved
        seq_diff, seq_seq_count = ar.calc_seq_seq(sol_map[2], list(sol_map[2].values())[1], puzzle)
        self.assertEqual(seq_diff, -3)
        self.assertEqual(seq_seq_count, 5)

        # Test base case where nuc in question has moved
        seq_diff, seq_seq_count = ar.calc_seq_seq(sol_map[2], list(sol_map[2].values())[0], puzzle)
        self.assertEqual(seq_diff, 3)
        self.assertEqual(seq_seq_count, 5)

        # Test case where one of the sequences is excluded because it has no nuc starting in that column
        seq_diff, seq_seq_count = ar.calc_seq_seq(sol_map[0], list(sol_map[0].values())[0], puzzle)
        self.assertEqual(seq_diff, -1)
        self.assertEqual(seq_seq_count, 4)

        # Test case where only one sequence aligned with it originally
        seq_diff, seq_seq_count = ar.calc_seq_seq(sol_map[7], list(sol_map[7].values())[0], puzzle)
        self.assertEqual(seq_diff, 1)
        self.assertEqual(seq_seq_count, 1)

        # Test case where if no other sequences aligned with it originally, it will return 0/0
        puzzle.uncollapsed_puzzle = ['-ACCT--G', 'GCCTG---', 'GGCTG---', 'AAACG---', 'GAGTA---', 'GCGGG---']
        sol_map = ar.map_orig_sol(puzzle, sol)
        seq_diff, seq_seq_count = ar.calc_seq_seq(sol_map[7], list(sol_map[7].values())[0], puzzle)
        self.assertEqual(seq_diff, 0)
        self.assertEqual(seq_seq_count, 0)

    def test_get_sol(self):
        puzzle = copy.deepcopy(self.test_puzzle)
        first_sol = ['-A-CCTG-', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        solution = {'playerSolutions': [first_sol]}

        # Test with valid solution nothing will change
        sol = ar.get_sol(puzzle, solution)
        self.assertEqual(sol, first_sol)

        # Test if one sequence needs to be fixed, the others will stay the same
        sec_sol = ['-A-CCTG-', 'GCCT-G', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        solution = {'playerSolutions': [sec_sol]}
        sol = ar.get_sol(puzzle, solution)
        self.assertEqual(sol, first_sol)

        # Test if all need to be fixed, with different lengths
        third_sol = ['-A-CCTG', 'GCCT-G', 'G--GCTG', '-AAACG', 'GAGTA', 'GCGG-G']
        solution = {'playerSolutions': [third_sol]}
        sol = ar.get_sol(puzzle, solution)
        self.assertEqual(sol, first_sol)

    def fill_matrix_key(self, key, cols_to_inspect, dict):
        dict[key] = {}
        for i in range(6):
            dict[key][i] = {}
            for pos in cols_to_inspect:
                dict[key][i][pos] = math.nan

    def test_add_solutions(self):
        cols_to_inspect = [10, 11, 12, 13]
        sol = ['-A-CCTG-', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        solution = {'playerSolutions': [sol]}
        puzzle = DnaPuzzle(puzzle=self.test_puzzle.puzzle, IDs=self.test_puzzle.ids, columns=list(range(10, 18)))
        puzzle.uncollapsed_puzzle = self.test_puzzle.uncollapsed_puzzle
        offset = 0
        offset_direction = 0
        identical_ids = {i: [i] for i in puzzle.IDs}
        all_realign_value = {}
        all_gap_value = {}
        all_guide_value = {}
        position_mapping = {}
        self.fill_matrix_key(0, cols_to_inspect, all_realign_value)
        self.fill_matrix_key(0, cols_to_inspect, all_gap_value)
        self.fill_matrix_key(0, cols_to_inspect, all_guide_value)
        self.fill_matrix_key(0, cols_to_inspect, position_mapping)

        # Test base case
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][0], {10: math.nan, 11: [-3, 4], 12: [3, 5], 13: [1, 4]})
        self.assertEqual(all_realign_value[0][1], {10: [-1, 4], 11: [-3, 4], 12: [-3, 5], 13: [-4, 4]})
        self.assertEqual(all_realign_value[0][5], {10: [-1, 4], 11: math.nan, 12: [-9, 5], 13: math.nan})

        self.assertEqual(all_guide_value[0][0], {10: math.nan, 11: [0, 1], 12: [1, 1], 13: [1, 1]})
        self.assertEqual(all_guide_value[0][2], {10: [0, 1], 11: [2, 1], 12: [2, 1], 13: [2, 1]})
        self.assertEqual(all_guide_value[0][5], {10: [0, 1], 11: math.nan, 12: [-1, 1], 13: math.nan})

        self.assertEqual(all_gap_value[0][0], {10: math.nan, 11: [1, 5], 12: [0, 5], 13: [2, 5]})
        self.assertEqual(all_gap_value[0][2], {10: [2, 5], 11: [0, 5], 12: [2, 5], 13: [1, 5]})

        self.assertEqual(position_mapping[0][0], {10: math.nan, 11: {0: 1}, 12: {1: 1}, 13: {1: 1}})
        self.assertEqual(position_mapping[0][2], {10: {0: 1}, 11: {2: 1}, 12: {2: 1}, 13: {2: 1}})
        self.assertEqual(position_mapping[0][5], {10: {0: 1}, 11: math.nan, 12: {-1: 1}, 13: math.nan})

        # Test adding a second duplicate solution
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][0], {10: math.nan, 11: [-6, 8], 12: [6, 10], 13: [2, 8]})
        self.assertEqual(all_realign_value[0][1], {10: [-2, 8], 11: [-6, 8], 12: [-6, 10], 13: [-8, 8]})
        self.assertEqual(all_realign_value[0][5], {10: [-2, 8], 11: math.nan, 12: [-18, 10], 13: math.nan})

        self.assertEqual(all_guide_value[0][0], {10: math.nan, 11: [0, 2], 12: [2, 2], 13: [2, 2]})
        self.assertEqual(all_guide_value[0][2], {10: [0, 2], 11: [4, 2], 12: [4, 2], 13: [4, 2]})
        self.assertEqual(all_guide_value[0][5], {10: [0, 2], 11: math.nan, 12: [-2, 2], 13: math.nan})

        self.assertEqual(all_gap_value[0][0], {10: math.nan, 11: [2, 10], 12: [0, 10], 13: [4, 10]})
        self.assertEqual(all_gap_value[0][2], {10: [4, 10], 11: [0, 10], 12: [4, 10], 13: [2, 10]})

        self.assertEqual(position_mapping[0][0], {10: math.nan, 11: {0: 2}, 12: {1: 2}, 13: {1: 2}})
        self.assertEqual(position_mapping[0][2], {10: {0: 2}, 11: {2: 2}, 12: {2: 2}, 13: {2: 2}})
        self.assertEqual(position_mapping[0][5], {10: {0: 2}, 11: math.nan, 12: {-1: 2}, 13: math.nan})

        # Test a solution with offset
        offset = 1
        offset_direction = 1
        all_realign_value = {}
        all_gap_value = {}
        all_guide_value = {}
        position_mapping = {}
        self.fill_matrix_key(1, cols_to_inspect, all_realign_value)
        self.fill_matrix_key(1, cols_to_inspect, all_gap_value)
        self.fill_matrix_key(1, cols_to_inspect, all_guide_value)
        self.fill_matrix_key(1, cols_to_inspect, position_mapping)

        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)

        self.assertEqual(all_guide_value[1][0], {10: math.nan, 11: [-1, 1], 12: [0, 1], 13: [0, 1]})
        self.assertEqual(all_guide_value[1][2], {10: [-1, 1], 11: [1, 1], 12: [1, 1], 13: [1, 1]})
        self.assertEqual(all_guide_value[1][5], {10: [-1, 1], 11: math.nan, 12: [-2, 1], 13: math.nan})

        self.assertEqual(position_mapping[1][0], {10: math.nan, 11: {-1: 1}, 12: {0: 1}, 13: {0: 1}})
        self.assertEqual(position_mapping[1][2], {10: {-1: 1}, 11: {1: 1}, 12: {1: 1}, 13: {1: 1}})
        self.assertEqual(position_mapping[1][5], {10: {-1: 1}, 11: math.nan, 12: {-2: 1}, 13: math.nan})

        # Test that adding same solution with different offset will go into different dict
        offset = 0
        offset_direction = 0
        self.fill_matrix_key(0, cols_to_inspect, all_realign_value)
        self.fill_matrix_key(0, cols_to_inspect, all_gap_value)
        self.fill_matrix_key(0, cols_to_inspect, all_guide_value)
        self.fill_matrix_key(0, cols_to_inspect, position_mapping)
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_guide_value[0][0], {10: math.nan, 11: [0, 1], 12: [1, 1], 13: [1, 1]})
        self.assertEqual(all_guide_value[0][2], {10: [0, 1], 11: [2, 1], 12: [2, 1], 13: [2, 1]})
        self.assertEqual(all_guide_value[0][5], {10: [0, 1], 11: math.nan, 12: [-1, 1], 13: math.nan})

        self.assertEqual(all_guide_value[1][0], {10: math.nan, 11: [-1, 1], 12: [0, 1], 13: [0, 1]})
        self.assertEqual(all_guide_value[1][2], {10: [-1, 1], 11: [1, 1], 12: [1, 1], 13: [1, 1]})
        self.assertEqual(all_guide_value[1][5], {10: [-1, 1], 11: math.nan, 12: [-2, 1], 13: math.nan})

        self.assertEqual(position_mapping[1][0], {10: math.nan, 11: {-1: 1}, 12: {0: 1}, 13: {0: 1}})
        self.assertEqual(position_mapping[1][2], {10: {-1: 1}, 11: {1: 1}, 12: {1: 1}, 13: {1: 1}})
        self.assertEqual(position_mapping[1][5], {10: {-1: 1}, 11: math.nan, 12: {-2: 1}, 13: math.nan})

        # Test a solution with multiple responses
        solution = {'playerSolutions': [sol, sol, sol]}
        all_realign_value = {}
        all_gap_value = {}
        all_guide_value = {}
        position_mapping = {}
        cols = [10, 11, 12, 13, 14, 15, 16, 17]
        self.fill_matrix_key(0, cols, all_realign_value)
        self.fill_matrix_key(0, cols, all_gap_value)
        self.fill_matrix_key(0, cols, all_guide_value)
        self.fill_matrix_key(0, cols_to_inspect, position_mapping)
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][1],
                         {10: [-3, 12], 11: [-9, 12], 12: [-9, 15], 13: [-12, 12], 14: math.nan, 15: math.nan,
                          16: math.nan, 17: math.nan})
        self.assertEqual(all_guide_value[0][2],
                         {10: [0, 3], 11: [6, 3], 12: [6, 3], 13: [6, 3], 14: math.nan, 15: math.nan, 16: math.nan,
                          17: math.nan})
        self.assertEqual(all_gap_value[0][2],
                         {10: [6, 15], 11: [0, 15], 12: [6, 15], 13: [3, 15], 14: math.nan, 15: math.nan, 16: math.nan,
                          17: math.nan})
        self.assertEqual(position_mapping[0][2], {10: {0: 3}, 11: {2: 3}, 12: {2: 3}, 13: {2: 3}})

        # Test a second puzzle only adds onto dicts, not replace them
        puzzle.columns = list(range(14, 22))
        cols_to_inspect = [14, 15, 16, 17]
        solution = {'playerSolutions': [sol]}
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][1], {10: [-3, 12], 11: [-9, 12], 12: [-9, 15], 13: [-12, 12], 14: [-1, 4],
                                                   15: [-3, 4], 16: [-3, 5], 17: [-4, 4]})
        self.assertEqual(all_guide_value[0][2], {10: [0, 3], 11: [6, 3], 12: [6, 3], 13: [6, 3], 14: [0, 1], 15: [2, 1],
                                                 16: [2, 1], 17: [2, 1]})
        self.assertEqual(all_gap_value[0][2], {10: [6, 15], 11: [0, 15], 12: [6, 15], 13: [3, 15], 14: [2, 5],
                                               15: [0, 5], 16: [2, 5], 17: [1, 5]})
        self.assertEqual(position_mapping[0][2], {10: {0: 3}, 11: {2: 3}, 12: {2: 3}, 13: {2: 3}, 14: {0: 1},
                                                  15: {2: 1}, 16: {2: 1}, 17: {2: 1}})

        # Test adding a different solution for position mapping
        puzzle.columns = list(range(10, 18))
        cols_to_inspect = [10, 11, 12, 13, 14, 15, 16, 17]
        sol = ['---ACCTG', '---GCCTG', '---GGCTG', '---AAACG', '---GAGTA', '---GCGGG']
        solution = {'playerSolutions': [sol]}
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][1], {10: [-3, 16], 11: [-8, 16], 12: [-7, 20], 13: [-11, 16], 14: [2, 9],
                                                   15: [-3, 4], 16: [-3, 5], 17: [-4, 4]})
        self.assertEqual(all_guide_value[0][2], {10: [3, 4], 11: [9, 4], 12: [9, 4], 13: [9, 4], 14: [3, 2], 15: [2, 1],
                                                 16: [2, 1], 17: [2, 1]})
        self.assertEqual(all_gap_value[0][2], {10: [6, 20], 11: [0, 20], 12: [6, 20], 13: [3, 20], 14: [2, 10],
                                               15: [0, 5], 16: [2, 5], 17: [1, 5]})
        self.assertEqual(position_mapping[0][2], {10: {0: 3, 3: 1}, 11: {2: 3, 3: 1}, 12: {2: 3, 3: 1},
                                                  13: {2: 3, 3: 1}, 14: {0: 1, 3: 1}, 15: {2: 1}, 16: {2: 1},
                                                  17: {2: 1}})


        # Test a puzzle keeping 100 percent of puzzle
        puzzle.columns = list(range(10, 18))
        cols_to_inspect = [10, 11, 12, 13, 14, 15, 16, 17]
        sol = ['-A-CCTG-', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        solution = {'playerSolutions': [sol]}
        all_realign_value = {}
        all_gap_value = {}
        all_guide_value = {}
        position_mapping = {}
        self.fill_matrix_key(0, cols_to_inspect, all_realign_value)
        self.fill_matrix_key(0, cols_to_inspect, all_gap_value)
        self.fill_matrix_key(0, cols_to_inspect, all_guide_value)
        self.fill_matrix_key(0, cols_to_inspect, position_mapping)
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][0],
                         {10: math.nan, 11: [-3, 4], 12: [3, 5], 13: [1, 4], 14: [3, 5], 15: math.nan, 16: math.nan,
                          17: [1, 1]})
        self.assertEqual(all_realign_value[0][5],
                         {10: [-1, 4], 11: math.nan, 12: [-9, 5], 13: math.nan, 14: [-15, 5], 15: math.nan, 16: [0, 0],
                          17: [-1, 1]})
        self.assertEqual(all_guide_value[0][2],
                         {10: [0, 1], 11: [2, 1], 12: [2, 1], 13: [2, 1], 14: [2, 1], 15: math.nan, 16: math.nan,
                          17: math.nan})
        self.assertEqual(all_gap_value[0][2],
                         {10: [2, 5], 11: [0, 5], 12: [2, 5], 13: [1, 5], 14: [4, 5], 15: math.nan, 16: math.nan,
                          17: math.nan})
        self.assertEqual(position_mapping[0][5], {10: {0: 1}, 11: math.nan, 12: {-1: 1}, 13: math.nan, 14: {-2: 1},
                                                  15: math.nan, 16: {-3: 1}, 17: {-2: 1}})

        # Test a puzzle keeping 75 percent of puzzle
        puzzle.columns = list(range(10, 18))
        cols_to_inspect = [10, 11, 12, 13, 14, 15]
        all_realign_value = {}
        all_gap_value = {}
        all_guide_value = {}
        position_mapping = {}
        self.fill_matrix_key(0, cols_to_inspect, all_realign_value)
        self.fill_matrix_key(0, cols_to_inspect, all_gap_value)
        self.fill_matrix_key(0, cols_to_inspect, all_guide_value)
        self.fill_matrix_key(0, cols_to_inspect, position_mapping)
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][0],
                         {10: math.nan, 11: [-3, 4], 12: [3, 5], 13: [1, 4], 14: [3, 5], 15: math.nan})
        self.assertEqual(all_realign_value[0][5],
                         {10: [-1, 4], 11: math.nan, 12: [-9, 5], 13: math.nan, 14: [-15, 5], 15: math.nan})
        self.assertEqual(all_guide_value[0][2],
                         {10: [0, 1], 11: [2, 1], 12: [2, 1], 13: [2, 1], 14: [2, 1], 15: math.nan})
        self.assertEqual(all_gap_value[0][2],
                         {10: [2, 5], 11: [0, 5], 12: [2, 5], 13: [1, 5], 14: [4, 5], 15: math.nan})
        self.assertEqual(position_mapping[0][5], {10: {0: 1}, 11: math.nan, 12: {-1: 1}, 13: math.nan, 14: {-2: 1},
                                                  15: math.nan})

        # Test a puzzle keeping 25 percent of puzzle
        puzzle.columns = list(range(10, 18))
        cols_to_inspect = [10, 11]
        all_realign_value = {}
        all_gap_value = {}
        all_guide_value = {}
        self.fill_matrix_key(0, cols_to_inspect, all_realign_value)
        self.fill_matrix_key(0, cols_to_inspect, all_gap_value)
        self.fill_matrix_key(0, cols_to_inspect, all_guide_value)
        self.fill_matrix_key(0, cols_to_inspect, position_mapping)
        ar.add_solutions(cols_to_inspect, solution, puzzle, offset, offset_direction, identical_ids, all_realign_value,
                         all_gap_value, all_guide_value, position_mapping)
        self.assertEqual(all_realign_value[0][0], {10: math.nan, 11: [-3, 4]})
        self.assertEqual(all_realign_value[0][5], {10: [-1, 4], 11: math.nan})
        self.assertEqual(all_guide_value[0][2], {10: [0, 1], 11: [2, 1]})
        self.assertEqual(all_gap_value[0][2], {10: [2, 5], 11: [0, 5]})
        self.assertEqual(position_mapping[0][5], {10: {0: 1}, 11: math.nan})

    def test_add_to_solution_map(self):
        cols_to_inspect = [10, 11, 12, 13]
        sol = ['-A-CCTG-', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        solution = {'playerSolutions': [sol]}
        puzzle = DnaPuzzle(puzzle=self.test_puzzle.puzzle, IDs=self.test_puzzle.ids, columns=list(range(10, 18)))
        puzzle.uncollapsed_puzzle = self.test_puzzle.uncollapsed_puzzle
        offset_direction = 0
        identical_ids = {i: [i] for i in puzzle.IDs}

        solution_mapping = {}
        self.fill_matrix_key(0, cols_to_inspect, solution_mapping)
        self.fill_matrix_key(1, cols_to_inspect, solution_mapping)

        # Test base case
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0], {10: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  11: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 0},
                                                  12: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  13: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0}})

        # Test adding with different offset
        offset_direction = 1
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0], {10: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  11: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 0},
                                                  12: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  13: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0}})
        self.assertEqual(solution_mapping[1][0], {10: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  11: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 0},
                                                  12: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  13: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0}})

        # Test full puzzle, and adding to existing dictionaries
        offset_direction = 0
        cols_to_inspect = [10, 11, 12, 13, 14, 15, 16, 17]
        solution_mapping = {}
        self.fill_matrix_key(0, cols_to_inspect, solution_mapping)
        self.fill_matrix_key(1, cols_to_inspect, solution_mapping)

        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0], {10: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  11: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 0},
                                                  12: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  13: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                  14: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                  15: {'A': 0, 'C': 0, 'G': 0, 'T': 1, '-': 0},
                                                  16: {'A': 0, 'C': 0, 'G': 1, 'T': 0, '-': 0},
                                                  17: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1}})

        # Test adding a solution with different results
        sol = ['ACGTA---', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        solution = {'playerSolutions': [sol]}
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0],
                         {10: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                          11: {'A': 1, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                          12: {'A': 0, 'C': 0, 'G': 1, 'T': 0, '-': 1},
                          13: {'A': 0, 'C': 1, 'G': 0, 'T': 1, '-': 0},
                          14: {'A': 1, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                          15: {'A': 0, 'C': 0, 'G': 0, 'T': 1, '-': 1},
                          16: {'A': 0, 'C': 0, 'G': 1, 'T': 0, '-': 1},
                          17: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 2}})

        # Test case where columns before 5 get cut off
        solution_mapping = {}
        sol = ['-A-CCTG-', 'GCCT-G--', 'G--GCTG-', '-AAACG--', 'GAGTA---', 'GCGG-G--']
        solution = {'playerSolutions': [sol]}
        cols_to_inspect = [3, 4, 5, 6, 7, 8]
        self.fill_matrix_key(0, cols_to_inspect, solution_mapping)
        puzzle.columns = list(range(3, 11))
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0], {3: math.nan, 4: math.nan,
                                                  5: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  6: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                  7: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                  8: {'A': 0, 'C': 0, 'G': 0, 'T': 1, '-': 0}})

        # Test case where columns after 179 get cut off
        solution_mapping = {}
        cols_to_inspect = [178, 179, 180, 181, 182, 183, 184, 185]
        self.fill_matrix_key(0, cols_to_inspect, solution_mapping)
        puzzle.columns = list(range(178, 186))
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0], {178: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                  179: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 0},
                                                  180: math.nan, 181: math.nan, 182: math.nan, 183: math.nan,
                                                  184: math.nan, 185: math.nan})

        # Test flipped puzzle
        cols_to_inspect = [17, 16, 15, 14]
        self.fill_matrix_key(100, cols_to_inspect, solution_mapping)
        puzzle.columns = list(range(17, 9, -1))
        offset_direction = 100
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[100][0], {17: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                    16: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 0},
                                                    15: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                    14: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0}})

        # Test flipped puzzle where columns get cut off at column 5
        solution_mapping = {}
        cols_to_inspect = [10, 9, 8, 7, 6, 5, 4, 3]
        self.fill_matrix_key(100, cols_to_inspect, solution_mapping)
        puzzle.columns = list(range(10, 2, -1))
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[100][0], {10: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                    9: {'A': 1, 'C': 0, 'G': 0, 'T': 0, '-': 0},
                                                    8: {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 1},
                                                    7: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                    6: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                    5: {'A': 0, 'C': 0, 'G': 0, 'T': 1, '-': 0},
                                                    4: math.nan, 3: math.nan})

        # Test flipped puzzle where columns get cut off at column 180
        solution_mapping = {}
        cols_to_inspect = [182, 181, 180, 179, 178, 177]
        self.fill_matrix_key(100, cols_to_inspect, solution_mapping)
        puzzle.columns = list(range(182, 174, -1))
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[100][0], {182: math.nan, 181: math.nan, 180: math.nan,
                                                    179: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                    178: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 0},
                                                    177: {'A': 0, 'C': 0, 'G': 0, 'T': 1, '-': 0}})

        # Test having sequences with duplicate ids includes them
        solution_mapping = {}
        cols_to_inspect = [10, 11, 12, 13]
        self.fill_matrix_key(0, cols_to_inspect, solution_mapping)
        puzzle.columns = list(range(10, 18))
        puzzle.IDs = [0, 0, 0, 1, 1, 2]
        offset_direction = 0
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0], {10: {'A': 0, 'C': 0, 'G': 2, 'T': 0, '-': 1},
                                                  11: {'A': 1, 'C': 1, 'G': 0, 'T': 0, '-': 1},
                                                  12: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 2},
                                                  13: {'A': 0, 'C': 1, 'G': 1, 'T': 1, '-': 0}})

        # Test soft window skipping columns
        solution_mapping = {}
        cols_to_inspect = [10, 12, 14, 16]
        self.fill_matrix_key(0, cols_to_inspect, solution_mapping)
        puzzle.columns = [10, 12, 14, 16, 18, 20, 22, 24]
        ar.add_to_solution_map(cols_to_inspect, solution, puzzle, offset_direction, identical_ids, solution_mapping)
        self.assertEqual(solution_mapping[0][0], {10: {'A': 0, 'C': 0, 'G': 2, 'T': 0, '-': 1},
                                                  12: {'A': 1, 'C': 1, 'G': 0, 'T': 0, '-': 1},
                                                  14: {'A': 0, 'C': 1, 'G': 0, 'T': 0, '-': 2},
                                                  16: {'A': 0, 'C': 1, 'G': 1, 'T': 1, '-': 0}})


    def test_black_box(self):
        annotate_output_path = "annotate_test.pickle"
        filter_params = ar.get_default_filter_params()
        puzzle_params = ar.get_default_puzzle_params()

        puzzles_path = "puzzles_test.pickle"

        aln_file = '../../alignments/cleaned_aligned_PASTA_9667.fasta'

        try:
            puzzles = pickle.load(open(puzzles_path, "rb"))
            align_list = list(SeqIO.parse(aln_file, "fasta"))
            align_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))
            base_results = pickle.load(open("results_test.pickle", "rb"))
        except FileNotFoundError:
            raise

        consensus = ar.assess_column_conservation(align_list)

        # Test base case
        results = ar.analyze_results(filter_params, puzzle_params, [annotate_output_path], puzzles,
                                        consensus, align_dict)

        for key in results["merged_realign_df"].keys():
            self.assert_frame_equal(results["merged_realign_df"][key], base_results["merged_realign_df"][key])
            self.assert_frame_equal(results["merged_gap_df"][key], base_results["merged_gap_df"][key])
            self.assert_frame_equal(results["merged_guide_df"][key], base_results["merged_guide_df"][key])
            self.assert_frame_equal(results["mapped_guide_df"][key], base_results["mapped_guide_df"][key])

        # Test different puzzle percent leads to different results
        puzzle_params["rows_to_inspect"]["0"] = {
            "1": [0, 1, 2, 3, 4],
            "2": [0, 1, 2, 3, 4, 5],
            "3": [0, 1, 2, 3, 4, 5, 6, 7, 8],
            "4": [0, 1, 2, 3, 4, 5, 6, 7, 8],
            "5": [0, 1, 2, 3, 4, 5, 6, 7, 8],
            "6": [0, 1, 2, 3, 4, 5, 6, 7, 8],
            "7": [0, 1, 2, 3, 4, 5, 6, 7, 8],
            "8": [0, 1, 2, 3, 4, 5, 6, 7, 8],
            "9": [0, 1, 2, 3, 4, 5, 6, 7, 8]
            }
        results = ar.analyze_results(filter_params, puzzle_params, [annotate_output_path], puzzles,
                                        consensus, align_dict)
        for key in results["merged_realign_df"].keys():
            self.assert_frame_not_equal(results["merged_realign_df"][key], base_results["merged_realign_df"][key])
            self.assert_frame_not_equal(results["merged_gap_df"][key], base_results["merged_gap_df"][key])
            self.assert_frame_not_equal(results["merged_guide_df"][key], base_results["merged_guide_df"][key])
            self.assert_frame_not_equal(results["mapped_guide_df"][key], base_results["mapped_guide_df"][key])

        # Test different pareto distance leads to different results
        filter_params['Value'] = [2.5] 
        puzzle_params["rows_to_inspect"]["0"] = {
            "1": [0, 1, 2, 3],
            "2": [0, 1, 2, 3],
            "3": [0, 1, 2, 3, 4, 5],
            "4": [0, 1, 2, 3, 4, 5],
            "5": [0, 1, 2, 3, 4, 5],
            "6": [0, 1, 2, 3, 4, 5],
            "7": [0, 1, 2, 3, 4, 5],
            "8": [0, 1, 2, 3, 4, 5],
            "9": [0, 1, 2, 3, 4, 5]
            }
        results = ar.analyze_results(filter_params, puzzle_params, [annotate_output_path], puzzles,
                                        consensus, align_dict)
        for key in results["merged_realign_df"].keys():
            self.assert_frame_not_equal(results["merged_realign_df"][key], base_results["merged_realign_df"][key])
            self.assert_frame_not_equal(results["merged_gap_df"][key], base_results["merged_gap_df"][key])
            self.assert_frame_not_equal(results["merged_guide_df"][key], base_results["merged_guide_df"][key])
            self.assert_frame_not_equal(results["mapped_guide_df"][key], base_results["mapped_guide_df"][key])
