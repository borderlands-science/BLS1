__author__ = "Roman"
"""
This script takes as input the output from incremental_batch which is a pickle of the IDs of solutions for each puzzle,
and grabs the actual solutions (strings) associated with those. Process the output of this with analyze_results_forRealign.py
IMPORTANT NOTE: this script can not be run outside our private servers.
"""

import pickle
from dotenv import load_dotenv
load_dotenv(".env")
from phylox_analysis import db, models, api
import ast


# NOVEMBER RESULTS
# Note: This files are NOT provided.
resultsA = pickle.load(open("FEB15_NEW_ALL1ALNRESULTS.pickle", "rb"))
resultsB = pickle.load(open("FEB15_NEW_ALL2ALNRESULTS.pickle", "rb"))
resultsC = pickle.load(open("FEB15_NEW_ALL3ALNRESULTS.pickle", "rb"))
resultsD = pickle.load(open("FEB15_NEW_ALL4ALNRESULTS.pickle", "rb"))
resultsE = pickle.load(open("FEB15_NEW_ALL5ALNRESULTS.pickle", "rb"))
resultsF = pickle.load(open("FEB15_NEW_ALL6ALNRESULTS.pickle", "rb"))

sol_dict = {}
densities = [0] * 186
solsToGet = []

pr = {}
for res in [resultsA, resultsB, resultsC, resultsD, resultsE, resultsF]:  # adjust this list
    for i in sorted(res.keys()):
        print(i)
        pos = ast.literal_eval(i)
        for sol in res[i]:
            for solID in sol[5]:
                solsToGet.append(solID)
toGet = list(set(solsToGet))
print(len(toGet))

chunks = [toGet[x:x + 10000] for x in range(0, len(toGet), 10000)]

solutions = []
session = db.get_session()
for t in chunks:
    s = session.query(models.Solution).filter(models.Solution.id.in_(t)).all()
    solutions += s
allsols = solutions
print("NUMBER OF ALLSOLS", len(allsols))
solutions = {}
for s in allsols:
    iD = s.id
    solutions[iD] = s

noProb = 0
prob = 0
count = 0
for res in [resultsA, resultsB, resultsC, resultsD, resultsE, resultsF]:  # adjust this list
    for i in sorted(res.keys()):
        pos = ast.literal_eval(i)
        if len(pos) > 0:
            if str(pos) not in pr:
                pr[str(pos)] = []
            for sol in res[i]:
                this_sol = {"originalCode": sol[0], "pikdik": sol[1], "nGaps": sol[3], "score": sol[4],
                            "pareto": sol[2], "playerSolutions": [], "playerIDs": []}
                for solID in sol[5]:
                    if solID in solutions:
                        this_sol["playerSolutions"].append(solutions[solID].solution)
                        this_sol["playerIDs"].append(solutions[solID].player_id)
                        count += 1
                    else:
                        prob += 1
                        print("ERROR")
                pr[str(pos)].append(this_sol)
print("NEW DICTIONARY", pr.keys())
for i in pr.keys():
    print(i, len(pr[i]))
pickle.dump(pr, open("/data/10M_solutions_novfeb.pickle", "wb"))
print("NUMBER OF KEYS IN DICT", len(pr.keys()))
print("no problem:", noProb, "problem", prob, "total correctly processed", count)
