import copy
import unittest
from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle
from processing_scripts.dnapuzzle import Puzzle as DnaPuzzle


class TestPuzzle(unittest.TestCase):
    puzzle = ['-ACCT--G', 'GCCUG---', 'GGCTG---',
              'AAACG---', 'GAGTA---', 'G-C-g-GG']
    consensus = [('G', 'T'), ('A', 'T'), ('T', 'G'), ('G', 'T'),
                 ('-', 'C'), ('T', 'G'), ('G', 'T'), ('A', 'C')]
    ids = list(range(6))
    test_puzzle = Puzzle(puzzle=puzzle, ids=ids, consensus=consensus)

    def test_empty_init(self):
        # Test that puzzle class can be created without any parameters
        try:
            test_puzzle = Puzzle()
            self.assertEqual(test_puzzle.bonus, 1.15)
        except Exception:
            self.fail("Puzzle() raised an exception unexpectedly")

    def test_init(self):
        # Test that puzzle can be created with parameters and properly sets them
        puzzle = copy.deepcopy(self.test_puzzle)
        self.assertEqual(puzzle.uncollapsed_puzzle[3], 'AAACG---')

        # Test that incomplete scoring scheme will use default parameters instead
        parms = {'match': 5}
        parm_puzzle = Puzzle(parms=parms)
        self.assertEqual(parm_puzzle.match, 1)
        self.assertEqual(parm_puzzle.bonus, 1.15)

        # Test that valid scoring scheme will update scores
        parms = {'match': 2, 'mismatch': -1, 'gap_open': -5, 'gap_extension': -2, 'bonus': 5}
        parm_puzzle = Puzzle(parms=parms)
        self.assertEqual(parm_puzzle.match, 2)
        self.assertEqual(parm_puzzle.mismatch, -1)
        self.assertEqual(parm_puzzle.gap_open, -5)
        self.assertEqual(parm_puzzle.gap_extension, -2)
        self.assertEqual(parm_puzzle.bonus, 5)

    def test_collapse(self):
        # Test that collapse function properly collapses strings and nothing else
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.collapse()
        self.assertEqual(puzzle.puzzle[0], 'ACCTG---')
        self.assertEqual(puzzle.puzzle[1], 'GCCTG---')
        self.assertEqual(puzzle.puzzle[-1], 'GCGGG---')
        self.assertEqual(puzzle.uncollapsed_puzzle[0], '-ACCT--G')
        self.assertEqual(puzzle.uncollapsed_puzzle[1], 'GCCTG---')
        self.assertEqual(puzzle.uncollapsed_puzzle[-1], 'G-C-G-GG')

        # Test that collapse properly removes and adds gaps for sequences not at the same length as consensus
        puzzle.puzzle = ['ACTGACTG---', 'ACTG--ACTGA-', '-----ACTG--', 'AAAA', 'AAAAAAAAAA']
        puzzle.collapse()
        self.assertEqual(puzzle.puzzle[0], 'ACTGACTG')
        self.assertEqual(puzzle.puzzle[1], 'ACTGACTG')
        self.assertEqual(puzzle.puzzle[2], 'ACTG----')
        self.assertEqual(puzzle.puzzle[3], 'AAAA----')
        self.assertEqual(puzzle.puzzle[4], 'AAAAAAAA')

    def test_clean(self):
        # Test that clean collapses, sets everything to upper, and replaces appropriate characters
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.puzzle = ['-ACCT--G', 'GCCUG---', 'G-C-g-GG', 'AxGyczu-']
        puzzle.clean_puzzle()
        self.assertEqual(puzzle.puzzle[0], '-ACCT--G')
        self.assertEqual(puzzle.puzzle[1], 'GCCTG---')
        self.assertEqual(puzzle.puzzle[2], 'G-C-G-GG')
        self.assertEqual(puzzle.puzzle[3], 'A-G-C-T-')

    def test_copy(self):
        # Test that deep copy ensures no changes in old puzzle
        puzzle = copy.deepcopy(self.test_puzzle)
        self.assertNotEqual(puzzle.puzzle[0], puzzle.uncollapsed_puzzle[0])

    def test_consensus(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that score_consensus can properly score
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 13)

        # Test that score_consensus can handle an empty puzzle
        puzzle.puzzle = []
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 0)

        # Test that score_consensus can handle sequences longer than consensus
        puzzle.puzzle = ['--------ACT']
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 0)

        # Test that score_consensus can handle varying sequence lengths
        puzzle.puzzle = ['ACT', 'ACTG']
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 3.3)

    def test_count_gaps(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test count can handle no gaps
        gaps = puzzle.count_gaps(puzzle.puzzle)
        self.assertEqual(gaps, 0)

        # Test gaps are counted correctly
        puzzle.puzzle[0] = '-ACCT--G'
        gaps = puzzle.count_gaps(puzzle.puzzle)
        self.assertEqual(gaps, 3)
        puzzle.collapse()

        # Test count can handle empty puzzle
        puzzle.puzzle = []
        gaps = puzzle.count_gaps(puzzle.puzzle)
        self.assertEqual(gaps, 0)

    def test_from_dnapuzzle(self):
        puzzle = ['-ACCT--G', 'GCCUG---', 'GGCTG---',
                  'AAACG---', 'GAGTA---', 'G-C-g-GG']
        consensus = [('G', 'T'), ('A', 'T'), ('T', 'G'), ('G', 'T'),
                     ('-', 'C'), ('T', 'G'), ('G', 'T'), ('A', 'C')]
        ids = list(range(6))
        parms = {'match': 2, 'mismatch': -1, 'gap_open': -5, 'gap_extension': -2, 'bonus': 5}
        dna_puzzle = DnaPuzzle(puzzle=puzzle, consensus=consensus, IDs=ids)
        dna_puzzle.collapse()

        # Test that conversion will fail if not enough information is present in the dna puzzle
        with self.assertRaises(AttributeError):
            Puzzle.from_dnapuzzle(dna_puzzle, parms)

        # Test that conversion from dna puzzle to new format will succeed
        dna_puzzle.set_uniqueID(None)
        dna_puzzle.add_solution(puzzle, 0, 0, 0)
        new_puzzle = Puzzle.from_dnapuzzle(dna_puzzle, parms)
        self.assertEqual(new_puzzle.uncollapsed_puzzle[-1], 'G-C-G-GG')
        self.assertEqual(new_puzzle.puzzle[-1], 'GCGGG---')
        self.assertEqual(new_puzzle.consensus, consensus)
        self.assertEqual(new_puzzle.ids, ids)
        self.assertEqual(new_puzzle.bonus, 5)

    def test_to_dnapuzzle(self):
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.puzzle_sol = puzzle.puzzle
        dna_puzzle = puzzle.to_dnapuzzle()

        # Test to ensure conversion copies everything over successfully
        self.assertEqual(dna_puzzle.puzzle[0], ['A', 'C', 'C', 'T', 'G', '-', '-', '-'])
        self.assertEqual(dna_puzzle.greedy_sol[0], ['A', 'C', 'C', 'T', 'G', '-', '-', '-'])
        self.assertEqual(dna_puzzle.uncollapsed_puzzle[0], ['-', 'A', 'C', 'C', 'T', '-', '-', 'G'])
        self.assertEqual(len(dna_puzzle.playerResults), 3)


if __name__ == '__main__':
    unittest.main()
