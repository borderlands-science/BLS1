import json
import unittest
import processing_scripts.puzzle_builder.soft_window_puzzles as sp
import processing_scripts.puzzle_builder.hard_window_puzzles as hp

class TestLists(unittest.TestCase):
    test_file = "'../../alignments/test_alignment.fasta"

    def setUp(self):
        test_parms_loc = "../puzzle_parameters/other_parms/test_parms.json"
        with open(test_parms_loc) as test_parms_file:
            self.test_parms = json.load(test_parms_file)
        self.test_parms['offset'] = 0
        self.test_parms['flank_size'] = 0

    def test_no_list(self):
        self.test_parms['use_blacklist'] = False
        self.test_parms['use_whitelist'] = False

        # Test hard base case with no blacklist or whitelist
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 4)

        self.test_parms['target_builder'] = 'soft'
        # Test soft base case with no blacklist or whitelist
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 4)


    def test_hard_blacklist(self):
        # Test that blacklist will not be used if bool 'use_blacklist' in parms is false
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [0, 1, 2], [])
        self.assertEqual(len(sorted_tuples), 4)

        # Test using blacklist restricts results to only 1
        self.test_parms['use_blacklist'] = True
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [2], [])
        self.assertEqual(len(sorted_tuples), 1)

        # Test case where no windows are possible
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [3], [])
        self.assertEqual(len(sorted_tuples), 0)

    def test_soft_blacklist(self):
        self.test_parms['target_builder'] = 'soft'
        # Test that blacklist will not be used if bool 'use_blacklist' in parms is false
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [0, 1, 2], [])
        self.assertEqual(len(sorted_tuples), 4)

        # Test case where no windows are possible
        self.test_parms['use_blacklist'] = True
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [0, 1, 2, 3], [])
        self.assertEqual(len(sorted_tuples), 0)

    def test_hard_whitelist(self):
        # Test that blacklist will not be used if bool 'use_whitelist' in parms is false
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [0, 1, 2, 3])
        self.assertEqual(len(sorted_tuples), 4)

        # Test using whitelist restricts results to only 1
        self.test_parms['use_whitelist'] = True
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [0, 1, 2, 3])
        self.assertEqual(len(sorted_tuples), 1)

        # Test using whitelist restricts results to only 2 overlapping
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [0, 1, 2, 3, 4])
        self.assertEqual(len(sorted_tuples), 2)

        # Test using whitelist restricts results to only 2 distinct
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [],
                                        [0, 1, 2, 3, 5, 6, 7, 8])
        self.assertEqual(len(sorted_tuples), 2)

        # Test gapped whitelist return nothing
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [],
                                        [0, 2, 4, 6, 8])
        self.assertEqual(len(sorted_tuples), 0)

        # Test case where no windows are possible
        sorted_tuples = hp.find_hard_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [3], [])
        self.assertEqual(len(sorted_tuples), 0)

    def test_soft_whitelist(self):
        self.test_parms['target_builder'] = 'soft'
        # Test that blacklist will not be used if bool 'use_whitelist' in parms is false
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [0, 1, 2, 3])
        self.assertEqual(len(sorted_tuples), 4)

        # Test using whitelist restricts results to only 1
        self.test_parms['use_whitelist'] = True
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [2, 3, 4, 5])
        self.assertEqual(len(sorted_tuples), 1)

        # Test using whitelist restricts results to only 2 overlapping
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [], [0, 1, 2, 3, 4])
        self.assertEqual(len(sorted_tuples), 2)

        # Test using whitelist restricts results to only 2 distinct
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [],
                                        [0, 1, 2, 3, 5, 6, 7, 8])
        self.assertEqual(len(sorted_tuples), 2)

        # Test gapped whitelist will return no windows
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [],
                                        [0, 2, 4, 6, 8])
        self.assertEqual(len(sorted_tuples), 0)

        # Test case where no windows are possible
        sorted_tuples = sp.find_soft_windows([0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8], 4, self.test_parms, [4], [])
        self.assertEqual(len(sorted_tuples), 0)