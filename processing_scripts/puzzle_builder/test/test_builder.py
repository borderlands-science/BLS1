import json
import math
import unittest
import processing_scripts.puzzle_builder.build_puzzles as bp
import processing_scripts.puzzle_builder.puzzle_functions as pf
import processing_scripts.puzzle_builder.soft_window_puzzles as sp
import processing_scripts.puzzle_builder.hard_window_puzzles as hp
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


class TestBuilder(unittest.TestCase):
    test_file = "'../../alignments/test_alignment.fasta"

    def setUp(self):
        test_parms_loc = "../puzzle_parameters/other_parms/test_parms.json"
        with open(test_parms_loc) as test_parms_file:
            self.test_parms = json.load(test_parms_file)
        self.test_parms['flank_size'] = 0
        self.test_parms['offset'] = 0

    def test_consensus(self):
        # Test that in the event there's only 1 nucleotide that is what will be returned
        nucs = ['-', 'A', 'C', 'G', 'T']
        column = [0, 3, 0, 0, 0]
        consensus = bp.get_consensus(column, nucs)
        self.assertEqual(consensus, ('A', '-'))

        # Test that it returns the top two nucleotides
        column = [0, 3, 0, 0, 2]
        consensus = bp.get_consensus(column, nucs)
        self.assertEqual(consensus, ('A', 'T'))

        # Test it does not just get the first two nucleotides found
        column = [0, 3, 0, 1, 2]
        consensus = bp.get_consensus(column, nucs)
        self.assertEqual(consensus, ('A', 'T'))

        # Test that it can handle an empty column
        column = [0, 0, 0, 0, 0]
        consensus = bp.get_consensus(column, nucs)
        self.assertEqual(consensus, ('-', '-'))

    def test_column_conservation(self):
        sequences = None
        with self.assertRaises(ValueError):
            bp.assess_column_conservation(sequences, self.test_parms['target_builder'])
        sequences = []
        with self.assertRaises(ValueError):
            bp.assess_column_conservation(sequences, self.test_parms['target_builder'])


        sequences = [SeqRecord(Seq("AAAAA")), SeqRecord(Seq("ACCCA")), SeqRecord(Seq("ACGGA")), SeqRecord(Seq("TTTGA"))]

        # Test that consensus returned is all correct, and scores are accurate
        scores, consensus = bp.assess_column_conservation(sequences, self.test_parms['target_builder'])
        self.assertEqual(consensus[0], ('A', 'T'))
        self.assertEqual(consensus[1][0], 'C')
        self.assertEqual(consensus[-1], ('A', '-'))
        self.assertEqual(scores[0], 0.625)

        # Test that invalid nucleotides do not impact results
        sequences = [SeqRecord(Seq("AAAyx")), SeqRecord(Seq("ACCyx")), SeqRecord(Seq("ACGyx")), SeqRecord(Seq("TTTyA"))]
        scores, consensus = bp.assess_column_conservation(sequences, self.test_parms['target_builder'])
        self.assertEqual(consensus[3], ('-', '-'))
        self.assertEqual(consensus[4], ('A', '-'))

    def test_hard_windows(self):
        # 5 possible windows. Assert that the last window does not meet the threshold
        sorted_tuples = hp.find_hard_windows([1.0, 0.75, 0.50, 0.25, 1.0, 1.0, 1.0], 3, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 4)

        # Test that setting an offset works, and catches edge cases
        self.test_parms['flank_size'] = 0
        self.test_parms['offset'] = 2
        sorted_tuples = hp.find_hard_windows([1.0, 0.75, 0.50, 0.25, 0.5, 0.5, 0.5], 3, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 5)

        # Test that setting a flank works, and catches edge cases
        self.test_parms['flank_size'] = 2
        self.test_parms['offset'] = 0
        sorted_tuples = hp.find_hard_windows([1.0, 0.75, 0.50, 0.25, 0.5, 0.5, 0.5], 3, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 5)

        # Test that all edge cases are met for given both offset and flank
        self.test_parms['flank_size'] = 2
        self.test_parms['offset'] = 1
        sorted_tuples = hp.find_hard_windows([1.0, 0.75, 0.50, 0.25, 0.5, 0.5, 0.5], 3, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 3)

        # Test that if flank and offset leave no room for a window none will be generated
        self.test_parms['flank_size'] = 5
        self.test_parms['offset'] = 5
        sorted_tuples = hp.find_hard_windows([1.0, 0.75, 0.50, 0.25, 0.5, 0.5, 0.5], 3, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 0)

        # Test that if sequence is shorter than window size no windows will be generated
        self.test_parms['flank_size'] = 0
        self.test_parms['offset'] = 0
        sorted_tuples = hp.find_hard_windows([1.0, 0.75, 0.50, 0.25, 0.5, 0.5, 0.5], 10, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 0)

        # Test that find_windows correctly returns starting points for soft windows
        self.test_parms['flank_size'] = 0
        self.test_parms['offset'] = 0
        self.test_parms['target_builder'] = 'soft'
        sorted_tuples = sp.find_soft_windows([1.0, 0.75, 0.50, 0.25, 1.0, 1.0, 1.0], 3, self.test_parms, [], [])
        self.assertEqual(len(sorted_tuples), 4)

    def test_make_puzzles(self):
        puzzle_states = [(['AAAA---', 'CC-GG--', 'GGGG---', 'AATT---', 'CCCC---', 'GGGT---'],
                          [('T', '-'), ('T', '-'), ('T', '-'), ('T', '-'),
                           ('T', '-'), ('T', '-'), ('T', '-'), ('T', '-')]),
                         (['AAAA---', 'GG-CC--', 'GGGG---', 'TTAA---', 'CCCC---', 'TGGG---'],
                          [('T', '-'), ('T', '-'), ('T', '-'), ('T', '-'),
                           ('T', '-'), ('T', '-'), ('T', '-'), ('T', '-')])
                         ]
        puzzle_ids = ['0', '1', '2', '3', '4', '5']
        puzzle_cols = [([2, 3, 4, 5, 6, 7, 8], [7, 8]), ([6, 5, 4, 3, 2, 1, 0], [1, 0])]

        # Test that 2 greedy puzzles were successfully created
        puzzles = pf.make_puzzles(puzzle_states, puzzle_ids, puzzle_cols, 1, {'bonus': 1.15, 'puzzle_type': 0})
        self.assertEqual(len(puzzles), 2)

        # Test that 2 pairwise puzzles were successfully created
        puzzles = pf.make_puzzles(puzzle_states, puzzle_ids, puzzle_cols, 1, {'bonus': 1.15, 'puzzle_type': 1})
        self.assertEqual(len(puzzles), 2)

        # Test that 4 puzzles were successfully created
        puzzles = pf.make_puzzles(puzzle_states, puzzle_ids, puzzle_cols, 1, {'bonus': 1.15, 'puzzle_type': -1})
        self.assertEqual(len(puzzles), 4)

        # Test that all puzzles uncollapsed states are correct
        self.assertEqual(puzzles[0].uncollapsed_puzzle[1], 'CC-GG--')
        self.assertEqual(puzzles[1].uncollapsed_puzzle[1], 'GG-CC--')
        self.assertEqual(puzzles[2].uncollapsed_puzzle[1], 'CC-GG--')
        self.assertEqual(puzzles[3].uncollapsed_puzzle[1], 'GG-CC--')

    def test_seq_distance(self):
        seq1 = 'AAAA---'
        seq2 = 'CCCC---'
        seq3 = 'ACAC--C'

        # Test identical sequences, and ensure it ignores gaps
        score = pf.get_seq_distance(seq1, seq1)
        self.assertEqual(score, -4)

        # Test different sequences score properly
        score = pf.get_seq_distance(seq1, seq2)
        self.assertEqual(score, 8)

        # Test summation of score works fine, and comparisons between gaps and nucleotides work
        score = pf.get_seq_distance(seq1, seq3)
        self.assertEqual(score, 4)

        # Test it can handle empty strings and returns -2000000
        score = pf.get_seq_distance('', seq1)
        self.assertEqual(score, -2000000)

    def test_freq_cols(self):
        coverage = [0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8]
        column_range = 3

        # Test overly frequent columns are caught
        overly_freq_cols = bp.get_freq_cols(coverage, column_range, [])
        self.assertEqual(len(overly_freq_cols), 5)
        self.assertEqual(overly_freq_cols, [6, 7, 8, 9, 10])

        # Test windows meeting requirements exclude entire window, not just individual nucleotides
        overly_freq_cols = bp.get_freq_cols(coverage, 6, [])
        self.assertEqual(len(overly_freq_cols), 7)

        # Test blacklist is appropriately added to overly_freq_cols
        coverage = [0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8]
        overly_freq_cols = bp.get_freq_cols(coverage, column_range, [0, 1, 2])
        self.assertEqual(len(overly_freq_cols), 7)
        self.assertEqual(overly_freq_cols, [0, 1, 2, 7, 8, 9, 10])

        # Test no columns will be added if none meet the requirements
        coverage = [1, 1, 1, 1]
        overly_freq_cols = bp.get_freq_cols(coverage, 2, [])
        self.assertEqual(len(overly_freq_cols), 0)

    def test_prepare_sequences(self):
        sequence = 'A-GTAC-T'
        width = len(sequence)
        prep_seq, prep_flipped = pf.prepare_sequences(sequence, width)

        # Test that preparing a sequence will leave it uncollapsed
        self.assertEqual(prep_seq[:4], 'A-GT')
        self.assertEqual(prep_flipped[:4], 'T-CA')

        # Run 100 times and verify that the random length will always leave at least half the nucleotides untouched
        for i in range(100):
            prep_seq, prep_flipped = pf.prepare_sequences(sequence, width)
            self.assertGreaterEqual(math.ceil(width/2.0), prep_seq.count('-'))
            self.assertGreaterEqual(math.ceil(width / 2.0), prep_flipped.count('-'))
