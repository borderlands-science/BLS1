import copy
import unittest
import numpy as np
from processing_scripts.puzzle_builder.puzzle.pairwise_puzzle import PairwisePuzzle


class TestPuzzle(unittest.TestCase):
    puzzle = ['-ACCT--G', 'GCCUG---', 'GGCTG---',
              'AAACG---', 'GAGTA---', 'G-C-g-GG']
    consensus = [('G', 'T'), ('A', 'T'), ('T', 'G'), ('G', 'T'),
                 ('-', 'C'), ('T', 'G'), ('G', 'T'), ('A', 'C')]
    ids = list(range(6))
    test_puzzle = PairwisePuzzle(puzzle=puzzle, ids=ids, consensus=consensus)

    def test_empty_init(self):
        # Test that puzzle class can be created without any parameters
        try:
            test_puzzle = PairwisePuzzle()
            self.assertEqual(test_puzzle.bonus, 1.15)
        except Exception:
            self.fail("Puzzle() raised an exception unexpectedly")

    def test_good_calc_base(self):
        # Test that base score calculation works
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.calc_prior_puzzle()
        self.assertEqual(puzzle.base_score, 13)
        self.assertEqual(puzzle.prior_score, 8.4)

    def test_uncollapsed(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that setup did not collapse prior to making a copy of original
        self.assertEqual(puzzle.uncollapsed_puzzle[0], '-ACCT--G')
        self.assertEqual(puzzle.uncollapsed_puzzle[-1], 'G-C-G-GG')

        # Test that solution leaves uncollapsed puzzle untouched
        puzzle.calc_solution()
        self.assertEqual(puzzle.uncollapsed_puzzle[0], '-ACCT--G')
        self.assertEqual(puzzle.uncollapsed_puzzle[-1], 'G-C-G-GG')

    def test_bad_calc_base(self):
        # Test that a puzzle with no alignment gives no score
        puzzle = ['AAAA---', 'CCCC---', 'GGGG---',
                  'AAAA---', 'CCCC---', 'GGGG---']
        consensus = [('T', '-'), ('T', '-'), ('T', '-'), ('T', '-'),
                     ('T', '-'), ('T', '-'), ('T', '-'), ('T', '-')]
        test_puzzle = PairwisePuzzle(puzzle=puzzle, ids=self.ids, consensus=consensus)
        self.assertEqual(test_puzzle.base_score, 0)

        # Test that base calc can handle empty puzzle
        test_puzzle.puzzle = []
        self.assertEqual(test_puzzle.base_score, 0)

        # Test that puzzle can handle sequences longer than consensus and won't score them
        puzzle[0] = 'AAAA----TTT'
        test_puzzle.puzzle = puzzle
        self.assertEqual(test_puzzle.base_score, 0)

    def test_good_pairwise_sol(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that pairwise solution gives a puzzle and score better than the original
        puzzle.calc_solution()
        self.assertGreater(puzzle.sol_score, puzzle.base_score)
        self.assertGreaterEqual(len(puzzle.sol_steps), 3)
        self.assertNotEqual(puzzle.puzzle, puzzle.puzzle_sol)

    def test_column_align(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that a column will properly align with the consensus
        column = 'AGGGG---'
        result = puzzle.get_best_col_align(column)
        self.assertEqual(result, '-AGG-GG-')

        # Test that a section won't align if the gap penalty outweighs the bonus
        column = 'GAA-----'
        result = puzzle.get_best_col_align(column)
        self.assertEqual(result, 'GAA-----')

        # Test that if given multiple options, it will align with the best given option
        column = 'TTTT----'
        result = puzzle.get_best_col_align(column)
        self.assertEqual(result, 'TTTT----')

        # Test if no good alignment found, it will remain unchanged
        column = 'CTA-----'
        result = puzzle.get_best_col_align(column)
        self.assertEqual(result, 'CTA-----')

        # Test that it can handle an empty column
        column = ''
        result = puzzle.get_best_col_align(column)
        self.assertEqual(result, '--------')

    def test_traceback(self):
        puzzle = copy.deepcopy(self.test_puzzle)
        matrix = np.zeros((5, 5), np.float)

        # Test an empty matrix will leave the column untouched
        result = puzzle.traceback(matrix, 'AGCT')
        self.assertEqual(result, 'AGCT')

        # Test that gaps will be added properly
        matrix = np.zeros((5, 3), np.float)
        matrix[4][2] = 1.0
        matrix[3][2] = 1.0
        result = puzzle.traceback(matrix, 'AG')
        self.assertEqual(result, 'AG--')

        matrix[3][2] = 0.0
        matrix[2][1] = 1.0
        result = puzzle.traceback(matrix, 'AG')
        self.assertEqual(result, 'A-G-')

        matrix[4][2] = 0.0
        matrix[2][1] = 0.0
        matrix[1][0] = 1.0
        matrix[2][0] = 1.0
        result = puzzle.traceback(matrix, 'AG')
        self.assertEqual(result, '--AG')



if __name__ == '__main__':
    unittest.main()
