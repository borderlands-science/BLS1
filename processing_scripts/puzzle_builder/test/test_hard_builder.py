import json
import unittest
import processing_scripts.puzzle_builder.hard_window_puzzles as hw
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


class TestBuilder(unittest.TestCase):
    test_file = "'../../alignments/test_alignment.fasta"

    def setUp(self):
        test_parms_loc = "../puzzle_parameters/other_parms/test_parms.json"
        with open(test_parms_loc) as test_parms_file:
            self.test_parms = json.load(test_parms_file)

    def test_alignment_columns(self):
        sequences = [SeqRecord(Seq("AAAAA"), id="0"), SeqRecord(Seq("ACCCA"), id="1"),
                     SeqRecord(Seq("ACGGA"), id="2"), SeqRecord(Seq("TTTGA"), id="3")]
        # Test alignment_columns works as intended
        results = hw.get_alignment_columns(sequences, 0, 5)
        self.assertEqual(len(results), 4)
        self.assertEqual(results[0][0], "AAAAA")

        # Test setting start/end work as intended
        results = hw.get_alignment_columns(sequences, 1, 3)
        self.assertEqual(results[1][0], "CC")

        # Test invalid start/end reset to 0 end length of sequence
        results = hw.get_alignment_columns(sequences, -1, 10)
        self.assertEqual(results[0][0], "AAAAA")

        # Test that sequences with 2 identical ids will not be inserted twice
        sequences = [SeqRecord(Seq("TTTGA"), id="0"), SeqRecord(Seq("ACCCA"), id="1"),
                     SeqRecord(Seq("ACGGA"), id="2"), SeqRecord(Seq("TTTGA"), id="0")]
        results = hw.get_alignment_columns(sequences, 0, 5)
        self.assertEqual(len(results), 3)

    def test_puzzle_states(self):
        sequences = [('AAAAA', 'AAAAA', '0'), ('ACCCA', 'ACCCA', '1'), ('ACGGA', 'AGGCA', '2'), ('TTTGA', 'AGTTT', '3')]
        consensus = [('A', '-'), ('C', '-'), ('A', 'T'), ('C', 'G'), ('A', 'T'),
                     ('G', 'C'), ('A', '-'), ('G', '-'), ('T', '-')]

        # Test that basic states without offset or flank work
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 0, 0, [2, 7], -1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', 'T'), ('C', 'G'), ('A', 'T'), ('G', 'C'), ('A', '-')])
        self.assertEqual(puzzle_states[1][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[1][1], [('A', '-'), ('G', 'C'), ('A', 'T'), ('C', 'G'), ('A', 'T')])

        # Test that offset works properly
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 0, 2, [2, 7], -1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', '-'), ('C', '-'), ('A', 'T'), ('C', 'G'), ('A', 'T')])
        self.assertEqual(puzzle_states[1][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[1][1], [('T', '-'), ('G', '-'), ('A', '-'), ('G', 'C'), ('A', 'T')])

        # Test that with offset only one value will be returned at the ends
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 0, 2, [0, 5], -1)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[0][1], [('A', '-'), ('G', 'C'), ('A', 'T'), ('C', 'G'), ('A', 'T')])
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 0, 2, [4, 9], -1)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', 'T'), ('C', 'G'), ('A', 'T'), ('G', 'C'), ('A', '-')])

        # Test forward direction excludes mirrored state
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 0, 0, [2, 7], 0)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', 'T'), ('C', 'G'), ('A', 'T'), ('G', 'C'), ('A', '-')])
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 2, 0, [3, 8], 0)
        self.assertEqual(len(puzzle_states), 0)

        # Test backward direction excludes normal state
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 0, 0, [2, 7], 1)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[0][1], [('A', '-'), ('G', 'C'), ('A', 'T'), ('C', 'G'), ('A', 'T')])
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 2, 0, [1, 6], 1)
        self.assertEqual(len(puzzle_states), 0)

        # Test that an offset value too high will result in no states
        puzzle_states = hw.get_puzzle_states(sequences, consensus, 0, 10, [0, 5], -1)
        self.assertEqual(len(puzzle_states), 0)

    def test_puzzle_cols(self):
        # Test puzzle_cols without flank size
        puzzle_cols = hw.get_puzzle_cols([0, 3], 0, -1)
        self.assertEqual(puzzle_cols[0][0], [0, 1, 2])
        self.assertEqual(puzzle_cols[0][1], [])
        self.assertEqual(puzzle_cols[1][0], [2, 1, 0])
        self.assertEqual(puzzle_cols[1][1], [])

        # Test puzzle_cols with flank
        puzzle_cols = hw.get_puzzle_cols([0, 3], 2, -1)
        self.assertEqual(puzzle_cols[0][0], [0, 1, 2, 3, 4])
        self.assertEqual(puzzle_cols[0][1], [3, 4])
        self.assertEqual(puzzle_cols[1][0], [2, 1, 0, -1, -2])
        self.assertEqual(puzzle_cols[1][1], [-1, -2])

    def test_filter_by_range(self):
        master_seq = ('AAAAAA', '0')
        clustered_seqs = [('CCCCCC', '1'), ('AAACCC', '2'), ('AAAAAC', '3')]
        min_dist = -2
        max_dist = 8
        flank_size = 2

        # Test accepted sequences. First one is too different, second one is fine, last one is too similar
        accepted_seqs = hw.filter_seqs_for_distance_range(master_seq, clustered_seqs, min_dist, max_dist)
        self.assertEqual(len(accepted_seqs), 1)
        self.assertEqual(accepted_seqs[0][0], 'AAACCC')

        # Test if master is empty, should return none
        master_seq = ('', '0')
        accepted_seqs = hw.filter_seqs_for_distance_range(master_seq, clustered_seqs, min_dist, max_dist)
        self.assertEqual(len(accepted_seqs), 0)
