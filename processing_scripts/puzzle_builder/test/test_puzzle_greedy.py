import copy
import unittest
from processing_scripts.puzzle_builder.puzzle.greedy_puzzle import GreedyPuzzle


class TestPuzzle(unittest.TestCase):
    puzzle = ['-ACCT--G', 'GCCUG---', 'GGCTG---',
              'AAACG---', 'GAGTA---', 'G-C-g-GG']
    consensus = [('G', 'T'), ('A', 'T'), ('T', 'G'), ('G', 'T'),
                 ('-', 'C'), ('T', 'G'), ('G', 'T'), ('A', 'C')]
    ids = list(range(6))
    test_puzzle = GreedyPuzzle(puzzle=puzzle, ids=ids, consensus=consensus)

    def test_empty_init(self):
        # Test that puzzle class can be created without any parameters
        try:
            test_puzzle = GreedyPuzzle()
            self.assertEqual(test_puzzle.bonus, 1.15)
        except Exception:
            self.fail("Puzzle() raised an exception unexpectedly")

    def test_good_calc_base(self):
        # Test that base score calculation works
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.calc_prior_puzzle()
        self.assertEqual(puzzle.base_score, 13)
        self.assertEqual(puzzle.prior_score, 8.4)

    def test_bad_calc_base(self):
        # Test that a puzzle with no alignment gives no score
        puzzle = ['AAAA---', 'CCCC---', 'GGGG---',
                  'AAAA---', 'CCCC---', 'GGGG---']
        consensus = [('T', '-'), ('T', '-'), ('T', '-'), ('T', '-'),
                     ('T', '-'), ('T', '-'), ('T', '-'), ('T', '-')]
        test_puzzle = GreedyPuzzle(puzzle=puzzle, ids=self.ids, consensus=consensus)
        self.assertEqual(test_puzzle.base_score, 0)

        # Test that base calc can handle empty puzzle
        test_puzzle.puzzle = []
        self.assertEqual(test_puzzle.base_score, 0)

        # Test that puzzle can handle sequences longer than consensus and won't score them
        puzzle[0] = 'AAAA----TTT'
        test_puzzle.puzzle = puzzle
        self.assertEqual(test_puzzle.base_score, 0)

    def test_good_greedy_sol(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that greedy solution gives a puzzle and score better than the original
        puzzle.calc_solution()
        self.assertGreater(puzzle.sol_score, puzzle.base_score)
        self.assertGreaterEqual(len(puzzle.sol_steps), 3)
        self.assertNotEqual(puzzle.puzzle, puzzle.puzzle_sol)

    def test_uncollapsed(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that setup did not collapse prior to making a copy of original
        self.assertEqual(puzzle.uncollapsed_puzzle[0], '-ACCT--G')
        self.assertEqual(puzzle.uncollapsed_puzzle[-1], 'G-C-G-GG')

        # Test that solution leaves uncollapsed puzzle untouched
        puzzle.calc_solution()
        self.assertEqual(puzzle.uncollapsed_puzzle[0], '-ACCT--G')
        self.assertEqual(puzzle.uncollapsed_puzzle[-1], 'G-C-G-GG')

    def test_gap_add(self):
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.puzzle_sol = puzzle.puzzle

        # Test that it can properly add a gap
        score, move = puzzle.get_best_gap_add(0, [])
        self.assertGreater(score, 0)
        self.assertNotEqual(move, [])

        # Test that it won't insert a gap if the score cannot exceed the current score
        score, move = puzzle.get_best_gap_add(5000, [])
        self.assertEqual(score, -2000000)
        self.assertEqual(move, [])

        # Test that if the number of gaps causes the score to fall before the current score it won't add a gap
        score, move = puzzle.get_best_gap_add(0, list(range(1000)))
        self.assertEqual(score, -2000000)
        self.assertEqual(move, [])

        # Test that gap can be inserted even if there is no gap at the end
        puzzle.puzzle_sol = ['AAAA', 'CCCC', 'GGGG', 'TTTT']
        score, move = puzzle.get_best_gap_add(0, [])
        self.assertNotEqual(score, -2000000)
        self.assertNotEqual(move, [])

        # Test that function can handle empty puzzle
        puzzle.puzzle_sol = []
        score, move = puzzle.get_best_gap_add(0, [])
        self.assertEqual(score, -2000000)
        self.assertEqual(move, [])

        # Test that puzzle can handle a row longer than the consensus
        puzzle.puzzle_sol = ['ACGTACGT-']
        score, move = puzzle.get_best_gap_add(0, [])
        self.assertGreater(score, 0)
        self.assertNotEqual(move, [])

        # Test that the puzzle will insert a gap in the best position
        puzzle.puzzle_sol = ['ACGT-']
        puzzle.consensus = [('A', '-'), ('G', '-'), ('C', '-'), ('G', '-'),
                            ('T', '-')]
        score, move = puzzle.get_best_gap_add(0, [])
        self.assertEqual(round(score), 4)
        self.assertEqual(move, [0, 1])

    def test_gap_remove(self):
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.puzzle_sol = puzzle.puzzle

        # Test that puzzle won't try to remove a gap if none exist
        score, move = puzzle.get_best_gap_removal(0, [])
        self.assertEqual(score, -2000000)
        self.assertEqual(move, [])

        # Test that gap can be removed if score is improved
        puzzle.puzzle_sol[4] = 'G-AGTA--'
        score, move = puzzle.get_best_gap_removal(0, [[4, 1]])
        self.assertEqual(round(score), 13)
        self.assertEqual(move, [4, 1])

        # Test that puzzle will remove the best gap
        puzzle.puzzle_sol[0] = '-ACCTG--'
        score, move = puzzle.get_best_gap_removal(0, [[0, 0], [4, 1]])
        self.assertEqual(move, [4, 1])

        # Test that puzzle will not remove a gap if it does not improve the score
        puzzle.puzzle_sol[4] = 'GAGTA---'
        score, move = puzzle.get_best_gap_removal(13.4, [[0, 0]])
        self.assertEqual(score, -2000000)
        self.assertEqual(move, [])

    def test_best_shift(self):
        puzzle = copy.deepcopy(self.test_puzzle)
        puzzle.puzzle_sol = puzzle.puzzle

        # Test that puzzle will not shift a gap if none exist
        score, move = puzzle.get_best_gap_shift(0, [])
        self.assertEqual(score, -2000000)
        self.assertEqual(move, [])

        # Test that puzzle can shift a gap into the best position
        puzzle.puzzle_sol[4] = 'G-AGTA--'
        score, move = puzzle.get_best_gap_shift(0, [[4, 1]])
        self.assertEqual(score, 10.4)
        self.assertEqual(move, [4, 1, 2])

        # Test that puzzle can choose the best gap to shift
        puzzle.puzzle_sol[4] = 'G-AGT-A-'
        score, move = puzzle.get_best_gap_shift(0, [[4, 1], [4, 5]])
        self.assertEqual(move, [4, 5, 4])

        # Test that puzzle will not move gap if already in best position
        puzzle.puzzle_sol[4] = 'GA-GTA--'
        score, move = puzzle.get_best_gap_shift(11.4, [[4, 2]])
        self.assertEqual(score, -2000000)
        self.assertEqual(move, [])

    def test_consensus(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that score_consensus can properly score
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 13)

        # Test that score_consensus can handle an empty puzzle
        puzzle.puzzle = []
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 0)

        # Test that score_consensus can handle sequences longer than consensus
        puzzle.puzzle = ['--------ACT']
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 0)

        # Test that score_consensus can handle varying sequence lengths
        puzzle.puzzle = ['ACT', 'ACTG']
        score = puzzle.score_consensus(puzzle.puzzle)
        self.assertEqual(score, 3.3)

    def test_count_gaps(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test count can handle no gaps
        gaps = puzzle.count_gaps(puzzle.puzzle)
        self.assertEqual(gaps, 0)

        # Test gaps are counted correctly
        puzzle.puzzle[0] = '-ACCT--G'
        gaps = puzzle.count_gaps(puzzle.puzzle)
        self.assertEqual(gaps, 3)
        puzzle.collapse()

        # Test count can handle empty puzzle
        puzzle.puzzle = []
        gaps = puzzle.count_gaps(puzzle.puzzle)
        self.assertEqual(gaps, 0)

    def test_gap_score(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test gap score returns correct value
        score = puzzle.get_gap_score(6)
        self.assertEqual(score, -3.6)

    def test_score_rows(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test that individual rows are equal to total score
        scores = puzzle.score_rows(puzzle.puzzle)
        self.assertEqual(sum(scores), 13)

        # Test puzzle ignores rows beyond consensus
        puzzle.puzzle[0] = 'ACCTG---T'
        scores = puzzle.score_rows(puzzle.puzzle)
        self.assertEqual(sum(scores), 13)
        puzzle.consensus.append(('T', '-'))
        scores = puzzle.score_rows(puzzle.puzzle)
        self.assertEqual(sum(scores), 14)

        # Test score rows can handle empty puzzle
        puzzle.puzzle = []
        scores = puzzle.score_rows(puzzle.puzzle)
        self.assertEqual(scores, [])

    def test_insert_gap(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test normal insertion
        column = puzzle.insert_gap('ACCTG---', 0)
        self.assertEqual(column, '-ACCTG--')

        # Test insertion where there is a gap with gap at end
        column = puzzle.insert_gap('ACCTG---', 7)
        self.assertEqual(column, 'ACCTG---')

        # Test inserting a gap with nuc at end
        column = puzzle.insert_gap('ACCTG-T', 6)
        self.assertEqual(column, 'ACCTG--T')

        # Test inserting gap into row shorter than consensus
        column = puzzle.insert_gap('ACCTGAA', 2)
        self.assertEqual(column, 'AC-CTGAA')

        # Test inserting gap into complete column
        column = puzzle.insert_gap('ACGTACGT', 1)
        self.assertEqual(column, 'ACGTACGT')

        # Test inserting gap into column exceeding consensus
        column = puzzle.insert_gap('ACGTACGTACGT', 1)
        self.assertEqual(column, 'ACGTACGTACGT')

    def test_remove_gap(self):
        puzzle = copy.deepcopy(self.test_puzzle)

        # Test normal gap removal
        column = puzzle.remove_gap('-ACCTG--', 0)
        self.assertEqual(column, 'ACCTG---')

        # Test gap removal where there is no gap
        column = puzzle.remove_gap('ACCTGAAA', 5)
        self.assertEqual(column, 'ACCTGAAA')

        # Test gap won't be added at end if column >= consensus
        column = puzzle.remove_gap('A-CCTGAAA', 1)
        self.assertEqual(column, 'ACCTGAAA')

        # Test column length will be fixed on removal when column shorter than consensus
        column = puzzle.remove_gap('A-CCTG', 1)
        self.assertEqual(column, 'ACCTG---')


if __name__ == '__main__':
    unittest.main()
