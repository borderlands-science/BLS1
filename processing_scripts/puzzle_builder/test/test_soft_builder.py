import json
import unittest
import processing_scripts.puzzle_builder.soft_window_puzzles as sw
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


class TestBuilder(unittest.TestCase):
    test_file = "'../../alignments/test_alignment.fasta"

    def setUp(self):
        test_parms_loc = "../puzzle_parameters/other_parms/test_parms.json"
        with open(test_parms_loc) as test_parms_file:
            self.test_parms = json.load(test_parms_file)

    def test_alignment_columns(self):
        sequences = [SeqRecord(Seq("AAAAA"), id="0"), SeqRecord(Seq("ACCCA"), id="1"),
                     SeqRecord(Seq("ACGGA"), id="2"), SeqRecord(Seq("TTTGA"), id="3")]
        map = [[0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [1, 2, 3, 4, 5]]
        # Test alignment_columns works as intended
        seq_results, ref_results = sw.get_alignment_columns(5, sequences, map, 0, 5)
        self.assertEqual(len(seq_results), 4)
        self.assertEqual(''.join(list(seq_results[0][0].values())), "AAAAA")
        self.assertEqual(len(ref_results), 3)

        # Test setting start/end work as intended
        seq_results, ref_results = sw.get_alignment_columns(5, sequences, map, 1, 2)
        self.assertEqual(''.join(list(seq_results[1][0].values())), "CC")

        # Test invalid start/end reset to 0 end length of sequence
        seq_results, ref_results = sw.get_alignment_columns(5, sequences, map, -1, 10)
        self.assertEqual(''.join(list(seq_results[0][0].values())), "AAAAA")

        # Test that sequences with 2 identical ids will not be inserted twice
        sequences = [SeqRecord(Seq("TTTGA"), id="0"), SeqRecord(Seq("ACCCA"), id="1"),
                     SeqRecord(Seq("ACGGA"), id="2"), SeqRecord(Seq("TTTGA"), id="0")]
        seq_results, ref_results = sw.get_alignment_columns(5, sequences, map, 0, 5)
        self.assertEqual(len(seq_results), 3)


    def test_puzzle_states(self):
        sequences = ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA']
        flipped_seqs = ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT']
        consensus = [('A', '-'), ('C', '-'), ('A', 'T'), ('C', 'G'), ('A', 'T'),
                     ('G', 'C'), ('A', '-'), ('G', '-'), ('T', '-')]

        # Test that basic states without offset or flank work
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 0, 0, [2, 3, 4, 5, 6], -1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', 'T'), ('C', 'G'), ('A', 'T'), ('G', 'C'), ('A', '-')])
        self.assertEqual(puzzle_states[1][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[1][1], [('A', '-'), ('G', 'C'), ('A', 'T'), ('C', 'G'), ('A', 'T')])

        # Test that offset works properly
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 0, 2, [2, 3, 4, 5, 6], -1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', '-'), ('C', '-'), ('A', 'T'), ('C', 'G'), ('A', 'T')])
        self.assertEqual(puzzle_states[1][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[1][1], [('T', '-'), ('G', '-'), ('A', '-'), ('G', 'C'), ('A', 'T')])

        # Test that with offset only one value will be returned at the ends
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 0, 2, [0, 1, 2, 3, 4], -1)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[0][1], [('A', '-'), ('G', 'C'), ('A', 'T'), ('C', 'G'), ('A', 'T')])
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 0, 2, [4, 5, 6, 7, 8], -1)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', 'T'), ('C', 'G'), ('A', 'T'), ('G', 'C'), ('A', '-')])

        # Test forward direction excludes mirrored state
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 0, 0, [2, 3, 4, 5, 6], 0)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'ACGGA', 'TTTGA'])
        self.assertEqual(puzzle_states[0][1], [('A', 'T'), ('C', 'G'), ('A', 'T'), ('G', 'C'), ('A', '-')])
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 2, 0, [3, 4, 5, 6, 7], 0)
        self.assertEqual(len(puzzle_states), 0)

        # Test backward direction excludes normal state
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 0, 0, [2, 3, 4, 5, 6], 1)
        self.assertEqual(len(puzzle_states), 1)
        self.assertEqual(puzzle_states[0][0], ['AAAAA', 'ACCCA', 'AGGCA', 'AGTTT'])
        self.assertEqual(puzzle_states[0][1], [('A', '-'), ('G', 'C'), ('A', 'T'), ('C', 'G'), ('A', 'T')])
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 2, 0, [1, 2, 3, 4, 5], 1)
        self.assertEqual(len(puzzle_states), 0)

        # Test that an offset value too high will result in no states
        puzzle_states = sw.get_puzzle_states(sequences, flipped_seqs, consensus, 0, 10, [0, 1, 2, 3, 4], -1)
        self.assertEqual(len(puzzle_states), 0)

    def test_puzzle_cols(self):
        # Test puzzle_cols without flank size
        puzzle_cols = sw.get_puzzle_cols([0, 1, 2], 0, -1)
        self.assertEqual(puzzle_cols[0][0], [0, 1, 2])
        self.assertEqual(puzzle_cols[1][0], [2, 1, 0])

        # Test puzzle_cols with flank
        puzzle_cols = sw.get_puzzle_cols([0, 1, 2], 2, -1)
        self.assertEqual(puzzle_cols[0][0], [0, 1, 2, 3, 4])
        self.assertEqual(puzzle_cols[1][0], [2, 1, 0, -1, -2])

    def test_filter_by_range(self):
        master_seq = ({100: 'A', 101: 'A', 102: 'A', 103: 'A', 104: 'A', 105: 'A'}, '0')
        clustered_seqs = [({100: 'C', 101: 'C', 102: 'C', 103: 'C', 104: 'C', 105: 'C'}, '1'),
                          ({100: 'A', 101: 'A', 102: 'A', 103: 'C', 104: 'C', 105: 'C'}, '2'),
                          ({100: 'A', 101: 'A', 102: 'A', 103: 'A', 104: 'A', 105: 'C'}, '3')]
        min_dist = -2
        max_dist = 8
        puzzle_width = 8

        # Test accepted sequences. First one is too different, second one is fine, last one is too similar
        accepted_seqs = sw.filter_seqs_for_distance_range(master_seq, clustered_seqs, min_dist, max_dist, puzzle_width)
        self.assertEqual(len(accepted_seqs), 1)
        self.assertEqual(list(accepted_seqs[0][0].values())[:3], ['A', 'A', 'A'])

        # Test if master is empty, should return none
        master_seq = ({}, '0')
        accepted_seqs = sw.filter_seqs_for_distance_range(master_seq, clustered_seqs, min_dist, max_dist, puzzle_width)
        self.assertEqual(len(accepted_seqs), 0)

    def test_aligning_seqs(self):
        seqs = [({3: 'A', 4: 'C', 5: 'G', 6: 'T', 8: 'A', 9: 'C', 10: 'G', 11: 'T'}, '0'),
                ({3: 'A', 4: 'C', 5: 'G', 6: 'T', 8: 'A', 9: 'C', 10: 'G', 11: 'T'}, '1'),
                ({3: 'A', 4: 'C', 5: 'G', 6: 'T', 8: 'A', 9: 'C', 10: 'G', 11: 'T'}, '2'),
                ({3: 'A', 4: 'C', 5: 'G', 6: 'T', 9: 'A', 10: 'C', 11: 'G', 12: 'T'}, '3')]

        # Test that columns will align based on columns
        gapped_seqs, cols = sw.align_sequences(seqs, 8)
        self.assertEqual(gapped_seqs[1], 'ACGTACGT')
        self.assertEqual(gapped_seqs[3], 'ACGT-ACG')
        self.assertEqual(len(cols), 8)
        self.assertFalse(7 in cols)

        # Test that width will properly trim seqs to appropriate length
        gapped_seqs, cols = sw.align_sequences(seqs, 5)
        self.assertEqual(gapped_seqs[0], 'ACGTA')
        self.assertEqual(gapped_seqs[3], 'ACGT-')
        self.assertEqual(len(cols), 5)

        # Test that seq nucleotides going beyond the range of the earliest seq get trimmed out
        seqs.append(({13: 'A', 14: 'C', 15: 'G', 16: 'T', 17: 'A', 18: 'C', 19: 'G', 20: 'T'}, ''))
        seqs.append(({9: 'A', 11: 'C', 13: 'G', 16: 'T', 17: 'A', 18: 'C', 19: 'G', 20: 'T'}, ''))
        gapped_seqs, cols = sw.align_sequences(seqs, 8)
        self.assertEqual(gapped_seqs[0], 'ACGTACGT')
        self.assertEqual(gapped_seqs[4], '--------')
        self.assertEqual(gapped_seqs[5], '-----A-C')

