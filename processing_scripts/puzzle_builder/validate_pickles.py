import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import math
import glob
import json
import pickle
from Bio import SeqIO
from collections import deque
from processing_scripts.puzzle_builder import build_puzzles as bp
from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle


def get_logs(batch_prefix):
    puzzle_parms = glob.glob('puzzle_parameters/logs/' + batch_prefix + '/puzzle_parms.json')[0]
    with open(puzzle_parms) as parms_file:
        parms = json.load(parms_file)

    blacklist_parms = glob.glob('puzzle_parameters/logs/' + batch_prefix + '/blacklist.json')[0]
    with open(blacklist_parms) as blacklist_file:
        blacklist = json.load(blacklist_file)
    whitelist_parms = glob.glob('puzzle_parameters/logs/' + batch_prefix + '/whitelist.json')[0]
    with open(whitelist_parms) as whitelist_file:
        whitelist = json.load(whitelist_file)
    return parms, blacklist, whitelist


def validate_pickles(align, column_consensus, parms, puzzles):
    for dp in puzzles:
        puzzle = Puzzle.from_dnapuzzle(dp, parms)
        guide_result = validate_guide(puzzle, column_consensus, parms)
        puzzle_result = validate_puzzle(puzzle, align, parms)
        if not guide_result or not puzzle_result:
            return False
    return True


def validate_guide(puzzle, column_consensus, parms):
    shifted_columns = deque(puzzle.columns)
    shifted_columns.rotate(parms['offset'])
    shifted_columns = list(shifted_columns)
    if puzzle.columns[0] < puzzle.columns[-1]:
        for i in range(0, parms['offset']):
            shifted_columns[i] = shifted_columns[parms['offset']] + (i - parms['offset'])
    else:
        for i in range(0, parms['offset']):
            shifted_columns[i] = shifted_columns[parms['offset']] + (parms['offset'] - i)

    for i in range(len(shifted_columns)):
        global_guide = column_consensus[shifted_columns[i]]
        puzzle_guide = puzzle.consensus[i]
        if puzzle_guide != global_guide:
            return False
    return True


def validate_puzzle(puzzle, align, parms):
    cols = parms["grid_size"][str(puzzle.level)]["n_cols"] - parms["flank_size"]
    width = math.ceil(cols/2.0)

    for i in range(len(puzzle.uncollapsed_puzzle)):
        puz_row = puzzle.uncollapsed_puzzle[i]
        full_seq = align[puzzle.ids[i]].seq
        for j in range(width):
            column = puzzle.columns[j]
            if puz_row[j] != full_seq[column]:
                return False
    return True


def parse_batches(batch_prefix):
    all_succeeded = True
    parms, blacklist, whitelist = get_logs(batch_prefix)
    aln_file = '../../alignments/' + parms['alignment_file']
    try:
        align_list = list(SeqIO.parse(aln_file, "fasta"))
        align_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise

    column_scores, column_consensus = bp.assess_column_conservation(align_list, parms['target_builder'])

    curr_batch = batch_prefix + '000000'
    for i in range(parms['batches']):
        pickle_file = 'pickles/' + curr_batch + '/' + curr_batch + '.pickle'
        try:
            all_puzzles = pickle.load(open(pickle_file, "rb"))
        except OSError:
            print('File not found:', pickle_file)
            continue
        result = validate_pickles(align_dict, column_consensus, parms, all_puzzles)
        if not result:
            all_succeeded = False
            print(batch_prefix + ' failed')
        curr_batch = str(int(curr_batch) + 1000)
    return all_succeeded

if __name__ == '__main__':
    batch_prefixes = sys.argv[1:]
    success = True
    for batch_prefix in batch_prefixes:
        result = parse_batches(batch_prefix)
        if not result:
            success = False
    if success:
        print('All batches successfully validated')
