import math
import pickle
import random
import string
import time

from processing_scripts.puzzle_builder.puzzle.greedy_puzzle import GreedyPuzzle
from processing_scripts.puzzle_builder.puzzle.pairwise_puzzle import PairwisePuzzle


def make_puzzles(puzzle_states, puzzle_ids, puzzle_cols, difficulty, parms):
    """
    Create puzzles based on given states, IDs, columns, and parameters.

    Parameters:
    puzzle_states : list
        List of puzzle states (sequences and consensus).
    puzzle_ids : list
        List of identifiers for each puzzle.
    puzzle_cols : list
        List of column indices for each puzzle.
    difficulty : int
        Difficulty level of the puzzles.
    parms : dict
        Parameters for puzzle generation.

    Returns:
    list
        A list of generated puzzle objects.
    """
    puzzle_types = [GreedyPuzzle, PairwisePuzzle]
    if parms['puzzle_type'] in range(len(puzzle_types)):
        puzzle_types = [puzzle_types[parms['puzzle_type']]]

    puzzles = []
    for puzzle_type in puzzle_types:
        for i in range(len(puzzle_states)):
            new_puzzle = puzzle_type(puzzle_states[i][0], puzzle_ids, puzzle_states[i][1], puzzle_cols[i][0],
                                     puzzle_cols[i][1], parms)
            unique_id = ''.join(
                random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in
                range(16))
            new_puzzle.set_unique_id(unique_id)
            new_puzzle.set_difficulty(difficulty)
            new_puzzle.process_puzzle()
            puzzles.append(new_puzzle)
    return puzzles


def validate_puzzles(puzzles, parms, difficulty):
    """
    Validate generated puzzles against specified parameters.

    Parameters:
    puzzles : list
        List of generated puzzles.
    parms : dict
        Parameters for puzzle validation.
    difficulty : int
        Difficulty level of the puzzles.

    Returns:
    bool
        Returns True if puzzles meet validation criteria, False otherwise.
    """
    base_score_improvs = [math.floor(a.sol_score) - math.ceil(a.base_score) for a in puzzles]
    prior_score_improvs = [math.floor(a.sol_score) - math.ceil(a.prior_score) for a in puzzles]
    moves = [len(x.sol_steps) for x in puzzles]
    varied_steps = [len(set(x[0] for x in puzzle.sol_steps)) for puzzle in puzzles]
    worst_base_score = min(base_score_improvs)
    worst_prior_score = min(prior_score_improvs)
    least_moves = min(moves)
    min_varied_steps = min(varied_steps)

    added_columns = max(0, (parms['grid_size'][str(difficulty)]["n_cols"] - parms['min_columns']))
    added_sequences = max(0, (parms['grid_size'][str(difficulty)]["n_seqs"] - parms['min_sequences']))
    min_improvement = parms['min_improvement'] + (added_columns * parms['improv_per_column']) + \
                      (added_sequences * parms['improv_per_sequence'])
    if worst_base_score >= min_improvement and worst_prior_score >= min_improvement \
            and least_moves >= parms['min_moves'] and min_varied_steps >= parms['min_varied_steps']:
        return True

    return False


def get_seq_distance(lst1, lst2):
    """
    Calculate the distance between two sequences.

    Parameters:
    lst1 : list
        First sequence.
    lst2 : list
        Second sequence.

    Returns:
    int
        The calculated distance between the two sequences.
    """
    if len(lst1) == 0:
        return -2000000

    score = 0
    for i in range(len(lst1)):
        if lst1[i] == lst2[i] and lst1[i] != "-":
            score -= 1
        elif lst1[i] != lst2[i]:
            score += 2
    return score


def rand_seq_length(max_length):
    """
    Generate a random sequence length based on the maximum length and exponentialy decreasing probabilities for choices.

    Parameters:
    max_length : int
        Maximum length of the sequence.

    Returns:
    int
        A random sequence length.
    """
    min_length = math.floor(max_length / 2.0)
    weights = [10.0] * min_length
    weights = [weights[i] / (2 ** i) for i in reversed(range(len(weights)))]
    weights.append(100 - sum(weights))
    rand_range = list(range(min_length + 1))
    return random.choices(rand_range, weights=weights, k=1)[0]


def trim_seq(sequence, max_length, extra):
    """
    Trim a sequence to a specified length.

    Parameters:
    sequence : str
        The sequence to be trimmed.
    max_length : int
        Maximum length of the sequence.
    extra : int
        Extra length to consider during trimming.

    Returns:
    tuple
        Tuple containing the trimmed sequence and its flipped version.
    """
    stripped_seq = sequence.replace('-', '')

    seq = stripped_seq[:math.ceil(max_length / 2.0) + extra]
    flipped_seq = stripped_seq[::-1][:math.ceil(max_length / 2.0) + extra]
    return seq, flipped_seq


def prepare_sequences(sequence, puzzle_width):
    """
    Prepare sequences for puzzle creation.

    Parameters:
    sequence : str
        The sequence to be prepared.
    puzzle_width : int
        Width of the puzzle to be created.

    Returns:
    tuple
        Tuple containing the prepared sequence and its flipped version.
    """
    extra = rand_seq_length(puzzle_width)
    seq, flipped_seq = trim_seq(sequence, puzzle_width, extra)
    stripped_seq = sequence.replace('-', '')

    uncollapsed_seq = sequence
    removed_nucs = len(stripped_seq) - len(seq)
    for i in range(removed_nucs):
        position = max(uncollapsed_seq.rfind(j) for j in 'ACGT')
        uncollapsed_seq = uncollapsed_seq[:position] + '-' + uncollapsed_seq[position + 1:]

    uncollapsed_flipped = sequence[::-1]
    removed_nucs = len(stripped_seq) - len(flipped_seq)
    for i in range(removed_nucs):
        position = max(uncollapsed_flipped.rfind(j) for j in 'ACGT')
        uncollapsed_flipped = uncollapsed_flipped[:position] + '-' + uncollapsed_flipped[position + 1:]
    return uncollapsed_seq, uncollapsed_flipped


def check_blacklist_whitelist(columns, parms, unwanted_cols, wanted_cols):
    """
    Check if columns comply with the blacklist/whitelist criteria.

    Parameters:
    columns : list
        List of column indices to check.
    parms : dict
        Parameters containing blacklist/whitelist settings.
    unwanted_cols : list
        List of blacklisted columns.
    wanted_cols : list
        List of whitelisted columns.

    Returns:
    bool
        True if columns comply with the criteria, False otherwise.
    """
    if parms['use_blacklist'] and all(column not in unwanted_cols for column in columns):
        return True
    elif parms['use_whitelist'] and all(column in wanted_cols for column in columns):
        return True
    elif not parms['use_blacklist'] and not parms['use_whitelist']:
        return True
    else:
        return False


def output_puzzles(ready_puzzles, pickle_id, difficulty, subprocess):
    pickle_out = "pickles/" + pickle_id + "/ready_puzzles" + pickle_id + "proc" + str(difficulty) + \
                 chr(97 + subprocess) + ".pickle"
    pickle.dump(ready_puzzles, open(pickle_out, "wb"))

def print_details(ready_puzzles, difficulty, total, start_time):
    print("================================")
    print("Generated", str(len(ready_puzzles)), "puzzles for difficulty ", difficulty)
    if len(ready_puzzles):
        print("Average improvement:", round(total / len(ready_puzzles), 2))
    print("TIME", str(round(time.time() - start_time, 1)))
