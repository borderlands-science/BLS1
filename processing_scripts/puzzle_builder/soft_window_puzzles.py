import math
import random
import time
from collections import deque
from processing_scripts.puzzle_builder import puzzle_functions as pf


def find_soft_windows(column_scores, window_size, parms, unwanted_cols, wanted_cols):
    """
    Identify window boundaries using soft strategy.

    Parameters:
    column_scores : list
        Scores for each column in the alignment.
    window_size : int
        Size of the window to consider for puzzle creation.
    parms : dict
        Parameters for puzzle generation.
    unwanted_cols : list
        List of columns to be excluded.
    wanted_cols : list
        List of preferred columns.

    Returns:
    list
        List of starting points of soft windows.
    """
    window_scores = {}
    start_end_offset = min(parms['flank_size'], parms['offset'])
    for i in range(start_end_offset, len(column_scores) - (window_size - 1) - start_end_offset):
        score_range = [abs(column_scores[i])]
        columns = [i]
        j = i + 1
        while len(score_range) < window_size and j < (len(column_scores) - start_end_offset):
            if column_scores[j] > 0:
                score_range.append(column_scores[j])
                columns.append(j)
            j += 1
        if len(score_range) <= 1 or ((columns[-1] + 1) - columns[0]) < window_size:
            continue
        score = sum(score_range) / len(score_range)

        if score < parms['conservation_threshold'] and pf.check_blacklist_whitelist(columns, parms, unwanted_cols,
                                                                                    wanted_cols):
            window_scores[i] = score

    sorted_points = [x[0] for x in sorted(window_scores.items(), key=lambda item: item[1])]
    if parms['n_windows'] != -1:
        return sorted_points[:parms['n_windows']]
    return sorted_points


def get_alignment_columns(align_length, collapsed_aln, aln_map, start, puzzle_width):
    """
    Extract columns from a collapsed alignment within specified boundaries.

    Parameters:
    align_length : int
        Total length of the alignment.
    collapsed_aln : list
        List of collapsed alignment sequences.
    aln_map : list
        Mapping of original alignment positions.
    start : int
        Starting position of the window.
    puzzle_width : int
        Width of the puzzle to be created.

    Returns:
    tuple
        Tuple containing sequences and reference sequences from the specified window.
    """
    seq_list = {}
    ref_list = {}
    if start < 0 or start > align_length:
        start = 0
    if start + puzzle_width > align_length:
        puzzle_width = align_length - start

    for i in range(len(collapsed_aln)):
        start_pos = next((x[0] for x in enumerate(aln_map[i]) if x[1] >= start), None)
        if start_pos is not None:
            seq = str(collapsed_aln[i].seq[start_pos:start_pos + puzzle_width])
            columns = aln_map[i][start_pos:start_pos + puzzle_width]
            if collapsed_aln[i].id not in seq_list.keys():
                seq_list[collapsed_aln[i].id] = dict(zip(columns, seq))
                if aln_map[i][start_pos] == start and len(seq) > math.ceil(puzzle_width / 2.0):
                    ref_list[collapsed_aln[i].id] = dict(zip(columns, seq))
            else:
                print("IDENTICAL RECORD IN FILE", collapsed_aln[i].id)
    seq_results = [(v, k) for k, v in seq_list.items()]
    ref_results = [(v, k) for k, v in ref_list.items()]
    return seq_results, ref_results


def get_puzzle_states(prepared_seqs, prepared_flipped_seqs, consensus, flank, offset, base_columns, direction):
    """
    Determine the states of the puzzle considering normal and flipped sequences.

    Parameters:
    prepared_seqs : list
        List of prepared puzzle sequences.
    prepared_flipped_seqs : list
        List of flipped puzzle sequences.
    consensus : list
        Consensus sequence for the puzzle.
    flank : int
        Size of the flank to be considered.
    offset : int
        Offset of the puzzle.
    base_columns : list
        Base column indices for the puzzle.
    direction : int
        Direction, 0=normal and 1=flipped.

    Returns:
    list
        List of tuples containing puzzle sequences and consensus.
    """
    puzzle_states = []
    normal_puzzle_seqs = [x + ('-' * flank) for x in prepared_seqs]
    if direction != 1 and base_columns[0] - offset >= 0 and base_columns[-1] - offset + flank < len(consensus):
        shifted_columns = deque(base_columns + list(range(base_columns[-1] + 1,
                                                          base_columns[-1] + 1 + flank)))
        shifted_columns.rotate(offset)
        shifted_columns = list(shifted_columns)
        for i in range(0, offset):
            shifted_columns[i] = shifted_columns[offset] + (i - offset)
        normal_consensus = [consensus[x] for x in shifted_columns]
        puzzle_states.append((normal_puzzle_seqs, normal_consensus))

    flipped_puzzle_seqs = [x + ('-' * flank) for x in prepared_flipped_seqs]
    if direction != 0 and base_columns[0] + offset - flank >= 0 and base_columns[-1] + offset < len(consensus):
        shifted_columns = deque(reversed(list(range(base_columns[0] - flank,
                                                    base_columns[0])) + base_columns))
        shifted_columns.rotate(offset)
        shifted_columns = list(shifted_columns)
        for i in range(0, offset):
            shifted_columns[i] = shifted_columns[offset] + (offset - i)
        flipped_consensus = [consensus[x] for x in shifted_columns]
        puzzle_states.append((flipped_puzzle_seqs, flipped_consensus))

    return puzzle_states


def get_puzzle_cols(columns, flank_size, direction):
    """
    Get column indices for the puzzle and flanks.

    Parameters:
    columns : list
        List of column indices to check.
    flank_size : int
        Size of the flank to be considered.
    direction : int
        Direction for flipping the sequences.

    Returns:
    list
        List of tuples containing puzzle column indices and flank column indices.
    """
    puzzle_cols = []
    normal_puzzle_cols = columns + list(range(columns[-1] + 1, columns[-1] + 1 + flank_size))
    normal_flank_cols = normal_puzzle_cols[-flank_size:]

    flipped_puzzle_cols = list(range(columns[0] - flank_size, columns[0])) + columns
    flipped_puzzle_cols.reverse()
    flipped_flank_cols = flipped_puzzle_cols[-flank_size:]

    if direction != 1:
        puzzle_cols.append((normal_puzzle_cols, normal_flank_cols))
    if direction != 0:
        puzzle_cols.append((flipped_puzzle_cols, flipped_flank_cols))
    return puzzle_cols


def filter_seqs_for_distance_range(master, clustered_seqs, min_dist, max_dist, puzzle_width):
    """
    Filter sequences for a specific distance range from a reference sequence.

    Parameters:
    master : tuple
        The reference sequence for comparison.
    clustered_seqs : list
        List of sequences to be filtered.
    min_dist : int
        Minimum distance threshold.
    max_dist : int
        Maximum distance threshold.
    puzzle_width : int
        Width of the puzzle to be considered.

    Returns:
    list
        Filtered list of sequences within the specified distance range.
    """
    min_length = math.floor(puzzle_width / 2)
    acceptable_seqs = []
    for entry in clustered_seqs:
        gapped_results, columns = align_sequences([master, entry], puzzle_width)
        if max_dist >= pf.get_seq_distance(gapped_results[0], gapped_results[1]) >= min_dist \
                and min_length >= gapped_results[0].count('-') and min_length >= gapped_results[1].count('-'):
            acceptable_seqs.append((dict(zip(columns, gapped_results[1])), entry[1]))

    return acceptable_seqs


def align_sequences(sequences, puzzle_width):
    """
    Align sequences for puzzle creation.

    Parameters:
    sequences : list
        List of sequences to be aligned.
    puzzle_width : int
        Width of the puzzle to be created.

    Returns:
    tuple
        Tuple containing aligned sequences and column indices.
    """
    gapped_sequences = [''] * len(sequences)

    all_columns = set()
    for sequence in sequences:
        columns = set(sequence[0].keys())
        all_columns |= columns
    all_columns = sorted(all_columns)

    for column in all_columns[:puzzle_width]:
        for i in range(len(sequences)):
            value = '-' if column not in sequences[i][0].keys() else sequences[i][0][column]
            gapped_sequences[i] += value

    return gapped_sequences, all_columns[:puzzle_width]


def get_puzzle_entries(ref_seqs, window_column_seqs, difficulty, parms):
    """
    Get a random reference sequence and then get random sequence entries for creating puzzles from list of window column sequences.

    Parameters:
    ref_seqs : list
        List of reference sequences.
    window_column_seqs : list
        List of column sequences in the window.
    difficulty : int
        Difficulty level of the puzzle.
    parms : dict
        Parameters for puzzle generation.

    Returns:
    list
        List of puzzle entries selected for puzzle creation.
    """
    puzzle_entries = []

    puzzle_length = parms['grid_size'][str(difficulty)]["n_seqs"]
    puzzle_width = parms['grid_size'][str(difficulty)]["n_cols"] - parms['flank_size']
    reference_seq = random.choices(ref_seqs, k=1)[0]
    acceptable_seqs = filter_seqs_for_distance_range(reference_seq, window_column_seqs, parms['min_dist'],
                                                     parms['max_dist'], puzzle_width)
    if len(acceptable_seqs) >= puzzle_length - 1:
        puzzle_entries = random.choices(acceptable_seqs, k=puzzle_length - 1)
        puzzle_entries.append(reference_seq)
    return puzzle_entries


def prepare_puzzles(puzzle_entries, puzzle_width, column_consensus, parms):
    """
    Prepare puzzles for creation, considering the states and sequences.

    Parameters:
    puzzle_entries : list
        List of puzzle entries.
    puzzle_width : int
        Width of the puzzle to be created.
    column_consensus : list
        Consensus sequence for the puzzle.
    parms : dict
        Parameters for puzzle generation.

    Returns:
    tuple
        Tuple containing possible puzzle states and base column indices.
    """
    puzzle_states = []
    base_seqs, base_columns = align_sequences(puzzle_entries, puzzle_width)
    max_gaps = math.floor(puzzle_width / 2)
    highest_gap_count = max([seq.count('-') for seq in base_seqs])
    if max_gaps >= highest_gap_count and len(set(base_seqs)) == len(base_seqs):
        prepared_seqs = []
        prepared_flipped_seqs = []
        for unprepared_seq in base_seqs:
            seq, flipped_seq = pf.prepare_sequences(unprepared_seq, puzzle_width)
            prepared_seqs.append(seq)
            prepared_flipped_seqs.append(flipped_seq)

        puzzle_states = get_puzzle_states(prepared_seqs, prepared_flipped_seqs, column_consensus, parms['flank_size'],
                                          parms['offset'], base_columns, parms['direction'])
    return puzzle_states, base_columns


def create_window_puzzles(ready_puzzles, total, align_length, collapsed_align, column_consensus,
                          align_map, start, difficulty, parms, unwanted_cols, wanted_cols):
    accepted_puzzles_in_window = 0
    puzzle_width = parms['grid_size'][str(difficulty)]["n_cols"] - parms['flank_size']
    window_column_seqs, ref_seqs = get_alignment_columns(align_length, collapsed_align, align_map, start, puzzle_width)
    if not ref_seqs:
        return

    for i in range(int(parms['puzzles_per_window'] * (parms['ppw_parm_1'] - parms['ppw_parm_2'] * difficulty))):
        if accepted_puzzles_in_window >= parms['max_puzzles_per_window']:
            break
        puzzle_entries = get_puzzle_entries(ref_seqs, window_column_seqs, difficulty, parms)
        if not puzzle_entries:
            continue

        puzzle_states, base_columns = prepare_puzzles(puzzle_entries, puzzle_width, column_consensus, parms)

        if puzzle_states:
            puzzle_cols = get_puzzle_cols(base_columns, parms['flank_size'], parms['direction'])
            puzzle_ids = [x[1] for x in puzzle_entries]
            puzzles = pf.make_puzzles(puzzle_states, puzzle_ids, puzzle_cols, difficulty, parms)
            accept_puzzle = pf.validate_puzzles(puzzles, parms, difficulty)

            if accept_puzzle:
                if parms['puzzle_type'] == -1:
                    puzzles = puzzles[:int(len(puzzles) / 2)]
                for puzzle in puzzles:
                    if accepted_puzzles_in_window < parms['max_puzzles_per_window'] and pf.check_blacklist_whitelist(
                            puzzle.columns, parms, unwanted_cols, wanted_cols):
                        dna_puzzle = puzzle.to_dnapuzzle()
                        ready_puzzles.append(dna_puzzle)
                        total += (puzzle.sol_score - puzzle.prior_score)
                        accepted_puzzles_in_window += 1


def build_puzzles(collapsed_align, align_map, align_length, parms, difficulty,
                  column_scores, column_consensus, subprocess=0, pickle_id="0",
                  unwanted_cols=[], wanted_cols=[]):
    start_time = time.time()

    ready_puzzles = []
    total = 0
    puzzle_width = parms['grid_size'][str(difficulty)]["n_cols"] - parms['flank_size']
    starting_points = find_soft_windows(column_scores, puzzle_width, parms, unwanted_cols, wanted_cols)

    for start in starting_points:
        create_window_puzzles(ready_puzzles, total, align_length, collapsed_align, column_consensus,
                              align_map, start, difficulty, parms, unwanted_cols, wanted_cols)

    pf.output_puzzles(ready_puzzles, pickle_id, difficulty, subprocess)
    if parms['output_details']:
        pf.print_details(ready_puzzles, difficulty, total, start_time)
