import re
import abc
import copy
import random
import string
import itertools
from processing_scripts.dnapuzzle import Puzzle as DNAPuzzle


class Puzzle:
    """
    A base class for creating and solving DNA puzzles.

    This class provides a framework for representing DNA puzzles and includes
    methods for processing, scoring, and manipulating these puzzles. It can be
    used as a base class for more specialized puzzle-solving algorithms. The class
    handles the basic functionalities like initializing the puzzle, scoring based on
    consensus alignment, counting gaps, and converting the puzzle into different formats.

    Attributes:
    puzzle : list
        List of strings representing columns of the puzzle.
    ids : list
        List of identifiers for each sequence in the puzzle.
    consensus : list
        Multidimensional array indicating preferred nucleotides per row.
    columns : list
        List of column positions relative to complete sequences.
    flanks : list
        List of flank sequences.
    parms : dict
        Dictionary of parameters for scoring and other operations.

    Methods:
    from_dnapuzzle(dp, parms) - Create a Puzzle instance from a DNAPuzzle object.
    calc_prior_puzzle() - Abstract method to calculate the initial score of the puzzle.
    calc_solution() - Abstract method to calculate the solution of the puzzle.
    set_difficulty(dif) - Set the difficulty level of the puzzle.
    set_unique_id(u_id) - Set a unique identifier for the puzzle.
    set_scores(parms) - Set the scoring parameters for the puzzle.
    add_new_tokens(new_tokens) - Add new tokens to the puzzle.
    copy_puzzle() - Create a deep copy of the puzzle.
    clean_puzzle() - Clean the puzzle by standardizing nucleotide representations.
    collapse() - Collapse the puzzle to match the length of the consensus.
    process_puzzle() - Process the puzzle to calculate prior and solution scores.
    score_consensus(puzzle) - Score the puzzle based on consensus alignment.
    count_gaps(puzzle) - Count the number of gaps in the puzzle.
    show() - Display the base state of the puzzle.
    show_solution() - Display the solved state of the puzzle.
    can_split_screen(MAX_HEIGHT) - Determine if the puzzle can be split across a screen.
    to_dnapuzzle() - Convert the Puzzle instance to a DNAPuzzle object.
    """
    def __init__(self, puzzle=[], ids=[], consensus=[], columns=[], flanks=[], parms=None):
        self.puzzle = copy.deepcopy(puzzle)  # list of strings. Each string is a column
        self.clean_puzzle()
        self.uncollapsed_puzzle = self.copy_puzzle()
        self.puzzle_sol = []  # Solution to the puzzle
        self.ids = ids  # list of ids for each array, could turn these both into a dictionary?
        self.consensus = consensus  # multidimensional array indicating prefered nucs per row
        self.columns = columns  # list of column positions relative to complete sequences
        self.flanks = flanks
        self.set_scores(parms)

        self.level = -1  # difficulty
        self.puzzle_id = None  # puzzle id
        self.n_extra_tokens = [1, 3, 6]  # list of number of tokens per puzzle iteration
        self.prior_score = -2000000
        self.sol_score = -2000000
        self.sol_steps = []

        self.collapse()
        self.base_score = self.score_consensus(self.puzzle)

    @classmethod
    def from_dnapuzzle(cls, dp, parms):
        try:
            puzzle_base = ["".join(x) for x in dp.uncollapsed_puzzle]
            new_puzzle = cls(puzzle_base, dp.IDs, dp.consensus, dp.columns, dp.flanks, parms)
            puzzle_sol = ["".join(x) for x in dp.greedy_sol]
            new_puzzle.puzzle_sol = puzzle_sol
            new_puzzle.sol_score = dp.eval()
            new_puzzle.n_extra_tokens = dp.get_n_extra_tokens()
            new_puzzle.puzzle_id = dp.windowID
            new_puzzle.level = dp.level
        except AttributeError:
            raise AttributeError("Missing critical attributes")
        return new_puzzle

    @abc.abstractmethod
    def calc_prior_puzzle(self):
        return

    @abc.abstractmethod
    def calc_solution(self):
        return

    def set_difficulty(self, dif):
        self.level = dif

    def set_unique_id(self, u_id):
        self.puzzle_id = u_id

    def set_scores(self, parms):
        if parms:
            try:
                self.match = parms['match']
                self.mismatch = parms['mismatch']
                self.gap_open = parms['gap_open']
                self.gap_extension = parms['gap_extension']
                self.bonus = parms['bonus']
                return
            except KeyError:
                print("Missing certain scoring parameters. Using defaults")
        self.match = 1
        self.mismatch = 0
        self.gap_open = -0.6
        self.gap_extension = -0.6
        self.bonus = 1.15

    def add_new_tokens(self, new_tokens):
        self.n_extra_tokens.append(new_tokens)

    def copy_puzzle(self):
        return copy.deepcopy(self.puzzle)

    def clean_puzzle(self):
        for i, column in enumerate(self.puzzle):
            cleaned_column = column.upper().replace('U', 'T')
            cleaned_column = re.sub(r'[^ACGT]', '-', cleaned_column)
            self.puzzle[i] = cleaned_column

    def collapse(self):
        for i in range(len(self.puzzle)):
            row = self.puzzle[i]
            column_length = len(self.consensus)
            row = row.replace('-', '')
            row = row.ljust(column_length, '-')
            self.puzzle[i] = row[:column_length]

    def process_puzzle(self):
        self.calc_prior_puzzle()
        self.calc_solution()

    def score_consensus(self, puzzle):
        score = 0
        rows = [list(filter(None, i)) for i in itertools.zip_longest(*puzzle)]
        for i, row in enumerate(rows):
            if i < len(self.consensus):
                row_consensus = list(self.consensus[i])
                if '-' in row_consensus:
                    row_consensus.remove('-')
                row_score = sum(map(row.count, row_consensus)) * self.match
                if row_score == len(rows[0]):
                    row_score = round(row_score * self.bonus, 2)
                score += row_score
        return score

    def count_gaps(self, puzzle):
        n_gaps = 0
        for row in puzzle:
            row = row.rstrip('-')
            n_gaps += row.count('-')
        return n_gaps

    def show(self):
        print('Base score: ', self.base_score)
        for i in self.puzzle:
            print(i)
        print("=" * len(i))
        print("".join([x[0] for x in self.consensus]))
        print("".join([x[1] for x in self.consensus]))

    def show_solution(self):
        print('Solution score: ', self.sol_score)
        print('Steps:', self.sol_steps)
        for i in self.puzzle_sol:
            print(i)
        print("=" * len(i))
        print("".join([x[0] for x in self.consensus]))
        print("".join([x[1] for x in self.consensus]))

    def can_split_screen(self, MAX_HEIGHT):
        height = len(self.puzzle[0])
        max_len = 2 + height - min([x.count("-") for x in self.puzzle])
        return max_len < MAX_HEIGHT

    def to_dnapuzzle(self):
        adjusted_puzzle = [list(x) for x in self.puzzle]
        adjusted_sol = [list(x) for x in self.puzzle_sol]
        adjusted_uncollapsed = [list(x) for x in self.uncollapsed_puzzle]
        converted_puzzle = DNAPuzzle.from_lst(
            adjusted_puzzle,
            self.ids,
            self.consensus,
            [],
            self.columns,
            self.flanks,
            10000,
            self.bonus)
        converted_puzzle.uncollapsed_puzzle = adjusted_uncollapsed
        converted_puzzle.gap_score = self.count_gaps(self.puzzle)
        converted_puzzle.set_uniqueID(self.puzzle_id)
        converted_puzzle.set_difficulty(self.level)
        converted_puzzle.set_extra_tokens(self.n_extra_tokens)
        converted_puzzle.score = self.sol_score
        converted_puzzle.consensus_score = self.score_consensus(self.puzzle)
        greedy_cons = self.score_consensus(self.puzzle_sol)
        gaps = self.count_gaps(self.puzzle_sol)
        converted_puzzle.add_solution(adjusted_sol, greedy_cons, gaps, len(self.sol_steps))

        for extra_token in converted_puzzle.get_n_extra_tokens():
            oCode = ''.join(
                random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))
            converted_puzzle.set_originalCode(oCode)
            converted_puzzle.update_player_results(
                oCode,
                (converted_puzzle.greedy_n_gaps + extra_token),
                converted_puzzle.gearbox_score(),
                (converted_puzzle.greedy_n_gaps + extra_token),
                0,
                0,
                ["".join(x) for x in converted_puzzle.greedy_sol],
                ["".join(x) for x in converted_puzzle.greedy_sol]
            )

        return converted_puzzle
