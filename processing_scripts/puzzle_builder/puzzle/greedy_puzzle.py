import itertools

from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle


class GreedyPuzzle(Puzzle):
    """
    A subclass of Puzzle that implements a greedy algorithm to solve DNA puzzles.

    GreedyPuzzle extends the basic Puzzle structure with methods specifically
    designed for solving puzzles using a greedy approach. It involves iteratively
    making the locally optimal choice at each stage aiming at finding the highest
    possible puzzle score.
    This is achieved through adding, shifting, or removing gaps in the puzzle
    sequence to improve alignment and maximize the overall score.

    Attributes:
    (Inherits all attributes from the Puzzle class)

    Methods:
    calc_prior_puzzle() - Calculate the initial score of the puzzle before applying any solution.
    calc_solution() - Apply the greedy algorithm to find the best solution for the puzzle.
    get_best_gap_add(curr_score, gaps) - Determine the best position to add a gap in the puzzle.
    get_best_gap_removal(curr_score, gaps) - Identify the best gap to remove for improving the score.
    get_best_gap_shift(curr_score, gaps) - Find the optimal position to shift a gap in the puzzle.
    get_gap_score(gaps) - Calculate the score wrt. gaps.
    score_rows(puzzle) - Score the puzzle based on row alignments.
    insert_gap(column, pos) - Insert a gap into a column at the specified position.
    remove_gap(column, pos) - Remove a gap from a column at the specified position.
    """
    def __init__(self, puzzle=[], ids=[], consensus=[], columns=[], flanks=[], parms=None):
        super().__init__(puzzle, ids, consensus, columns, flanks, parms)

    def calc_prior_puzzle(self):
        n_gaps = self.count_gaps(self.uncollapsed_puzzle)
        self.prior_score = self.score_consensus(self.uncollapsed_puzzle) + self.get_gap_score(n_gaps)

    def calc_solution(self):
        self.puzzle_sol = self.copy_puzzle()
        self.sol_steps = []
        curr_score = self.base_score
        gaps = []

        while True:
            gap_add_score, gap_add_move = self.get_best_gap_add(curr_score, gaps)
            gap_shift_score, gap_shift_move = self.get_best_gap_shift(curr_score, gaps)
            gap_remove_score, gap_remove_move = self.get_best_gap_removal(curr_score, gaps)

            scores = [gap_add_score, gap_shift_score, gap_remove_score]
            moves = [gap_add_move, gap_shift_move, gap_remove_move]

            best_score_pos = scores.index(max(scores))
            if moves[best_score_pos]:
                self.sol_steps.append(moves[best_score_pos])

            if scores[best_score_pos] == -2000000:
                break
            new_column = self.puzzle_sol[moves[best_score_pos][0]]
            if best_score_pos > 0:
                new_column = self.remove_gap(new_column, moves[best_score_pos][1])
                gaps.remove([moves[best_score_pos][0], moves[best_score_pos][1]])
            if best_score_pos < 2:
                new_column = self.insert_gap(new_column, moves[best_score_pos][-1])
                gaps.append([moves[best_score_pos][0], moves[best_score_pos][-1]])

            self.puzzle_sol[moves[best_score_pos][0]] = new_column
            curr_score = scores[best_score_pos]
        self.sol_score = curr_score

    def get_best_gap_add(self, curr_score, gaps):
        best_move = []

        base_row_scores = self.score_rows(self.puzzle_sol)
        for i, column in enumerate(self.puzzle_sol):
            if column[-1] == '-' or len(column) < len(self.consensus):
                self.puzzle_sol[i] = self.insert_gap(column, 0)
                shifted_row_scores = self.score_rows(self.puzzle_sol)
                if sum(shifted_row_scores) + self.get_gap_score(len(gaps) + 1) > curr_score:
                    curr_score = sum(shifted_row_scores) + self.get_gap_score(len(gaps) + 1)
                    best_move = [i, 0]
                for j in range(1, min(len(self.consensus) - 1, len(column) - 1)):
                    if column[j] != '-':
                        gap_score = base_row_scores[j]
                        if gap_score > len(self.puzzle_sol):
                            gap_score /= self.bonus
                        if column[j] in self.consensus[j]:
                            gap_score -= self.match
                        score = sum(base_row_scores[:j]) + gap_score + sum(shifted_row_scores[j + 1:]) + \
                                self.get_gap_score(len(gaps) + 1)
                        if score > curr_score:
                            curr_score = score
                            best_move = [i, j]
                    self.puzzle_sol[i] = column

        best_score = curr_score if best_move else -2000000
        return best_score, best_move

    def get_best_gap_removal(self, curr_score, gaps):
        best_move = []
        for gap in gaps:
            old_column = self.puzzle_sol[gap[0]]
            self.puzzle_sol[gap[0]] = self.remove_gap(old_column, gap[1])
            score = self.score_consensus(self.puzzle_sol) + \
                    self.get_gap_score(len(gaps) - 1)
            if score > curr_score:
                curr_score = score
                best_move = gap
            self.puzzle_sol[gap[0]] = old_column

        best_score = curr_score if best_move else -2000000
        return best_score, best_move

    def get_best_gap_shift(self, curr_score, gaps):
        best_move = []
        base_row_scores = self.score_rows(self.puzzle_sol)

        for gap in gaps:
            old_column = self.puzzle_sol[gap[0]]
            stripped_column = self.remove_gap(old_column, gap[1])
            self.puzzle_sol[gap[0]] = self.insert_gap(stripped_column, 0)
            shifted_row_scores = self.score_rows(self.puzzle_sol)

            if sum(shifted_row_scores) + self.get_gap_score(len(gaps)) > curr_score:
                curr_score = sum(shifted_row_scores) + self.get_gap_score(len(gaps))
                best_move = [gap[0], gap[1], 0]

            for j in range(1, len(self.consensus) - 1):
                if old_column[j] != '-':
                    gap_score = base_row_scores[j]
                    if gap_score > len(self.puzzle_sol):
                        gap_score /= self.bonus
                    if old_column[j] in self.consensus[j]:
                        gap_score -= self.match
                    score = sum(base_row_scores[:j]) + gap_score + sum(shifted_row_scores[j + 1:]) + \
                            self.get_gap_score(len(gaps))
                    if score > curr_score:
                        curr_score = score
                        best_move = [gap[0], gap[1], j]
            self.puzzle_sol[gap[0]] = old_column

        best_score = curr_score if best_move else -2000000
        return best_score, best_move

    def get_gap_score(self, gaps):
        return round(self.gap_extension * gaps, 2)

    def score_rows(self, puzzle):
        scores = []
        rows = [list(filter(None, i)) for i in itertools.zip_longest(*puzzle)]
        for i, row in enumerate(rows):
            if i < len(self.consensus):
                row_consensus = list(self.consensus[i])
                if '-' in row_consensus:
                    row_consensus.remove('-')
                row_score = sum(map(row.count, row_consensus)) * self.match
                if row_score == len(rows[0]):
                    row_score = round(row_score * self.bonus, 2)
                scores.append(row_score)
        return scores

    def insert_gap(self, column, pos):
        if len(column) < len(self.consensus) or column[-1] == '-':
            return column[:pos] + '-' + column[pos:len(self.consensus)-1]
        else:
            return column

    def remove_gap(self, column, pos):
        if column[pos] == '-':
            column = column[:pos] + column[pos + 1:]
            column = column.ljust(len(self.consensus), '-')
        return column



