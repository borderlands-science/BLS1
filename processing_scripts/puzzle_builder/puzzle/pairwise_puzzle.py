import numpy as np
import itertools
from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle


class PairwisePuzzle(Puzzle):
    """
    A subclass of Puzzle that implements a pairwise alignment approach to solve DNA puzzles.

    PairwisePuzzle extends the basic Puzzle class by introducing methods for solving
    puzzles using pairwise alignment techniques. It calculates the optimal alignment
    of sequences by scoring matches, mismatches, and gaps, thereby finding a solution
    that maximizes the alignment score. The class uses dynamic programming for
    alignment and scoring, making it suitable for puzzles where pairwise alignments
    are key to finding an optimal solution.

    Attributes:
    (Inherits all attributes from the Puzzle class)

    Methods:
    calc_prior_puzzle() - Calculate the initial score of the puzzle before applying the solution.
    calc_solution() - Apply pairwise alignment techniques to solve the puzzle.
    get_best_col_align(column) - Align a single column to the consensus sequence optimally.
    traceback(matrix, column) - Perform traceback in dynamic programming to reconstruct the alignment.
    get_column_steps(column_pos, column) - Get the steps needed to align a column to the consensus.
    score_affine_gap(puzzle) - Calculate the score of a puzzle based on affine gap penalties.
    score_bonus_points(puzzle) - Calculate bonus points based on complete row matches to the consensus.
    """
    def __init__(self, puzzle=[], ids=[], consensus=[], columns=[], flanks=[], parms=None):
        super().__init__(puzzle, ids, consensus, columns, flanks, parms)

    def calc_prior_puzzle(self):
        self.prior_score = self.score_affine_gap(self.uncollapsed_puzzle) \
                           + self.score_bonus_points(self.uncollapsed_puzzle)

    def calc_solution(self):
        self.puzzle_sol = self.copy_puzzle()
        self.sol_steps = []

        for pos, column in enumerate(self.puzzle_sol):
            column = self.get_best_col_align(column)
            self.sol_steps += self.get_column_steps(pos, column)
            self.puzzle_sol[pos] = column
        self.sol_score = self.score_affine_gap(self.puzzle_sol) + self.score_bonus_points(self.puzzle_sol)

    def get_best_col_align(self, column):
        column = column.rstrip('-')
        matrix = np.zeros((len(self.consensus) + 1, len(column) + 1), np.float)
        trace_matrix = matrix.copy()
        for pos in range(1, len(self.consensus) - (len(column) - 1)):
            matrix[pos][0] = self.gap_open + ((pos - 1) * self.gap_extension)
            trace_matrix[pos][0] = 1

        for i in range(1, matrix.shape[0]):
            for j in range(max(1, i - (len(self.consensus) - len(column))), min(i + 1, matrix.shape[1])):
                match = round(matrix[i - 1, j - 1] + (self.match if column[j - 1] in self.consensus[i - 1]
                                                      else self.mismatch), 2)
                if j == matrix.shape[1] - 1 and i > len(column):
                    skip = matrix[i - 1, j]
                elif len(column[i:]) < len(self.consensus[j:]) and i > j:
                    if trace_matrix[i - 1, j] == 0:
                        skip = matrix[i - 1, j] + self.gap_open
                    else:
                        skip = matrix[i - 1, j] + self.gap_extension
                else:
                    skip = -2000000
                skip = round(skip, 2)
                matrix[i, j] = max(match, skip)
                if skip >= match:
                    trace_matrix[i, j] = 1
        new_column = self.traceback(trace_matrix, column)
        return new_column

    def traceback(self, matrix, column):
        new_column = ''
        i, j = matrix.shape[0] - 1, matrix.shape[1] - 1
        while i != 0 or j != 0:
            if matrix[i][j] == 0:
                new_column += column[-1]
                column = column[:-1]
                i -= 1
                j -= 1
            else:
                new_column += '-'
                i -= 1
        return new_column[::-1]

    def get_column_steps(self, column_pos, column):
        gaps = []
        stripped_col = column.rstrip('-')
        for row in range(len(stripped_col)):
            if column[row] == '-':
                gaps.append([column_pos, row])
        return gaps

    def score_affine_gap(self, puzzle):
        score = 0
        for row in puzzle:
            trimmed_row = row[:len(self.consensus)].rstrip('-')
            for pos in range(len(trimmed_row)):
                if trimmed_row[pos] == '-' and (pos == 0 or trimmed_row[pos - 1] != '-'):
                    score += self.gap_open
                elif trimmed_row[pos] == '-':
                    score += self.gap_extension
                elif trimmed_row[pos] in self.consensus[pos]:
                    score += self.match
                else:
                    score += self.mismatch

        return score

    def score_bonus_points(self, puzzle):
        bonus_points = 0
        rows = [list(filter(None, i)) for i in itertools.zip_longest(*puzzle)]
        for i, row in enumerate(rows):
            if i < len(self.consensus):
                row_consensus = list(self.consensus[i])
                if '-' in row_consensus:
                    row_consensus.remove('-')
                row_score = sum(map(row.count, row_consensus)) * self.match
                if row_score == len(rows[0]):
                    row_score = round(row_score * self.bonus, 2)
                    bonus_points += (row_score - len(rows[0]))
        return bonus_points
