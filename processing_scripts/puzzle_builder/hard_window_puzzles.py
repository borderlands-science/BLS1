import math
import random
import time
from processing_scripts.puzzle_builder import puzzle_functions as pf


def find_hard_windows(column_scores, window_size, parms, unwanted_cols, wanted_cols):
    """
    Identify window boundaries in the alignment.

    Parameters:
    column_scores : list
        Scores for each column in the alignment.
    window_size : int
        Size of the window to consider for puzzle creation.
    parms : dict
        Parameters for puzzle generation.
    unwanted_cols : list
        List of columns to be excluded.
    wanted_cols : list
        List of preferred columns.

    Returns:
    list
        List of tuples indicating the start and end of hard windows.
    """
    window_scores = {}
    start_end_offset = min(parms['flank_size'], parms['offset'])
    for i in range(start_end_offset, len(column_scores) - (window_size - 1) - start_end_offset):
        start, end = i, i + window_size
        score = sum(column_scores[start:end]) / (end - start)
        if score < parms['conservation_threshold'] and pf.check_blacklist_whitelist(range(start, end), parms,
                                                                                    unwanted_cols, wanted_cols):
            window_scores[(start, end)] = score

    sorted_points = [x[0] for x in sorted(window_scores.items(), key=lambda item: item[1])]
    if parms['n_windows'] != -1:
        return sorted_points[:parms['n_windows']]
    return sorted_points


def get_alignment_columns(aln, start, end):
    """
    Extract columns from an alignment within specified boundaries.

    Parameters:
    aln : list
        List of alignment sequences.
    start : int
        Starting position of the window.
    end : int
        Ending position of the window.

    Returns:
    list
        List of sequences within the specified window.
    """
    min_length = math.floor((end - start) / 2.0)
    seq_list = {}
    if start < 0 or start > end or start > len(aln[0].seq):
        start = 0
    if end == -1 or end > len(aln[0].seq):
        end = len(aln[0].seq)

    for record in aln:
        gapped_seq = str(record.seq[start:end])
        if record.id not in seq_list.keys():
            if gapped_seq.count('-') <= min_length:
                seq_list[record.id] = gapped_seq
        else:
            print("IDENTICAL RECORD IN FILE", record.id)
    seq_results = [(v, k) for k, v in seq_list.items()]
    return seq_results


def get_puzzle_states(puzzle_entries, consensus, flank, offset, slice, direction):
    """
    Determine the states of the puzzle being normal or flipped.

    Parameters:
    puzzle_entries : list
        List of puzzle sequences.
    consensus : list
        Consensus sequence for the puzzle.
    flank : int
        Size of the flank to be considered.
    offset : int
        Offset of the puzzle.
    slice : tuple
        Start and end positions of the puzzle window.
    direction : int
        Direction, 0 normal, 1 for flipped.

    Returns:
    list
        List of tuples containing puzzle sequences and consensus.
    """
    puzzle_states = []
    normal_puzzle_seqs = [x[0] + ('-' * flank) for x in puzzle_entries]
    if direction != 1 and slice[0] - offset >= 0 and slice[1] + flank <= len(consensus):
        normal_consensus = consensus[slice[0] - offset:slice[1] - offset + flank]
        puzzle_states.append((normal_puzzle_seqs, normal_consensus))

    flipped_puzzle_seqs = [x[1] + ('-' * flank) for x in puzzle_entries]
    if direction != 0 and slice[0] - flank >= 0 and slice[1] + offset <= len(consensus):
        flipped_consensus = list(reversed(consensus[slice[0] - flank + offset:slice[1] + offset]))
        puzzle_states.append((flipped_puzzle_seqs, flipped_consensus))

    return puzzle_states


def get_puzzle_cols(slice, flank_size, direction):
    """
    Get column indices for the puzzle based on direction and flanks.

    Parameters:
    slice : tuple
        Start and end positions of the puzzle window.
    flank_size : int
        Size of the flank.
    direction : int
        Direction of the puzzle.

    Returns:
    list
        List of tuples containing puzzle column indices and flank column indices.
    """
    puzzle_cols = []
    normal_puzzle_cols = list(range(slice[0], slice[1] + flank_size))
    normal_flank_cols = [slice[1] + x for x in range(flank_size)]

    flipped_puzzle_cols = list(range(slice[0] - flank_size, slice[1]))
    flipped_puzzle_cols.reverse()
    flipped_flank_cols = [slice[0] - x for x in range(1, flank_size + 1)]

    if direction != 1:
        puzzle_cols.append((normal_puzzle_cols, normal_flank_cols))
    if direction != 0:
        puzzle_cols.append((flipped_puzzle_cols, flipped_flank_cols))
    return puzzle_cols


def filter_seqs_for_distance_range(master, clustered_seqs, min_dist, max_dist):
    """
    Filter acceptable sequences within a specific distance range from a reference sequence.

    Parameters:
    master : tuple
        The reference sequence for comparison.
    clustered_seqs : list
        List of sequences to be filtered.
    min_dist : int
        Minimum distance threshold.
    max_dist : int
        Maximum distance threshold.

    Returns:
    list
        List of acceptable of sequences within the specified distance range.
    """
    min_length = math.floor(len(master[0]) / 2)
    acceptable_seqs = []
    for entry in clustered_seqs:
        if max_dist >= pf.get_seq_distance(master[0], entry[0]) >= min_dist and min_length >= entry[0].count('-'):
            acceptable_seqs.append(entry)

    return acceptable_seqs


def get_puzzle_entries(window_column_seqs, difficulty, parms):
    """
    Randomly chooses a references sequence from the window sequences, and then
    randomly chooses sequences from acceptable sequences to be the puzzle entries.

    Parameters:
    window_column_seqs : list
        List of valid sequences in the window.
    difficulty : int
        Difficulty level of the puzzle.
    parms : dict
        Parameters for puzzle generation.

    Returns:
    list
        List of puzzle entries selected for puzzle creation.
    """
    puzzle_entries = []

    puzzle_length = parms['grid_size'][str(difficulty)]["n_seqs"]
    reference_seq = random.choices(window_column_seqs, k=1)[0]
    acceptable_seqs = filter_seqs_for_distance_range(reference_seq, window_column_seqs,
                                                     parms['min_dist'], parms['max_dist'])
    if len(acceptable_seqs) >= puzzle_length - 1:
        puzzle_entries = random.choices(acceptable_seqs, k=puzzle_length - 1)
        puzzle_entries.append(reference_seq)
    return puzzle_entries


def prepare_puzzles(puzzle_entries, puzzle_width, column_consensus, slice, parms):
    """
    Prepares puzzles for creation, considering the states and sequences.

    Parameters:
    puzzle_entries : list
        List of puzzle entries.
    puzzle_width : int
        Width of the puzzle to be created.
    column_consensus : list
        Consensus sequence for the puzzle.
    slice : tuple
        Start and end positions of the puzzle window.
    parms : dict
        Parameters for puzzle generation.

    Returns:
    list
        List of tuples containing possible puzzle states.
    """
    puzzle_states = []
    only_seqs = [i[0] for i in puzzle_entries]
    if len(set(only_seqs)) == len(only_seqs):
        prepared_puz_entries = []
        for unprepared_seq in puzzle_entries:
            seq, flipped_seq = pf.prepare_sequences(unprepared_seq[0], puzzle_width)
            prepared_puz_entries.append((seq, flipped_seq, unprepared_seq[1]))

        puzzle_states = get_puzzle_states(prepared_puz_entries, column_consensus, parms['flank_size'],
                                          parms['offset'], slice, parms['direction'])
    return puzzle_states


def create_window_puzzles(ready_puzzles, total, align, column_consensus, slice, difficulty, parms):
    accepted_puzzles_in_window = 0
    puzzle_width = parms['grid_size'][str(difficulty)]["n_cols"] - parms['flank_size']
    window_column_seqs = get_alignment_columns(align, slice[0], slice[1])
    if not window_column_seqs:
        return

    for i in range(int(parms['puzzles_per_window'] * (parms['ppw_parm_1'] - parms['ppw_parm_2'] * difficulty))):
        if accepted_puzzles_in_window >= parms['max_puzzles_per_window']:
            break
        puzzle_entries = get_puzzle_entries(window_column_seqs, difficulty, parms)
        if not puzzle_entries:
            continue

        puzzle_states = prepare_puzzles(puzzle_entries, puzzle_width, column_consensus, slice, parms)
        if puzzle_states:
            puzzle_cols = get_puzzle_cols(slice, parms['flank_size'], parms['direction'])
            puzzle_ids = [x[1] for x in puzzle_entries]

            puzzles = pf.make_puzzles(puzzle_states, puzzle_ids, puzzle_cols, difficulty, parms)
            accept_puzzle = pf.validate_puzzles(puzzles, parms, difficulty)

            if accept_puzzle:
                if parms['puzzle_type'] == -1:
                    puzzles = puzzles[:int(len(puzzles) / 2)]
                for puzzle in puzzles:
                    if accepted_puzzles_in_window < parms['max_puzzles_per_window']:
                        dna_puzzle = puzzle.to_dnapuzzle()
                        ready_puzzles.append(dna_puzzle)
                        total += (puzzle.sol_score - puzzle.prior_score)
                        accepted_puzzles_in_window += 1


def build_puzzles(align, parms, difficulty, column_scores, column_consensus, subprocess=0, pickle_id="0",
                  unwanted_cols=[], wanted_cols=[]):
    """
    Build puzzles for a specific difficulty and a given alignment.

    Parameters:
    align : list
        List of alignment sequences.
    parms : dict
        Parameters for puzzle generation.
    difficulty : int
        Difficulty level for the puzzles.
    column_scores : list
        Scores for each column in the alignment.
    column_consensus : list
        Consensus sequence for the puzzles.
    subprocess : int
        Subprocess identifier.
    pickle_id : str
        Identifier for the pickle file.
    unwanted_cols : list
        List of columns to be excluded.
    wanted_cols : list
        List of preferred columns.
    """
    flank_size = parms['flank_size']
    start_time = time.time()

    ready_puzzles = []
    total = 0
    puzzle_width = parms['grid_size'][str(difficulty)]["n_cols"] - flank_size
    slice_boundaries = find_hard_windows(column_scores, puzzle_width, parms, unwanted_cols, wanted_cols)

    for slice in slice_boundaries:
        create_window_puzzles(ready_puzzles, total, align, column_consensus, slice,
                              difficulty, parms)

    pf.output_puzzles(ready_puzzles, pickle_id, difficulty, subprocess)
    if parms['output_details']:
        pf.print_details(ready_puzzles, difficulty, total, start_time)
