## Generating new puzzles

1. Modify puzzle generation parameters found in:\
    /borderlands-science/processing_scripts/puzzle_builder/puzzle_parameters/puzzle_parms.json

2. Modify blacklist and/or whitelist parameters found in:\
    /borderlands-science/processing_scripts/puzzle_builder/puzzle_parameters/blacklist.json\
    /borderlands-science/processing_scripts/puzzle_builder/puzzle_parameters/whitelist.json

3. Run /borderlands-science/processing_scripts/puzzle_builder/build_puzzles.py\
    i. `python build_puzzles.py 202XXXXXXXXXXX`\
    ii. Batch id is determined by year, month, and number of times run, followed by 6 0's for batch versions.\
        example: 20220802000000

4. Results are placed in json and pickle files found in the directories:\
    /borderlands-science/processing_scripts/puzzle_builder/jsons/\
    /borderlands-science/processing_scripts/puzzle_builder/pickles/

5. Run validate_pickles.py to ensure there are no issues with offsets, direction, or uncollapsed puzzle:\
    `python validate_pickles.py 20220100 20220101 ...`

### Parameter Help
1. Scoring scheme uses the Affine Gap Cost model.
    - Greedy solution algorithm only uses match and gap extension, typically set to 1.0, and -0.6
    - Pairwise solution algorithm uses full model. To mimic greedy algorithm scoring, set match = 1, mismatch = 0, gap_open = -0.6, gap_extension = -0.6
    - Pairwise solution algorithm works fine with standard values, match = 1, mismatch = -1, gap_open = -5, gap_extension = -1
2. Pairwise solution algorithm ignores row bonus, but applies it at the end if applicable
3. Pairwise solution algorithm with standard affine gap cost model does not work well with normal and mirrored puzzles simultaneously
4. Hard window builder is designed for more condensed alignment files such as Pasta
5. Soft window builder is designed for alignment files with large regions of gapped columns which it can ignore such as Muscle
6. Easiest way to populate the blacklist and whitelist files from command line is to make a mini python file which prints an array of numbers. Export the results to a file, and use command line to copy and paste

### Puzzle Parameters
Parameters influence number of puzzles generated, and methods for puzzle selection.
1. alignment_file [string] name of alignment file to generate puzzles from
2. difficulties [array of ints] determines which difficulties to use related to grid size table below
3. batches [int] number of times entire process is run
4. offset [int] number that the consensus is off by. If off by 1, all columns would need 1 gap to line up correctly
5. sub_processes [int] number of times a difficulty is run per batch. Converts values from ascii, so 3 sub processes will create files with a, b, and c
6. flank size [int] number of forced empty rows at the end of the puzzle to allow room to add gaps
7. n_windows [int] total number of windows than can be used for puzzle generation. If -1, will use all possible windows
8. puzzles_per_window [int] number of attempts to create puzzles per window
9. ppw_parm_1 [float] parameter for puzzles formula determining number of puzzles per window based on difficulty
10. ppw_parm_2 [float] parameter for puzzles formula determining number of puzzles per window based on difficulty
11. max_puzzles_per_window [int] limit for puzzles per window
12. conservation_threshold [float] Used to ensure window average score stays below threshold, so window has variety
13. min_improvement [int] Value which the solution improvement must exceed based on the original alignment to be considered a good puzzle
14. min_columns [int] The base number of columns in a puzzle for min_improvement
15. min_sequences [int] The base number of sequences in a puzzle for min_improvement
16. improv_per_column [float] If the number of columns in a puzzle exceeds min_columns, it will increase the min_improvement based on this value multiplied by the difference
17. improv_per_sequence [float] If the number of sequences in a puzzle exceeds min_columns, it will increase the min_improvement based on this value multiplied by the difference
18. min_moves [int] number of moves puzzle solution must have taken to be accepted
19. min_varied_steps [int] number of sequences that must have gaps added, to avoid a puzzle only containing gaps in a single sequence
20. min_dist [int] minimum distance between 2 sequences to be accepted for puzzle
21. max_dist [int] maximum distance between 2 sequences to be accepted for puzzle
22. bonus [float] multiplicative value for in a row matches the consensus
23. direction [int] 0 = normal puzzles, 1 = mirrored puzzles, -1 = both
24. puzzle_type [int] type of solution algorithm. 0 = greedy solution, 1 = pairwise solution, -1 = both solutions
25. target_builder [string] hard = standard window size defined by n_cols in grid size, soft = flexible window size where n_cols skips columns with gaps
26. match [float] point value if a nucleotide matches the consensus
27. mismatch [float] point value if a nucleotide does not match the consensus
28. gap_open [float] point value for creating a gap between two nucleotides
29. gap_extension [float] point value if adding a gap to an existing gapped region
30. use_blacklist [bool] Whether or not to use the blacklist file. Cannot use blacklist and whitelist simultaneously
31. use_whitelist [bool] Whether or not to use the whitelist file. Cannot use blacklist and whitelist simultaneously
32. output_details [bool] if false, will show less output while running
33. grid_size [dict] Dimensions of puzzle difficulties\
    33.1. Key: difficulty [string] integer indicating difficulty\
    33.2. n_seqs [int] number of sequences in a puzzle. From the puzzle perspective this would be columns\
    33.3. n_cols [int] number of columns in each sequence. From the puzzle perspective this would be rows

### Blacklist Parameters
Blacklist file to exclude certain columns or sequences which do not require coverage
1. ignore_overly_freq_columns [bool] If true, will find columns which generated excess puzzles in comparison, and exclude them to ensure other columns gain more focus
2. columns [array of ints] columns that do not require coverage. Puzzle generation will exclude any puzzles containing these columns
3. sequences [array of strings] sequences that do not require coverage. Puzzle generation will exclude any puzzles containing these sequences

### Whitelist Parameters
Whitelist file to include certain columns or sequences which require coverage
1. columns [array of ints] columns that require coverage. Puzzle generation will only accept a puzzle if all columns are whitelisted
2. sequences [array of strings] sequences that require coverage. Puzzle generation will only accept puzzles containing these sequences
