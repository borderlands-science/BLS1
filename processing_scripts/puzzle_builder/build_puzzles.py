import os,sys
import shutil

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import math
import glob
import copy
import heapq
import json
import pickle
import random
import string
import statistics
from multiprocessing import Process
from Bio import SeqIO
from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle
from processing_scripts.puzzle_builder import hard_window_puzzles as hw
from processing_scripts.puzzle_builder import soft_window_puzzles as sw


def get_consensus(totals, nucs):
    """
    Determine the consensus nucleotides for a column based on nucleotide counts.

    Parameters:
    totals : list
        A list of counts of each nucleotide in the column.
    nucs : list
        A list of nucleotide types.

    Returns:
    tuple
        A tuple containing the top two nucleotides based on counts.
    """
    heap = list(zip(totals, nucs))
    maxs = heapq.nlargest(2, heap)
    for i in range(2):
        if maxs[i][0] == 0 or maxs[i][1] not in nucs[1:]:
            maxs[i] = (0, '-')
    consensus = (maxs[0][1], maxs[1][1])
    return consensus


def assess_column_conservation(aln, window_type):
    """
    Assess the conservation of each column in an alignment.

    Parameters:
    aln : list
        List of alignment sequences.
    window_type : str
        Type of window ('soft' or other).

    Returns:
    tuple
        Returns two lists: one for column scores and one for consensus nucleotides.
    """
    if aln is None or len(aln) == 0:
        raise ValueError("alignment is empty. Invalid")

    nucs = ['-', 'A', 'C', 'G', 'T']
    consensuses = []
    column_scores = []
    tot = len(aln)

    for i in range(0, len(aln[0].seq)):
        col = [nuc[i].upper() for nuc in aln]
        col[:] = [x if x != 'U' else 'T' for x in col]
        totals = list(map(col.count, nucs))
        score = sum([(i/tot)**2 for i in totals])
        # Adjust score for 'soft' windows
        if window_type == 'soft' and totals[0] / sum(totals) > 0.99:
            score = -score
        column_scores.append(score)
        consensus = get_consensus(totals, nucs)
        consensuses.append(consensus)
    return column_scores, consensuses


def get_freq_cols(cov, column_range, column_blacklist):
    """
    Identify columns that are overly frequent in the coverage.

    Parameters:
    cov : list
        List of coverage values for each column.
    column_range : int
        The range of columns to consider.
    column_blacklist : list
        List of columns to exclude.

    Returns:
    list
        List of columns that are overly frequent.
    """
    range_totals = []
    for x in range(len(cov) - (column_range - 1)):
        window = list(range(x, x + column_range))
        if all(item not in column_blacklist for item in window):
            range_totals.append((x, sum(cov[x:x + column_range])))
    overly_freq_cols = column_blacklist
    totals = [x[1] for x in range_totals]
    m = statistics.mean(totals)
    for pos, total in range_totals:
        if total > 1.5 * m:
            for j in range(column_range):
                if pos + j not in overly_freq_cols:
                    overly_freq_cols.append(pos + j)

    return overly_freq_cols


def set_max_puzzles_per_window(parms, unwanted_cols, wanted_cols):
    """
    Determine the maximum number of puzzles per window.

    Parameters:
    parms : dict
        Dictionary of parameters.
    unwanted_cols : int
        Number of columns that are unwanted.
    wanted_cols : int
        Number of columns that are wanted.

    Returns:
    int
        Maximum number of puzzles that can be set per window.
    """
    max_puzzles = parms['max_puzzles_per_window']
    if wanted_cols == 0:
        wanted_cols = math.inf
    if parms['use_blacklist']:
        if unwanted_cols > 150:
            max_puzzles += int(unwanted_cols * 20)

        elif unwanted_cols > 120:
            max_puzzles += int(unwanted_cols * 2)

        elif unwanted_cols > 20:
            max_puzzles += int(unwanted_cols / 10)
    elif parms['use_whitelist']:
        if wanted_cols < 20:
            max_puzzles += int(wanted_cols * 20)

        elif wanted_cols < 50:
            max_puzzles += int(wanted_cols * 2)

        elif wanted_cols < 150:
            max_puzzles += int(wanted_cols / 10)
    return max_puzzles


def store_tasks(puzzles, parms):
    """
    Store puzzle tasks with their scoring and other information.

    Parameters:
    puzzles : list
        List of puzzle objects to be stored.
    parms : dict
        Dictionary of parameters used for scoring.

    Returns:
    list
        A list of dictionaries, each containing information about a puzzle task.
    """
    tasks = []
    for dp in puzzles:
        puzzle = Puzzle.from_dnapuzzle(dp, parms)
        cons_score = puzzle.score_consensus(puzzle.puzzle_sol)
        par_score = puzzle.base_score + math.ceil((cons_score - puzzle.base_score) / 2)
        for extra_token in dp.n_extra_tokens:
            original_code = ''.join(
                random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))
            if par_score == puzzle.base_score:
                par_score = cons_score
            info = {
                # Initialized at greedy score then updated based on feedback from players
                "parScore": par_score,
                # moves needed to reach parScore
                "parMoves": (puzzle.count_gaps(puzzle.puzzle_sol) + extra_token),
                # updated version of parScore
                "parScoreGlobal": 0,
                "parMovesGlobal": 0,
                "minimumScore": 0,
                "splitscreenPossible": puzzle.can_split_screen(7),
                "sourcePuzzleID": puzzle.puzzle_id,
                "scoring": {
                    "version": "GSQ",
                    # those are irrelvant
                    "gapPenalty": -3,
                    "gapLengthPenalty": -1,
                    "maxGapCount": (puzzle.count_gaps(puzzle.puzzle_sol) + extra_token),
                    "match": 1,
                    "mismatch": -1,
                    "acceptedPairs": puzzle.consensus,
                    "completeRowBonusMultiplier": parms['bonus']
                }

            }
            embargoDate = "2012-04-23T18:25:43.511Z"

            tasks.append(
                {"originalCode": original_code, "assets": puzzle.puzzle, "solution": puzzle.puzzle_sol,
                 "difficulty": puzzle.level, "info": info, "embargoDate": embargoDate})
    return tasks


def generate_json(tasks, pickle_id):
    """
    Generate a JSON dictionary from puzzle tasks.

    Parameters:
    tasks : list
        List of tasks to be included in the JSON.
    pickle_id : str
        Identifier for the pickle file.

    Returns:
    dict
        JSON dictionary with tasks and other information.
    """
    json_dict = {
        "code": pickle_id,
        "version": "2018-09-17",
        "taskCount": len(tasks),
        "goldStandardCount": 0,
        "priority": 2,
        "info": {
            "contacts": {
                "technical": {
                    "name": "John Doe",
                    "email": "john.doe@cs.mcgill.ca"
                },
                "authorization": {
                    "name": "Jerome Waldisphul",
                    "email": "jeromew@cs.mcgill.ca"
                }
            },
            "statistics": {
                "startTime": "2015-08-20T18:25:43.511Z",
                "endTime": "2015-08-20T18:45:43.511Z"
            }

        },
        "tasks": tasks
    }
    return json_dict


def write_json(align, pickle_id, parms):
    """
    Write puzzle tasks to a JSON file.

    Parameters:
    align : list
        Alignment list used for puzzle generation.
    pickle_id : str
        Identifier for the pickle file.
    parms : dict
        Parameters used for puzzle generation.

    Returns:
    tuple
        Returns coverage and the number of tasks written to the JSON file.
    """
    all_puzzles = []
    for proc in parms['difficulties']:
        for subproc in range(parms['sub_processes']):
            procname = "pickles/" + pickle_id + "/ready_puzzles" + pickle_id + "proc" + str(proc) + \
                       chr(97 + subproc) + ".pickle"
            try:
                these_puz = pickle.load(open(procname, "rb"))
                all_puzzles += these_puz
            except OSError:
                print('File not found:', procname)
                continue

    max_length = max([len(a) for a in align])
    cov = [0] * max_length
    for puz in all_puzzles:
        for i in puz.columns[:-parms['flank_size']]:
            cov[i] += 1

    print("PUZZLES DUMPED. NUMBER OF SOURCE PUZZLES IN BATCH", pickle_id, len(all_puzzles))
    tasks = store_tasks(all_puzzles, parms)
    pickle.dump(all_puzzles, open("pickles/" + pickle_id + "/" + pickle_id + ".pickle", "wb"))

    json_dict = generate_json(tasks, pickle_id)

    with open("jsons/" + pickle_id + '.json', 'w') as fp:
        json.dump(json_dict, fp)
    print("PUZZLES DUMPED. NUMBER OF TASKS IN BATCH", pickle_id, len(tasks))
    return cov, len(tasks)


def collapse_alignment(align):
    """
    Collapse an alignment by removing gaps and recording their positions.

    Parameters:
    align : list
        List of alignment sequences.

    Returns:
    tuple
        Returns a tuple with collapsed alignments and a map of original gap positions.
    """
    collapsed_align = []
    align_map = []
    for i in range(len(align)):
        align_map.append([])
        seq = copy.deepcopy(align[i])
        row_finder = seq.seq
        seq.seq = str(seq.seq).replace('-', '')
        for nuc in seq:
            orig_pos = row_finder.index(nuc)
            row_finder = ('-' * (orig_pos + 1)) + row_finder[orig_pos + 1:]
            align_map[i].append(orig_pos)
        collapsed_align.append(seq)
    return collapsed_align, align_map


def build_batch(align, parms, batch_id, unwanted_cols=[], wanted_cols=[]):
    """
    Build a batch of puzzles based on alignment sequences and parameters.

    Parameters:
    align : list
        List of alignment sequences.
    parms : dict
        Parameters for puzzle generation.
    batch_id : str
        Identifier for the batch.
    unwanted_cols : list
        List of columns to be excluded.
    wanted_cols : list
        List of preferred columns.

    Returns:
    tuple
        Returns coverage and the number of tasks in the batch.
    """
    collapsed_align, align_map = collapse_alignment(align)
    jobs = []
    path = os.path.join('pickles', batch_id)
    if not os.path.exists(path):
        os.mkdir(path)

    parms['max_puzzles_per_window'] = set_max_puzzles_per_window(parms,
                                                                 len(unwanted_cols), len(wanted_cols))
    column_scores, column_consensus = assess_column_conservation(align, parms['target_builder'])
    for difficulty in parms['difficulties']:
        for subproc in range(parms['sub_processes']):
            if parms['target_builder'] == 'soft':
                this_proc = Process(target=sw.build_puzzles, args=(collapsed_align, align_map, len(align[0].seq), parms,
                                                                   difficulty, column_scores, column_consensus, subproc,
                                                                   batch_id, unwanted_cols, wanted_cols))
            else:
                this_proc = Process(target=hw.build_puzzles, args=(align, parms, difficulty, column_scores,
                                                                   column_consensus, subproc, batch_id, unwanted_cols,
                                                                   wanted_cols))
            jobs.append(this_proc)
            this_proc.start()
    for i in jobs:
        i.join()

    cov, num_tasks = write_json(align, batch_id, parms)
    return cov, num_tasks


def main(unfiltered_align, batch_id, parms, blacklist, whitelist):
    """
    Main function to process alignment sequences and generate puzzle batches.

    Parameters:
    unfiltered_align : list
        List of unfiltered alignment sequences.
    batch_id : str
        Starting batch identifier.
    parms : dict
        Parameters for puzzle generation.
    blacklist : dict
        Blacklist of sequences and columns.
    whitelist : dict
        Whitelist of sequences and columns.
    """
    current_id = int(batch_id)

    align = []
    for record in unfiltered_align:
        if parms['use_blacklist'] and record.id not in blacklist['sequences']:
            align.append(record)
        elif parms['use_whitelist'] and (record.id in whitelist['sequences'] or len(whitelist['sequences']) == 0):
            align.append(record)
        elif not parms['use_blacklist'] and not parms['use_whitelist']:
            align.append(record)

    max_length = max([len(a) for a in align])
    column_range = parms['grid_size']['1']['n_cols']
    all_cov = [0] * max_length

    n_tasks = []
    blacklist_cols = blacklist['columns']
    whitelist_cols = whitelist['columns']
    if len(whitelist_cols) == 0:
        whitelist_cols = list(range(max_length))
    print("Generating", parms['batches'], "batches starting at ID", batch_id)
    for i in range(parms['batches']):
        cov, n = build_batch(align, parms, str(current_id), blacklist_cols, whitelist_cols)
        n_tasks.append(n)

        for j in range(len(all_cov)):
            all_cov[j] += cov[j]
        if not blacklist['ignore_overly_freq_columns']:
            blacklist_cols = get_freq_cols(all_cov, column_range, blacklist['columns'])
        current_id += 1000
        if parms['output_details']:
            print("BATCH ", i, " COMPLETE")
            print("BATCH COVERAGE", cov)
            print("GLOBAL COVERAGE", all_cov)
            if parms['use_blacklist']:
                print("Unwanted cols:", blacklist_cols)
            elif parms['use_whitelist']:
                print("Wanted cols:", whitelist_cols)

    log_path = os.path.join('puzzle_parameters/logs', str(batch_id)[:8])
    if not os.path.exists(log_path):
        os.mkdir(log_path)
    shutil.copy('puzzle_parameters/puzzle_parms.json', log_path)
    shutil.copy('puzzle_parameters/whitelist.json', log_path)
    shutil.copy('puzzle_parameters/blacklist.json', log_path)
    print(n_tasks)


if __name__ == '__main__':
    batch_id = int(sys.argv[1])
    puzzle_parms = glob.glob('puzzle_parameters/puzzle_parms.json')[0]
    with open(puzzle_parms) as parms_file:
        parms = json.load(parms_file)
    if parms['use_blacklist'] and parms['use_whitelist']:
        raise SystemExit('Can not use blacklist and whitelist simultaneously')

    blacklist_parms = glob.glob('puzzle_parameters/blacklist.json')[0]
    with open(blacklist_parms) as blacklist_file:
        blacklist = json.load(blacklist_file)
    whitelist_parms = glob.glob('puzzle_parameters/whitelist.json')[0]
    with open(whitelist_parms) as whitelist_file:
        whitelist = json.load(whitelist_file)

    aln_file = '../../alignments/' + parms['alignment_file']
    try:
        align = list(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise

    main(align, batch_id, parms, blacklist, whitelist)
