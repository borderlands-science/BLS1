__author__ = "Roman"
"""
This scripts generates a pikdik, which is a pickled dictionary indexed by originalCodes of the puzzles that appear in a 
specific json batch. It allows us to find puzzle objects given the info in a json. Run this every time you run upload.py
"""

import pickle
import json
import os

allIDs = {}
resultsYes = 0
resultsNo = 0

os.chdir("/data/pickles")

counter = 0
for fname in os.listdir():
    if "ready_puzzles" in fname or "dict" in fname or "ended" in fname:
        continue
    else:
        bcode = fname.split(".")[0]
        # TODO update batch name here befor run
        if "202112" not in bcode:
            continue
        batch = pickle.load(open(fname, "rb"))
        print("BATCH", bcode)
        puzd = {}

        with open("../jsons/" + bcode + ".json", "r") as g:
            j = json.load(g)
            realL = 0
            for p in batch:
                FOUND = False
                pind = realL
                if [] in p.puzzle:
                    continue
                CANCEL_PUZZLE = False
                for row in p.puzzle:
                    if row.count("-") == len(row):
                        CANCEL_PUZZLE = True
                if CANCEL_PUZZLE:
                    continue

                realL += 1
                jtasks = [pind * 3, pind * 3 + 1, pind * 3 + 2]  # 0:0,1,2   5:15,16,17   6:18,19,20
                if any(x > 10000 for x in jtasks):
                    continue
                codes = []
                firstCode = j["tasks"][jtasks[0]]["originalCode"]
                badpickle = False
                p.original_code = []
                p.playerResults = {}
                for task in jtasks:
                    t = j["tasks"][task]
                    oCode = t["originalCode"]
                    tokens = t["info"]["scoring"]["maxGapCount"]
                    parScore = t["info"]["parScore"]
                    sol = t["solution"]
                    codes.append(t["info"]["sourcePuzzleID"])
                    codes.append(t["originalCode"])

                    p.set_originalCode(oCode)
                    p.update_player_results(oCode, tokens, parScore, tokens, 0, 0, sol, sol)

                for c in p.original_code:
                    puzd[c] = p
            print("TASK", bcode, "N PICKLES", realL * 3, "N JSONS", len(j["tasks"]))
            pickle.dump(puzd, open("../pikdiks/d" + bcode + ".pickle", "wb"))
