This folder contains processing scripts used for building puzzles, getting the solutions, annotating the solutions, and filtering and aggregating the solutions. 

## Puzzle generation

For detailed puzzle generation, see folder `puzzle_builder`.\
**generate_pikdiks_v2.py**:  This scripts generates a pikdik, which is a pickled dictionary indexed by `originalCodes` of the puzzles that appear in a specific json batch. It allows us to find puzzle objects given the info in a json. This was run every time we generated new puzzles and uploaded them to our servers. Each time we refer to a *puzzles data*, we are referring to the pikdik, unless specified otherwise.
 

## Preparing the player submitted solutions for Multiple Sequence Alignment

1. The first step is to get the solutions and user info from the servers. This is performed in `incremental_batch_fixed_pareto.py`, `extract_data.py`, and `extract_user_data.py`. Since this can only be done using valid credentials and private libraries, one can not run these scripts outside our private servers. We do not own and therefore have the right to release the code for the data server and mentioned libraries. However, we have released the **output of these scripts: our data**, which can be found in this [link](https://games.cs.mcgill.ca/bls/).

2. The next step is to annotate the solutions with different parameters that will be used in filtering. This is done in `annotate.py`\
usage:\
    `python annotate.py [output_name] [input_extract_path] [output_dir] [puzzles_path] [-use_paretoDist] [-use_dist_score]`
    - output_name: the desired identifier for the output of the script.
    - input_extract_path: the input data, which was generated with the `extract...` scripts.
    - output_dir: the directory that the output should be in.
    - puzzles_path: the path to the data structure that contains information about puzzles the solutions of which are to be annotated.
    - -use_paretoDist: boolean, indicates whether to use pareto distance.
    - -use_dist_score: boolena, indicates whether to use weighted/horizontal weighted distance.

3. The final step is running the `analyze_results_forRealign.py` file, which filters the solutions and performs a simple aggregation, outputting a dictionary of four dataframes with all the useful information. It is relatively simple to run.\
usage:\
    `python analyze_results_forRealign.py [Output Dir] [Puzzles File] [Base Alignment File] [Solution File(s)] [Additional Args]`\
    Additional arguments:
    - -filter_puzzles_path [string] An additional file for specifying how to process puzzles
    - -filter_params_path [string] An additional file for specifying how to filter puzzles

`

## Other scripts found in this folder
- `dnapuzzle.py` defines the core puzzle class used in creating and storing puzzles.
- Some scripts define functions and utilities that are used by other scripts, such as `tools_for_puzzles.py`.
- `sample_puzzles.py` samples solutions. Running it is similar to `analyze_results_forRealign.py`.

