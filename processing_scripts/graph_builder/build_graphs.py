import os,sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import glob
import json
import pickle
from Bio import SeqIO
from processing_scripts.puzzle_builder.build_puzzles import assess_column_conservation
from processing_scripts.graph_builder.puzzle_plotter import PuzzlePlot

if __name__ == '__main__':
    batches = []
    for batch in sys.argv[1:]:
        batches.append(int(batch))
    # Loading puzzle parameters
    puzzle_parms = glob.glob('../puzzle_builder/puzzle_parameters/puzzle_parms.json')[0]
    with open(puzzle_parms) as parms_file:
        parms = json.load(parms_file)
    # Reading alignment file
    aln_file = '../../alignments/' + parms['alignment_file']
    # Loading blacklist parameters
    try:
        align = list(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise
    blacklist_parms = glob.glob('../puzzle_builder/puzzle_parameters/blacklist.json')[0]
    with open(blacklist_parms) as blacklist_file:
        blacklist = json.load(blacklist_file)

    # Gathering puzzles for each batch
    all_puzzles = []
    for batch in batches:
        batch_puzzles = []
        procname = "../puzzle_builder/pickles/" + str(batch) + "/" + str(batch) + ".pickle"
        try:
            these_puz = pickle.load(open(procname, "rb"))
            batch_puzzles += these_puz
            all_puzzles.append(batch_puzzles)
        except OSError:
            print('File not found:', procname)
            continue

     # Calculating column scores and consensus for the alignment
    all_columns = list(range(max([len(a) for a in align])))
    column_scores, column_consensus = assess_column_conservation(align)

    # Initializing the puzzle plotter with calculated data
    plotter = PuzzlePlot(all_puzzles, batches, all_columns, column_scores, column_consensus, align, parms, blacklist)
    # Generating various plots based on the puzzle data
    plotter.plot_column_cov_by_diff()
    plotter.plot_sequence_cov_by_diff()
    plotter.plot_mean_improv_by_diff()
    plotter.plot_columns_by_window_score()
    # Creating additional plots if three batches are provided - dirty
    if len(batches) == 3:
        plotter.build_group_bar_graph(['Old Greedy', 'New Greedy', 'Greedy & PW'])
        plotter.plot_all_mean_improv_by_diff(['Old Greedy', 'New Greedy', 'Greedy & PW'])
