import os, sys
import pickle
import copy
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
from Bio import SeqIO

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

from processing_scripts import dnapuzzle


def get_column_coverage(puzzles, align_length):
    """
    Calculate and output column coverage graphs.

    This function evaluates the column coverage of the puzzles and generates
    bar graphs to visualize the coverage across different difficulty levels.

    Parameters
    ----------
    puzzles : dict
        Dictionary of puzzles to analyze.
    align_length : int
        Length of the alignment.

    Returns
    -------
    None
    """
    col_diff_count = eval_col_coverage(puzzles, align_length)
    output_col_cov_graphs(col_diff_count)


def get_full_coverage(puzzles, align_dict):
    """
    Calculate and output full coverage heatmaps.

    This function evaluates both full and simple coverage of the puzzles and generates
    heatmaps to visualize the coverage for each puzzle.

    Parameters
    ----------
    puzzles : dict
        Dictionary of puzzles to analyze.
    align_dict : dict
        Dictionary of alignment sequences.

    Returns
    -------
    None
    """
    full_coverage_dict, simple_coverage_dict = eval_full_coverage(puzzles, align_dict)
    output_ful_cov_heatmaps(full_coverage_dict, "puzzle_full_coverage.html")
    output_ful_cov_heatmaps(simple_coverage_dict, "puzzle_full_simple_coverage.html")


def eval_col_coverage(puzzles, align_length):
    """
    Evaluate column coverage for each puzzle difficulty level.

    Parameters
    ----------
    puzzles : dict
        Dictionary of puzzles to analyze.
    align_length : int
        Length of the alignment.

    Returns
    -------
    list
        A list containing column coverage counts for each difficulty level.
    """
    col_diff_count = []
    for i in range(9):
        col_diff_count.append([0] * align_length)
    for puzzle in puzzles.values():
        for col in puzzle.columns:
            col_diff_count[puzzle.level - 1][col] += 1
    return col_diff_count


def eval_full_coverage(puzzles, align_dict):
    """
    Evaluate full coverage puzzles.

    Parameters
    ----------
    puzzles : dict
        Dictionary of puzzles to analyze.
    align_dict : dict
        Dictionary of alignment sequences.

    Returns
    -------
    tuple
        A tuple containing two lists: full coverage and simple coverage.
    """
    full_coverage_dict = []
    empty_matrix = fill_matrix(align_dict)
    for i in range(9):
        full_coverage_dict.append(copy.deepcopy(empty_matrix))
    simple_coverage_dict = copy.deepcopy(full_coverage_dict)
    for puzzle in puzzles.values():
        for seq in puzzle.IDs:
            for col in puzzle.columns:
                full_coverage_dict[puzzle.level - 1][seq][col] += 1
                simple_coverage_dict[puzzle.level - 1][seq][col] = 1
    return full_coverage_dict, simple_coverage_dict


def fill_matrix(align):
    """
    Create an empty matrix based on alignment sequences.

    Parameters
    ----------
    align : dict
        Dictionary of alignment sequences.

    Returns
    -------
    dict
        A dictionary with sequence IDs as keys and lists of zeros as values.
    """
    dict = {}
    for id, seq in align.items():
        dict[id] = [0] * len(seq.seq)
    return dict


def output_col_cov_graphs(col_diff_count):
    """
    Output column coverage graphs as HTML files.

    Parameters
    ----------
    col_diff_count : list
        List containing column coverage counts for each difficulty level.

    Returns
    -------
    None
    """
    difficulties = list(range(1, 10))
    cov_dict = dict(zip(difficulties, col_diff_count))
    test = pd.DataFrame.from_dict(cov_dict)
    fig = px.bar(test, x=test.index, y=test.columns)
    fig.update_layout(title="Column Coverage by Difficulty", xaxis=dict(title="Columns"), yaxis=dict(title="Count"),
                      legend=dict(title="Difficulties"))
    fig_file = "graphs/puzzle_col_coverage.html"
    fig.write_html(fig_file)
    return


def output_ful_cov_heatmaps(full_coverage_dict, output_file):
    """
    Output full coverage heatmaps as HTML files.

    Parameters
    ----------
    full_coverage_dict : list
        A list of dictionaries, each representing coverage for a difficulty level.
    output_file : str
        Name of the output HTML file.

    Returns
    -------
    None
    """
    full_coverage_dfs = []
    for i in range(len(full_coverage_dict)):
        full_coverage_dfs.append(pd.DataFrame.from_dict(full_coverage_dict[i], orient='index'))

    frames = [go.Frame(data=go.Heatmap(z=df.values, x=df.columns, y=df.index), name=i+1)
              for i, df in enumerate(full_coverage_dfs)]

    fig = go.Figure(data=frames[0].data, frames=frames).update_layout(
        updatemenus=[
            {
                "buttons": [{"args": [None, {"frame": {"duration": 500, "redraw": True}}],
                             "label": "Play", "method": "animate", },
                            {"args": [[None], {"frame": {"duration": 0, "redraw": False},
                                               "mode": "immediate", "transition": {"duration": 0}, }, ],
                             "label": "Pause", "method": "animate", }, ],
                "type": "buttons",
            }
        ],
        # iterate over frames to generate steps... NB frame name...
        sliders=[{"steps": [{"args": [[f.name], {"frame": {"duration": 0, "redraw": True},
                                                 "mode": "immediate", }, ],
                             "label": f.name, "method": "animate", }
                            for f in frames], }],
        height=800,
        yaxis={"title": 'Sequences'},
        xaxis={"title": "Full Difficulty Coverage", "tickangle": 45, 'side': 'top'},
        title_x=0.5,

    )

    fig_file = "graphs/" + output_file
    fig.write_html(fig_file)
    return


if __name__ == '__main__':
    aln_file = sys.argv[1]
    puzzles_path = sys.argv[2]

    try:
        puzzles = pickle.load(open(puzzles_path, "rb"))
        align_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise

    get_column_coverage(puzzles, len(list(align_dict.values())[0].seq))
    get_full_coverage(puzzles, align_dict)
