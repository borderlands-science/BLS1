import math
import os
import numpy as np
import matplotlib.pyplot as plt
import processing_scripts.puzzle_builder.puzzle_functions as pf
from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle
from processing_scripts.puzzle_builder.puzzle.greedy_puzzle import GreedyPuzzle

class PuzzlePlot:
    """
    Class to create various plots for DNA puzzles.

    This class is responsible for generating different types of visualizations
    based on DNA puzzle data. It includes methods for plotting column coverage,
    sequence coverage, and scoring improvements across different difficulty levels.

    Attributes
    ----------
    puzzles : list
        List of puzzles to be visualized.
    batches : list
        List of batch identifiers.
    columns : list
        List of all column indices.
    filtered_columns : ndarray
        Array of column indices, filtered by the blacklist.
    column_scores : list
        List of scores for each column.
    column_consensus : list
        List of consensus values for each column.
    aligns : list
        List of sequence alignments.
    parms : dict
        Dictionary of parameters used for puzzle processing.
    blacklist : dict
        Dictionary specifying blacklisted columns.

    Methods
    -------
    plot_column_cov_by_diff():
        Plots the coverage of columns across different difficulty levels.
    plot_sequence_cov_by_diff():
        Plots the coverage of sequences across different difficulty levels.
    plot_mean_improv_by_diff():
        Plots the average improvement in score by difficulty.
    plot_all_mean_improv_by_diff(labels):
        Plots the average improvement in score by difficulty with given labels.
    plot_columns_by_window_score():
        Plots column coverage ordered by window score.
    build_group_bar_graph(labels):
        Builds a group bar graph for total puzzles per difficulty level.
    """


    def __init__(self, puzzles, batches, all_columns, column_scores, column_consensus, aligns, parms, blacklist):
        self.puzzles = puzzles
        self.batches = batches
        self.columns = all_columns
        self.filtered_columns = np.setdiff1d(all_columns, blacklist['columns'])
        self.column_scores = column_scores
        self.column_consensus = column_consensus
        self.aligns = aligns
        self.parms = parms
        self.blacklist = blacklist

        for batch in batches:
            if not os.path.exists('graphs/' + str(batch)):
                os.mkdir('graphs/' + str(batch))

    def plot_column_cov_by_diff(self):
        for index, batch_puzzles in enumerate(self.puzzles):
            # Initialize dictionary to count column coverage for each difficulty level
            diff_column_count = {}
            # Counting column coverage for each puzzle in the batch
            for puzzle in batch_puzzles:
                # Initialize the count array for new difficulty levels
                if puzzle.level not in diff_column_count.keys():
                    diff_column_count[puzzle.level] = [0]*len(self.columns)
                # Increment coverage count for each column in the puzzle
                for i in puzzle.columns[:-self.parms['flank_size']]:
                    diff_column_count[puzzle.level][i] += 1
            # Filter out blacklisted columns from the count
            for key in diff_column_count.keys():
                diff_column_count[key] = [diff_column_count[key][i] for i in self.filtered_columns]

            width = 0.35
            fig, ax = plt.subplots()
            bottom = [0]*len(self.filtered_columns)
            for key, value in diff_column_count.items():
                ax.bar(self.filtered_columns, value, width, bottom=bottom, label=key)
                for i in range(len(bottom)):
                    bottom[i] += value[i]
            ax.set_ylabel('Total')
            ax.set_xlabel('Columns')
            ax.set_title('Coverage of columns by difficulty')
            ax.legend()

            plt.savefig('graphs/' + str(self.batches[index]) + '/ColumnCoverageByDiff.png')
            plt.clf()
        return 0


    def plot_sequence_cov_by_diff(self):
        for index, batch_puzzles in enumerate(self.puzzles):
            diff_sequence_count = {}
            sequence_count = {}
            for seq in self.aligns:
                sequence_count[seq.id] = 0

            for puzzle in batch_puzzles:
                if puzzle.level not in diff_sequence_count.keys():
                    diff_sequence_count[puzzle.level] = sequence_count
                for i in puzzle.IDs:
                    diff_sequence_count[puzzle.level][i] += 1

            width = 0.35
            fig, ax = plt.subplots()
            bottom = [0] * len(self.aligns)
            x_axis = [i.id for i in self.aligns]
            for key, value in diff_sequence_count.items():
                x_values = list(value.values())
                ax.bar(x_axis, x_values, width, bottom=bottom, label=key)
                for i in range(len(bottom)):
                    bottom[i] += x_values[i]
            ax.set_ylabel('Total')
            ax.set_xlabel('Sequences')
            ax.set_title('Coverage of sequences by difficulty')
            ax.legend()

            plt.savefig('graphs/' + str(self.batches[index]) + '/SequenceCoverageByDiff.png')
            plt.clf()
        return 0

    def plot_mean_improv_by_diff(self):
        for index, batch_puzzles in enumerate(self.puzzles):
            diff_score_improv = {}

            for dp in batch_puzzles:
                puzzle = Puzzle.from_dnapuzzle(dp, self.parms)
                if puzzle.level not in diff_score_improv.keys():
                    diff_score_improv[puzzle.level] = []
                if int(str(self.batches[index])[:4]) < 2021:
                    diff_score_improv[puzzle.level].append(
                        dp.greedy_consensus_score - (dp.greedy_n_gaps * 0.6) - puzzle.base_score)
                else:
                    diff_score_improv[puzzle.level].append(puzzle.sol_score - puzzle.base_score)

            mean_scores = []
            yerr = []
            for key, scores in diff_score_improv.items():
                scores = diff_score_improv[key]
                yerr.append([np.mean(scores) - min(scores), max(scores) - np.mean(scores)])
                mean_scores.append(np.mean(scores))

            yerr = np.transpose(yerr)
            plt.errorbar(self.parms['difficulties'], mean_scores, yerr=yerr, fmt='o-', capsize=3)
            plt.title('Average improvement by difficulty')
            plt.ylabel('score improvement from base')
            plt.xlabel('difficulties')
            plt.savefig('graphs/' + str(self.batches[index]) + '/MeanImprovByDiff.png')
            plt.clf()

    def plot_all_mean_improv_by_diff(self, labels):
        for index, batch_puzzles in enumerate(self.puzzles):
            diff_score_improv = {}
            for dp in batch_puzzles:
                puzzle = GreedyPuzzle.from_dnapuzzle(dp, self.parms)
                puzzle.calc_prior_puzzle()
                if puzzle.level not in diff_score_improv.keys():
                    diff_score_improv[puzzle.level] = []
                if int(str(self.batches[index])[:4]) < 2021:
                    diff_score_improv[puzzle.level].append(
                        dp.greedy_consensus_score - (dp.greedy_n_gaps * 0.6) - puzzle.base_score)
                else:
                    diff_score_improv[puzzle.level].append(puzzle.sol_score - puzzle.prior_score)

            mean_scores = []
            yerr = []
            for key, scores in diff_score_improv.items():
                scores = diff_score_improv[key]
                yerr.append([np.mean(scores) - min(scores), max(scores) - np.mean(scores)])
                mean_scores.append(np.mean(scores))
            yerr = np.transpose(yerr)
            plt.errorbar(self.parms['difficulties'], mean_scores, yerr=yerr, fmt='o-', capsize=3, label=labels[index])

        plt.title('Average improvement by difficulty')
        plt.ylabel('score improvement from base')
        plt.xlabel('difficulties')
        plt.legend()
        plt.savefig('graphs/allImprovByDiff.png')
        plt.clf()

    def plot_columns_by_window_score(self):
        for index, batch_puzzles in enumerate(self.puzzles):
            diff_column_count = {}

            for puzzle in batch_puzzles:
                if puzzle.level not in diff_column_count.keys():
                    diff_column_count[puzzle.level] = [0]*len(self.columns)
                for i in puzzle.columns[:-self.parms['flank_size']]:
                    diff_column_count[puzzle.level][i] += 1

            for key in diff_column_count.keys():
                diff_column_count[key] = [diff_column_count[key][i] for i in self.filtered_columns]
                windows = pf.find_windows(self.parms['flank_size'], self.parms['offset'], self.column_scores,
                                self.parms['grid_size'][str(key)]["n_cols"] - self.parms['flank_size'],
                                self.blacklist['columns'], self.parms['n_windows'],
                                self.parms['conservation_threshold'], self.parms['target_builder'])
                if self.parms['target_builder'] == 'soft':
                    diff_column_count[key] = [diff_column_count[key][i] for i in windows]
                else:
                    diff_column_count[key] = [diff_column_count[key][i[0]] for i in windows]

            max_length = max([len(value) for value in diff_column_count.values()])
            for key, value in diff_column_count.items():
                if len(value) < max_length:
                    diff_column_count[key] = value + ([0]*(max_length-len(value)))

            width = 0.35
            fig, ax = plt.subplots()
            bottom = [0] * max_length
            for key, value in diff_column_count.items():
                ax.bar(list(range(max_length)), value, width, bottom=bottom, label=key)
                for i in range(len(bottom)):
                    bottom[i] += value[i]

            ax.set_ylabel('Total')
            ax.set_xlabel('Windows')
            ax.set_title('Column coverage ordered by window score')
            ax.legend()

            plt.savefig('graphs/' + str(self.batches[index]) + '/ColumnsByWinScore.png')
            plt.clf()
        return 0

    def build_group_bar_graph(self, labels):
        width = 0.2
        bar_positions = list(range(len(self.puzzles)))
        bar_positions = [i - math.floor(len(self.puzzles)/2) for i in bar_positions]
        x = np.arange(len(self.parms['difficulties']))
        for index, batch_puzzles in enumerate(self.puzzles):
            diff_puzzle_count = {}
            for puzzle in batch_puzzles:
                if puzzle.level not in diff_puzzle_count.keys():
                    diff_puzzle_count[puzzle.level] = 0
                diff_puzzle_count[puzzle.level] += 1
            plt.bar(x + (bar_positions[index] * width), list(diff_puzzle_count.values()), width, label=labels[0])

        plt.ylabel('Total puzzles')
        plt.xlabel('Difficulties')
        plt.title('Total puzzles per difficulty')
        plt.xticks(x, self.parms['difficulties'])
        plt.legend(labels)

        plt.savefig('graphs/totalPuzzlesByPuzzleType.png')
        plt.clf()
