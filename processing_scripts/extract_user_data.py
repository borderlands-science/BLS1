from dotenv import load_dotenv

load_dotenv(".env")
import pickle
import ast
import json
from phylox_analysis.helpers.mmos_data_processor import reverse_shuffle

resultsA = pickle.load(open("JUNE_157_180ALNRESULTS0510.pickle", "rb"))
resultsB = pickle.load(open("JUNE_180_200ALNRESULTS0510.pickle", "rb"))
resultsC = pickle.load(open("JUNE_200_220ALNRESULTS0510.pickle", "rb"))
resultsD = pickle.load(open("JUNE_220_240ALNRESULTS0510.pickle", "rb"))
resultsE = pickle.load(open("JUNE_260_267ALNRESULTS0510.pickle", "rb"))

sol_dict = {}
densities = [0] * 186
solsToGet = []

pr = {}
for res in [resultsA, resultsB, resultsC, resultsD, resultsE]:
    for i in sorted(res.keys()):
        print(i)
        pos = ast.literal_eval(i)
        for sol in res[i]:
            for solID in sol[5]:
                solsToGet.append(solID)
toGet = list(set(solsToGet))
print(len(toGet))

allsols = pickle.load(open("ALLSOLS157-267batches.pickle", "rb"))
print("NUMBER OF ALLSOLS", len(allsols))
solutions = {}
for s in allsols:
    iD = s.id
    solutions[iD] = s

noProb = 0
prob = 0
for i in toGet:
    try:
        result_j = json.loads(solutions[i].result)
        solution_deshuffled = reverse_shuffle(result_j["solution"], result_j["shuffle"])
        result_j["solution"] = solution_deshuffled
        sol_dict[i] = (solutions[i].player_id, json.dumps(result_j))
        noProb += 1
    except:
        print("WE HAVE A PROBLEM")
        print(i)
        prob += 1
        continue
print("no problem:", noProb, "problem", prob)
count = 0
for res in [resultsA, resultsB, resultsC, resultsD, resultsE]:
    for i in sorted(res.keys()):
        pos = ast.literal_eval(i)
        if len(pos) > 0:
            if str(pos) not in pr:
                pr[str(pos)] = []
            for sol in res[i]:
                this_sol = {"originalCode": sol[0], "nGaps": sol[3], "score": sol[4], "pareto": sol[2],
                            "playerSolutions": []}
                for solID in sol[5]:
                    if solID in sol_dict:
                        this_sol["playerSolutions"].append(sol_dict[solID])
                        count += 1
                    else:
                        prob += 1
                        print("ERROR")
                pr[str(pos)].append(this_sol)
print("NEW DICTIONARY", pr.keys())
for i in pr.keys():
    print(i, len(pr[i]))
pickle.dump(pr, open("ALLSOLS157_267_missing240_260_withAnswers_with_playerIDs_lighter.pickle", "wb"))
print("NUMBER OF KEYS IN DICT", len(pr.keys()))
print("no problem:", noProb, "problem", prob, "total correctly processed", count)
