import copy
import itertools
import json
import math
import os, sys
import heapq
import pickle
import time
import pandas as pd
import argparse
import numpy as np
from datetime import date
from collections import deque
from operator import itemgetter
from Bio import SeqIO

sys.path.append('..')


def get_consensus(totals, nucs):
    """
    Determine the consensus nucleotides for a column based on nucleotide counts.

    Parameters:
    totals : list
        A list of counts of each nucleotide in the column.
    nucs : list
        A list of nucleotide types.

    Returns:
    tuple
        A tuple containing the top two nucleotides based on counts.
    """
    heap = list(zip(totals, nucs))
    maxs = heapq.nlargest(2, heap)
    for i in range(2):
        if maxs[i][0] == 0 or maxs[i][1] not in nucs[1:]:
            maxs[i] = (0, '-')
    consensus = (maxs[0][1], maxs[1][1])
    return consensus


def assess_column_conservation(aln):
    """
    Assess the conservation of each column in an alignment.

    Parameters:
    aln : list
        List of alignment sequences.

    Returns:
    list
        List of consensus nucleotides for each column in the alignment.
    """
    if aln is None or len(aln) == 0:
        raise ValueError("alignment is empty. Invalid")

    nucs = ['-', 'A', 'C', 'G', 'T']
    consensuses = []

    for i in range(0, len(aln[0].seq)):
        col = [nuc[i].upper() for nuc in aln]
        col[:] = [x if x != 'U' else 'T' for x in col]
        totals = list(map(col.count, nucs))
        consensus = get_consensus(totals, nucs)
        consensuses.append(consensus)
    return consensuses


def find_offset(puzzle, column_consensus):
    """
    Find the offset for a puzzle based on the column consensus.

    Parameters:
    puzzle : object
        Puzzle object to be evaluated.
    column_consensus : list
        List of consensus nucleotides for columns.

    Returns:
    tuple
        Tuple indicating if a valid offset was found and the offset value.
    """
    offset = 0
    while offset < 3:
        shifted_columns = deque(puzzle.columns)
        shifted_columns.rotate(offset)
        shifted_columns = list(shifted_columns)
        if puzzle.columns[0] < puzzle.columns[-1]:
            for i in range(0, offset):
                shifted_columns[i] = shifted_columns[offset] + (i - offset)
        else:
            for i in range(0, offset):
                shifted_columns[i] = shifted_columns[offset] + (offset - i)
        shifted_puzzle = copy.deepcopy(puzzle)
        shifted_puzzle.columns = shifted_columns
        match = validate_guide(shifted_puzzle, column_consensus)
        if match:
            return True, offset
        else:
            offset += 1
    return False, offset


def evaluate_puzzle(puzzle, align, consensus):
    """
    Evaluate a puzzle for validity based on alignment and consensus.

    Parameters:
    puzzle : object
        Puzzle object to be evaluated.
    align : list
        List of alignment sequences.
    consensus : list
        List of consensus nucleotides for columns.

    Returns:
    tuple
        Tuple indicating if the puzzle is valid and the offset value.
    """
    valid_puzzle = validate_puzzle(puzzle, align)
    valid_guide, offset = find_offset(puzzle, consensus)
    valid_flank = validate_flank(puzzle)

    if not valid_puzzle:
        for i in range(len(puzzle.uncollapsed_puzzle)):
            puzzle.uncollapsed_puzzle[i] = puzzle.uncollapsed_puzzle[i][2:] + ['-', '-']
        valid_puzzle = validate_puzzle(puzzle, align)
    if valid_puzzle and valid_guide and valid_flank:
        return True, offset
    else:
        return False, -1


def validate_guide(puzzle, column_consensus):
    """
    Validate the guide of a puzzle against the column consensus.

    Parameters:
    puzzle : object
        Puzzle object to be validated.
    column_consensus : list
        List of consensus nucleotides for columns.

    Returns:
    bool
        True if the puzzle guide is valid, False otherwise.
    """
    for i in range(len(puzzle.columns)):
        global_guide = column_consensus[puzzle.columns[i]]
        puzzle_guide = puzzle.consensus[i]
        if puzzle_guide != global_guide:
            return False
    return True


def validate_puzzle(puzzle, align):
    """
    Validate the puzzle against the original alignment.

    Parameters:
    puzzle : object
        Puzzle object to be validated.
    align : list
        List of alignment sequences.

    Returns:
    bool
        True if the puzzle is valid, False otherwise.
    """
    cols = len(puzzle.columns) - len(puzzle.flanks)
    width = math.ceil(cols / 2.0)

    for i in range(len(puzzle.uncollapsed_puzzle)):
        puz_row = puzzle.uncollapsed_puzzle[i]
        full_seq = align[puzzle.IDs[i]].seq
        for j in range(width):
            column = puzzle.columns[j]
            if puz_row[j] != full_seq[column]:
                return False
    return True


def validate_flank(puzzle):
    """
    Validate the flank of a puzzle.

    Parameters:
    puzzle : object
        Puzzle object to be validated.

    Returns:
    bool
        True if the puzzle flank is valid, False otherwise.
    """
    if puzzle.columns[-2:] == puzzle.flanks:
        return True
    for row in puzzle.puzzle:
        if len(row) > len(puzzle.columns):
            return False
    return True


def validate_solution(solution, puzzle, filter_params):
    """
    Validate a puzzle solution against given parameters.

    Parameters:
    solution : dict
        Solution object to be validated.
    puzzle : object
        Puzzle object related to the solution.
    filter_params : DataFrame
        Parameters to filter the solution.

    Returns:
    bool
        True if the solution meets the filter parameters, False otherwise.
    """
    comparator_dict = {
        "<": lambda x, y: x < y,
        ">": lambda x, y: x > y,
        "=": lambda x, y: x == y,
        "<=": lambda x, y: x <= y,
        ">=": lambda x, y: x >= y,
        "!=": lambda x, y: x != y
    }

    for index, row in filter_params.iterrows():
        if row['Key'] in solution:
            if not comparator_dict[row["Comparator"]](solution[row['Key']], row['Value']):
                return False

    max_sol_len = max([len(x) for x in solution['playerSolutions'][0]])
    if max_sol_len > len(puzzle.columns):
        return False
    if puzzle.puzzle != solution['pikdik'].puzzle or puzzle.columns != solution['pikdik'].columns:
        return False
    return True


def get_default_puzzle_params():
    puzzle_params = {
        "difficulties": [1, 2, 3, 4, 5, 6, 7, 8, 9],
        "copy_to_dup_instances": False,
        "inspect_offset_diff": False,
        "inspect_direction_diff": False,
        "rows_to_inspect": {
            "0": {
                "1": [0, 1, 2, 3],
                "2": [0, 1, 2, 3],
                "3": [0, 1, 2, 3, 4, 5],
                "4": [0, 1, 2, 3, 4, 5],
                "5": [0, 1, 2, 3, 4, 5],
                "6": [0, 1, 2, 3, 4, 5],
                "7": [0, 1, 2, 3, 4, 5],
                "8": [0, 1, 2, 3, 4, 5],
                "9": [0, 1, 2, 3, 4, 5]
            }
        }
    }
    return puzzle_params


def get_default_filter_params():
    return pd.DataFrame({'Key': ['paretoDist'], 'Comparator': ["<"], 'Value': [1.5]})


def find_puzzle_seq_dups(puzzle, cols_inspected, align, puzzle_params):
    """
    Identify duplicate sequences in the puzzle.

    Parameters:
    puzzle : object
        Puzzle object to be inspected.
    cols_inspected : list
        List of column indices to inspect for duplicates.
    align : dict
        Dictionary of aligned sequences.
    puzzle_params : dict
        Parameters for puzzle processing.

    Returns:
    dict
        A dictionary mapping each sequence ID to a list of duplicate sequence IDs.
    """
    seq_dups = {i: [] for i in puzzle.IDs}
    if not puzzle_params["copy_to_dup_instances"]:
        return {i: [i] for i in puzzle.IDs}
    for seq_id in puzzle.IDs:
        base_nucs = list(itemgetter(*cols_inspected)(align[seq_id].seq))
        for align_id, seq in align.items():
            align_nucs = list(itemgetter(*cols_inspected)(seq.seq))
            if base_nucs == align_nucs:
                seq_dups[seq_id].append(align_id)
    return seq_dups


def map_orig_sol(puzzle, sol):
    """
    Map the original solution to the puzzle structure.

    Parameters:
    puzzle : object
        Puzzle object to which the solution will be mapped.
    sol : list
        List of sequences representing the solution.

    Returns:
    dict
        A dictionary mapping puzzle column indices to solution column indices.
    """
    min_sol_len = min([len(x) for x in sol])
    if min_sol_len < len(puzzle.columns):
        for i in range(len(sol)):
            sol[i] = sol[i].ljust(len(puzzle.columns), '-')
    align_map = {}
    for i in range(len(puzzle.uncollapsed_puzzle)):
        seq_id = puzzle.IDs[i]
        puzzle_row = copy.deepcopy(puzzle.uncollapsed_puzzle[i])
        puzzle_row = ''.join(puzzle_row)
        sol_row = copy.deepcopy(sol[i])
        puzzle_finder = puzzle_row
        sol_finder = sol_row
        sol_row = sol_row.replace('-', '')
        for nuc in sol_row:
            try:
                puz_pos = puzzle_finder.index(nuc)
                sol_pos = sol_finder.index(nuc)
                puzzle_finder = ('-' * (puz_pos + 1)) + puzzle_finder[puz_pos + 1:]
                sol_finder = ('-' * (sol_pos + 1)) + sol_finder[sol_pos + 1:]
                if puzzle.columns[puz_pos] not in align_map.keys():
                    align_map[puzzle.columns[puz_pos]] = {}
                align_map[puzzle.columns[puz_pos]][seq_id] = puzzle.columns[sol_pos]
            except ValueError:
                pass
    return align_map


def calc_seq_seq(seqs, pos, puzzle):
    """
    Calculate the sequence difference for a given position in the puzzle.

    Parameters:
    seqs : dict
        Dictionary mapping sequence IDs to their positions.
    pos : int
        The position in the puzzle to calculate the difference for.
    puzzle : object
        Puzzle object used for the calculation.

    Returns:
    tuple
        A tuple containing the sequence difference and the count of sequences.
    """
    other_seqs = list(seqs.values())
    other_seqs.remove(pos)
    seq_diff = sum(other_seqs) - (pos * len(other_seqs))
    if puzzle.columns[0] < puzzle.columns[-1]:
        seq_diff = -seq_diff
    return seq_diff, len(other_seqs)


def get_sol(puzzle, solution):
    """
    Extract the solution sequences from a puzzle solution object.

    Parameters:
    puzzle : object
        Puzzle object related to the solution.
    solution : dict
        Solution object containing the solution sequences.

    Returns:
    list
        List of sequences representing the solution.
    """
    sol = copy.deepcopy(solution['playerSolutions'][0])
    min_sol_len = min([len(x) for x in sol])
    if min_sol_len < len(puzzle.columns):
        for i in range(len(sol)):
            sol[i] = sol[i].ljust(len(puzzle.columns), '-')
    return sol


def prime_columns(seq, col, offset_direction, all_realign_value, all_gap_value, all_guide_value):
    """
    Initialize the column values for realignment, gap, and guide matrices.

    Parameters:
    seq : str
        Sequence ID for which the columns are being primed.
    col : int
        The column index being primed.
    offset_direction : int
        The direction and offset being considered.
    all_realign_value : dict
        Dictionary to store realignment values.
    all_gap_value : dict
        Dictionary to store gap values.
    all_guide_value : dict
        Dictionary to store guide values.

    Returns:
    None
    """
    all_realign_value[offset_direction][seq][col] = [0, 0]
    all_guide_value[offset_direction][seq][col] = [0, 0]
    all_gap_value[offset_direction][seq][col] = [0, 0]


def process_solutions(filter_params, puzzle_params, annotate_output_paths, puzzles, consensus, align):
    """
    Process puzzle solutions and compile various metrics.

    Parameters:
    filter_params : DataFrame
        Parameters to filter puzzle solutions.
    puzzle_params : dict
        Parameters for puzzle processing.
    annotate_output_paths : list
        List of file paths containing annotated puzzle solutions.
    puzzles : dict
        Dictionary of puzzles to be processed.
    consensus : list
        Consensus sequence for columns in the alignment.
    align : dict
        Dictionary of aligned sequences.

    Returns:
    tuple
        A tuple of dictionaries containing various metrics extracted from processed solutions.
    """
    all_realign_value, all_gap_value, all_guide_value, solution_mapping, position_mapping = {}, {}, {}, {}, {}
    for annotate_file in annotate_output_paths:
        file_time = time.time()
        solutions = pickle.load(open(annotate_file, "rb"))
        for puz_id, puz_solutions in solutions.items():
            puzzle = puzzles[puz_id]

            if puzzle.level not in puzzle_params["difficulties"]:
                continue

            valid_puzzle, offset = evaluate_puzzle(puzzle, align, consensus)
            if valid_puzzle:
                direction = 0 if puzzle.columns[0] < puzzle.columns[-1] else 100
                offset_direction = direction + offset
                if offset_direction not in all_realign_value.keys():
                    fill_matrices_key([all_realign_value, all_gap_value, all_guide_value, solution_mapping,
                                       position_mapping], align, offset_direction)
                for solution in puz_solutions:
                    valid_solution = validate_solution(solution, puzzle, filter_params)
                    if valid_solution:
                        cols_inspected = get_cols_inspected(puzzle, puzzle_params, offset, direction)
                        identical_seqs = find_puzzle_seq_dups(puzzle, cols_inspected, align, puzzle_params)
                        add_solutions(cols_inspected, solution, puzzle, offset, offset_direction, identical_seqs,
                                      all_realign_value, all_gap_value, all_guide_value, position_mapping)
                        add_to_solution_map(cols_inspected, solution, puzzle, offset_direction, identical_seqs,
                                            solution_mapping)
        print("FINISHED FILE", annotate_file, "IN", round(time.time() - file_time, 1))
    return all_realign_value, all_gap_value, all_guide_value, solution_mapping, position_mapping


def add_solutions(cols_inspected, solution, puzzle, offset, offset_direction, identical_seqs, all_realign_value,
                  all_gap_value, all_guide_value, position_mapping):
    """
    Add puzzle solutions to various matrices targeting realignment, gap information, and guide information.

    Parameters:
    cols_inspected : list
        List of column indices inspected in the puzzle.
    solution : dict
        Solution object containing the solution sequences.
    puzzle : object
        Puzzle object related to the solution.
    offset : int
        Offset value considered for the puzzle.
    offset_direction : int
        Direction and offset being considered.
    identical_seqs : dict
        Dictionary of identical sequences found in the puzzle.
    all_realign_value : dict
        Dictionary to store counts for realignment.
    all_gap_value : dict
        Dictionary to store gap information.
    all_guide_value : dict
        Dictionary to store guide information.
    position_mapping : dict
        Dictionary to map sequence positions.

    Returns:
    None
    """
    cols_of_interest = list(range(5, 180))
    sol = get_sol(puzzle, solution)
    sol_map = map_orig_sol(puzzle, sol)
    unique_rows = {puzzle.IDs[i]: sol[i] for i in range(len(puzzle.IDs))}
    sol_rows = [''.join(list(filter(None, i))) for i in itertools.zip_longest(*list(unique_rows.values()))]

    for col in cols_inspected:
        if col in sol_map.keys() and col in cols_of_interest:
            seqs = sol_map[col]
            for seq_id, new_pos in seqs.items():
                seq_seq_diff, seq_seq_count = calc_seq_seq(seqs, new_pos, puzzle)
                offset = -offset if offset_direction >= 100 else offset
                guide_diff = new_pos - col - offset
                gap_count = sol_rows[puzzle.columns.index(new_pos)].count('-')

                for seq in identical_seqs[seq_id]:
                    if not isinstance(all_realign_value[offset_direction][seq][col], list):
                        prime_columns(seq, col, offset_direction, all_realign_value, all_gap_value, all_guide_value)
                        position_mapping[offset_direction][seq][col] = {}

                    num_sols = len(solution['playerSolutions']) * puzzle.IDs.count(seq)
                    all_realign_value[offset_direction][seq][col][0] += (seq_seq_diff * num_sols)
                    all_realign_value[offset_direction][seq][col][1] += (seq_seq_count * num_sols)
                    all_guide_value[offset_direction][seq][col][0] += (guide_diff * num_sols)
                    all_guide_value[offset_direction][seq][col][1] += num_sols
                    all_gap_value[offset_direction][seq][col][0] += (gap_count * num_sols)
                    all_gap_value[offset_direction][seq][col][1] += ((len(sol_rows[puzzle.columns.index(new_pos)]) - 1)
                                                                     * num_sols)
                    if guide_diff not in position_mapping[offset_direction][seq][col].keys():
                        position_mapping[offset_direction][seq][col][guide_diff] = 0
                    position_mapping[offset_direction][seq][col][guide_diff] += num_sols
    return


def add_to_solution_map(cols_inspected, solution, puzzle, offset_direction, identical_seqs, solution_mapping):
    """
    Add puzzle solution sequences to a mapping based on their positions.

    Parameters:
    cols_inspected : list
        List of column indices inspected in the puzzle.
    solution : dict
        Solution object containing the solution sequences.
    puzzle : object
        Puzzle object related to the solution.
    offset_direction : int
        Direction and offset being considered.
    identical_seqs : dict
        Dictionary of identical sequences found in the puzzle.
    solution_mapping : dict
        Dictionary to store the mapping of solution sequences.

    Returns:
    None
    """
    cols_of_interest = list(range(5, 180))
    cols_inspected = [x for x in cols_inspected if x in cols_of_interest]
    sol = get_sol(puzzle, solution)

    if cols_inspected:
        first_col = puzzle.columns.index(cols_inspected[0])
        last_col = puzzle.columns.index(cols_inspected[-1])
        for pos, row in enumerate(sol):
            sol[pos] = list(row[first_col:last_col + 1])

        for pos, row in enumerate(sol):
            seq_id = puzzle.IDs[pos]
            for seq in identical_seqs[seq_id]:
                for column in cols_inspected:
                    nuc = row[cols_inspected.index(column)]
                    if not isinstance(solution_mapping[offset_direction][seq][column], dict):
                        solution_mapping[offset_direction][seq][column] = {'A': 0, 'C': 0, 'G': 0, 'T': 0, '-': 0}
                    solution_mapping[offset_direction][seq][column][nuc] += len(solution['playerSolutions'])
    return


def fill_matrices_key(matrix_dicts, align, key):
    for matrix_dict in matrix_dicts:
        fill_matrix_key(matrix_dict, align, key)


def fill_matrix_key(dict, align, key):
    dict[key] = {}
    for id, seq in align.items():
        dict[key][id] = {}
        for pos in range(len(seq.seq)):
            if pos not in dict[key][id].keys():
                dict[key][id][pos] = math.nan


def get_cols_inspected(puzzle, puzzle_params, offset, direction):
    """
    Determine the columns to be inspected in a puzzle based on parameters.

    Parameters:
    puzzle : object
        Puzzle object to be inspected.
    puzzle_params : dict
        Parameters defining what to inspect.
    offset : int
        Offset value considered for the puzzle.
    direction : int
        Direction considered for the puzzle.

    Returns:
    list
        List of column indices to be inspected.
    """
    columns = np.array(puzzle.columns)
    if not puzzle_params["inspect_offset_diff"] and not puzzle_params["inspect_direction_diff"]:
        cols_inspected = list(columns[puzzle_params["rows_to_inspect"]["0"][str(puzzle.level)]])
    elif puzzle_params["inspect_offset_diff"] and not puzzle_params["inspect_direction_diff"]:
        cols_inspected = list(columns[puzzle_params["rows_to_inspect"][str(offset)][str(puzzle.level)]])
    elif not puzzle_params["inspect_offset_diff"] and puzzle_params["inspect_direction_diff"]:
        cols_inspected = list(columns[puzzle_params["rows_to_inspect"][str(direction)][str(puzzle.level)]])
    else:
        cols_inspected = list(columns[puzzle_params["rows_to_inspect"][str(direction + offset)][str(puzzle.level)]])
    if columns[-1] in cols_inspected:
        cols_inspected.remove(columns[-1])
    if columns[-2] in cols_inspected:
        cols_inspected.remove(columns[-2])
    return cols_inspected


def analyze_results(filter_params, puzzle_params, annotate_output_paths, puzzles, consensus, align):
    """
    Analyze puzzle results based on provided filter and puzzle parameters.

    Parameters:
    filter_params : DataFrame
        Parameters to filter puzzle solutions.
    puzzle_params : dict
        Parameters for puzzle analysis.
    annotate_output_paths : list
        List of file paths containing annotated puzzle solutions.
    puzzles : dict
        Dictionary of puzzles to be analyzed.
    consensus : list
        Consensus sequence for columns in the alignment.
    align : dict
        Dictionary of aligned sequences.

    Returns:
    dict
        Dictionary containing various dataframes with analysis results.
    """
    all_realign_value, all_gap_value, all_guide_value, solution_mapping, position_mapping = process_solutions(
        filter_params, puzzle_params, annotate_output_paths, puzzles, consensus, align)
    all_realign_df, all_gap_df, all_guide_df, solution_mapping_df, position_mapping_df = {}, {}, {}, {}, {}
    for key in all_realign_value.keys():
        all_realign_df[key] = pd.DataFrame.from_dict(all_realign_value[key], orient='index')
        all_gap_df[key] = pd.DataFrame.from_dict(all_gap_value[key], orient='index')
        all_guide_df[key] = pd.DataFrame.from_dict(all_guide_value[key], orient='index')
        solution_mapping_df[key] = pd.DataFrame.from_dict(solution_mapping[key], orient='index')
        position_mapping_df[key] = pd.DataFrame.from_dict(position_mapping[key], orient='index')

    return {'merged_realign_df': all_realign_df, 'merged_gap_df': all_gap_df,
            'merged_guide_df': all_guide_df, 'mapped_guide_df': solution_mapping_df,
            'mapped_position_df': position_mapping_df}


def output_log_file(args, filter_params, puzzle_params):
    log_file = open(args.output_dir + "/analyze_results_dfs_" + str(date.today()) + ".log", "w")
    log_file.write("CURRENT GIT HASH\n")
    stream = os.popen('git rev-parse HEAD')
    output = stream.read()
    log_file.write(output)

    log_file.write("\nINPUT ARGUMENTS\n")
    args = vars(args)
    for k, v in args.items():
        line = str(k) + ":\t" + str(v) + "\n"
        log_file.write(line)
    log_file.write("\nFILTER PARAMETERS\n")
    for index, row in filter_params.iterrows():
        log_file.write(str(row.values) + '\n')

    log_file.write("\nPUZZLE PARAMETERS\n")
    for k, v in puzzle_params.items():
        line = str(k) + ":\t" + str(v) + "\n"
        log_file.write(line)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('output_dir', type=str)
    parser.add_argument('puzzles_path', type=str)
    parser.add_argument('align_file', type=str)
    parser.add_argument('annotate_output_paths', nargs='*', type=str)

    parser.add_argument('-filter_puzzles_path', type=str, default="")
    parser.add_argument('-filter_params_path', type=str, default="")

    args = parser.parse_args()
    output_dir = args.output_dir
    puzzles_path = args.puzzles_path
    align_file = args.align_file
    annotate_output_paths = args.annotate_output_paths

    filter_puzzles_path = args.filter_puzzles_path
    filter_params_path = args.filter_params_path

    start_time = time.time()

    aln_file = '../alignments/' + align_file
    try:
        for annotate_file in annotate_output_paths:
            test_path = os.path.isfile(annotate_file)
            if not test_path:
                raise FileNotFoundError
        puzzles = pickle.load(open(puzzles_path, "rb"))
        align_list = list(SeqIO.parse(aln_file, "fasta"))
        align_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))

        if filter_puzzles_path != "":
            with open(filter_puzzles_path) as filter_puzzles_file:
                puzzle_params = json.load(filter_puzzles_file)
        else:
            puzzle_params = get_default_puzzle_params()
        if filter_params_path != "":
            filter_params = pd.read_csv(filter_params_path)
        else:
            filter_params = get_default_filter_params()

        print("Using following filters: \n" + filter_params.to_string())
    except FileNotFoundError:
        raise

    load_time = time.time()
    print("LOAD TIME", str(round(load_time - start_time, 1)))
    consensus = assess_column_conservation(align_list)
    results = analyze_results(filter_params, puzzle_params, annotate_output_paths, puzzles, consensus,
                              align_dict)
    pickle.dump(results, open(output_dir + "/analyze_results_dfs_" + str(date.today()) + ".pickle", "wb"))
    output_log_file(args, filter_params, puzzle_params)
    print("RUN TIME", str(round(time.time() - load_time, 1)))
