import os, sys
import pickle
from datetime import date
from random import sample
import argparse
from Bio import SeqIO
from processing_scripts.analyze_results_forRealign import assess_column_conservation, find_offset, validate_puzzle, \
    validate_flank

sys.path.append('..')


def process_solutions(annotate_output_paths, puzzles, consensus, align, sample_size):
    solutions = {}
    sample_solutions = {}
    sample_puzzles = {}
    for annotate_file in annotate_output_paths:
        file_solutions = pickle.load(open(annotate_file, "rb"))
        solutions.update(file_solutions)

    sample_ids = sample(list(solutions.keys()), min(len(solutions), sample_size))

    for puz_id in sample_ids:
        puz_solutions = solutions[puz_id]
        puzzle = puzzles[puz_id]

        valid_puzzle = validate_puzzle(puzzle, align)
        valid_guide, offset = find_offset(puzzle, consensus)
        valid_flank = validate_flank(puzzle)

        if not valid_puzzle:
            for i in range(len(puzzle.uncollapsed_puzzle)):
                puzzle.uncollapsed_puzzle[i] = puzzle.uncollapsed_puzzle[i][2:] + ['-', '-']
            valid_puzzle = validate_puzzle(puzzle, align)

        if valid_puzzle and valid_guide and valid_flank:
            sample_solutions[puz_id] = puz_solutions
            sample_puzzles[puz_id] = puzzle

    pickle.dump(sample_solutions, open(output_dir + "/" + str(sample_size) + "_sample_solutions_" + str(date.today())
                                       + ".pickle", "wb"))
    pickle.dump(sample_puzzles, open(output_dir + "/" + str(sample_size) + "_sample_puzzles_" + str(date.today())
                                     + ".pickle", "wb"))
    return


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('output_dir', type=str)
    parser.add_argument('puzzles_path', type=str)
    parser.add_argument('annotate_output_paths', nargs='*', type=str)
    parser.add_argument('-sample_size', type=int, default=5000)
    parser.add_argument('-align_file', type=str, default="cleaned_aligned_PASTA_9667.fasta")

    args = parser.parse_args()
    output_dir = args.output_dir
    puzzles_path = args.puzzles_path
    align_file = args.align_file
    sample_size = args.sample_size
    annotate_output_paths = args.annotate_output_paths

    aln_file = '../alignments/' + align_file
    try:
        for annotate_file in annotate_output_paths:
            test_path = os.path.isfile(annotate_file)
            if not test_path:
                raise FileNotFoundError
        puzzles = pickle.load(open(puzzles_path, "rb"))
        align_list = list(SeqIO.parse(aln_file, "fasta"))
        align_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise

    consensus = assess_column_conservation(align_list)
    process_solutions(annotate_output_paths, puzzles, consensus, align_dict, sample_size)
