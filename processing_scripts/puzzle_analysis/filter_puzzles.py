import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import csv
import glob
import json
import pickle
import numpy as np
from Bio import SeqIO
import matplotlib.pyplot as plt
from processing_scripts.puzzle_builder.puzzle.puzzle import Puzzle
from processing_scripts.puzzle_builder.puzzle.greedy_puzzle import GreedyPuzzle
from processing_scripts.puzzle_analysis import find_redundancy as fr


def get_top_puzzles(all_puzzles, batches, parms, percent):
    """
    Get top puzzles based on improvement, filtered by a percentage.

    Parameters:
    all_puzzles: list of puzzles
    batches: list of batch identifiers
    parms: dictionary of parameters
    percent: percentage threshold to filter top puzzles

    Returns:
    Dictionary of top puzzles sorted by improvement.
    """
    puzzle_improvement_by_diff = {}

    for index, batch_puzzles in enumerate(all_puzzles):
        for dp in batch_puzzles:
            puzzle = GreedyPuzzle.from_dnapuzzle(dp, parms)
            if puzzle.level not in puzzle_improvement_by_diff.keys():
                puzzle_improvement_by_diff[puzzle.level] = {}
            if int(str(batches[index])[:4]) < 2021:
                puzzle_improvement_by_diff[puzzle.level][puzzle.puzzle_id] = (dp.greedy_consensus_score -
                                                                              (dp.greedy_n_gaps * 0.6) -
                                                                              puzzle.base_score)
            else:
                # Puzzle type must be defined to calculate prior score
                puzzle.calc_prior_puzzle()
                puzzle_improvement_by_diff[puzzle.level][puzzle.puzzle_id] = (puzzle.sol_score - puzzle.prior_score)
    
    # Sort and filter puzzles by the specified percentage
    for difficulty, puzzles in puzzle_improvement_by_diff.items():
        sorted_scores = {k: v for k, v in sorted(puzzles.items(), key=lambda item: item[1], reverse=True)}
        top_diff_puzzles = list(sorted_scores.items())[:round(len(sorted_scores) * percent/100.0)+1]
        puzzle_improvement_by_diff[difficulty] = {k: v for k, v in top_diff_puzzles}

    return puzzle_improvement_by_diff


def get_diff_puzzle_coverage(top_puzzles, puzzle_dict, parms):
    """
    Get puzzle coverage for different difficulties.

    Parameters:
    top_puzzles: list of top puzzles
    puzzle_dict: dictionary of all puzzles
    parms: dictionary of parameters

    Returns:
    Dictionary of total coverage for each puzzle difficulty.
    """
    total_coverage = {}
    for difficulty, puzzle_ids in top_puzzles.items():
        total_coverage[difficulty] = {}
        for puzzle_id in puzzle_ids:
            dp = puzzle_dict[puzzle_id]
            puzzle = Puzzle.from_dnapuzzle(dp, parms)
            for id in puzzle.ids:
                if id not in total_coverage[difficulty]:
                    total_coverage[difficulty][id] = {}
                for column in puzzle.columns:
                    if column not in total_coverage[difficulty][id]:
                        total_coverage[difficulty][id][column] = 0
                    total_coverage[difficulty][id][column] += 1
    return total_coverage

def get_full_puzzle_coverage(top_puzzles, all_ranges, align, puzzle_dict, parms):
    """
    Get full puzzle coverage.

    Parameters:
    top_puzzles: list of top puzzles
    all_ranges: range of all puzzles
    align: sequence alignments
    puzzle_dict: dictionary of all puzzles
    parms: dictionary of parameters

    Returns:
    Dictionary of total full coverage.
    """
    total_coverage = {}
    for seq in align:
        total_coverage[str(seq.id)] = [0] * len(align[0].seq)
    total_coverage = {k: v for k, v in sorted(total_coverage.items(), key=lambda item: item[0])}

    for difficulty, puzzle_ids in top_puzzles.items():
        for puzzle_id in puzzle_ids:
            dp = puzzle_dict[puzzle_id]
            puzzle = Puzzle.from_dnapuzzle(dp, parms)
            if puzzle.columns[0] > puzzle.columns[-1]:
                puzzle.columns.reverse()
            range_subseq = all_ranges[tuple(puzzle.columns)]
            seqs = [seq for seq in align if str(seq.id) in puzzle.ids]
            ids = set(puzzle.ids)
            for seq in seqs:
                subseq = str(seq.seq[puzzle.columns[0]:puzzle.columns[-1]+1])
                identical_ids = range_subseq[subseq]
                ids.update(identical_ids)
            for id in ids:
                for column in puzzle.columns:
                    total_coverage[id][column] += 1
    return total_coverage

def compare_coverage(all_coverage, top_coverage, parms):
    """
    Compare coverage between all puzzles and top puzzles.

    Parameters:
    all_coverage: coverage data for all puzzles
    top_coverage: coverage data for top puzzles
    parms: dictionary of parameters
    """
    nucleotides_covered = []
    for difficulty in parms['difficulties']:
        diff_nuc_covered = 0
        for sequence, columns in all_coverage[difficulty].items():
            diff_nuc_covered += len(columns)
        nucleotides_covered.append(diff_nuc_covered)
    print('Total coverage:')
    print(nucleotides_covered)

    top_nucleotides_covered = []
    for difficulty in parms['difficulties']:
        diff_top_covered = 0
        for sequence, columns in top_coverage[difficulty].items():
            diff_top_covered += len(columns)
        top_nucleotides_covered.append(diff_top_covered)
    print(top_nucleotides_covered)
    percent_covered = [x / y for x, y in zip(top_nucleotides_covered, nucleotides_covered)]
    print(percent_covered)

def compare_full_coverage(top_coverage, all_nucleotides_covered):
    """
    Compare full coverage between top puzzles and all nucleotides.

    Parameters:
    top_coverage: coverage data for top puzzles
    all_nucleotides_covered: total nucleotides covered by all puzzles

    Returns:
    Percentage of coverage.
    """
    top_nucleotides_covered = 0
    for sequence, columns in top_coverage.items():
        top_nucleotides_covered += sum(i > 0 for i in columns)
    percent_covered = float(top_nucleotides_covered)/float(all_nucleotides_covered) * 100
    print(percent_covered)
    return percent_covered

def output_csv_coverage(sequences):
    """
    Output CSV file with coverage data.

    Parameters:
    sequences: sequence data to be outputted
    """
    column_key = list(range(len(align[0].seq)))
    index = ['Columns'] + column_key
    for difficulty in parms['difficulties']:
        all_file_name = 'all_puzzles/all_puzzles_' + str(difficulty) + '.csv'
        with open(all_file_name, 'w', newline='') as all_file:
            wr = csv.writer(all_file, quoting=csv.QUOTE_ALL)
            wr.writerow(index)
            for sequence, columns in sequences.items():
                if sequence in all_coverage[difficulty].keys():
                    puzzle_columns = all_coverage[difficulty][sequence]
                    for key, value in puzzle_columns.items():
                        columns[key] += value
                sequence_row = [sequence] + columns
                wr.writerow(sequence_row)

if __name__ == '__main__':
    batches = []
    for batch in sys.argv[1:]:
        batches.append(int(batch))

    puzzle_parms = glob.glob('../puzzle_builder/puzzle_parameters/puzzle_parms.json')[0]
    with open(puzzle_parms) as parms_file:
        parms = json.load(parms_file)
    blacklist_parms = glob.glob('../puzzle_builder/puzzle_parameters/blacklist.json')[0]
    with open(blacklist_parms) as blacklist_file:
        blacklist = json.load(blacklist_file)

    aln_file = '../../alignments/' + parms['alignment_file']
    try:
        unfiltered_align = list(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise

    align = []
    for record in unfiltered_align:
        if record.id not in blacklist['sequences']:
            align.append(record)

    all_puzzles = []
    puzzle_dict = {}
    for batch in batches:
        batch_puzzles = []
        procname = "../puzzle_builder/pickles/" + str(batch) + "/" + str(batch) + ".pickle"
        try:
            these_puz = pickle.load(open(procname, "rb"))
            batch_puzzles += these_puz
            all_puzzles.append(batch_puzzles)
            puzzle_dict.update({puzz.windowID: puzz for puzz in batch_puzzles})
        except OSError:
            print('File not found:', procname)
            continue

        all_ranges = fr.find_redundancy(align, parms)
        unfiltered_puzzles = get_top_puzzles(all_puzzles, batches, parms, 100.0)
        all_coverage = get_full_puzzle_coverage(unfiltered_puzzles, all_ranges, align, puzzle_dict, parms)

    all_nucleotides_covered = 0
    for sequence, columns in all_coverage.items():
        all_nucleotides_covered += sum(i > 0 for i in columns)
    percent_coverage = []
    for i in range(10, 100, 10):
        print('Top ' + str(i) + '%')
        top_puzzles = get_top_puzzles(all_puzzles, batches, parms, i)
        top_coverage = get_full_puzzle_coverage(top_puzzles, all_ranges, align, puzzle_dict, parms)
        percent_coverage.append(compare_full_coverage(top_coverage, all_nucleotides_covered))

    xpoints = np.arange(10, 100, 10)
    plt.xlabel("Top percent puzzles")
    plt.ylabel("Percent coverage over all puzzles")
    plt.title("Coverage over all batch puzzles with top scoring puzzles")
    plt.ylim(0, 100)
    plt.plot(xpoints, percent_coverage, 'o-')
    plt.savefig('graphs/coverage_progression.png')
    plt.clf()

    sequence_array = np.asarray(list(all_coverage.values()))
    plt.figure(figsize=(7, 6))
    plt.pcolormesh(sequence_array)
    plt.colorbar()
    plt.savefig('graphs/puzzle_coverage.png')
    plt.clf()

    output_csv_coverage(all_coverage)
