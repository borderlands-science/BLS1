import os,sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)

import glob
import json
import pickle
from Bio import SeqIO

def find_redundancy(align, parms):
    """
    Find redundancies in sequences based on specified parameters.

    Parameters:
    align : list
        A list of alignment sequences.
    parms : dict
        Dictionary of parameters including 'difficulties' and 'grid_size'.

    Returns:
    dict
        A dictionary mapping column ranges to subsequence redundancies.
    """
    all_ranges = {}
    total_columns = len(align[0].seq)
    for difficulty in parms['difficulties']:
        n_cols = parms['grid_size'][str(difficulty)]['n_cols']
        for column in range(total_columns - n_cols):
            column_range = tuple(range(column, column+n_cols))
            if column_range not in all_ranges.keys():
                all_ranges[column_range] = {}
            # Identify redundant subsequences for each sequence
            for seq in align:
                subseq = str(seq.seq[column:column + n_cols])
                if subseq not in all_ranges[column_range].keys():
                    all_ranges[column_range][subseq] = []
                all_ranges[column_range][subseq].append(seq.id)
    return all_ranges


def reformat_redundancies(all_ranges):
    """
    Reformat redundancies for easier analysis.

    Parameters:
    all_ranges : dict
        A dictionary mapping column ranges to subsequence redundancies.

    Returns:
    int
        Returns 0 to indicate successful execution.
    """
    sequence_redundancies = {}
    total_aligns = len(align)
    current = 0
    # Process each sequence to identify redundancies
    for seq in align:
        print(current, total_aligns)
        current += 1
        if seq.id not in sequence_redundancies.keys():
            sequence_redundancies[seq.id] = {}
            for columns, win_redundancy in all_ranges.items():
                subseq = str(seq.seq[columns[0]:columns[-1]+1])
                identical_seqs = win_redundancy[subseq]
                identical_seqs.remove(seq.id)
                for id in identical_seqs:
                    if id not in sequence_redundancies[seq.id].keys():
                        sequence_redundancies[seq.id][id] = []
                    sequence_redundancies[seq.id][id].append(columns)

    with open('redundancy.p', 'wb') as file:
        pickle.dump(sequence_redundancies, file, protocol=pickle.HIGHEST_PROTOCOL)
    with open('redundancy.json', 'w') as json_file:
        json.dump(sequence_redundancies, json_file)
    return 0


if __name__ == '__main__':
    reformat = False
    puzzle_parms = glob.glob('../puzzle_builder/puzzle_parameters/puzzle_parms.json')[0]
    with open(puzzle_parms) as parms_file:
        parms = json.load(parms_file)

    aln_file = '../../alignments/' + parms['alignment_file']
    try:
        align = list(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise

    all_ranges = find_redundancy(align, parms)
    with open('redundancy.pickle', 'wb') as file:
        pickle.dump(all_ranges, file, protocol=pickle.HIGHEST_PROTOCOL)
    json_ranges = json.dumps({str(k): v for k, v in all_ranges.items()})
    with open('redundancy.json', 'w') as json_file:
        json.dump(json_ranges, json_file)
    if reformat:
        reformat_redundancies(all_ranges)
