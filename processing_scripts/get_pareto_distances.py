import sys
import csv
import pickle
from Bio import SeqIO
import processing_scripts.analyze_results_forRealign as ar

if __name__ == '__main__':
    align_file = sys.argv[1]
    puzzles_file = sys.argv[2]
    solution_files = sys.argv[3:]

    aln_file = '../alignments/' + align_file
    try:
        puzzles = pickle.load(open(puzzles_file, "rb"))
        align_list = list(SeqIO.parse(aln_file, "fasta"))
        align_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))
    except FileNotFoundError:
        raise
    consensus = ar.assess_column_conservation(align_list)

    with open('data/pareto_distances.csv', 'w', newline='') as pareto_file:
        csv_writer = csv.writer(pareto_file, delimiter=',')
        for i, file in enumerate(solution_files):
            csv_writer.writerow(["Stage " + str(i)])
            pareto_distances = [[0], [1], [100], [101]]
            pareto_map = {0: 0, 1: 1, 100: 2, 101: 3}

            solution_puzzles = pickle.load(open(file, "rb"))
            for puz_id, solutions in solution_puzzles.items():
                puzzle = puzzles[puz_id]

                valid_puzzle = ar.validate_puzzle(puzzle, align_dict)
                valid_guide, offset = ar.find_offset(puzzle, consensus)
                valid_flank = ar.validate_flank(puzzle)

                if not valid_puzzle:
                    for i in range(len(puzzle.uncollapsed_puzzle)):
                        puzzle.uncollapsed_puzzle[i] = puzzle.uncollapsed_puzzle[i][2:] + ['-', '-']
                    valid_puzzle = ar.validate_puzzle(puzzle, align_dict)

                if valid_puzzle and valid_guide and valid_flank:
                    direction = 0 if puzzle.columns[0] < puzzle.columns[-1] else 100
                    offset_direction = direction + offset
                    for solution in solutions:
                        for player_solution in solution["playerSolutions"]:
                            pareto_distances[pareto_map[offset_direction]].append(solution["paretoDist"])
            csv_writer.writerows(pareto_distances)
