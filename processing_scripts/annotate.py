import os,sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)
import pickle
import seaborn as sns; sns.set()
import numpy as np
import argparse
import time
from scipy.optimize import fmin_cobyla
from toolsForPuzzle import *
from sympy import *
from datetime import date
from multiprocessing import Pool, Manager
from itertools import islice
from sklearn.cluster import AgglomerativeClustering

#this script annotates player solution with scores relating to other player solutions.
#in other words, it's our quality assessment of solutions, and following filtering.


np.seterr(invalid='ignore')

bottomList = ["2","4","6","8","10","12","14"]

def paretoFront(df):
    """
    Calculate the Pareto frontier for a given DataFrame of puzzle solution metrics.

    Parameters:
    df : DataFrame
        A DataFrame containing columns 'nGaps' and 'score' for each solution.

    Returns:
    tuple
        A tuple of symbolic solutions for a, b, c in the quadratic equation ax^2 + bx + c.
    """
    y1=df['nGaps'].min()
    x1=df[df['nGaps']==y1]['score'].max()
    x3=df['score'].max()
    y3=df[df['score']==x3]['nGaps'].min()
    l=list(df[df['nGaps']<=y3]['nGaps'].unique())
    l.sort()
    y2=l[(len(l)-1)//2]
    x2=df[df['nGaps']==y2]['score'].max()

    unique_points = get_unique_points(y1, x1, x3, y3, y2, x2)

    if (len(unique_points) == 3): #Best case scenario, can use original algorithm
        pass #proceed to the original algorithm below

    elif (len(unique_points) == 2): #Take the midpoint between these two points
        x1, y1 = unique_points[0]
        x2, y2 = unique_points[1]
        x3 = (x2 - x1)/2+x1 #Take the x-coordinate of the point in between x1 and x2
        y3 = (y2 - y1)/2+y1 #Take the y-coordinate of the point in between y1 and y2

    elif (len(unique_points) == 1): #Take the line from (0, 0) to this point
        x1 = 0
        y1 = 0
        x2, y2 = unique_points[0]
        x3 = (x2 - x1)/2+x1 #Take the x-coordinate of the point in between x1 and x2
        y3 = (y2 - y1)/2+y1 #Take the y-coordinate of the point in between y1 and y2

    else: #if some weird shit happens, continue to next puzzle
        return [-1] #In the main method, this will make this puzzle be skipped over

    a,b,c = symbols('a b c')

    #important, flipped x and y so that x is estimating score
    return solve([Eq(a*y1**2+b*y1+c, x1), Eq(a*y2**2+b*y2+c, x2),Eq(a*y3**2+b*y3+c, x3)], [a,b,c])

def get_unique_points(y1, x1, x3, y3, y2, x2):
    """
    Determine unique points from given coordinates to aid in calculating the Pareto frontier.

    Parameters:
    y1, x1, x3, y3, y2, x2 : float
        Coordinates used to define points on the Pareto frontier.

    Returns:
    list
        A list of unique points determined from the given coordinates.
    """
    # to make sure pareto frontiers can be found for all puzzles:
    if (x1 == x2): #These few if statements are to make sure that there aren't points with same x and different y (parabola can't be drawn this way)
        if (y1 < y2):
            y1 = y2
        else:
            y2 = y1

    if (x2 == x3):
        if (y2 < y3):
            y2 = y3
        else:
            y3 = y2

    if (x1 == x3):
        if (y1 < y3):
            y1 = y3
        else:
            y3 = y1

    p1 = (x1, y1)
    p2 = (x2, y2)
    p3 = (x3, y3)

    unique_points = list(set([p1, p2, p3]))
    return unique_points


def process_puzzle_solutions_pareto_dist(puzzle, sol, greedy_outputs):
    """
    Process puzzle solutions to calculate the distance of each solution from the Pareto frontier.

    Parameters:
    puzzle : Puzzle object
        The puzzle for which solutions are being processed.
    sol : list
        A list of solution dictionaries.
    greedy_outputs : dict
        A dictionary to store outputs related to the greedy algorithm for comparison.

    Returns:
    bool
        A boolean indicating if the Pareto frontier was successfully estimated.
    """
    result_df = pd.DataFrame(sol, columns=["score", "nGaps"])
    minGaps = result_df['nGaps'].max()
    maxGaps = result_df['nGaps'].min()

    min_score = result_df['score'].min()

    #estimate the pareto frontier
    ans=paretoFront(result_df) #ans holds the values of a, b, c that defines the pareto frontier,
    if len(ans)!=3:
        return False

    #calculate shortest distance to pareto frontier for every solution
    for i, p in enumerate(sol):

        p['paretoDist'] = get_shortest_distance_to_pareto(maxGaps, minGaps, p['score'], p['nGaps'], ans)
        p['horizontalDist'] = get_score_distance_to_pareto(p['score'], p['nGaps'], ans)
        p['horizontalProportion'] = get_horizontal_proportion(p['score'], p['nGaps'], ans, min_score)

    greedy_outputs['paretoDist'] = get_shortest_distance_to_pareto(maxGaps, minGaps, puzzle.greedy_consensus_score, puzzle.greedy_n_gaps, ans)
    greedy_outputs['horizontalDist'] = get_score_distance_to_pareto(puzzle.greedy_consensus_score, puzzle.greedy_n_gaps, ans)
    greedy_outputs['horizontalProportion'] = get_horizontal_proportion(puzzle.greedy_consensus_score, puzzle.greedy_n_gaps, ans, min_score)
    return True


def get_horizontal_proportion(score, nGaps, ans, min_score):
    """
    Calculate the horizontal proportion of a score relative to the Pareto frontier.

    Parameters:
    score : float
        The score of a solution.
    nGaps : int
        The number of gaps in the solution.
    ans : tuple
        The symbolic solutions for a, b, c in the quadratic equation defining the Pareto frontier.
    min_score : float
        The minimum score observed among all solutions.

    Returns:
    float
        The calculated horizontal proportion.
    """
    a, b, c = symbols('a b c')
    def f(x):
        return ans[a]*x**2+ans[b]*x+ans[c]
    
    paretoFrontScore = abs(f(nGaps))

    if paretoFrontScore == min_score:
        return 1
    return (score - min_score) / (paretoFrontScore - min_score)

def classifyByFamilyID(data, puzzles_pickle):
    """
    Group solutions by family ID of their source puzzles.

    Parameters:
    data : dict
        A dictionary containing solutions.
    puzzles_pickle : dict
        A dictionary of puzzles with their metadata.

    Returns:
    dict
        A dictionary of solutions classified by family ID.
    """
    puzzles={}

    total_solutions = 0
    for p in data:
        for i in data[p]:
            originalCode = i['originalCode']
            total_solutions += len(i['playerSolutions'])


            siblings = puzzles_pickle[originalCode].original_code
            familyID = ''.join(sorted(siblings))

            i['familyID'] = familyID
            i['windowID'] = puzzles_pickle[originalCode].windowID
            puzzles.setdefault(familyID,[]).append(i)

    print("Total Solutions: ")
    print(total_solutions)

    print("Total FamilyID: ")
    print(len(puzzles))

    return puzzles


def classifyBySourcePuzzles(data, puzzles_pickle):
    """
    Group solutions by their source puzzle IDs.

    Parameters:
    data : dict
        A dictionary containing solutions.
    puzzles_pickle : dict
        A dictionary of puzzles with their metadata.

    Returns:
    dict
        A dictionary of solutions classified by source puzzle ID.
    """
    puzzles={}

    print("Data amount: ")
    print(len(data))

    total_solutions = 0
    for p in data:
        for i in data[p]:
            originalCode = i['originalCode']
            total_solutions += len(i['playerSolutions'])
            sourceID = puzzles_pickle[originalCode].windowID
            puzzles.setdefault(sourceID,[]).append(i)

    print("Total Solutions: ")
    print(total_solutions)
    return puzzles


def get_numpy_roots_pareto_dist(score, nGaps, ans):
    """
    Calculate the roots of the equation defining the Pareto frontier for a given score and gap count.

    Parameters:
    score : float
        The score of a solution.
    nGaps : int
        The number of gaps in the solution.
    ans : tuple
        The symbolic solutions for a, b, c in the quadratic equation defining the Pareto frontier.

    Returns:
    float
        The shortest distance from the given score and gap count to the Pareto frontier.
    """
    a, b, c = symbols('a b c')

    p = []
    p[0] = 2*(ans[a]**2)
    p[1] = 3* ans[a] * ans[b]
    p[2] = ans[b]**2 + 2* ans[a]* ans[c] - 2 * ans[a] * nGaps + 1
    p[3] = ans[b]*ans[c] - ans[b]*nGaps - score
    roots = np.roots(p)
    dist = math.inf
    for x in roots:
        new_dist = np.sqrt((x-score)**2 + (ans[a] * x**2 + ans[b] * x + ans[c] - nGaps)**2)
        if new_dist < dist:
            dist = new_dist
    return new_dist

def get_shortest_distance_to_pareto(maxGaps, minGaps, score, nGaps, ans):
    """
    Compute the shortest distance from a given solution to the Pareto frontier.

    Parameters:
    maxGaps : int
        Maximum number of gaps among the solutions.
    minGaps : int
        Minimum number of gaps among the solutions.
    score : float
        The score of the solution.
    nGaps : int
        The number of gaps in the solution.
    ans : tuple
        The symbolic solutions for a, b, c in the quadratic equation defining the Pareto frontier.

    Returns:
    float
        The shortest distance to the Pareto frontier.
    """
    a, b, c = symbols('a b c')
    def f(x):
        return ans[a]*x**2+ans[b]*x+ans[c]

    def distance(X):
        x,y = X
        return np.sqrt((x - nGaps)**2 + (y - score)**2)

    def c1(X):
        x,y = X
        return f(x)-y
    def c2(X):
        x,y = X
        return y-f(x)

    X = fmin_cobyla(distance, x0=[minGaps,maxGaps], cons=[c2,c1],rhobeg=10,rhoend=0.5, maxfun=20)
    return distance(X)

def get_score_distance_to_pareto(score, nGaps, ans):
    """
    Calculate the distance from a solution's score to the Pareto frontier.

    Parameters:
    score : float
        The score of the solution.
    nGaps : int
        The number of gaps in the solution.
    ans : tuple
        The symbolic solutions for a, b, c in the quadratic equation defining the Pareto frontier.

    Returns:
    float
        The score distance to the Pareto frontier.
    """
    a, b, c = symbols('a b c')
    def f(x):
        return ans[a]*x**2+ans[b]*x+ans[c]

    return abs(f(nGaps) - score)


def process_puzzle_solutions_dist_score(puzzle, sol, greedy_outputs):
    """
    Process puzzle solutions to calculate distribution scores.

    Parameters:
    puzzle : Puzzle object
        The puzzle for which solutions are being processed.
    sol : list
        A list of solution dictionaries.
    greedy_outputs : dict
        A dictionary to store outputs related to the greedy algorithm for comparison.

    Returns:
    None
    """
    shape = get_solution_shape(sol)
    dist = np.zeros(shape)
    hw_dist = np.zeros(shape)
    w_dist = np.zeros(shape)

    dist_wBottom_list = {}
    hw_dist_wBottom_list = {}
    w_dist_wBottom_list = {}

    for amount in bottomList:
        dist_wBottom_list[amount] = np.zeros(shape)
        hw_dist_wBottom_list[amount] = np.zeros(shape)
        w_dist_wBottom_list[amount] = np.zeros(shape)

    for p in sol:
        player_solution = p["playerSolutions"][0]
        matrix = get_solution_matrix(player_solution, shape) * len(p["playerSolutions"])

        p['bottomRowGapsOnly'] = (np.nonzero(matrix)[1] > 0).sum() == 0

        dist = dist + matrix
        hw_dist = hw_dist + (matrix + (matrix * float(p["horizontalProportion"])))*0.5
        w_dist = w_dist + (matrix * float(p["horizontalProportion"]))

        for amount in bottomList:
            if not p["bottom" + amount + "HorizontalProportion"]:
                dist_wBottom_list[amount] = dist_wBottom_list[amount] + matrix
                hw_dist_wBottom_list[amount] = hw_dist_wBottom_list[amount] + (matrix + (matrix * float(p["horizontalProportion"])))*0.5
                w_dist_wBottom_list[amount] = w_dist_wBottom_list[amount] + (matrix * float(p["horizontalProportion"]))

    # normalize distribution matrices
    dist = dist/np.linalg.norm(dist)
    hw_dist = hw_dist/np.linalg.norm(hw_dist)
    w_dist = w_dist/np.linalg.norm(w_dist)

    for amount in bottomList:
        dist_wBottom_list[amount] = dist_wBottom_list[amount]/np.linalg.norm(dist_wBottom_list[amount])
        hw_dist_wBottom_list[amount] = hw_dist_wBottom_list[amount]/np.linalg.norm(hw_dist_wBottom_list[amount])
        w_dist_wBottom_list[amount] = w_dist_wBottom_list[amount]/np.linalg.norm(w_dist_wBottom_list[amount])

    # get the similarity between the player solutions and the distributions
    for i, p in enumerate(sol):
        player_solution = p["playerSolutions"][0]

        p['distScore'] = get_cosine_similarity(dist, get_solution_matrix(player_solution, shape))
        p['halfWeightedDistScore'] = get_cosine_similarity(hw_dist, get_solution_matrix(player_solution, shape))
        p['weightedDistScore'] = get_cosine_similarity(w_dist, get_solution_matrix(player_solution, shape))

        for amount in bottomList:
            p["distScore_wBottom" + amount] = get_cosine_similarity(dist_wBottom_list[amount] , get_solution_matrix(player_solution, shape))
            p["halfWeightedDistScore_wBottom" + amount] = get_cosine_similarity(hw_dist_wBottom_list[amount] , get_solution_matrix(player_solution, shape))
            p["weightedDistScore_wBottom" + amount] = get_cosine_similarity(w_dist_wBottom_list[amount] , get_solution_matrix(player_solution, shape))

    greedy_outputs['distScore'] = get_cosine_similarity(dist, get_solution_matrix(puzzle.greedy_sol, shape))
    greedy_outputs['weightedDistScore'] = get_cosine_similarity(w_dist, get_solution_matrix(puzzle.greedy_sol, shape))
    for amount in bottomList:
        greedy_outputs["distScore_wBottom" + amount] = get_cosine_similarity(dist_wBottom_list[amount] , get_solution_matrix(puzzle.greedy_sol, shape))
        greedy_outputs["halfWeightedDistScore_wBottom" + amount] = get_cosine_similarity(hw_dist_wBottom_list[amount] , get_solution_matrix(puzzle.greedy_sol, shape))
        greedy_outputs["weightedDistScore_wBottom" + amount] = get_cosine_similarity(w_dist_wBottom_list[amount] , get_solution_matrix(puzzle.greedy_sol, shape))


def get_solution_shape(sol):
    sample_solution = sol[0]["playerSolutions"][0]
    x = len(sample_solution)
    codes = [string.replace('-','') for string in sample_solution]
    y = len(max(codes, key=len))

    return (x,y)

def get_solution_matrix(player_solution, shape):
    matrix = np.zeros(shape)
    for j, code in enumerate(player_solution):
        if j < shape[0]:
            count = 0
            for k, char in enumerate(code):
                if char == '-':
                    if k-count < shape[1]:
                        matrix[j,k-count] += 1
                        count += 1
    return matrix

def get_cosine_similarity(distribution, otherDistribution):
    a = np.matrix.flatten(distribution)
    b = np.matrix.flatten(otherDistribution)
    dem = (np.linalg.norm(a) * np.linalg.norm(b))
    if dem == 0:
        return 0
    return np.inner(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('output_name', type=str)
    parser.add_argument('input_extract_path', type=str)
    parser.add_argument('output_dir', type=str)

    parser.add_argument('puzzles_path', type=str)
    
    parser.add_argument('-use_paretoDist', type=bool, default=True)
    parser.add_argument('-use_dist_score', type=bool, default=False)

    args = parser.parse_args()

    output_name = args.output_name
    input_extract_path = args.input_extract_path
    output_dir = args.output_dir

    puzzles_path = args.puzzles_path

    use_paretoDist = args.use_paretoDist
    use_dist_score = args.use_dist_score

    start_time = time.time()

    infile = open(input_extract_path,'rb')
    data = pickle.load(infile)

    print("data " + str(len(data)))

    puzzles = pickle.load(open(puzzles_path, "rb"))
    infile.close()

    load_time = time.time()
    print("LOAD TIME", str(round(load_time - start_time, 1)))

    sol_by_puzzles = classifyByFamilyID(data, puzzles) #IMPORTANT, this groups the puzzles by their familyID

    print("data " + str(len(sol_by_puzzles)))

    sol_by_puzzles

    today = str(date.today())

    print("Total number of keys:", len(sol_by_puzzles.keys()))

    def chunks(data, SIZE=10000):
        it = iter(data)
        for i in range(0, len(data), SIZE):
            yield {k:data[k] for k in islice(it, SIZE)}

    def process_puzzles_solutions(sol_by_puzzles):

        greedy_outputs = {}

        for (key, sol) in sol_by_puzzles.items():
            puzzle = puzzles[sol[0]['originalCode']]

            greedy_outputs[key] = {}

            total_solution_count = 0

            for i, p in enumerate(sol):
                total_solution_count += len(p['playerSolutions'])

            for i, p in enumerate(sol):
                p['totalFamilyIDSolutionCount'] = total_solution_count

            if (use_paretoDist):
                is_successful = process_puzzle_solutions_pareto_dist(puzzle, sol, greedy_outputs[key])

                if is_successful:
                    if use_dist_score:
                        process_puzzle_solutions_dist_score(puzzle, sol, greedy_outputs[key])

        return sol_by_puzzles, greedy_outputs

    pool = Pool(4)   

    greedy_outputs = {}

    for i, (result_puzzles, greedy_output) in enumerate(pool.imap_unordered(process_puzzles_solutions, chunks(sol_by_puzzles, 5000))):
        sol_by_puzzles.update(result_puzzles)
        greedy_outputs.update(greedy_output)
        print("Done with", i*5000, "out of", len(sol_by_puzzles.items()))

    print("data " + str(len(sol_by_puzzles)))

    pickle.dump(sol_by_puzzles,open(output_dir+"/"+"annotate_output_"+output_name+"_"+today+".pickle","wb"))
    pickle.dump(greedy_outputs,open(output_dir+"/"+"annotate_greedy_output_"+output_name+"_"+today+".pickle","wb"))
    print("RUN TIME", str(round(time.time() - load_time, 1)))
