__author__ = "Roman"
"""
This script extracts specific subsets of puzzle solutions to display pareto-optimality in a 2D plot.
Generate relevant plots with plot_pareto.py.
"""
import pickle
import ast

res = pickle.load(open("/data/TEST_SOLUTIONS_MORECLASSES_ANNOTATED4.pickle", "rb"))

clusters = []
prevPuz = None
curCluster = []
for i in sorted(res.keys())[0:]:
    print(i)
    print(len(res[i]), "SOLUTIONS AT THOSE COLUMNS")
    pos = ast.literal_eval(i)
    for sol in res[i]:
        oCode = sol["originalCode"]
        pik = sol["pikdik"]
        nGaps = sol["nGaps"]
        score = sol["score"]
        isPareto = sol["pareto"]
        solutions = sol["playerSolutions"]
        if len(curCluster) == 0:
            curCluster.append((score, nGaps, isPareto))
        else:
            if prevPuz is None:
                curCluster.append((score, nGaps, isPareto))
            else:
                if sorted(prevPuz.IDs) == sorted(pik.IDs):
                    curCluster.append((score, nGaps, isPareto))
                else:
                    clusters.append(curCluster)
                    curCluster = []
        prevPuz = pik
    break
print(len(clusters))
pickle.dump(clusters, open("JULY4_pareto_cluster.pickle", "wb"))
