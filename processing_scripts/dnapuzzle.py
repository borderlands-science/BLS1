__author__ = "Roman"
"""
This is the core puzzle class. it is what is stored in the pickles.
all other scripts import Puzzle() object.
"""

from Bio.Alphabet import IUPAC, Gapped
from Bio.Align import MultipleSeqAlignment
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import copy
import random


class Nucleotide:
    def __init__(self, value, sequence_ID, column):
        self.seqID = sequence_ID
        self.colID = column
        self.nuc = value


class Puzzle:
    def __init__(self, puzzle=[], par_puzzle=[], IDs=[], consensus="", cons_scores=[], columns=[-1], flanks=[-1],
                 n=10000, bonus=1.15):
        self.gap_proportion = None
        self.uncollapsed_puzzle = None
        self.nuc_order = None
        self.par_order = None
        self.reprint = None
        self.n_moves = None
        self.greedy_n_gaps = None
        self.greedy_consensus_score = None
        self.greedy_sol = None
        self.min_max = None
        self.score = None
        self.gap_score = None
        self.consensus_score = None
        self.playerResults = None
        self.n_extra_tokens = None
        self.flankedTarget = None
        self.original_code = None
        self.windowID = None
        self.puzzle = puzzle
        self.par_puzzle = par_puzzle
        self.IDs = IDs
        self.columns = columns
        self.flanks = flanks
        self.consensus = consensus
        self.cons_scores = cons_scores
        self.n = n
        self.bonus = bonus
        self.level = -1
        self.extra_guides = 0

    @classmethod
    def from_lst(cls, aln, ids, consensus, cons_scores, columns, flanks, n, bonus):
        par_puzzle = aln
        puzzle = aln
        IDs = ids
        return cls(puzzle, par_puzzle, IDs, consensus, cons_scores, columns, flanks, n, bonus)

    @classmethod
    def from_puzzle(cls, p):
        par_puzzle = p.par_puzzle
        puzzle = p.copy()
        IDs = p.IDs
        consensus = p.consensus
        cons_scores = p.cons_scores
        columns = p.columns
        flanks = p.flanks
        n = p.n
        bonus = p.bonus
        return cls(puzzle, par_puzzle, IDs, consensus, cons_scores, columns, flanks, n, bonus)

    def set_difficulty(self, dif):
        self.level = dif

    def set_uniqueID(self, uID):
        self.windowID = uID

    def set_originalCode(self, oCode):
        try:
            self.original_code.append(oCode)
        except:
            self.original_code = [oCode]

    def set_targetToFlank(self, target):
        self.flankedTarget = target

    def get_n_extra_tokens(self):
        try:
            return self.n_extra_tokens
        except:
            self.n_extra_tokens = [0, 2, 5]
            return self.n_extra_tokens

    def set_extra_tokens(self, extra):
        self.n_extra_tokens = extra

    def add_maxTokens(self, t):
        try:
            self.n_extra_tokens.append(t)
        except:
            self.n_extra_tokens = [t]

    def update_player_results(self, puzzleID, nTokens, parScore, parMoves, parScoreGlobal, parMovesGlobal, solution,
                              solutionGlobal):
        try:
            self.playerResults[puzzleID] = {"nTokens": nTokens, "parScore": parScore, "parMoves": parMoves,
                                            "parScoreGlobal": parScoreGlobal, "parMovesGlobal": parMovesGlobal,
                                            "solution": solution, "solutionGlobal": solutionGlobal}
        except:
            self.playerResults = {puzzleID: {"nTokens": nTokens, "parScore": parScore, "parMoves": parMoves,
                                             "parScoreGlobal": parScoreGlobal, "parMovesGlobal": parMovesGlobal,
                                             "solution": solution, "solutionGlobal": solutionGlobal}}

    def generate_nuc_order(self):
        NucOrder = []
        puz = self.puzzle
        sol = self.greedy_sol
        IDs = self.IDs
        cols = list(range(self.start - 1, self.end + 2))

        for i, row in enumerate(puz):
            row_order = []
            for j, nuc in enumerate(row):
                if nuc != "-":
                    row_order.append(Nucleotide(nuc, IDs[i], cols[j]))
            NucOrder.append(row_order)
        self.nuc_order = NucOrder

        solOrder = []
        for i, row in enumerate(sol):
            row_order = []
            for j, nuc in enumerate(row):
                if nuc != "-":
                    row_order.append(Nucleotide(nuc, IDs[i], cols[j]))
                else:
                    after = list(row[j + 1:])
                    if j > 0 and after.count("-") < len(after):
                        row_order.append(Nucleotide(nuc, IDs[i], cols[j]))
            solOrder.append(row_order)
        self.par_order = solOrder

    def to_biopython_aln(self):
        align = MultipleSeqAlignment([], Gapped(IUPAC.unambiguous_rna, "-"))
        for i in range(len(self.IDs)):
            seqobj = Seq("".join(self.puzzle[i]))
            align.append(SeqRecord(seq=seqobj, id=self.IDs[i]))
        return align

    def copy(self):
        return copy.deepcopy(self.puzzle)

    def collapse(self):
        # collapses everything to the left
        self.uncollapsed_puzzle = self.puzzle.copy()
        collapsed_puzzle = []
        for row in self.puzzle:
            count = 0
            new_row = []
            for i in row:
                if i != "-":
                    new_row.append(i)
                else:
                    count += 1
            for i in range(count):
                new_row.append("-")
            collapsed_puzzle.append(new_row)
        self.puzzle = collapsed_puzzle

    def move(self, row, move_from, move_to):
        self.puzzle[row][move_from], self.puzzle[row][move_to] = "-", self.puzzle[row][move_from]

    def add_solution(self, solution, cons_score, n_gaps, n):
        self.greedy_sol = solution
        self.greedy_consensus_score = cons_score
        self.greedy_n_gaps = n_gaps
        self.n_moves = n
        self.reprint = True

    def was_already_visited(self, visited_list):
        for i in visited_list:
            if i.puzzle == self.puzzle:
                return True
        return False

    def respects_cover(self, min_cov, max_cov):
        rows = self.puzzle
        max_N = len(rows) * len(rows[0])
        gapcount = sum([x[:-2].count("-") for x in rows])
        self.gap_proportion = float(gapcount) / float(max_N)
        return min_cov < self.gap_proportion < max_cov

    def eval(self):
        try:
            return self.score
        except:
            if len(self.puzzle) == 0:
                self.consensus_score = -1000000
                self.gap_score = 1000000
                self.score = -2000000
            else:
                self.consensus_score = score_consensus(self.puzzle, self.consensus, self.cons_scores, self.n)
                self.gap_score = score_gaps(self.puzzle)
                self.score = self.consensus_score - 0.6 * self.gap_score[1]
            return self.score

    def get_min_max(self):
        self.min_max = self.gearbox_score() * 1.15
        return self.min_max

    def stochastic_adjust(self, TO_ADD, NGAPS):

        BASE_SHORTEN_PROB = 0.15
        BASE_EXTEND_PROB = 0.15

        if NGAPS < 0.1 * len(self.puzzle) * len(self.puzzle[0]):
            BASE_SHORTEN_PROB = 0.6
        elif NGAPS > 0.5 * len(self.puzzle) * len(self.puzzle[0]):
            BASE_EXTEND_PROB = 0.85

        puzzle = self.puzzle
        rows = self.puzzle
        max_N = len(rows) * len(rows[0])
        gapcount = sum([x[:-2].count("-") for x in rows])
        min_space = min([x.count("-") for x in rows])
        self.gap_proportion = float(gapcount) / float(max_N)

        for rind, row in enumerate(rows):
            # if not a lot of space
            if min_space < 4:
                counter = min_space
                bonus = 1  # bonus as we remove subsequent nucleotides
                while counter < 3:
                    r = random.random()
                    if r > BASE_SHORTEN_PROB / bonus:
                        break
                    else:
                        for ind, i in enumerate(row):
                            if i == "-":
                                break
                        puzzle[rind][ind - 1] = "-"
                        bonus += 1
                        counter += 1
            elif min_space >= 4:
                counter = min_space
                bonus = 1  # bonus as we remove subsequent nucleotides
                while counter > 1 and bonus < 3:
                    r = random.random()
                    if r > BASE_EXTEND_PROB / bonus:
                        break
                    else:
                        for ind, i in enumerate(row):
                            if i == "-":
                                break
                        puzzle[rind][ind] = TO_ADD[rind][bonus - 1]
                        bonus += 1
                        counter -= 1

        self.puzzle = puzzle

    def show(self):
        counter = 0
        for i in self.puzzle:
            print("".join(i))
            counter += 1
        print("-" * len(i))
        print("".join([x[0] for x in self.consensus]))
        print("".join([x[1] for x in self.consensus]))

    def show_greedy_answer(self):
        counter = 0
        for i in self.greedy_sol:
            print(">" + str(counter))
            print("".join(i))
            counter += 1
        print(" " + "".join([x[0] for x in self.consensus]))
        print(" " + "".join([x[1] for x in self.consensus]))

    def gearbox_score(self):
        bonus = self.bonus
        consensus = self.consensus
        rows = self.greedy_sol

        score = 0
        for col_ind in range(0, len(rows[0])):
            col_bonus = True
            col_tot = 0
            for row in rows:
                i = row[col_ind]
                if i == '-':
                    col_bonus = False
                    continue

                if i.upper() == "U":
                    i = "T"
                else:
                    i = i.upper()

                if i in consensus[col_ind]:
                    col_tot += 1
                else:
                    col_bonus = False

            if col_bonus:
                score += col_tot * bonus
            else:
                score += col_tot
        self.greedy_consensus_score = score
        return score

    def gearbox_init_score(self):
        bonus = self.bonus
        consensus = self.consensus
        rows = self.puzzle

        score = 0
        for col_ind in range(0, len(rows[0])):
            col_bonus = True
            col_tot = 0
            for row in rows:
                i = row[col_ind]
                if i == '-':
                    col_bonus = False
                    continue

                if i.upper() == "U":
                    i = "T"
                else:
                    i = i.upper()

                if i in consensus[col_ind]:
                    col_tot += 1
                else:
                    col_bonus = False

            if col_bonus:
                score += col_tot * bonus
            else:
                score += col_tot

        return score

    def show_gearbox(self):
        for subseq in self.puzzle:
            seq_str = ""
            for tile in subseq:
                seq_str += tile.nuc
            print(seq_str)

        print("_____")
        print("".join([x[0] for x in self.consensus]))
        print("".join([x[1] for x in self.consensus]))

    def show_gearbox_sol(self):
        for subseq in self.greedy_sol:
            seq_str = ""
            for tile in subseq:
                seq_str += tile.nuc
            print(seq_str)
        print("_____")
        print("".join([x[0] for x in self.consensus]))
        print("".join([x[1] for x in self.consensus]))

    def can_split_screen(self, MAX_HEIGHT):
        height = len(self.puzzle[0])
        max_len = 2 + height - min([x.count("-") for x in self.puzzle])
        return max_len < MAX_HEIGHT


def score_column_consensus(col, cons_dict):
    score = 0
    for i in col:
        if i == "-":
            continue
        if i.upper() == "U":
            i = "T"
        else:
            i = i.upper()
        if i in cons_dict:
            score += cons_dict[i]
    return score


def score_consensus(rows, cons):
    score = 0
    rows = list(zip(*rows))
    for i, row in enumerate(rows):
        row_score = 0
        for j, x in enumerate(row):
            if x in cons[i] and x != "-":
                row_score += 1
        if row_score == len(row):
            row_score = row_score * 1.15
        score += row_score
    return score


def score_gaps(rows):
    n_gaps = 0
    for row in rows:
        for j in range(0, len(row)):
            right = row[j + 1:]
            if row[j] == "-" and any(x in ["A", "C", "G", "T", "U", "N"] for x in right):
                n_gaps += 1
    gap_proportion = n_gaps / (len(rows[0] * len(rows)))
    return gap_proportion, n_gaps


def identity_clustering(align):
    clusters = {}
    for record in align:
        id = record.id
        seq = str(record.seq)
        if seq in clusters:
            clusters[seq].append(id)
        else:
            clusters[seq] = [id]
    return clusters


def get_move_options(p, row, col, visited):
    potential_moves = []
    # base case where base has no immediately accessible gap
    ROW_LEN = len(p.puzzle[row])
    if col == 0:
        if p.puzzle[row][1] != "-":
            return []

        else:
            to_col = col + 1
            while p.puzzle[row][to_col] == "-" and to_col < ROW_LEN:
                new_puzzle = Puzzle.from_puzzle(p)
                new_puzzle.move(row, col, to_col)
                if not new_puzzle.was_already_visited(visited):
                    potential_moves.append(new_puzzle)
                if to_col + 1 == ROW_LEN:
                    break
                to_col += 1

    elif col == len(p.puzzle[row]) - 1:
        if p.puzzle[row][len(p.puzzle[row]) - 2] != "-":
            return []

        else:
            to_col = col - 1
            while p.puzzle[row][to_col] == "-" and to_col > 0:
                new_puzzle = Puzzle.from_puzzle(p)
                new_puzzle.move(row, col, to_col)
                if not new_puzzle.was_already_visited(visited):
                    potential_moves.append(new_puzzle)
                if to_col == 1:
                    break
                to_col -= 1

    else:
        if p.puzzle[row][col - 1] != "-" and p.puzzle[row][col + 1] != "-":
            return []

        if p.puzzle[row][col + 1] == "-":
            to_col = col + 1
            while p.puzzle[row][to_col] == "-" and to_col < ROW_LEN:
                new_puzzle = Puzzle.from_puzzle(p)
                new_puzzle.move(row, col, to_col)
                if not new_puzzle.was_already_visited(visited):
                    potential_moves.append(new_puzzle)
                if to_col + 1 == ROW_LEN:
                    break
                to_col += 1

        if p.puzzle[row][col - 1] == "-":
            to_col = col - 1
            while p.puzzle[row][to_col] == "-" and to_col > 0:
                new_puzzle = Puzzle.from_puzzle(p)
                new_puzzle.move(row, col, to_col)
                if not new_puzzle.was_already_visited(visited):
                    potential_moves.append(new_puzzle)
                if to_col == 1:
                    break
                to_col -= 1

    return potential_moves


# takes as input an alignment and a list of alignments which have been visited before, outputs all alignments that
# are one move away
def get_legal_actions(p, visited_positions):
    all_next_position_puzzles = []

    for ind1, i in enumerate(p.puzzle):
        for ind2, j in enumerate(i):
            if j != "-":
                reachable_positions = get_move_options(p, ind1, ind2, visited_positions)
                all_next_position_puzzles = all_next_position_puzzles + reachable_positions
                visited_positions = visited_positions + reachable_positions

    return all_next_position_puzzles, visited_positions


def get_legal_gap_reducing_actions(p, visited_positions):
    all_next_position_puzzles = []
    old_gaps = score_gaps(p.puzzle)[1]
    for ind1, i in enumerate(p.puzzle):
        for ind2, j in enumerate(i):
            if j != "-":
                reachable_positions = get_move_options(p, ind1, ind2, visited_positions)
                validated_positions = []

                for pos in reachable_positions:
                    pos.eval()
                    new_gaps = pos.gap_score[1]
                    if new_gaps < old_gaps:
                        validated_positions.append(pos)
                all_next_position_puzzles = all_next_position_puzzles + validated_positions
                visited_positions = visited_positions + reachable_positions

    return all_next_position_puzzles, visited_positions


def pick_random_actions(p, visited=[], start_score=0):
    if start_score == 0:
        p.eval()
    current_position = Puzzle().from_puzzle(p)
    accessible, just_visited = get_legal_actions(current_position, visited)
    visited = visited + just_visited

    if len(accessible) == 0:
        return current_position

    good_actions = []
    for position in accessible:
        good_actions.append(Puzzle().from_puzzle(position))
    if len(good_actions) == 0:
        return current_position

    else:
        current_best = random.choice(good_actions)
        return current_best


def iterate_random(start_puzzle):
    start_puzzle.eval()
    optimal_position_per_step = [start_puzzle]

    next_position = Puzzle()
    next_position.eval()
    print("ready")
    for i in range(100):
        next_position = pick_random_actions(optimal_position_per_step[-1], visited=optimal_position_per_step)
        next_position.eval()
        optimal_position_per_step.append(next_position)

    print("done")
    return optimal_position_per_step


def solve_puzzle(p, visited=[], start_score=0):
    if start_score == 0:
        p.eval()
        start_score = p.score
    current_position = Puzzle().from_puzzle(p)
    accessible, just_visited = get_legal_actions(current_position, visited)
    visited = visited + just_visited

    if len(accessible) == 0:
        return current_position

    good_actions = []
    for position in accessible:
        position.eval()
        if position.score >= start_score:
            good_actions.append(Puzzle().from_puzzle(position))
    if len(good_actions) == 0:
        return current_position

    else:
        best_score = start_score
        current_best = current_position
        for best_action in good_actions:
            best_action.eval()
            if best_action.score > best_score:
                best_score = best_action.score
                current_best = best_action
        return current_best


def solve_puzzle_fewer_gaps(p, visited=[]):
    p.eval()
    start_score = p.score
    current_position = Puzzle().from_puzzle(p)
    accessible, just_visited = get_legal_gap_reducing_actions(current_position, visited)
    visited = visited + just_visited

    if len(accessible) == 0:
        return current_position
    good_actions = []
    for position in accessible:
        position.eval()
        good_actions.append(Puzzle().from_puzzle(position))
    if len(good_actions) == 0:
        return current_position

    else:
        best_score = start_score
        current_best = current_position
        for best_action in good_actions:
            best_action.eval()
            if best_action.score > best_score:
                best_score = best_action.score
                current_best = best_action
        return current_best


def iterate_solve(start_puzzle):
    start_puzzle.eval()

    optimal_position_per_step = [start_puzzle]

    next_position = Puzzle()
    next_position.eval()
    counter = 0
    while True:
        next_position = solve_puzzle(optimal_position_per_step[-1], visited=optimal_position_per_step)
        next_position.eval()

        counter += 1
        if next_position.was_already_visited(optimal_position_per_step):
            break
        optimal_position_per_step.append(next_position)

    return optimal_position_per_step


def iterate_reduce_gaps(start_puzzle, early_cut=4):
    start_puzzle.eval()

    optimal_position_per_step = [start_puzzle]

    next_position = Puzzle()
    next_position.eval()
    counter = 0
    for i in range(early_cut):
        next_position = solve_puzzle_fewer_gaps(optimal_position_per_step[-1], visited=optimal_position_per_step)
        next_position.eval()

        counter += 1
        if next_position.was_already_visited(optimal_position_per_step):
            break
        optimal_position_per_step.append(next_position)

    if len(optimal_position_per_step) < 2:
        return []
    return optimal_position_per_step
