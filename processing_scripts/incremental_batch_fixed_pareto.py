__author__ = "Roman, Parham"
"""
This script parses resultBatches from the server and stores the solutions in a parsable python way.
Process the output of this with extract_data.py
"""
import os
import pickle
import copy
from dotenv import load_dotenv

load_dotenv(".env")
import sys
from phylox_analysis import db, models, api
from dnapuzzle import iterate_reduce_gaps


def load_leftright_dict():
    try:
        return pickle.load(open("resultDicts/" + "LEFTRIGHTpikdiks" + ".pickle", "rb"))
    except:
        d = {}
        for f in os.listdir("2020044pikdiks"):
            if "2020044" in f:
                thisd = pickle.load(open("2020044pikdiks/" + f, "rb"))
                for i in thisd:
                    d[i] = thisd[i]
        pickle.dump(d, open("resultDicts/" + "LEFTRIGHTpikdiks" + ".pickle", "wb"))
        return d


def process_batch(first_batch=-1):
    print("RUNNING BATCHES FROM ", first_batch)
    result_by_sourceID = {}
    pos, neg = api.puzzle.search_satisfying_puzzles(first_batch, par_score_plus_freq=5, min_games_with_same_score=1)
    source_puzzle_ids = {p["source_puzzle_id"] for p in pos}
    print("WE FOUND ", len(pos), "positive results in batch", first_batch, "and ", len(neg), " negative results")
    pos_puzzle_dict = {p["original_code"]: {k: v for k, v in p.items() if k != "original_code"} for p in pos}

    sourced_puzzles = api.puzzle.get_sourced_puzzles(
        source_puzzle_ids,
        first_batch,
        with_entities=[
            models.Puzzle.original_code,
            models.Puzzle.source_puzzle_id,
            models.Puzzle.batch_id,
            models.Puzzle.par_score,
            models.Puzzle.par_score_global,
            models.Puzzle.par_moves,
            models.Puzzle.par_moves_global,
            models.Puzzle.max_gap_count

        ])
    print("TOTAL NUMBER OF PUZZLES", len(sourced_puzzles))

    for original_code, source_puzzle_id, batch_id, par_score, par_score_global, par_moves, par_moves_global, max_gap_count in sourced_puzzles:
        rest_dict = pos_puzzle_dict.get(
            original_code,
            {
                "source_puzzle_id": source_puzzle_id,
                "max_gap_count": max_gap_count,
                "info": {}
            }
        )
        rest_dict.update({
            "batch_id": batch_id,
            "par_score": par_score,
            "par_score_global": par_score_global,
            "par_moves": par_moves,
            "par_moves_global": par_moves_global
        })
        if rest_dict["source_puzzle_id"] not in result_by_sourceID:
            result_by_sourceID[rest_dict["source_puzzle_id"]] = []
        result_by_sourceID[rest_dict["source_puzzle_id"]].append({"original_code": original_code, **rest_dict})

    session = db.get_session()
    session.rollback()
    return result_by_sourceID


def add_gaps_to_puzzle(gennedPuz, solID):
    fullPuz = []
    for row in solID:
        new_row = []
        counter = 0
        for j in range(len(gennedPuz.columns)):
            if counter < len(row):
                new_row.append(row[j])
            else:
                new_row.append("-")
            counter += 1
        fullPuz.append(new_row)
    return fullPuz


def get_pareto_front(bestScore_by_puz, bestScore_tokensUsed, oneSol, gennedPuz):
    pareto_points = []
    puzzle_is_broken = False

    if len(bestScore_by_puz) == 1:
        tokens = bestScore_tokensUsed[0]
        score = bestScore_by_puz[0]
        solID = oneSol[0]

        fewerGapsPuz = copy.deepcopy(gennedPuz)
        fewerGapsPuz.puzzle = add_gaps_to_puzzle(gennedPuz, solID)
        fewerGapsPuz.eval()

        pareto_points = [(tokens, score, solID)]

    elif len(bestScore_by_puz) == 2:
        this_score = bestScore_by_puz[0]
        this_gaps = bestScore_tokensUsed[0]

        next_score = bestScore_by_puz[1]
        next_gaps = bestScore_tokensUsed[1]

        if this_gaps == next_gaps:
            tokens = this_gaps
            score = this_score
            if this_score > next_score:
                solID = oneSol[0]
            else:
                solID = oneSol[1]

            fewerGapsPuz = copy.deepcopy(gennedPuz)

            fewerGapsPuz.puzzle = add_gaps_to_puzzle(fewerGapsPuz, solID)
            fewerGapsPuz.eval()

            fewerGapsPuzzle = iterate_reduce_gaps(fewerGapsPuz)
            if len(fewerGapsPuzzle) == 0:  # couldnt solve the puzzle
                pareto_points = [(tokens, score, solID), (tokens + 3, score, solID)]
            else:
                fewerGapsSol = fewerGapsPuzzle[-1].puzzle
                fewerGapsScore = fewerGapsPuzzle[-1].consensus_score
                fewerGapsTokens = fewerGapsPuzzle[-1].gap_score[1]

                pareto_points = [(fewerGapsTokens, fewerGapsScore, fewerGapsSol), (tokens, score, solID),
                                 (tokens + 3, score, solID)]

        else:
            if this_score > next_score and this_gaps <= next_gaps:
                imp = 10
            elif this_score < next_score and this_gaps >= next_gaps:
                imp = 10
            else:
                imp = (this_score - next_score) / (this_gaps - next_gaps)
            if (imp > 1 and this_score > next_score) or (imp < 1 and this_score < next_score):
                tokens = bestScore_tokensUsed[0]
                score = bestScore_by_puz[0]
                solID = oneSol[0]

                fewerGapsPuz = copy.deepcopy(gennedPuz)
                fewerGapsPuz.puzzle = add_gaps_to_puzzle(fewerGapsPuz, solID)
                fewerGapsPuz.eval()

                fewerGapsPuzzle = iterate_reduce_gaps(fewerGapsPuz)
                if len(fewerGapsPuzzle) == 0:  # couldnt solve the puzzle
                    pareto_points = [(tokens, score, solID)]  # we know the higher one was worse so we stay here
                else:
                    fewerGapsSol = fewerGapsPuzzle[-1].puzzle
                    fewerGapsScore = fewerGapsPuzzle[-1].consensus_score
                    fewerGapsTokens = fewerGapsPuzzle[-1].gap_score[1]

                    pareto_points = [(fewerGapsTokens, fewerGapsScore, fewerGapsSol), (tokens, score, solID)]
                    # we know the higher one was worse so we go lower.

            elif imp == 1:
                pareto_points = [(bestScore_tokensUsed[0], bestScore_by_puz[0]), oneSol[0],
                                 (bestScore_tokensUsed[1], bestScore_by_puz[1], oneSol[1])]
            else:
                tokens = bestScore_tokensUsed[1]
                score = bestScore_by_puz[1]
                solID = oneSol[1]
                pareto_points = [(tokens, score, solID), (tokens + 3, score, solID)]

    else:
        cutoff = 0.4  # as long as we don't fulfill our requirements of max 3, redo it.
        ship = False
        while True:
            for pind in range(len(bestScore_by_puz)):
                better_than = 0  # a comparison score. +1 for every other answer that's worse, +0.5 for equal,
                # +0 if the other is better.
                for p2 in range(len(bestScore_by_puz)):
                    if pind == p2:
                        continue

                    this_score = bestScore_by_puz[pind]
                    this_gaps = bestScore_tokensUsed[pind]

                    next_score = bestScore_by_puz[p2]
                    next_gaps = bestScore_tokensUsed[p2]

                    if this_gaps == next_gaps:
                        if this_score > next_score:
                            better_than += 1
                        elif this_score == next_score:
                            better_than += 0.5

                    elif this_gaps < next_gaps:
                        if next_score <= this_score:
                            better_than += 1

                        else:
                            imp = (next_score - this_score) / (next_gaps - this_gaps)
                            if imp == 1:
                                better_than += 0.5
                            elif imp < 1:
                                better_than += 1

                    else:
                        if this_score > next_score:

                            imp = (this_score - next_score) / (this_gaps - next_gaps)
                            if imp == 1:
                                better_than += 0.5
                            elif imp > 1:
                                better_than += 1
                if better_than / (len(bestScore_by_puz) - 1) > cutoff:
                    if (bestScore_tokensUsed[pind], bestScore_by_puz[pind], oneSol[pind]) not in pareto_points:
                        pareto_points.append((bestScore_tokensUsed[pind], bestScore_by_puz[pind], oneSol[pind]))

            if len(pareto_points) < 3:
                break
            if ship:
                break
            cutoff += 0.15
            if cutoff > 0.95:
                cutoff = 0.75
                ship = True

    return pareto_points, puzzle_is_broken


def eliminate_suboptimals(scores, gaps, oneSol, allSols, rd, code, gennedPuz):
    nScores = []
    nGaps = []
    n1Sols = []
    nASols = []

    if not (len(scores) == len(gaps) == len(oneSol) == len(allSols)):
        return [], [], [], [], rd

    for indi in range(len(scores)):
        paretopt = True
        for indj in range(len(scores)):
            if indi == indj:
                continue

            if scores[indi] == scores[indj] and gaps[indi] > gaps[indj]:
                paretopt = False
            elif scores[indi] < scores[indj] and gaps[indi] == gaps[indj]:
                paretopt = False

        if paretopt:
            nScores.append(scores[indi])
            nGaps.append(gaps[indi])
            n1Sols.append(oneSol[indi])
            nASols.append(allSols[indi])
        else:
            badSolInd = indi
            rd = rAdd(rd, gennedPuz.columns, [
                (code, gennedPuz, "FalseSuboptEliminated", gaps[badSolInd], scores[badSolInd], allSols[badSolInd])])

    return nScores, nGaps, n1Sols, nASols, rd


def load_dict():
    try:
        return pickle.load(open("resultDicts/" + "0221pikdiks" + ".pickle", "rb"))
    except:
        d = {}
        for f in os.listdir("mmos_pikdiks"):
            if ".pickle" in f:
                thisd = pickle.load(open("mmos_pikdiks/" + f, "rb"))
                for i in thisd:
                    d[i] = thisd[i]
        pickle.dump(d, open("resultDicts/" + "0221pikdiks" + ".pickle", "wb"))
        return d


def clear_results(f="ALLRES"):
    pickle.dump({}, open(f + "ALNRESULTS.pickle", "wb"))
    return {}


def load_results(f):
    try:
        return pickle.load(open(f + "ALNRESULTS.pickle", "rb"))
    except:
        return {}


def save_results(d, f):
    pickle.dump(d, open(f + "ALNRESULTS.pickle", "wb"))


def count_results(d):
    print("COUNTING RESULTS")
    tot = 0
    for i in d:
        for j in d[i]:
            # print(i,j)
            tot += len(j[5])
    return tot


def rAdd(r, columns, pareto_points):
    if str(columns) in r:
        for p in pareto_points:
            r[str(columns)].append(p)
    else:
        r[str(columns)] = []
        for p in pareto_points:
            r[str(columns)].append(p)
    return r


def main_oneBatch(first_batch=-1, f="ALLRES", pd={}):
    print("PROCESSING BATCH", first_batch)
    result_by_sourceID = process_batch(first_batch)  # returns a dictionary of the results organized by source ID
    import pickle  # need to make this

    retired_puzzles = []  # will not be back in next batch
    no_one_beats_par_score = []  # particularly bad
    puzzles_from_the_future = []  # will be back in next batch

    rd = load_results(f)

    counter = 0
    for source in result_by_sourceID:  # we want to iterate over each puzzle with the same sourceID
        counter += 1
        puz_clusters = []
        for oC in result_by_sourceID[source]:
            code = oC["original_code"]
            if code not in pd:
                continue
            puzObj = pd[code]
            ids = puzObj.IDs
            if len(puz_clusters) == 0:
                puz_clusters.append([(code, puzObj, oC)])
            else:
                added = False
                for p in puz_clusters:
                    if sorted(ids) == sorted(p[0][1].IDs):
                        p.append((source, puzObj, oC))
                        added = True
                if not added:
                    puz_clusters.append([(code, puzObj, oC)])
        counter += 1
        for cind, cluster in enumerate(puz_clusters):

            bestScore_by_puz = []  # the highest score for each number of tokens (or each puzzle object)
            bestScore_maxTokens = []  # containts the max tokens that could be spent for each best solution
            bestScore_tokensUsed = []  # contains the tokens spent for each best solution
            oneSol = []  # contains one solution per each unique pair (score, maxtokens)
            solsID = []
            cluster_pareto = []

            sRes = [x[2] for x in cluster]  # results for this source ID
            sig_sRes = []  # we want to remove insignificant results by removing the ones that dont have any solutions
            for rind, r in enumerate(sRes):
                if len(r["info"]) > 0:
                    sig_sRes.append(r)

            sRes = sig_sRes
            sRes = sorted(sRes,
                          key=lambda item: (item["max_gap_count"], item["original_code"]))  # sort by number of gaps

            for pind, puz in enumerate(
                    sRes):  # for each puzzle object with the same source puzzle ID (they have diff gaps #)
                jsonCode = puz["original_code"]
                code = jsonCode  # for gennedPuz in puzgen_dict[source]: #this is checking we have a match for this
                # puzzle in the generated puzzle pickle

                gennedPuz = cluster[pind][1]
                if len(puz["info"]) == 0:  # if there are no solution, we want to continue
                    exit("len 0")

                elif len(puz["info"]) == 1:  # if there is exactly one solution, it is the best solution for the puzzle.
                    if len(puz["info"][0]["solutions"]) == 1:
                        if puz["info"][0]["score"] >= max(puz["par_score"], 0):
                            IS_WORSE = False
                            for bs_ind, score in enumerate(bestScore_by_puz):
                                if score > puz["info"][0]["score"]:
                                    if bestScore_tokensUsed[bs_ind] <= puz["info"][0]["solutions"][0]["gaps_used"]:
                                        rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "False1score",
                                                                           puz["info"][0]["solutions"][0]["gaps_used"],
                                                                           puz["info"][0]["score"],
                                                                           puz["info"][0]["solutions"][0]["ids"])])
                                        IS_WORSE = True
                            if IS_WORSE:
                                continue

                            HAS_DUP = False
                            for bs_ind, score in enumerate(bestScore_by_puz):
                                if score == puz["info"][0]["score"]:
                                    if bestScore_tokensUsed[bs_ind] == puz["info"][0]["solutions"][0]["gaps_used"]:
                                        HAS_DUP = True

                            if HAS_DUP:
                                rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "TrueDuplicate",
                                                                   puz["info"][0]["solutions"][0]["gaps_used"],
                                                                   puz["info"][0]["score"],
                                                                   puz["info"][0]["solutions"][0]["ids"])])
                                continue

                            # indented to the left by one
                            bestScore_by_puz.append(puz["info"][0]["score"])
                            bestScore_tokensUsed.append(puz["info"][0]["solutions"][0]["gaps_used"])
                            oneSol.append(puz["info"][0]["solutions"][0]["solution"])
                            bestScore_maxTokens.append(puz["max_gap_count"])
                        # added
                        else:
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "FalseUnderPar",
                                                               puz["info"][0]["solutions"][0]["gaps_used"],
                                                               puz["info"][0]["score"],
                                                               puz["info"][0]["solutions"][0]["ids"])])

                    else:  # if there is more than one solution for the same score, find at least a pareto optimal one.
                        sols = sorted(puz["info"][0]["solutions"], key=lambda item: item["gaps_used"])
                        if puz["info"][0]["score"] >= max(puz["par_score"], 0):
                            IS_WORSE = False
                            HAS_DUP = False
                            for bs_ind, score in enumerate(bestScore_by_puz):
                                if score > puz["info"][0]["score"]:
                                    if bestScore_tokensUsed[bs_ind] <= sols[0]["gaps_used"]:
                                        rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "False1score",
                                                                           sols[0]["gaps_used"],
                                                                           puz["info"][0]["score"], sols[0]["ids"])])
                                        IS_WORSE = True
                            if IS_WORSE:
                                continue

                            for bs_ind, score in enumerate(bestScore_by_puz):
                                if score == puz["info"][0]["score"]:
                                    if bestScore_tokensUsed[bs_ind] == sols[0]["gaps_used"]:
                                        HAS_DUP = True

                            if HAS_DUP:
                                rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "TrueDuplicate",
                                                                   sols[0]["gaps_used"], puz["info"][0]["score"],
                                                                   sols[0]["ids"])])
                                continue

                            bestScore_by_puz.append(puz["info"][0]["score"])
                            bestScore_tokensUsed.append(sols[0]["gaps_used"])
                            oneSol.append(sols[0]["solution"])
                            solsID.append(sols[0]["ids"])
                            bestScore_maxTokens.append(puz["max_gap_count"])

                            if len(sols) > 1:
                                for other_sol in sols[1:]:
                                    rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "False1scoreSubopt",
                                                                       other_sol["gaps_used"], puz["info"][0]["score"],
                                                                       other_sol["ids"])])

                        else:
                            for other_sol in sols:
                                rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "FalseUnderPar",
                                                                   other_sol["gaps_used"], puz["info"][0]["score"],
                                                                   other_sol["ids"])])

                else:  # if there is more than one solution score.
                    bestScore_by_sol = []
                    bestTok_by_sol = []
                    oneSol_by_sol = []
                    solsID_by_sol = []

                    allSols = sorted(puz["info"], key=lambda item: item["score"],
                                     reverse=True)  # sort by decreasing score
                    for solsWithSameScore in allSols:  # "info" contains a list of arrays of solutions with the same score

                        sols = sorted(solsWithSameScore["solutions"],
                                      key=lambda item: item["gaps_used"])  # sorting solutions with same score by tokens
                        sc = solsWithSameScore["score"]  # score for all the solutions in the sols array
                        gaps = sols[0]["gaps_used"]  # minimum gaps used for the same score
                        bestScore_by_sol.append(sc)
                        bestTok_by_sol.append(gaps)
                        oneSol_by_sol.append(sols[0]["solution"])
                        solsID_by_sol.append(sols[0]["ids"])

                        if len(sols) > 1:
                            for other_sol in sols[1:]:
                                rd = rAdd(rd, gennedPuz.columns, [
                                    (code, gennedPuz, "FalseSubopt", other_sol["gaps_used"], sc, other_sol["ids"])])

                    # gets the pareto front of all the difference (score,minTokForScore) pairs
                    bestScore_by_sol, bestTok_by_sol, oneSol_by_sol, solsID_by_sol, rd = eliminate_suboptimals(
                        bestScore_by_sol, bestTok_by_sol, oneSol_by_sol, solsID_by_sol, rd, code, gennedPuz)

                    par_pts, broken = get_pareto_front(bestScore_by_sol, bestTok_by_sol, oneSol_by_sol, gennedPuz)
                    sol_inds = []

                    found_pareto_point = False
                    for par_pt in par_pts:
                        if par_pt[1] in bestScore_by_sol:
                            sol_inds.append(bestScore_by_sol.index(par_pt[1]))
                            if par_pt[1] > puz["par_score"]:
                                found_pareto_point = True

                    for sol_ind in sol_inds:
                        IS_WORSE = False
                        HAS_DUP = False

                        for bs_ind, score in enumerate(bestScore_by_puz):
                            if score >= bestScore_by_sol[sol_ind]:
                                if bestScore_tokensUsed[bs_ind] < bestTok_by_sol[sol_ind]:
                                    IS_WORSE = True

                        if IS_WORSE:
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "FalseStrictlyWorse",
                                                               bestTok_by_sol[sol_ind], bestScore_by_sol[sol_ind],
                                                               allSols[sol_ind]["solutions"][0]["ids"])])
                            continue

                        for bs_ind, score in enumerate(bestScore_by_puz):
                            if score == bestScore_by_sol[sol_ind]:
                                if bestScore_tokensUsed[bs_ind] == bestTok_by_sol[sol_ind]:
                                    HAS_DUP = True

                        if HAS_DUP:
                            rd = rAdd(rd, gennedPuz.columns, [(
                                code, gennedPuz, "TrueDuplicate", bestTok_by_sol[sol_ind],
                                bestScore_by_sol[sol_ind],
                                allSols[sol_ind]["solutions"][0]["ids"])])
                            continue

                        bestScore_by_puz.append(bestScore_by_sol[sol_ind])
                        bestScore_tokensUsed.append(bestTok_by_sol[sol_ind])
                        oneSol.append(oneSol_by_sol[sol_ind])
                        solsID.append(solsID_by_sol[sol_ind])
                        bestScore_maxTokens.append(puz["max_gap_count"])

                    for badSolInd in range(len(bestScore_by_sol)):
                        if badSolInd not in sol_inds:
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "FalseSuboptForScore",
                                                               bestTok_by_sol[badSolInd], bestScore_by_sol[badSolInd],
                                                               solsID_by_sol[badSolInd])])

                    if not found_pareto_point:
                        continue

                if len(bestScore_by_puz) == 0:
                    continue
                newScore = bestScore_by_puz[-1]
                newMoves = bestScore_tokensUsed[-1]
                newSol = oneSol[-1]

                try:
                    originalParScore = gennedPuz.playerResults["parScore"]
                    originalParMoves = gennedPuz.playerResults["parMoves"]
                    originalGlobalParScore = gennedPuz.playerResults["parScoreGlobal"]
                    originalGlobalParMoves = gennedPuz.playerResults["parMovesGlobal"]
                    originalSolution = gennedPuz.playerResults["solution"]
                    originalSolutionGlobal = gennedPuz.playerResults["solutionGlobal"]

                    # if we beat the globalparScore, update score, gaps, and solution
                    if newScore > originalGlobalParScore:
                        gennedPuz.update_player_results(puz["original_code"], puz["max_gap_count"], originalParScore,
                                                        originalParMoves, newScore, newMoves, originalSolution, newSol)
                        # keeps par score and par moves as initially set, and sets parScoreGlobal to player sol

                    else:  # if we dont beat it, update results.
                        gennedPuz.update_player_results(puz["original_code"], puz["max_gap_count"],
                                                        originalParScore, originalParMoves, originalGlobalParScore,
                                                        originalGlobalParMoves,
                                                        originalSolution, originalSolutionGlobal)

                except:
                    gennedPuz.update_player_results(puz["original_code"], puz["max_gap_count"], puz["par_score"],
                                                    puz["max_gap_count"], newScore, newMoves, gennedPuz.greedy_sol,
                                                    newSol)
                    # keeps par score and par moves as initially set, and sets parScoreGlobal to player sol

            bestScore_by_puz, bestScore_tokensUsed, oneSol, solsID, rd = eliminate_suboptimals(bestScore_by_puz,
                                                                                               bestScore_tokensUsed,
                                                                                               oneSol, solsID, rd, code,
                                                                                               gennedPuz)
            # if no solution was provided by players, we can't do anything, so we just retire the puzzle and move on.
            if len(bestScore_by_puz) == 0:  # or progress_made==False
                no_one_beats_par_score.append(puz)
                retired_puzzles.append(gennedPuz)
                for pind in range(len(bestScore_by_puz)):
                    rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "FalseUnderPar", bestScore_tokensUsed[pind],
                                                       bestScore_by_puz[pind], solsID[pind])])

            else:
                pareto_points = []
                if len(bestScore_by_puz) == 1:
                    tokens = bestScore_tokensUsed[0]
                    score = bestScore_by_puz[0]
                    solID = oneSol[0]
                    allsols = solsID[0]
                    rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "BestOnlyLocalOpt", tokens, score, allsols)])

                    # making puzzle with fewer gaps.
                    fewerGapsPuz = copy.deepcopy(gennedPuz)
                    fewerGapsPuz.puzzle = add_gaps_to_puzzle(fewerGapsPuz, solID)
                    fewerGapsPuzzle = iterate_reduce_gaps(fewerGapsPuz)
                    if len(fewerGapsPuzzle) == 0:  # couldn't solve the puzzle
                        pareto_points = [(tokens, score, solID), (tokens + 3, score, solID)]
                        cluster_pareto.append(pareto_points)
                    else:
                        fewerGapsPuzzle[-1].eval()
                        fewerGapsSol = fewerGapsPuzzle[-1].puzzle
                        fewerGapsScore = fewerGapsPuzzle[-1].consensus_score
                        fewerGapsTokens = fewerGapsPuzzle[-1].gap_score[1]
                        pareto_points = [(fewerGapsTokens, fewerGapsScore, fewerGapsSol), (tokens, score, solID),
                                         (tokens + 3, score, solID)]
                        cluster_pareto.append(pareto_points)
                elif len(bestScore_by_puz) == 2:
                    this_score = bestScore_by_puz[0]
                    this_gaps = bestScore_tokensUsed[0]
                    next_score = bestScore_by_puz[1]
                    next_gaps = bestScore_tokensUsed[1]

                    if this_gaps == next_gaps:
                        tokens = this_gaps
                        score = this_score
                        solID = oneSol[0]
                        allsols = solsID[0]
                        if score > next_score:
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "BestOfTwo", tokens, score, allsols)])
                            rd = rAdd(rd, gennedPuz.columns,
                                      [(code, gennedPuz, "TrueWorseOfTwo", next_gaps, score, solsID[1])])
                        else:
                            rd = rAdd(rd, gennedPuz.columns,
                                      [(code, gennedPuz, "TrueWorseOfTwo", tokens, score, allsols)])
                            rd = rAdd(rd, gennedPuz.columns,
                                      [(code, gennedPuz, "BestOfTwo", next_gaps, score, solsID[1])])

                        # making puzzle with fewer gaps.
                        fewerGapsPuz = copy.deepcopy(gennedPuz)
                        fewerGapsPuz.puzzle = add_gaps_to_puzzle(fewerGapsPuz, solID)

                        fewerGapsPuzzle = iterate_reduce_gaps(fewerGapsPuz)
                        if len(fewerGapsPuzzle) == 0:  # couldnt solve the puzzle
                            pareto_points = [(tokens, score, solID), (tokens + 3, score, solID)]
                            cluster_pareto.append(pareto_points)
                        else:
                            fewerGapsSol = fewerGapsPuzzle[-1].puzzle
                            fewerGapsScore = fewerGapsPuzzle[-1].consensus_score
                            fewerGapsTokens = fewerGapsPuzzle[-1].gap_score[1]

                            pareto_points = [(fewerGapsTokens, fewerGapsScore, fewerGapsSol), (tokens, score, solID),
                                             (tokens + 3, score, solID)]
                            cluster_pareto.append(pareto_points)

                    else:
                        if this_score > next_score and this_gaps <= next_gaps:
                            imp = 10
                        elif this_score < next_score and this_gaps >= next_gaps:
                            imp = 10
                        else:
                            imp = (this_score - next_score) / (this_gaps - next_gaps)

                        if (imp > 1 and this_score > next_score) or (
                                imp < 1 and this_score < next_score):  # if the first puzzle is the best, check this one, then one with less gaps, and one with more.
                            tokens = bestScore_tokensUsed[0]
                            score = bestScore_by_puz[0]
                            solID = oneSol[0]
                            allsols = solsID[0]
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "BestofTwo", tokens, score, allsols)])
                            rd = rAdd(rd, gennedPuz.columns,
                                      [(code, gennedPuz, "TrueWorseOfTwo", next_gaps, score, solsID[1])])
                            fewerGapsPuz = copy.deepcopy(gennedPuz)
                            fewerGapsPuz.puzzle = add_gaps_to_puzzle(fewerGapsPuz, solID)

                            fewerGapsPuzzle = iterate_reduce_gaps(fewerGapsPuz)
                            if len(fewerGapsPuzzle) == 0:  # couldnt solve the puzzle
                                pareto_points = [(tokens, score, solID), (tokens + 3, score, solID)]
                                cluster_pareto.append(pareto_points)
                            else:
                                fewerGapsSol = fewerGapsPuzzle[-1].puzzle
                                fewerGapsScore = fewerGapsPuzzle[-1].consensus_score
                                fewerGapsTokens = fewerGapsPuzzle[-1].gap_score[1]

                                pareto_points = [(fewerGapsTokens, fewerGapsScore, fewerGapsSol),
                                                 (tokens, score, solID), (tokens + 3, score, solID)]
                                cluster_pareto.append(pareto_points)

                        elif imp == 1:  # if equal, keep both as new bases.
                            pareto_points = [(bestScore_tokensUsed[0], bestScore_by_puz[0]), oneSol[0],
                                             (bestScore_tokensUsed[1], bestScore_by_puz[1], oneSol[1])]
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "BestOfTwoEqual",
                                                               bestScore_tokensUsed[0], bestScore_by_puz[0],
                                                               solsID[0])])
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "BestOfTwoEqual",
                                                               bestScore_tokensUsed[1], bestScore_by_puz[1],
                                                               solsID[1])])
                            cluster_pareto.append(pareto_points)

                        else:  # if second is better, keep that one, get  one with more.
                            tokens = bestScore_tokensUsed[1]
                            score = bestScore_by_puz[1]
                            solID = oneSol[1]
                            allsols = solsID[1]
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "BestOfTwo", tokens, score, allsols)])
                            rd = rAdd(rd, gennedPuz.columns, [(code, gennedPuz, "TrueWorseOfTwo",
                                                               bestScore_tokensUsed[0], bestScore_by_puz[0],
                                                               solsID[0])])

                            pareto_points = [(tokens, score, solID), (tokens + 3, score, solID)]
                            cluster_pareto.append(pareto_points)

                else:
                    # remove duplicates
                    cutoff = 0.4  # as long as we don't fulfill our requirements of max 3, redo it.
                    ship = False
                    while True:
                        for pind in range(len(bestScore_by_puz)):
                            better_than = 0  # a comparison score. +1 for every other answer that's worse, +0.5 for equal, +0 if the other is better.
                            for p2 in range(len(bestScore_by_puz)):
                                if pind == p2:
                                    continue
                                this_score = bestScore_by_puz[pind]
                                this_gaps = bestScore_tokensUsed[pind]
                                next_score = bestScore_by_puz[p2]
                                next_gaps = bestScore_tokensUsed[p2]
                                if this_gaps == next_gaps:
                                    if this_score > next_score:
                                        better_than += 1
                                    elif this_score == next_score:
                                        better_than += 0.5
                                elif this_gaps < next_gaps:
                                    if next_score <= this_score:
                                        better_than += 1
                                    else:
                                        imp = (next_score - this_score) / (next_gaps - this_gaps)
                                        if imp == 1:
                                            better_than += 0.5
                                        elif imp < 1:
                                            better_than += 1
                                else:
                                    if this_score > next_score:
                                        imp = (this_score - next_score) / (this_gaps - next_gaps)
                                        if imp == 1:
                                            better_than += 0.5
                                        elif imp > 1:
                                            better_than += 1
                            if better_than / (len(bestScore_by_puz) - 1) > cutoff:
                                pareto_points.append((bestScore_tokensUsed[pind], bestScore_by_puz[pind], oneSol[pind]))
                                if cutoff == 0.4:
                                    rd = rAdd(rd, gennedPuz.columns,
                                              [(code, gennedPuz, "BestOfMany", bestScore_tokensUsed[pind],
                                                bestScore_by_puz[pind], solsID[pind])])
                            else:
                                if cutoff == 0.4:
                                    rd = rAdd(rd, gennedPuz.columns,
                                              [(code, gennedPuz, "TrueWorseThanMany", bestScore_tokensUsed[pind],
                                                bestScore_by_puz[pind], solsID[pind])])
                        if ship:
                            break
                        if len(pareto_points) <= 3:
                            break

                        cutoff += 0.15
                        if cutoff > 0.95:
                            cutoff = 0.75
                            ship = True
                    cluster_pareto.append(pareto_points)

            print("NUMBER OF ID SOLUTIONS", len(rd))

    save_results(rd, f)
    overly_freq_cols = []
    cov = [0] * 190
    scores = [0] * 190

    for puz in retired_puzzles:
        try:
            this_puz_cols = list(range(puz.start, puz.end))
        except:
            this_puz_cols = puz.columns
        sumScores = 0
        sumGaps = 0
        puzLen = len(this_puz_cols)
        for scoreKey in puz.playerResults:
            sumScores += (puz.playerResults[scoreKey]["parScoreGlobal"])
            sumGaps += (puz.playerResults[scoreKey]["parMovesGlobal"])
        puzScore = sumScores / (sumGaps * puzLen + 1)
        for i in this_puz_cols:
            cov[i] += 1
            scores[i] += puzScore
    import statistics
    m = statistics.mean(cov)
    for i in range(len(cov)):
        if cov[i] > 1.5 * m:
            overly_freq_cols.append(i)

    pickle.dump(puzzles_from_the_future, open("future" + str(first_batch) + ".pickle", "wb"))

    pickle.dump(retired_puzzles, open("retired" + str(first_batch) + ".pickle", "wb"))

    pickle.dump((cov, scores, overly_freq_cols), open("batch_analytics" + str(first_batch) + ".pickle", "wb"))
    print("TOTAL number of solutions", count_results(rd))


if __name__ == "__main__":
    print("LOADING DICTIONARY")
    batches = api.result_batch.list()
    bcodes = [b.code for b in batches]
    pd = load_dict()

    nov_batches = pickle.load(open("novTODO1.pickle", "rb"))
    feb_batches = pickle.load(open("febTODO1.pickle", "rb"))
    TODO = {1: feb_batches[:20], 2: feb_batches[20:40], 3: feb_batches[40:], 4: nov_batches[:20], 5: nov_batches[20:40],
            6: nov_batches[40:]}

    if len(sys.argv) == 2:
        which = int(sys.argv[1])
        print("PROCESSING BATCH LIST", which)
        count = 0
        fname = "FEB15_NEW_ALL" + str(which)
        for b in TODO[which]:
            main_oneBatch(b, fname, pd=pd)
            count += 1
            print("COMPLETED BATCH", count, "OF", len(TODO[which]))

    elif len(sys.argv) == 3:
        a = int(sys.argv[1])
        b = int(sys.argv[2])
        count = 0
        fname = "NOV15_NEW_" + str(a) + "_" + str(b)
        clear_results(fname)
        for b in bcodes[a:b]:
            main_oneBatch(b, fname, pd=pd)
            count += 1
            print("COMPLETED BATCH", count, "OF", len(bcodes))
    db.close()
